/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

// Copied from https://bl.ocks.org/cjrd/6863459

 // define graphcreator object
  var GraphCreator = function(svg) {
    var thisGraph = this;

    thisGraph.nodes = [];
    thisGraph.edges = [];

    thisGraph.state = {
      selectedNode: null,
      selectedEdge: null,
      mouseDownNode: null,
      mouseOverNode: null,
      mouseDownLink: null,
      justDragged: false,
      justScaleTransGraph: false,
      lastKeyDown: -1,
      shiftNodeDrag: false,
      selectedText: null
    };

    // define arrow markers for graph links
    var defs = svg.append('svg:defs');
    defs.append('svg:marker')
      .attr('id', 'end-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', "32")
      .attr('markerWidth', 3.5)
      .attr('markerHeight', 3.5)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M0,-5L10,0L0,5');

    // define arrow markers for leading arrow
    defs.append('svg:marker')
      .attr('id', 'mark-end-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 7)
      .attr('markerWidth', 3.5)
      .attr('markerHeight', 3.5)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M0,-5L10,0L0,5');

    var grad = defs.append('svg:linearGradient')
        .attr('id', 'hat')
    grad.append("svg:stop")
        .attr("offset","0%")
        .attr("stop-color","#000000");
    grad.append("svg:stop")
        .attr("offset","100%")
        .attr("stop-color","#5593ff");


    var grad = defs.append('svg:linearGradient')
        .attr('id', 'hatobs')
    grad.append("svg:stop")
        .attr("offset","0%")
        .attr("stop-color","#000000");
    grad.append("svg:stop")
        .attr("offset","30%")
        .attr("stop-color","#5593ff");
    grad.append("svg:stop")
        .attr("offset","100%")
        .attr("stop-color","#51d13a");

    var grad = defs.append('svg:linearGradient')
        .attr('id', 'stepobs')
    grad.append("svg:stop")
          .attr("offset","0%")
          .attr("stop-color","#000000");
    grad.append("svg:stop")
          .attr("offset","100%")
          .attr("stop-color","#51d13a");

    var gradsc = defs.append('svg:linearGradient')
        .attr('id', 'linksc')
    gradsc.append("svg:stop")
        .attr("offset","0%")
        .attr("stop-color","#000000");
    gradsc.append("svg:stop")
        .attr("offset","100%")
        .attr("stop-color","#ff6464");

    var gradcs = defs.append('svg:linearGradient')
        .attr('id', 'linkcs')
    gradcs.append("svg:stop")
        .attr("offset","0%")
        .attr("stop-color","#ff6464");
    gradcs.append("svg:stop")
        .attr("offset","100%")
        .attr("stop-color","#000000");

    thisGraph.svg = svg;
    thisGraph.svgG = svg.append("g")
      .classed(thisGraph.consts.graphClass, true);
    var svgG = thisGraph.svgG;
	svgG.attr("width", width)
        .attr("height", height);
    // displayed when dragging between nodes
    thisGraph.dragLine = svgG.append('svg:path')
      .attr('class', 'link')
      .attr('linkType', 'drag')
      .style('marker-end', 'url(#mark-end-arrow)');

    // svg nodes and edges 
    thisGraph.paths = svgG.append("g").selectAll("g");
    thisGraph.circles = svgG.append("g").attr("id","nodes").selectAll("g");

    thisGraph.drag = d3.drag()
      .subject(function(d) {
        return {
          x: d.x,
          y: d.y
        };
      })
      .on("drag", function(args) {
        thisGraph.state.justDragged = true;
        thisGraph.dragmove.call(thisGraph, args);
      })
      .on("end", function(d) {
        thisGraph.MouseUp.call(thisGraph, d3.select(this), d);
        thisGraph.javaFXNodeDragged(thisGraph.nodes);
      });

    // listen for key events
    d3.select(window).on("keydown", function() {
        thisGraph.svgKeyDown.call(thisGraph);
      })
      .on("keyup", function() {
        thisGraph.svgKeyUp.call(thisGraph);
      });
    svg.on("mousedown", function(d) {
      thisGraph.svgMouseDown.call(thisGraph, d);
    });
    svg.on("mouseup", function(d) {
      console.log("mouse up on svg");
      thisGraph.svgMouseUp.call(thisGraph, d);
    });


    let transform = d3.zoomIdentity.translate(0, 0).scale(1);
    thisGraph.zoom = d3.zoom()
        // .scaleExtent([1, 100])
        .on('zoom', zoomed);

    thisGraph.svg.call(thisGraph.zoom).call(thisGraph.zoom.transform, transform);
    function zoomed() {
      let transform = d3.event.transform;
      let modifiedTransform = d3.zoomIdentity.scale( 1/transform.k ).translate( -transform.x - 50, -transform.y  + 50);

      let mapMainContainer = thisGraph.svgG
          .attr('transform', transform);

      console.log(mapMainContainer.node().getBBox().width)
      d3.select('#minimapRect')
          .attr('width', mapMainContainer.node().getBBox().width )
          .attr('height', mapMainContainer.node().getBBox().height)
          .attr('stroke', 'red')
          .attr('stroke-width', 10/modifiedTransform.k )
          .attr('stroke-dasharray', 10/modifiedTransform.k )
          .attr('fill', 'none')
          .attr('transform', modifiedTransform)

      thisGraph.updateGraph();
    }

    // listen for resize
    window.onresize = function() {
      thisGraph.updateWindow(svg);
    };
  };

  GraphCreator.prototype.consts = {
    selectedClass: "selected",
    connectClass: "connect-node",
    circleGClass: "conceptG",
    graphClass: "graph",
    activeEditId: "active-editing",
    BACKSPACE_KEY: 8,
    DELETE_KEY: 46,
    ENTER_KEY: 13,
    nodeRadius: 50
  };

  /* PROTOTYPE FUNCTIONS */

  GraphCreator.prototype.dragmove = function(d) {
    var thisGraph = this;
    if (thisGraph.state.shiftNodeDrag) {
      thisGraph.dragLine.attr('d', 'M' + d.x + ',' + d.y + 'L' + d3.mouse(thisGraph.svgG.node())[0] + ',' + d3.mouse(this.svgG.node())[1]);
    } else {
      d.x += d3.event.dx;
      d.y += d3.event.dy;
      thisGraph.updateGraph();
    }
  };

  /* select all text in element: taken from http://stackoverflow.com/questions/6139107/programatically-select-text-in-a-contenteditable-html-element */
  GraphCreator.prototype.selectElementContents = function(el) {
    var range = document.createRange();
    range.selectNodeContents(el);
    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
  };

  /* insert svg line breaks: taken from http://stackoverflow.com/questions/13241475/how-do-i-include-newlines-in-labels-in-d3-charts */
  GraphCreator.prototype.insertTitleLinebreaks = function(gEl, title) {
    var newtext = title.replace(/([^\n]{1,11})\s/g, '$1!:!');
    var words = newtext.split(/!:!/g),
      nwords = words.length;
    var el = gEl.append("text")
      .attr("text-anchor", "middle")
      .attr("dy", "-" + (nwords - 1) * 7.5);

    for (var i = 0; i < words.length; i++) {
      var tspan = el.append('tspan').text(words[i]);
      if (i > 0)
        tspan.attr('x', 0).attr('dy', '15');
    }
  };

  // remove edges associated with a node
  GraphCreator.prototype.spliceLinksForNode = function(node) {
    var thisGraph = this,
      toSplice = thisGraph.edges.filter(function(l) {
        return (l.source === node || l.target === node);
      });
    toSplice.map(function(l) {
      // remove edge only if target && source = step.
      if(l.source.type === "step" && l.target === "step") {
        thisGraph.javaFXDelEdge(l);
        thisGraph.edges.splice(thisGraph.edges.indexOf(l), 1);
      }
    });
  };

  GraphCreator.prototype.replaceSelectEdge = function(d3Path, edgeData) {
    var thisGraph = this;
    d3Path.classed(thisGraph.consts.selectedClass, true);
    if (thisGraph.state.selectedEdge) {
      thisGraph.removeSelectFromEdge();
    }
    thisGraph.state.selectedEdge = edgeData;
  };

  GraphCreator.prototype.replaceSelectNode = function(d3Node, nodeData) {
    var thisGraph = this;
    d3Node.classed(this.consts.selectedClass, true);
    if (thisGraph.state.selectedNode) {
      thisGraph.removeSelectFromNode();
    }
    thisGraph.state.selectedNode = nodeData;
    thisGraph.javaFXHighLightNode(nodeData);
  };

  GraphCreator.prototype.removeSelectFromNode = function() {
    var thisGraph = this;
    thisGraph.circles.filter(function(cd) {
      return cd.id === thisGraph.state.selectedNode.id;
    }).classed(thisGraph.consts.selectedClass, false);
    thisGraph.state.selectedNode = null;
  };

  GraphCreator.prototype.removeSelectFromEdge = function() {
    var thisGraph = this;
    thisGraph.paths.filter(function(cd) {
      return cd === thisGraph.state.selectedEdge;
    }).classed(thisGraph.consts.selectedClass, false);
    thisGraph.state.selectedEdge = null;
  };

  GraphCreator.prototype.pathMouseDown = function(d3path, d) {
    var thisGraph = this,
      state = thisGraph.state;
    d3.event.stopPropagation();
    state.mouseDownLink = d;

    if (state.selectedNode) {
      thisGraph.removeSelectFromNode();
    }

    var prevEdge = state.selectedEdge;
    if (!prevEdge || prevEdge !== d) {
      thisGraph.replaceSelectEdge(d3path, d);
    } else {
      thisGraph.removeSelectFromEdge();
    }
  };

  // mousedown on node
  GraphCreator.prototype.circleMouseDown = function(d3node, d) {
    var thisGraph = this,
      state = thisGraph.state;
    d3.event.stopPropagation();
    state.mouseDownNode = d;
    if (d3.event.shiftKey) {
      state.shiftNodeDrag = d3.event.shiftKey;
      // reposition dragged directed edge
      thisGraph.dragLine.classed('hidden', false)
        .attr('d', 'M' + d.x + ',' + d.y + 'L' + d.x + ',' + d.y);
      return;
    }
  };

  /* place editable text on node in place of svg text */

  // mouseup on nodes
  GraphCreator.prototype.MouseUp = function(d3node, d) {
    var thisGraph = this,
      state = thisGraph.state,
      consts = thisGraph.consts;
    // reset the states
    state.shiftNodeDrag = false;
    state.mouseDownLink = null;

    d3node.classed(consts.connectClass, false);

    var mouseOverNode = state.mouseOverNode;

    thisGraph.dragLine.classed("hidden", true);

    if (!mouseOverNode) return;


    if (mouseOverNode !== d && ((mouseOverNode.type === "step" || d.type === "step") && (mouseOverNode.type !== "hat" && d.type !== "hat"))) {
      // we're in a different node: create new edge for mousedown edge and add to graph
      var newEdge = {
        source: d,
        target: mouseOverNode
      };
      var filtRes = thisGraph.paths.filter(function(d) {
        if (d.source === newEdge.target && d.target === newEdge.source && thisGraph.javaFXEditable()) {
          // inversion de sens de l'arc.
          thisGraph.javaFXDelEdge(d);
          thisGraph.edges.splice(thisGraph.edges.indexOf(d), 1);
        }
        return d.source === newEdge.source && d.target === newEdge.target;
      });
      if (!filtRes.size() && thisGraph.javaFXEditable()) {
        thisGraph.edges.push(newEdge);
        thisGraph.updateGraph(false);
        thisGraph.javaFXAddEdge(newEdge.source, newEdge.target);
      }
    } else {
      // we're in the same node
      if (state.justDragged) {
        // dragged, not clicked
        state.justDragged = false;
      } else {
        // clicked, not dragged
        if (d3.event.shiftKey) {
        } else {
          if (state.selectedEdge) {
            thisGraph.removeSelectFromEdge();
          }
          var prevNode = state.selectedNode;

          if (!prevNode || prevNode.id !== mouseOverNode.id) {
            thisGraph.replaceSelectNode(d3node, mouseOverNode);
          } else {
            thisGraph.removeSelectFromNode();
          }
        }
      }
    }
    state.mouseDownNode = null;
    return;

  }; // end of mouseup

  // mousedown on main svg
  GraphCreator.prototype.svgMouseDown = function() {
    this.state.graphMouseDown = true;
  };

  // mouseup on main svg
  GraphCreator.prototype.svgMouseUp = function() {
    var thisGraph = this,
      state = thisGraph.state;
    if (state.justScaleTransGraph) {
      // dragged not clicked
      state.justScaleTransGraph = false;
    } else if (state.graphMouseDown && d3.event.shiftKey) {

    } else if (state.shiftNodeDrag) {
      // dragged from node
      state.shiftNodeDrag = false;
      thisGraph.dragLine.classed("hidden", true);
    }
    state.graphMouseDown = false;
  };

  // keydown on main svg
  GraphCreator.prototype.svgKeyDown = function() {

    var thisGraph = this,
      state = thisGraph.state,
      consts = thisGraph.consts;
    // make sure repeated key presses don't register for each keydown
    if (state.lastKeyDown !== -1) return;

    state.lastKeyDown = d3.event.keyCode;
    var selectedNode = state.selectedNode,
      selectedEdge = state.selectedEdge;

    switch (d3.event.keyCode) {
      case consts.BACKSPACE_KEY:
      case consts.DELETE_KEY:

        d3.event.preventDefault();
        if (selectedNode && selectedNode.type === "composition" && thisGraph.javaFXEditable()) {
 	  alert("Composition can't be removed here");
	  thisGraph.svgKeyUp(); // force keyup because event is blocked by alert !
	} else if(selectedNode && selectedNode.type === "hat" && thisGraph.javaFXEditable()) {
          alert("Box step can't be removed here");
          thisGraph.svgKeyUp(); // force keyup because event is blocked by alert !
        } else if (selectedNode && selectedNode.type === "step" && thisGraph.javaFXEditable()) {
          thisGraph.javaFXDelNode(selectedNode);
          thisGraph.nodes.splice(thisGraph.nodes.indexOf(selectedNode), 1);
          thisGraph.spliceLinksForNode(selectedNode);
          state.selectedNode = null;
          thisGraph.updateGraph(false);
        } else if(selectedEdge && selectedEdge.source.type === "hat" && selectedEdge.target.type === "step") {
          alert("Substep link can't be removed here");
          thisGraph.svgKeyUp();
        } else if (selectedEdge && thisGraph.javaFXEditable()) {
        thisGraph.javaFXDelEdge(selectedEdge);
        thisGraph.edges.splice(thisGraph.edges.indexOf(selectedEdge), 1);
        state.selectedEdge = null;
        thisGraph.updateGraph(false);
      }
        break;
    }
  };

  GraphCreator.prototype.svgKeyUp = function() {
    this.state.lastKeyDown = -1;
  };

  // call to propagate changes to graph
  GraphCreator.prototype.updateGraph = function() {
    var thisGraph = this,
      consts = thisGraph.consts,
      state = thisGraph.state;


    thisGraph.paths = thisGraph.paths.data(thisGraph.edges, d => String(d.source.id) + "+" + String(d.target.id)).join(
        enter => enter.append("path")
            .style('marker-end', 'url(#end-arrow)')
            .classed("link", true)
            .attr("linkType", function(d) {
              return d.source.type[0]+d.target.type[0];
            })
            .attr("d", function(d) {
              return "M" + d.source.x + "," + d.source.y + "L" + d.target.x + "," + d.target.y;
            })
            .on("mousedown", function(d) {
              thisGraph.pathMouseDown.call(thisGraph, d3.select(this), d);
            }),
        update => update.style('marker-end', 'url(#end-arrow)')
            .classed(consts.selectedClass, function(d) {
              return d === state.selectedEdge;
            })
            .attr("d", function(d) {
              return "M" + d.source.x + "," + d.source.y + "L" + d.target.x + "," + d.target.y;
            }),
        exit => exit.remove()
    );

    // update nodes data
    thisGraph.circles = thisGraph.circles.data(thisGraph.nodes, d => d.id).join(
        enter => {
          let gg = enter.append("g")
            .classed(consts.circleGClass, true)
              .attr("transform", d => "translate(" + d.x + "," + d.y + ")")
                .attr("nodeType", d => d.type)
                  .attr("obs", d => d.obs)
                  .on("mouseover", function(d) {
                    state.mouseOverNode = d;
                    if (state.shiftNodeDrag) {
                      d3.select(this).classed(consts.connectClass, true);
                    }
                  })
                  .on("mouseout", function(d) {
                    state.mouseOverNode = null;
                    d3.select(this).classed(consts.connectClass, false);
                  })
                  .on("mousedown", function(d) {
                    thisGraph.circleMouseDown.call(thisGraph, d3.select(this), d);
                  })
                  .call(thisGraph.drag)

          gg.append("circle")
            .attr("r", String(consts.nodeRadius));
          gg.each(function(d) {
            thisGraph.insertTitleLinebreaks(d3.select(this), d.title);
          });
          return gg;
        },
        update => update.attr("transform", d => "translate(" + d.x + "," + d.y + ")"),
        exit => exit.remove()
    );

    // mini map management
    var oldMiniMap = document.querySelector('#minimap>svg');
    var cloneRect;
    if (oldMiniMap != null) { // remove the old minimap if exist
      cloneRect = d3.select("#minimap>svg>g>rect").node().cloneNode(true);
      document.querySelector('#minimap').removeChild(oldMiniMap);
    }
    // creating a new minimap
    var newMiniMap = thisGraph.svg.node().cloneNode(true);

    document.querySelector('#minimap').append(newMiniMap);
    let minimap = d3.select('#minimap').select('svg')
        .attr('width', window.innerWidth * 0.25).attr("height", window.innerHeight * 0.25);
    if (cloneRect) {
      document.querySelector("#minimap>svg>g").append(cloneRect)
    } else {
      minimap.select("g").append('rect')
          .attr('id', 'minimapRect');
    }
    // fiting in the box

    var bboxMiniMap = minimap.node().getBBox();
    console.log("new bbox " + bboxMiniMap.width)
    minimap.attr("viewBox", [bboxMiniMap.x-3, bboxMiniMap.y-3, bboxMiniMap.width+6, bboxMiniMap.height+6]);
  };

  GraphCreator.prototype.fitWindow = function() {
    var bbox = this.svgG.node().getBBox();
    this.svg.attr("viewBox", [bbox.x-3, bbox.y-3, bbox.width+6, bbox.height+6]);
    this.updateGraph();
  };

  GraphCreator.prototype.updateWindow = function(svg) {
    console.log("update");
    var x = window.innerWidth ;
    var y = window.innerHeight;
    svg.attr("width", x).attr("height", y);
    this.updateGraph();
  };



GraphCreator.prototype.addNodeNoUpdate = function(nodeName, nodeId, nodeType,obsNb, posX, posY) {
  var thisGraph = this;
  var txt = nodeName;
  // if (obsNb > 0) {
  //   txt  = txt + "\n "+obsNb + " obs";
  // }
  var obs = 'no';
  if(obsNb > 0) {
    obs = 'yes';
  }
  var d = {
    id: nodeId,
    title: txt,
    obs: obs,
    x: posX,
    y: posY,
    type: nodeType
  };
  thisGraph.nodes.push(d);
};

GraphCreator.prototype.clear = function() {
  var thisGraph = this;
  thisGraph.nodes.splice(0,thisGraph.nodes.length)
  thisGraph.edges.splice(0,thisGraph.edges.length)
  thisGraph.updateGraph();
};

GraphCreator.prototype.addNode = function(nodeName, nodeId, nodeType,obsNb, posX, posY) {
    var thisGraph = this;
    var txt = nodeName;
    // if (obsNb > 0) {
    //   txt  = txt + "\n "+obsNb + " obs";
    // }
    var obs = 'no';
    if(obsNb > 0) {
      obs = 'yes';
    }
    var d = {
      id: nodeId,
      title: txt,
      obs: obs,
      x: posX,
      y: posY,
      type: nodeType
    };
    thisGraph.nodes.push(d);
    thisGraph.updateGraph();
    return "node added " + nodeId;
  };

GraphCreator.prototype.addEdgeNoUpdate = function(nodeSrc, nodeTgt) {
  var thisGraph = this;
  var d = {
    source: thisGraph.nodes[nodeSrc],
    target: thisGraph.nodes[nodeTgt]
  };
  thisGraph.edges.push(d);
};

GraphCreator.prototype.addEdge = function(nodeSrc, nodeTgt) {
    var thisGraph = this; 
    var d = {
      source: thisGraph.nodes[nodeSrc],
      target: thisGraph.nodes[nodeTgt]
    };
    thisGraph.edges.push(d);
    thisGraph.updateGraph(false);
  };

GraphCreator.prototype.javaFXEditable = function() {
    	var edit = javaConnector.checkEdition();
	if(!edit) {
		alert("Not in edition mode");
		graph.svgKeyUp(); // force keyup because event is blocked by alert !
	}
	return edit;
  }

GraphCreator.prototype.javaFXAddEdge = function(source, target) {
    javaConnector.addEdge(JSON.stringify(source), JSON.stringify(target));
  }

GraphCreator.prototype.javaFXDelEdge = function(edge) {
    javaConnector.delEdge(JSON.stringify(edge.source), JSON.stringify(edge.target));
  }

GraphCreator.prototype.javaFXDelNode = function(node){
    javaConnector.delNode(JSON.stringify(node));
  }

GraphCreator.prototype.javaFXHighLightNode = function(node) {
    javaConnector.highlightStep(JSON.stringify(node));
  }

GraphCreator.prototype.javaFXNodeDragged = function(nodes) {
  javaConnector.nodeDragged(JSON.stringify(nodes));
}

var width = window.innerWidth,
    height = window.innerHeight;


var viewport = d3.select("#viewport").attr("width", width).attr("height", height);

var svg = d3.select("#graph-div").append("svg")
    .attr("width", width)
    .attr("height", height);
    // .attr("viewBox", [0, 0, width, height]);

var graph = new GraphCreator(svg);
// graph.addNodeNoUpdate('PHBV Characterization','96b0f6b3-a06a-400b-bb75-dbab9d83fa28','step', 0,620.84,532.0);
// graph.addNodeNoUpdate('Extrusion PHBV-B00(10%)','9b5bf2b9-9908-4c39-a781-0e4227868fc0','step', 2,390.84,820.0);
// graph.addNodeNoUpdate('Pelletizing+Drying PHBV-B00(10%)','6f9de507-84a6-43df-9281-50dde279317e','step', 2,390.84,1108.0);
// graph.addNodeNoUpdate('Thermopressin PHBV-B00(10%)','a122f3de-d326-46ef-8976-ab02c3167b51','step', 6,390.84,1396.0);
// graph.addNodeNoUpdate('PHBV-B00(10%) film','eada0aed-c2c4-4d10-adea-469212a3b109','composition', 0 ,390.84,1540.0);
// graph.addNodeNoUpdate('PHBV-B00(10%) dried','5fce487c-fd69-49fc-8ebd-426257dff10f','composition', 0 ,545.84,1200.0);
// graph.addNodeNoUpdate('PHBV-B00(10%)','6a1772e9-9ad7-4db0-a767-480f66ccb2ec','composition', 0 ,531.84,964.0);
// graph.addNodeNoUpdate('PHBV','316db778-19c5-4cf7-b095-9874d57a92d9','composition', 0 ,737.84,676.0);
// graph.addNodeNoUpdate('B00 Characterization','3d2e525f-41a7-43ce-859a-da5755bc0b61','step', 2,9.840000000000003,676.0);
// graph.addNodeNoUpdate('Pelletizing + Drying PHBV-0.5wt%BN 2','3496953d-6f93-45d7-bbdb-e911d37d0b7c','step', 0,620.84,244.0);
// graph.addNodeNoUpdate('PHBV 2 dried','909ddee4-8972-4896-9cb2-68515e3b50e2','composition', 0 ,620.84,388.0);
// graph.addNodeNoUpdate('B00','2356e14d-b789-4bc3-b21b-4b8edcc659d3','composition', 0 ,278.84,676.0);
// graph.addNodeNoUpdate('PHBV 2','29c3c2e6-8be9-4afc-a1cc-6636911b30c0','composition', 0 ,620.84,100.0);
// graph.addEdgeNoUpdate(0,1);
// graph.addEdgeNoUpdate(0,7);
// graph.addEdgeNoUpdate(1,2);
// graph.addEdgeNoUpdate(1,6);
// graph.addEdgeNoUpdate(2,3);
// graph.addEdgeNoUpdate(2,5);
// graph.addEdgeNoUpdate(3,4);
// graph.addEdgeNoUpdate(5,3);
// graph.addEdgeNoUpdate(6,2);
// graph.addEdgeNoUpdate(7,1);
// graph.addEdgeNoUpdate(8,1);
// graph.addEdgeNoUpdate(9,10);
// graph.addEdgeNoUpdate(10,0);
// graph.addEdgeNoUpdate(11,1);
// graph.addEdgeNoUpdate(12,9);
//
//
// graph.updateGraph();
// graph.fitWindow();