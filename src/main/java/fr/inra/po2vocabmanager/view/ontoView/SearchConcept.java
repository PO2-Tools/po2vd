/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.view.ontoView;

import fr.inra.po2vocabmanager.MainApp;
import fr.inra.po2vocabmanager.utils.UITools;
import fr.inrae.po2engine.externalTools.CloudConnector;
import fr.inrae.po2engine.externalTools.JenaTools;
import fr.inrae.po2engine.model.VocabConcept;
import fr.inrae.po2engine.utils.Tools;
import fr.inrae.po2engine.vocabsearch.Cible;
import fr.inrae.po2engine.vocabsearch.Requete;
import fr.inrae.po2engine.vocabsearch.Result;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.util.Pair;

import java.util.*;

/**
 * Created by stephane on 11/07/17.
 */
public class SearchConcept {

    private final MainApp mainApp;
    private VocabConcept father;
    private Boolean create;
    TextField textLabel = new TextField();
    String createURI = "";
    ComboBox<String> comboLang = new ComboBox<>();
    TextArea textDesc = new TextArea();

    public SearchConcept(MainApp mainApp, Boolean create, String type, VocabConcept father) {
        this.mainApp = mainApp;
        this.father = father;
        this.create = create;
        Dialog<Pair<String, List<Cible>>> createNodeDialog = new Dialog<>();
        if(this.create) {
            createNodeDialog.setTitle("Create new " + type);
        } else {
            createNodeDialog.setTitle("Add new link to " + type + " " + father.getName());
        }
        DialogPane dialogPane = createNodeDialog.getDialogPane();
        dialogPane.setPrefSize(800, 500);
        ButtonType searchButton = new ButtonType("Search", ButtonBar.ButtonData.OK_DONE);
        ButtonType createConcept = new ButtonType("Create concept", ButtonBar.ButtonData.LEFT);

        dialogPane.getButtonTypes().addAll(searchButton,createConcept, ButtonType.CANCEL);
        if(!create) {
            dialogPane.lookupButton(createConcept).setVisible(false);
            dialogPane.lookupButton(createConcept).setDisable(true);
        }

        BorderPane borderPane = new BorderPane();
        VBox vBox = new VBox();
        Label labConcept = new Label("Concept : ");
        TextField concept = new TextField();
        Platform.runLater(() -> concept.requestFocus());
        dialogPane.lookupButton(searchButton).disableProperty().bind(concept.textProperty().isEmpty());
        Separator sep = new Separator();
        Label labSource = new Label("Sources : ");
        vBox.getChildren().addAll(labConcept, concept, sep, labSource);
        borderPane.setTop(vBox);

        Accordion accord = new Accordion();
        TextArea area = new TextArea();
        area.setWrapText(true);
        TitledPane pane = null;
        CheckBox check = null;
        HashMap<CheckBox, Cible> listCheck = new HashMap<>();

        // rafraichissement des cibles
        CloudConnector.refreshCible();

        for (Cible c : Requete.getDefautCibles()) {
            c.init();
            pane = new TitledPane(c.getName(), null);
            check = new CheckBox();
            check.setSelected(true);
            listCheck.put(check, c);
            pane.setTooltip(new Tooltip(c.getName()));
            pane.setFont(Font.font("Verdena", FontWeight.BOLD, 13));
            pane.setGraphic(check);
            pane.setOnMouseClicked(event -> {
                area.setText(c.getDescription());
            });

            accord.getPanes().add(pane);
        }

        ScrollPane scroll = new ScrollPane(accord);
        SplitPane split = new SplitPane();
        split.setDividerPositions(0.35);
        scroll.minWidthProperty().bind(split.widthProperty().multiply(0.35));
        scroll.maxWidthProperty().bind(split.widthProperty().multiply(0.35));
        accord.minWidthProperty().bind(scroll.minWidthProperty());
        accord.maxWidthProperty().bind(scroll.maxWidthProperty());

        split.setOrientation(Orientation.HORIZONTAL);
        split.getItems().add(scroll);
        split.getItems().add(area);

        borderPane.setCenter(split);
        dialogPane.setContent(borderPane);

        createNodeDialog.setResultConverter(clickButton -> {
            if (clickButton.getButtonData() == ButtonBar.ButtonData.OK_DONE) {
                ArrayList<Cible> l = new ArrayList<>();
                for (Map.Entry<CheckBox, Cible> e : listCheck.entrySet()) {
                    if (e.getKey().isSelected()) {
                        l.add(e.getValue());
                    }
                }

                return new Pair<>(concept.getText(), l);
            } else {
                if(clickButton.equals(createConcept)) {
                    return new Pair<>(null, null);
                }
            }
            return null;
        });

        createNodeDialog.initOwner(MainApp.primaryStage);
        Optional<Pair<String, List<Cible>>> conceptToSearch = createNodeDialog.showAndWait();
        conceptToSearch.ifPresent(conceptAndCible -> {
            if(conceptAndCible.getKey() == null && conceptAndCible.getValue() == null) {
                createNewConcept(type);
            } else {
                startSearch(type, conceptAndCible.getKey(), conceptAndCible.getValue(), create);
            }
        });
    }

    private void createNewConcept(String type) {
        VocabConcept root = mainApp.getOntologyControler().getVocabTree().getValue();



        Dialog<ButtonType> newNodeDialog = new Dialog<>();
        newNodeDialog.initModality(Modality.WINDOW_MODAL);
        newNodeDialog.initOwner(MainApp.primaryStage);
        newNodeDialog.setTitle("create new concept");
        ButtonType createFromURI = new ButtonType("Create from URI", ButtonBar.ButtonData.LEFT);

        newNodeDialog.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK, createFromURI);
        final Button btURI = (Button) newNodeDialog.getDialogPane().lookupButton(createFromURI);

        btURI.addEventFilter(
                ActionEvent.ACTION,
                event -> {
                    TextInputDialog dialogFromURI = new TextInputDialog("");
                    dialogFromURI.setTitle("Create from URI");
                    dialogFromURI.setHeaderText("Create concept from URI");
                    dialogFromURI.initOwner(MainApp.primaryStage);
                    Optional<String> result = dialogFromURI.showAndWait();
                    if (result.isPresent()){
                        if(!result.get().isEmpty()) {
                            Task<Boolean> worker = new Task<Boolean>() {
                                @Override
                                protected Boolean call() throws Exception {
                                    String uriToCheck = result.get();
                                    HashMap<String, HashMap<String, List<String>>> labDesc = JenaTools.getLabelsAndDescription(uriToCheck);
                                    final String[] tempLabDes = new String[4];
                                    tempLabDes[0] = "";
                                    tempLabDes[1] = "";
                                    tempLabDes[2] = "";
                                    tempLabDes[3] = "";

                                    for (Map.Entry<String, List<String>> e : labDesc.get("label").entrySet()) {
                                        if(e.getKey().equalsIgnoreCase("en")) {
                                            for(String ee : e.getValue()) {
                                                tempLabDes[0] = ee;
                                            }
                                        }
                                        if(e.getKey().equalsIgnoreCase("fr")) {
                                            for(String ee : e.getValue()) {
                                                tempLabDes[1] = ee;
                                            }
                                        }
                                    }
                                    for (Map.Entry<String, List<String>> e : labDesc.get("Comment").entrySet()) {
                                        if(e.getKey().equalsIgnoreCase("en")) {
                                            for(String ee : e.getValue()) {
                                                tempLabDes[2] = ee;
                                            }
                                        }
                                        if(e.getKey().equalsIgnoreCase("fr")) {
                                            for(String ee : e.getValue()) {
                                                tempLabDes[3] = ee;
                                            }
                                        }
                                    }

                                    Platform.runLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            if(!tempLabDes[0].isEmpty()) {
                                                textLabel.setText(tempLabDes[0]);
                                                textDesc.setText(tempLabDes[2]);
                                                comboLang.setValue("English");
                                                createURI = uriToCheck;
                                            } else {
                                                if(!tempLabDes[1].isEmpty()) {
                                                    textLabel.setText(tempLabDes[1]);
                                                    textDesc.setText(tempLabDes[3]);
                                                    comboLang.setValue("French");
                                                    createURI = uriToCheck;

                                                }
                                            }
                                        }
                                    });


                                    return true;
                                }
                            };
                            worker.setOnSucceeded(event2 -> {
                            });
                            worker.setOnFailed(event2 -> {
                                Alert failed = new Alert(Alert.AlertType.ERROR);
                                failed.setTitle("Unable to find concept");
                                failed.setContentText("Unable to find concept at " + result.get());
                                failed.initOwner(MainApp.primaryStage);
                                failed.initModality(Modality.APPLICATION_MODAL);
                                failed.showAndWait();
                            });
                            new Thread(worker).start();

                        }
                    }

                    event.consume();
                }
        );
        GridPane paneNewConcept = new GridPane();
        ColumnConstraints c1 = new ColumnConstraints();
        c1.setPercentWidth(70);
        paneNewConcept.getColumnConstraints().add(c1);

        Label labelLabel = new Label("Label : ");
        Label labelLangue = new Label("Language : ");
        Label labelDesc = new Label("Description : ");
        BooleanProperty errorConcept = new SimpleBooleanProperty(true);



        textLabel.textProperty().addListener((observableValue, oldValue, newValue) -> {
            String newURI = Tools.normalize(MainApp.getOntologyControler().getCurrentOntology().getBaseURI() + type + "/" + newValue.trim());
            if (root.conceptExist(newValue.trim(), "label") || root.conceptExist(newURI, "uri")) {
                textLabel.setStyle("-fx-text-inner-color: red;");
                errorConcept.setValue(true);
            } else {
                textLabel.setStyle("-fx-text-inner-color: black;");
                errorConcept.setValue(false);
            }
        });

        VBox bLabel = new VBox();
        bLabel.getChildren().addAll(labelLabel, textLabel);

        VBox bLang = new VBox();
        bLang.getChildren().addAll(labelLangue, comboLang);

        VBox bDesc = new VBox();
        bDesc.getChildren().addAll(labelDesc, textDesc);

        paneNewConcept.add(bLabel, 0,0);
        paneNewConcept.add(bLang, 1,0);
        paneNewConcept.add(bDesc,0,1,2,1);


        // setting lang in combo
        ObservableList<String> listLang = FXCollections.observableArrayList();
        listLang.add("English");
        listLang.add("French");
        comboLang.setItems(listLang);
        comboLang.setCellFactory(c-> {
            return new ListCell<String>() {
                @Override
                protected void updateItem(String s, boolean b) {
                    super.updateItem(s, b);
                    if (s != null && !b) {
                        setText(s);
                        ImageView i = null;
                        if (s.equalsIgnoreCase("English")) {
                            i = new ImageView(UITools.getImage("resources/images/flags/en.png"));

                        }
                        if (s.equalsIgnoreCase("French")) {
                            i = new ImageView(UITools.getImage("resources/images/flags/fr.png"));
                        }
                        if (i != null) {
                            i.setPreserveRatio(true);
                            i.setFitHeight(20.0);
                        }
                        setGraphic(i);
                    } else {
                        setText("");
                        setGraphic(null);
                    }
                }
            };
        });


        BooleanBinding bb = Bindings.or(errorConcept,Bindings.or(textLabel.textProperty().isEmpty(), comboLang.getSelectionModel().selectedIndexProperty().isEqualTo(-1)));
        newNodeDialog.getDialogPane().lookupButton(ButtonType.OK).disableProperty().bind(bb);

        newNodeDialog.getDialogPane().setContent(paneNewConcept);
        Optional<ButtonType> buttonCreateClick = newNodeDialog.showAndWait();
        if (buttonCreateClick.get().equals(ButtonType.OK) ) {
            VocabConcept newNode = new VocabConcept(MainApp.getOntologyControler().getCurrentOntology());
            newNode.addSkosScheme(MainApp.getOntologyControler().getCurrentOntology().getMainScheme());
            newNode.setPO2Node(type);
            String ll = "en";
            if(comboLang.getSelectionModel().getSelectedItem().equalsIgnoreCase("french")) {
                ll = "fr";
            }
            newNode.setLabel(ll, textLabel.getText().trim());
            if(!textDesc.getText().isEmpty()) {
                newNode.setDefinition(ll, textDesc.getText(), false);
            }

            this.father.addChildren(newNode);
            newNode.addFather(this.father);
            if(!createURI.isEmpty()) {
                newNode.importFrom( createURI,textLabel.getText().trim(),textDesc.getText().trim());
            }
            MainApp.getOntologyControler().rebuildTree(true);
            mainApp.openNode(newNode);
            MainApp.logger.info("adding node");
            mainApp.getOntologyControler().modified(true);
        }

    }

    private void startSearch(String type, String keyWord, List<Cible> listCible, Boolean create) {
        Requete query = new Requete(keyWord);
        query.selectCibles(listCible);

        Dialog<ButtonType> createNodeDialog = new Dialog<>();
        createNodeDialog.setTitle("Result for " + type + " " + keyWord);
        DialogPane dialogPane = createNodeDialog.getDialogPane();
        dialogPane.setPrefSize(800, 500);
        createNodeDialog.setResizable(true);
        ButtonType importButton = new ButtonType("Import", ButtonBar.ButtonData.OK_DONE);
        ButtonType newSearchButton = new ButtonType("New Search", ButtonBar.ButtonData.OTHER);
        ButtonType createConcept = new ButtonType("Create concept", ButtonBar.ButtonData.LEFT);

        dialogPane.getButtonTypes().addAll(importButton, newSearchButton, ButtonType.CANCEL, createConcept);
        dialogPane.lookupButton(importButton).setDisable(true);
        if(!create) {
            dialogPane.lookupButton(createConcept).setVisible(false);
            dialogPane.lookupButton(createConcept).setDisable(true);
        }

        BorderPane borderPane = new BorderPane();
        dialogPane.setContent(borderPane);

        //TOP
        VBox boxLabAndURI = new VBox(3);
        boxLabAndURI.prefWidthProperty().bind(dialogPane.widthProperty().multiply(0.4));
        boxLabAndURI.prefHeightProperty().bind(dialogPane.heightProperty().multiply(0.2));

        Label conceptLabel = new Label("Concept :");
        TextField conceptField = new TextField();
        conceptField.setEditable(false);
        conceptField.setStyle("-fx-text-inner-color: black;");
        Hyperlink uri = new Hyperlink();
        boxLabAndURI.getChildren().addAll(conceptLabel, conceptField, uri);

        VBox boxDesc = new VBox(3);
        boxDesc.prefWidthProperty().bind(dialogPane.widthProperty().multiply(0.6));
        boxDesc.prefHeightProperty().bind(dialogPane.heightProperty().multiply(0.2));
        Label descLab = new Label("Description : ");
        TextArea desc = new TextArea();
        desc.setWrapText(true);
        desc.setEditable(false);
        boxDesc.getChildren().addAll(descLab, desc);
        HBox hBox = new HBox(10);
        hBox.setPadding(new Insets(0, 0, 3, 0));
        hBox.getChildren().addAll(boxLabAndURI, boxDesc);

        borderPane.setTop(hBox);

        //CENTER
//         Accordion accord = new Accordion();


        TitledPane globale = new TitledPane("Global Search", null);
        ProgressIndicator pi = new ProgressIndicator();
        pi.setStyle(" -fx-progress-color: limegreen;");
        pi.progressProperty().bind(query.getCompletionProperty());
        globale.setGraphic(pi);


//        accord.getPanes().add(globale);
//        accord.setExpandedPane(globale);
        VBox globaleContent = new VBox(0);
//        globaleContent.min


        for (Cible c : listCible) {
            ProgressBar p = new ProgressBar();
            p.setPrefWidth(30.0);
            p.styleProperty().bind(c.styleProperty());
//            p.setStyle(" -fx-accent: limegreen");

//            Label t = new Label(c.getName());
            TitledPane t = new TitledPane(c.getName(), null);
            t.setGraphic(p);
            p.progressProperty().bind(c.isFinish());
            globaleContent.getChildren().add(t);
        }
        AnchorPane ap = new AnchorPane();
        AnchorPane.setRightAnchor(globaleContent, 0.0);
        AnchorPane.setTopAnchor(globaleContent, 0.0);
        AnchorPane.setLeftAnchor(globaleContent, 10.0);
        AnchorPane.setBottomAnchor(globaleContent, 0.0);
        ap.getChildren().add(globaleContent);
        ScrollPane scrollPane = new ScrollPane(ap);
        globale.setContent(scrollPane);
//        accord.setExpandedPane(globale);


        TableView<Result> tableResult = new TableView<Result>();
        tableResult.setItems(query.getResultProperty());
        TableColumn<Result, String> colLabel = new TableColumn("Label");
        colLabel.prefWidthProperty().bind(tableResult.widthProperty().multiply(0.25));
        colLabel.setCellValueFactory(r -> r.getValue().getLabelProperty());
        TableColumn<Result, String> colSource = new TableColumn("Source");
        colSource.prefWidthProperty().bind(tableResult.widthProperty().multiply(0.15));
        colSource.setCellValueFactory(r -> r.getValue().getSourceProperty());
        TableColumn<Result, String> colDesc = new TableColumn("Description");
        colDesc.prefWidthProperty().bind(tableResult.widthProperty().multiply(0.6));
        colDesc.setCellValueFactory(r -> r.getValue().getDescriptionProperty());
        tableResult.getColumns().addAll(colLabel, colSource, colDesc);
        tableResult.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Result>() {
            @Override
            public void changed(ObservableValue<? extends Result> observable, Result oldValue, Result newValue) {
                if (newValue != null) {
                    Hyperlink po2URI = new Hyperlink(Tools.normalize(MainApp.getOntologyControler().getCurrentOntology().getBaseURI() + type + "/" + newValue.getDescriptionProperty().get().trim()));

                    VocabConcept root = mainApp.getOntologyControler().getVocabTree().getValue();

                    if(create) {
                        if (root.conceptExist(newValue.getClasseProperty().get().trim(), "uri") || root.conceptExist(newValue.getLabelProperty().get().trim(), "label")) {
                            conceptField.setStyle("-fx-text-inner-color: red;");
                            dialogPane.lookupButton(importButton).setDisable(true);
                        } else {
                            conceptField.setStyle("-fx-text-inner-color: black;");
                            dialogPane.lookupButton(importButton).setDisable(false);
                        }
                    } else {
                        if (root.conceptExist(newValue.getClasseProperty().get().trim(), "uri")) {
                            conceptField.setStyle("-fx-text-inner-color: red;");
                            dialogPane.lookupButton(importButton).setDisable(true);
                        } else {
                            conceptField.setStyle("-fx-text-inner-color: black;");
                            dialogPane.lookupButton(importButton).setDisable(false);
                        }
                    }

                    desc.setText(newValue.getDescriptionProperty().get());
                    conceptField.setText(newValue.getLabelProperty().get());
                    uri.setText(newValue.getClasseProperty().get());
                    uri.setOnAction(new EventHandler<ActionEvent>() {

                        @Override
                        public void handle(ActionEvent event) {
                            MainApp.openBrowser(uri.getText());
                        }
                    });

                } else {
                    desc.setText("");
                    conceptField.setText("");
                }
            }
        });


//        ScrollPane scroll = new ScrollPane(accord);


//        scroll.minWidthProperty().bind(split.widthProperty().multiply(0.30));
//        scroll.maxWidthProperty().bind(split.widthProperty().multiply(0.30));
//        accord.minWidthProperty().bind(scroll.minWidthProperty());
//        accord.maxWidthProperty().bind(scroll.maxWidthProperty());


        AnchorPane ap1 = new AnchorPane(globale);
        AnchorPane.setTopAnchor(globale, 0.0);
        AnchorPane.setBottomAnchor(globale, 0.0);
        AnchorPane.setRightAnchor(globale, 0.0);
        AnchorPane.setLeftAnchor(globale, 0.0);
        SplitPane split = new SplitPane(ap1, tableResult);
        split.setOrientation(Orientation.HORIZONTAL);
        split.setDividerPositions(0);
//        split.getItems().add(area);
        borderPane.setCenter(split);


        createNodeDialog.initOwner(MainApp.primaryStage);

        Thread threadQuery = new Thread(query);
        threadQuery.start();
        Optional<ButtonType> buttonClick = createNodeDialog.showAndWait();
        if (buttonClick.get() == newSearchButton) {
            new SearchConcept(mainApp,create, type, this.father);
        } else {
            if (buttonClick.get() == importButton) {
                final VocabConcept[] newNode = {null};
                if(this.create) {
                    Task<Boolean> taskImport = new Task<Boolean>() {
                        @Override
                        protected Boolean call() throws Exception {
                            newNode[0] = new VocabConcept(MainApp.getOntologyControler().getCurrentOntology());
                            newNode[0].addSkosScheme(MainApp.getOntologyControler().getCurrentOntology().getMainScheme());
                            newNode[0].setPO2Node(type);
                            newNode[0].importFrom(uri.getText(), conceptField.getText(), desc.getText());
                            father.addChildren(newNode[0]);
                            newNode[0].addFather(father);
                            Platform.runLater(() -> {
                                MainApp.getOntologyControler().rebuildTree(true);
                            });
                            return true;
                        }
                    };
                    taskImport.setOnSucceeded(workerStateEvent -> {
                    });
                    taskImport.setOnFailed(workerStateEvent -> {
                    });
                    new Thread(taskImport).start();

                } else {
                    father.importFrom(uri.getText(), conceptField.getText(), desc.getText());
                }

                mainApp.getOntologyControler().modified(true);
            } else {
                if(buttonClick.get() == createConcept) {
                    createNodeDialog.close();
                    createNewConcept(type);
                }
            }
        }
    }
}
