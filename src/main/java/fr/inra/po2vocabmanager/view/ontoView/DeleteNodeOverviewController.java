/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.view.ontoView;

import fr.inra.po2vocabmanager.MainApp;
import fr.inrae.po2engine.model.VocabConcept;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class DeleteNodeOverviewController {

    private MainApp mainApp;

    private HashMap<String, String> deleteStyle;
    private VocabConcept node;
    private HashMap<VocabConcept, CheckBox> listParent;


    @FXML
    private Label title;
    @FXML
    private ChoiceBox<String> choiceBox;
    @FXML
    private TextArea description;
    @FXML
    private BorderPane deletePane;
    @FXML
    private Button delete;

    @FXML
    private void initialize() {
        delete.setDisable(true);
        deleteStyle = new HashMap<>();

        deleteStyle.put("node only", "Delete only the current node. All it's childs will be moved to the parent.\n\ne.g.  delete node B\n\n- node A                - node A\n|_ - node B     --->  |_ - node C\n  |_ - node C           |_ - node D\n  |_ - node D	");
        deleteStyle.put("node and descendants", "Delete the current node and all it's descendants.\n\ne.g.  delete node B\n\n- node A\n|_ - node B     --->    - node A\n  |_ - node C\n  |_ - node D");

        for (Entry<String, String> entry : deleteStyle.entrySet()) {
            choiceBox.getItems().add(entry.getKey());
        }

        choiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                description.setText(deleteStyle.get(newValue));
                description.setEditable(false);
                delete.setDisable(false);
            }
        });
    }

    private VocabConcept getParent(VocabConcept node, VocabConcept from) {
        if (from.getSubNode().contains(node)) {
            return from;
        } else {
            Iterator<VocabConcept> ite = from.getSubNode().iterator();
            while (ite.hasNext()) {
                VocabConcept n = getParent(node, ite.next());
                if (n != null) {
                    return n;
                }
            }
        }
        return null;
    }

    @FXML
    private void deleteNode() {
        if (listParent != null && listParent.size() > 0) {
            MainApp.logger.debug("deleting node : " + node.getName());
            VocabConcept nodeToSelect = null;
            Boolean all = true;
            switch (choiceBox.getValue()) {
                case "node only":
                    for (Entry<VocabConcept, CheckBox> en : listParent.entrySet()) {
                        if(nodeToSelect == null) {
                            nodeToSelect = en.getKey();
                        }
                        if (en.getValue().isSelected()) {
                            en.getKey().getSubNode().remove(node);
                            node.getFathers().remove(en.getKey());

                            en.getKey().addChildren(node.getSubNode());
                            for (VocabConcept pf : node.getSubNode()) {
                                pf.addFather(en.getKey());
                            }
                        } else {
                            all = false;
                        }
                    }
                    if (all) {
                        for (VocabConcept pf : node.getSubNode()) {
                            pf.getFathers().remove(node);
                        }
                    }

                    break;
                case "node and descendants":
                    for (Entry<VocabConcept, CheckBox> en : listParent.entrySet()) {
                        if (en.getValue().isSelected()) {
                            en.getKey().getSubNode().remove(node);
                            node.getFathers().remove(en.getKey());
                        }
                    }
                    break;

                default:
                    break;
            }
            MainApp.getOntologyControler().rebuildTree(true);
            mainApp.openNode(nodeToSelect);
            MainApp.getOntologyControler().modified(true);
        }
        ((Stage) (title.getScene().getWindow())).close();
    }

    @FXML
    private void cancel() {
        ((Stage) (title.getScene().getWindow())).close();
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }


    public void setNode(VocabConcept node) {
        this.node = node;
        this.listParent = new HashMap<>();

        if (this.node.getFathers().size() > 1) {
            deletePane.setPrefWidth(deletePane.getPrefWidth() + 200);
            VBox moreFather = new VBox();
            moreFather.getChildren().add(new Label("Remove this node from parents : "));
            for (VocabConcept p : this.node.getFathers()) {
                CheckBox b = new CheckBox(p.getName());
                this.listParent.put(p, b);
                moreFather.getChildren().add(b);
            }

            deletePane.setRight(moreFather);
        } else {
            CheckBox b = new CheckBox();
            b.setSelected(true);
            listParent.put(node.getFathers().get(0), b);
        }

        choiceBox.getSelectionModel().selectFirst();
    }

}
