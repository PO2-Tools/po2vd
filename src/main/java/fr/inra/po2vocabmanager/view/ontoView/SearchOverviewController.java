/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.view.ontoView;

import fr.inra.po2vocabmanager.MainApp;
import fr.inra.po2vocabmanager.utils.FilterableTreeItem;
import fr.inra.po2vocabmanager.utils.TreeItemPredicate;
import fr.inrae.po2engine.externalTools.JenaTools;
import fr.inrae.po2engine.model.VocabConcept;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.transformation.FilteredList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;

import java.util.stream.Collectors;

/**
 * @author stephane
 */
public class SearchOverviewController {

    private MainApp mainApp;

    @FXML
    private TextField textfield;
    @FXML
    private TableView<VocabConcept> tableView;
    @FXML
    private TableColumn<VocabConcept, String> labelColumn;
    @FXML
    private TableColumn<VocabConcept, String> descColumn;
    @FXML
    private TreeView<VocabConcept> treeView;
    @FXML
    private CheckBox checkProcess;
    @FXML
    private CheckBox checkComponent;
    @FXML
    private CheckBox checkAttribute;
    @FXML
    private CheckBox checkMaterial;
    @FXML
    private CheckBox checkMethod;
    @FXML
    private CheckBox checkStep;
    @FXML
    private CheckBox checkScale;
    private SimpleStringProperty checked;

    @FXML
    private void initialize() {
        labelColumn.setCellValueFactory(cellData -> cellData.getValue().labsToString());
        descColumn.setCellValueFactory(cellData -> cellData.getValue().descToString());
        checked = new SimpleStringProperty("");
        checked.bind(Bindings.concat(
                Bindings.when(checkProcess.selectedProperty()).then(checkProcess.textProperty()).otherwise(""),
                Bindings.when(checkComponent.selectedProperty()).then(checkComponent.textProperty()).otherwise(""),
                Bindings.when(checkAttribute.selectedProperty()).then(checkAttribute.textProperty()).otherwise(""),
                Bindings.when(checkMaterial.selectedProperty()).then(checkMaterial.textProperty()).otherwise(""),
                Bindings.when(checkMethod.selectedProperty()).then(checkMethod.textProperty()).otherwise(""),
                Bindings.when(checkStep.selectedProperty()).then(checkStep.textProperty()).otherwise(""),
                Bindings.when(checkScale.selectedProperty()).then(checkScale.textProperty()).otherwise("")
        ));

        tableView.setRowFactory(tv -> {
            TableRow<VocabConcept> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && !row.isEmpty()) {
                    mainApp.openNode(row.getItem());
                }
            });
            return row;
        });
        treeView.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent mouseEvent)
            {
                if(mouseEvent.getClickCount() == 2)
                {
                    TreeItem<VocabConcept> item = treeView.getSelectionModel().getSelectedItem();
                    mainApp.openNode(item.getValue());
                }
            }
        });
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
//        textfield.setOnKeyReleased(event -> {
//            if (event.getCode().equals(KeyCode.ENTER)) {
//                search();
//            }
//        });
        initTableView();
        initTreeView(JenaTools.uriToLoad.toArray(new String[0]));

    }

    public void initTableView() {
        FilteredList<VocabConcept> filteredList = new FilteredList<>(MainApp.getOntologyControler().getRootItem().getValue().flattened().collect(Collectors.toCollection(FXCollections::observableArrayList)), p -> true);
        filteredList.predicateProperty().bind(Bindings.createObjectBinding(() -> {
            if (textfield.getText() == null || textfield.getText().isEmpty()) {
                return vocabConcept -> true;
            }
            return vocabConcept -> {
                if(vocabConcept == null || vocabConcept.getSearchIn() == null || !checked.get().toLowerCase().contains(vocabConcept.isPO2Node().toLowerCase())) {
                    return false;
                }
                return vocabConcept.getSearchIn().toLowerCase().contains(textfield.getText().toLowerCase());
            };
        }, textfield.textProperty(), checked));
        tableView.setItems(filteredList);
    }

    private void buildTree(FilterableTreeItem<VocabConcept> filterLocalRoot, VocabConcept localRoot) {
        for(VocabConcept son : localRoot.getSubNode().filtered(vocabConcept -> !vocabConcept.getDeprecated())) {
            FilterableTreeItem<VocabConcept> vson = new FilterableTreeItem<>(son);
            filterLocalRoot.getInternalChildren().add(vson);
            buildTree(vson, son);
        }
    }
    public void initTreeView(String... topURIs) {


        VocabConcept frv = new VocabConcept("", null);
        frv.setName("fake-root");
        FilterableTreeItem<VocabConcept> fakeRoot = new FilterableTreeItem<>(frv);

        treeView.setShowRoot(false);


        treeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                // open in treeView node ???
            }
        });
        fakeRoot.getChildren().clear();
        treeView.setRoot(fakeRoot);

        if (MainApp.getOntologyControler() != null && MainApp.getOntologyControler().getCurrentOntology() != null) {
            fakeRoot.getChildren().clear();

            for(String topURI : topURIs) {
                // recherche du root item
                TreeItem<VocabConcept> localRoot = MainApp.getOntologyControler().getRootItem().getChildren().stream().filter(vocabConceptTreeItem -> {
                    return vocabConceptTreeItem.getValue().getURI().equalsIgnoreCase(topURI);
                }).findFirst().orElse(null);
                FilterableTreeItem<VocabConcept> filterLocalRoot = new FilterableTreeItem<>(localRoot.getValue());

                buildTree(filterLocalRoot, localRoot.getValue());

                filterLocalRoot.predicateProperty().bind(Bindings.createObjectBinding(() -> {
                    if (textfield.getText() == null || textfield.getText().isEmpty())
                        return TreeItemPredicate.create(value -> true);
                    return TreeItemPredicate.create(value -> {
                        if(!checked.get().toLowerCase().contains(value.isPO2Node().toLowerCase())) {
                            return false;
                        }
                        return value.getSearchIn().toLowerCase().contains(textfield.getText().toLowerCase());
                    });
                }, textfield.textProperty(), checked));
                fakeRoot.getChildren().add(filterLocalRoot);
            }
            treeView.setRoot(fakeRoot);

        }
    }



}
