/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.view.ontoView;

import fr.inra.po2vocabmanager.MainApp;
import fr.inrae.po2engine.model.VocabConcept;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class CreateNodeOverviewController {


    @FXML
    private TextField textLabel;
    @FXML
    private TextArea textDescription;
    @FXML
    private ComboBox<String> comboLangue;

    private String po2type;

    private VocabConcept fatherNode;

    private MainApp mainApp;

    @FXML
    private void initialize() {

    }

//    @FXML
//    private void createNode() {
//        VocabNode newNode = new VocabNode(uri.getText());
//        newNode.setLabel("en", enLab.getText());
//        newNode.setLabel("fr", frLab.getText());
//        newNode.setDescription("en", enDesc.getText());
//        newNode.setDescription("fr", frDesc.getText());
//        if (!po2type.isEmpty()) {
//            newNode.setPO2Node(po2type);
//        }
//        fatherNode.getSubNodeNoSort().add(newNode);
//        mainApp.getOntologyControler().modified(true);
//        mainApp.openNode(newNode);
//        ((Stage) (title.getScene().getWindow())).close();
//    }

//    @FXML
//    private void cancel() {
//        ((Stage) (title.getScene().getWindow())).close();
//    }

//    @FXML
//    private void setEnLabel() {
//        title.setText(enLab.getText().trim());
//        uri.setText(Tools.normalize(MainApp.baseURI + po2type + "/" + enLab.getText().trim()));
//
//        VocabNode root = mainApp.getOntologyControler().getVocabTree().getValue();
//
//        if (root.conceptExist(uri.getText().trim(), "uri") || root.conceptExist(enLab.getText().trim(), "label")) {
//            title.setTextFill(Color.RED);
//            uri.setTextFill(Color.RED);
//            createButton.setDisable(true);
//        } else {
//            title.setTextFill(Color.BLACK);
//            uri.setTextFill(Color.BLACK);
//            createButton.setDisable(false);
//        }
//    }

    public void setPO2Type(String type) {
        po2type = type;
    }

    public void setFatherNode(VocabConcept node) {
        fatherNode = node;
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

}
