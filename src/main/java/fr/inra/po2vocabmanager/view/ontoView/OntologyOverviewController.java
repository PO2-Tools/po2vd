/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.view.ontoView;

import com.kennycason.kumo.CollisionMode;
import com.kennycason.kumo.WordCloud;
import com.kennycason.kumo.bg.RectangleBackground;
import com.kennycason.kumo.font.scale.LinearFontScalar;
import com.kennycason.kumo.nlp.FrequencyAnalyzer;
import com.kennycason.kumo.palette.ColorPalette;
import fr.inra.po2vocabmanager.MainApp;
import fr.inra.po2vocabmanager.model.VocabNodeWLang;
import fr.inra.po2vocabmanager.utils.*;
import fr.inrae.po2engine.exception.AlreayLockException;
import fr.inrae.po2engine.exception.CantWriteException;
import fr.inrae.po2engine.exception.LocalLockException;
import fr.inrae.po2engine.exception.NotLoginException;
import fr.inrae.po2engine.externalTools.CloudConnector;
import fr.inrae.po2engine.externalTools.JenaTools;
import fr.inrae.po2engine.model.Ontologies;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.SkosScheme;
import fr.inrae.po2engine.model.VocabConcept;
import fr.inrae.po2engine.utils.ProgressPO2;
import fr.inrae.po2engine.utils.Tools;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableStringValue;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.util.Pair;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.csv.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipOutputStream;
import org.json.JSONArray;
import org.json.JSONObject;

import java.awt.*;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;


/**
 * @author stephane
 */
public class OntologyOverviewController {

    @FXML
    ImageView imageCloud;
    @FXML
    ImageView imageNode;
    @FXML
    TableView<VocabNodeWLang> tableView;
    @FXML
    TableColumn<VocabNodeWLang, String> labelColumn;
    @FXML
    TableColumn<VocabNodeWLang, String> synonymsColumn;
    @FXML
    TableColumn<VocabNodeWLang, String> descColumn;
    @FXML
    TableColumn<VocabNodeWLang, ImageView> flagColumn;
    @FXML
    VBox boxDimension;
    @FXML
    Hyperlink link;
    @FXML
    TableView<SkosScheme> tableViewConceptScheme;
    @FXML
    TableColumn<SkosScheme, SkosScheme> conceptSchemeColumn;


    @FXML
//    TableView<ObservableObjectValue<Hyperlink>> tableLink;
            TableView<SimpleStringProperty> tableExact;
@FXML
TableView<SimpleStringProperty> tableClose;
    @FXML
//    TableColumn<ObservableObjectValue<Hyperlink>, Hyperlink> linkColumn;
        TableColumn<SimpleStringProperty, Hyperlink> exactMatchColumn;
    @FXML
    TableColumn<SimpleStringProperty, Hyperlink> closeMatchColumn;

    private MainApp mainApp;
    private searchInfo searchInfo;
    private List<String> wordFrequency;
    private VocabConcept currentNode ;
    @FXML
    private HBox statusBox;
    @FXML
    private TreeView<VocabConcept> vocabTree;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private Button buttonCopy;
    @FXML
    private Label labelChemin;
    @FXML
    private TextArea textScopeNote;
    @FXML
    private TextArea textNote;
    @FXML
    private TextArea textEditorialNote;
    private Ontology currentOntology;
    Button addDim;

    private ChangeListener changeListener = new ChangeListener() {
        @Override
        public void changed(ObservableValue observableValue, Object o, Object t1) {
            modified(true);
        }
    };

    private ListChangeListener listChangeListener = new ListChangeListener() {
        @Override
        public void onChanged(Change change) {
            modified(true);
        }
    };

    private BooleanProperty modified = new SimpleBooleanProperty(false);
    private BooleanProperty canEdit = new SimpleBooleanProperty(false);
    private BooleanProperty syncCloud = new SimpleBooleanProperty(false);
    private BooleanProperty isOnMainScheme = new SimpleBooleanProperty(false);
    private BooleanProperty showDeprecated = new SimpleBooleanProperty(false);

    private TreeItem<VocabConcept> rootItem;
    private StringProperty title = new SimpleStringProperty();
    private StringProperty fileName = new SimpleStringProperty("");

    private StringProperty schemeName = new SimpleStringProperty("");

    private IntegerProperty majorVersion = new SimpleIntegerProperty(0);
    private IntegerProperty minorVersion = new SimpleIntegerProperty(0);

//    private VocabNode root = null;

    @FXML
    private void initialize() {
        currentOntology = null;
        currentNode = null;
        imageCloud.setPreserveRatio(false);
        imageNode.setPreserveRatio(true);
        boxDimension.setVisible(false);
        conceptSchemeColumn.setCellValueFactory(skosSchemeSkosSchemeCellDataFeatures -> new SimpleObjectProperty<>(skosSchemeSkosSchemeCellDataFeatures.getValue()));

        Label labDime = (Label) boxDimension.getChildren().get(0);

        addDim = new Button(null, new ImageView(UITools.getImage("resources/images/add_16.png")));

        addDim.setOnMouseClicked(mouseEvent -> {
            Dialog<ButtonType> editValue = new Dialog();
            editValue.setResizable(true);
            editValue.setTitle("Add dimension from unit");
            editValue.initModality(Modality.APPLICATION_MODAL);
            editValue.initOwner(tableView.getScene().getWindow());
            editValue.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
//                                                editValue.getDialogPane().setPrefSize(400, 150);
            TextField unitValue = new TextField();
            StringProperty printUnitValue = new SimpleStringProperty("none");
            unitValue.textProperty().addListener((observableValue, oldValue, newValue) -> {
                String prettyValueD = Tools.getPrettyUnit(newValue);
                if(prettyValueD != null) {
                    printUnitValue.setValue(Tools.getDimension(newValue));
                } else {
                    printUnitValue.setValue("Not a valid unit");
                }
            } );

            unitValue.setText("");

            Label labPrintUnit = new Label();
            labPrintUnit.textProperty().bind(Bindings.concat("Dimension : ", printUnitValue));
            GridPane paneShow = new GridPane();
            ColumnConstraints c1 = new ColumnConstraints();
            c1.setPercentWidth(35);
            paneShow.getColumnConstraints().addAll(c1);

            Button butShowPref = new Button("Show prefix code");
            butShowPref.setOnMouseClicked(mouseEvent1 -> {
                UITools.showUnitUcumPrefix();
            });
            Button butShowUnit = new Button("Show units code");
            butShowUnit.setOnMouseClicked(mouseEvent1 -> {
                UITools.showUnitUcumCode();
            });

            FlowPane p = new FlowPane(butShowPref, butShowUnit);
            paneShow.add(labPrintUnit, 0,0);
            paneShow.add(p, 1,0);

            VBox b = new VBox(4.0);
            b.getChildren().addAll(unitValue, paneShow);
            editValue.getDialogPane().setContent(b);
            Hyperlink linkToUCUM = new Hyperlink();
            linkToUCUM.setText("here");
            linkToUCUM.setOnAction(actionEvent -> MainApp.openBrowser("http://unitsofmeasure.org/ucum.html"));
            Hyperlink linkToUcumSem = new Hyperlink();
            linkToUcumSem.setText("here");
            linkToUcumSem.setOnAction(actionEvent -> MainApp.openBrowser("https://finto.fi/ucum/en/"));



            TextFlow unitInfo = new TextFlow();
            unitInfo.getChildren().add(new Text("Only Ucum code available "));
            unitInfo.getChildren().add(linkToUCUM);
            unitInfo.getChildren().add(new Text(" or "));
            unitInfo.getChildren().add(linkToUcumSem);
            unitInfo.getChildren().add(new Text(" can be used in the text field.\n" +
                    "To choose a prefix, click on \"Show prefix code\"\n" +
                    "   e.g. prefix for \"micro\" is u\n\n" +
                    "To display the existing codes, click on \"Show units code\"\n" +
                    "   e.g. code for \"degree Celsius\" is Cel\n\n" +
                    "Exponents are written directly as numbers following the unit.\n" +
                    "   e.g. m2 for square meter\n" +
                    "        m3 for cubic meter\n\n" +
                    "Care must also be taken to combine units correctly\n" +
                    "   e.g. m/s or m.s-1 are valid entries.\n\n" +
                    "If there is no entry in the field \"unit\", then the value is typed as a string of characters (qualitative value).\n\n" +
                    "For numeric values that have no unit or dimensionless numbers, use an annotation between curly braces (e.g. {dimensionless} or {one}).\nThis entry will be interpreted as a vector of dimension 1.\n\n" +
                    "If you want to add any details, you can annotate your unit with curly braces { }\n" +
                    "   e.g.  kg{total} --> kg(total) will be displayed in the field \"unit\"\n\n" +
                    "Also, please use curly braces if your unit is not a valid SI unit, or if you do not wish any conversion.\n" +
                    "There must be no spaces in the content entered between curly braces."));


            editValue.getDialogPane().setExpandableContent(unitInfo);

            Optional<ButtonType> result = editValue.showAndWait();
            if (result.get() == ButtonType.OK) {
                String stringDim = Tools.getDimension(unitValue.getText());
                if (stringDim != null) {
                    currentNode.getListDimension().add(new SimpleStringProperty(stringDim));
                    mainApp.getOntologyControler().modified(true);
                    showNodeDetails(vocabTree.getSelectionModel().getSelectedItem());
                } else {
                }
            }

        });
        labDime.setGraphic(addDim);
        wordFrequency = new ArrayList<>();
        vocabTree.setShowRoot(false);
        vocabTree.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {if (newValue != null) showNodeDetails(newValue);});
        KeyCodeCombination copyKeyCodeCombination = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_ANY);
        KeyCodeCombination cutKeyCodeCombination = new KeyCodeCombination(KeyCode.X, KeyCombination.CONTROL_ANY);
        KeyCodeCombination pastKeyCodeCombination = new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_ANY);

        vocabTree.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                if (copyKeyCodeCombination.match(ke) || cutKeyCodeCombination.match(ke)) {
                    VocabConcept node = vocabTree.getSelectionModel().getSelectedItem().getValue();
                    Clipboard cb = Clipboard.getSystemClipboard();
                    ClipboardContent cc = new ClipboardContent();
                    cc.putString(node.isPO2Node());
                    cc.putUrl(node.getURI());
                    if (copyKeyCodeCombination.match(ke)) {
                        cc.put(DataFormat.RTF, "COPY");
                    } else {
                        cc.put(DataFormat.RTF, "CUT");
                    }
                    cb.setContent(cc);
                }
                if (pastKeyCodeCombination.match(ke)) {
                    if(!mainApp.getOntologyControler().getCanEditProperty().get()) {
                        ke.consume();
                        return;
                    }
                    Clipboard cb = Clipboard.getSystemClipboard();
                    VocabConcept node = vocabTree.getSelectionModel().getSelectedItem().getValue();
                    if (node != null) {
                        if(node.isPO2Node().equalsIgnoreCase(cb.getString())) {
                            VocabConcept pasteNode = mainApp.getOntologyControler().getCurrentOntology().getNodeByURI(cb.getUrl());
                            if (pasteNode != null) {
                                MainApp.logger.debug("paste concept " + pasteNode.getName());
                                VocabConcept loop = pasteNode.getNodeByURI(node.getURI());
                                if (loop != null) {
                                    Alert loopAlert = new Alert(Alert.AlertType.ERROR);
                                    loopAlert.setTitle("Loop creation");
                                    loopAlert.setHeaderText("Movement cancelled. You are creating a loop.");
                                    loopAlert.initOwner(MainApp.primaryStage);
                                    loopAlert.initModality(Modality.WINDOW_MODAL);
                                    loopAlert.show();
                                } else {
                                    if (cb.getContent(DataFormat.RTF) == "CUT") {
                                        HashMap<VocabConcept, CheckBox> listParent = new HashMap<>();
                                        if (pasteNode.getFathers().size() == 1) {
                                            CheckBox b = new CheckBox();
                                            b.setSelected(true);
                                            listParent.put(pasteNode.getFathers().get(0), b);
                                        } else {
                                            if (pasteNode.getFathers().size() > 1) {
                                                Dialog selectParent = new Dialog();
                                                selectParent.setTitle(pasteNode.getName() + " have multiple parents");
                                                DialogPane dp = selectParent.getDialogPane();
                                                VBox box = new VBox();
                                                Label l = new Label("This concept have " + pasteNode.getFathers().size() + " parents.\nPlease select from which parents you want to move this concept.\n\n");
                                                box.getChildren().add(l);
                                                for (VocabConcept p : pasteNode.getFathers()) {
                                                    CheckBox b = new CheckBox(p.getName());
                                                    listParent.put(p, b);
                                                    box.getChildren().add(b);
                                                }
                                                dp.setContent(box);
                                                dp.getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
                                                selectParent.initOwner(MainApp.primaryStage);
                                                Optional<ButtonType> click = selectParent.showAndWait();

                                                if (click.isPresent() && click.get().getButtonData().equals(ButtonBar.ButtonData.CANCEL_CLOSE)) {
                                                    listParent.clear();
                                                }

                                            }
                                        }
                                        for (Map.Entry<VocabConcept, CheckBox> en : listParent.entrySet()) {

                                            if (en.getValue().isSelected() && !en.getKey().equals(node)) {
                                                en.getKey().getSubNode().removeAll(pasteNode);
                                                pasteNode.getFathers().remove(en.getKey());

                                                if (!node.getSubNode().contains(pasteNode)) {
                                                    node.addChildren(pasteNode);
                                                }
                                                if (!pasteNode.getFathers().contains(node)) {
                                                    pasteNode.addFather(node);
                                                }
                                                MainApp.getOntologyControler().modified(true);
                                            }

                                        }
                                        MainApp.getOntologyControler().refreshTree();
                                    }
                                    if (cb.getContent(DataFormat.RTF) == "COPY") {
                                        node.addChildren(pasteNode);
                                        pasteNode.addFather(node);
                                        MainApp.getOntologyControler().modified(true);
                                        MainApp.getOntologyControler().refreshTree();
                                    }
                                }
                            }
                        } else {
                            // paste on another hierachie
                            Alert pbHierarchy = new Alert(Alert.AlertType.ERROR);
                            pbHierarchy.setTitle("Can't paste concept here");
                            pbHierarchy.setHeaderText("Copy § Cut are allowed in the same hierarchy");
                            pbHierarchy.initOwner(MainApp.primaryStage);
                            pbHierarchy.initModality(Modality.WINDOW_MODAL);
                            pbHierarchy.show();
                        }
                    }
                }
            }
        });

        exactMatchColumn.setCellFactory(column -> {
//            return new TableCell<ObservableObjectValue<Hyperlink>, Hyperlink>() {
            return new TableCell<SimpleStringProperty, Hyperlink>() {


                    @Override
                protected void updateItem(Hyperlink item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {

                        setText(null);
                        setGraphic(null);
      //                  setContextMenu(null);
                    } else {
                        ContextMenu delLink = new ContextMenu();
                        javafx.scene.control.MenuItem itemDel = new javafx.scene.control.MenuItem("remove");
                        itemDel.setOnAction(event -> {
                            Alert a = new Alert(Alert.AlertType.CONFIRMATION);
                            a.setTitle("Delete link");
                            a.setHeaderText("Delete link " + item.getText() + " ?");
                            a.initModality(Modality.WINDOW_MODAL);
                            a.initOwner(MainApp.primaryStage);
                            Optional<ButtonType> res = a.showAndWait();
                            if (res.get() == ButtonType.OK) {
                                currentNode.removeExactMatch(item.getText());
                                mainApp.getOntologyControler().modified(true);
                            }

                            //         mainApp.showCreateNodeOverview(false, currentNode, currentNode.isPO2Node());
                        });
                        itemDel.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                        delLink.getItems().add(itemDel);
                        setGraphic(item);
                        setContextMenu(delLink);
                    }
                }
            };
        });
        exactMatchColumn.setCellValueFactory(cellData -> {
            Hyperlink li = new Hyperlink(cellData.getValue().getValue());
            ObservableValue<Hyperlink> obs = new SimpleObjectProperty<>(li);
            li.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    MainApp.openBrowser(li.getText());
                }
            });
            return obs;
        });

        closeMatchColumn.setCellFactory(column -> {
//            return new TableCell<ObservableObjectValue<Hyperlink>, Hyperlink>() {
            return new TableCell<SimpleStringProperty, Hyperlink>() {


                @Override
                protected void updateItem(Hyperlink item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {

                        setText(null);
                        setGraphic(null);
                        //                  setContextMenu(null);
                    } else {
                        ContextMenu delLink = new ContextMenu();
                        javafx.scene.control.MenuItem itemDel = new javafx.scene.control.MenuItem("remove");
                        itemDel.setOnAction(event -> {
                            Alert a = new Alert(Alert.AlertType.CONFIRMATION);
                            a.setTitle("Delete link");
                            a.setHeaderText("Delete link " + item.getText() + " ?");
                            a.initModality(Modality.WINDOW_MODAL);
                            a.initOwner(MainApp.primaryStage);
                            Optional<ButtonType> res = a.showAndWait();
                            if (res.get() == ButtonType.OK) {
                                currentNode.removeCloseMatch(item.getText());
                                MainApp.getOntologyControler().modified(true);
                            }

                            //         mainApp.showCreateNodeOverview(false, currentNode, currentNode.isPO2Node());
                        });
                        itemDel.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                        delLink.getItems().add(itemDel);
                        setGraphic(item);
                        setContextMenu(delLink);
                    }
                }
            };
        });

        closeMatchColumn.setCellValueFactory(cellData -> {
            Hyperlink li = new Hyperlink(cellData.getValue().getValue());
            ObservableValue<Hyperlink> obs = new SimpleObjectProperty<>(li);
            li.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    MainApp.openBrowser(li.getText());
                }
            });
            return obs;
        });

        flagColumn.setCellValueFactory(cellData -> cellData.getValue().getFlag());
        flagColumn.setCellFactory(column -> {
            return new TableCell<VocabNodeWLang, ImageView>() {

                @Override
                protected void updateItem(ImageView item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        item.setPreserveRatio(true);
                        item.setFitWidth(flagColumn.getWidth() - 10);
                        setGraphic(item);
                    }
                }
            };
        });

//        labelColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        labelColumn.setCellFactory(TextAreaForLabelTableCell.forTableColumn());

        labelColumn.setCellValueFactory(cellData -> cellData.getValue().getVocabNode().getLabel(cellData.getValue().getLang()));
        labelColumn.setOnEditCommit(event -> {
            Boolean labelChanged = event.getRowValue().getVocabNode().setLabel(event.getRowValue().getLang(), event.getNewValue());
            if(labelChanged) {
                vocabTree.refresh();
                modified(true);
            }
        });

        synonymsColumn.setCellValueFactory(cellData -> cellData.getValue().getVocabNode().getAltLabel(cellData.getValue().getLang()));
        synonymsColumn.setOnEditCommit(event -> {
            event.getRowValue().getVocabNode().setAltLabel(event.getRowValue().getLang(), event.getNewValue(), false);
            modified(true);
        });
        synonymsColumn.setCellFactory(TextAreaTableCell.forTableColumn());

        descColumn.setCellValueFactory(cellData -> cellData.getValue().getVocabNode().getDefinition(cellData.getValue().getLang()));
        descColumn.setOnEditCommit(event -> {
            event.getRowValue().getVocabNode().setDefinition(event.getRowValue().getLang(), event.getNewValue(), false);
            modified(true);
        });
        descColumn.setCellFactory(TextAreaTableCell.forTableColumn());

        buttonCopy.setGraphic(new ImageView(UITools.getImage("resources/images/clipboard.png")));
        buttonCopy.setTooltip(new Tooltip("copy value to clipboard"));

        searchInfo = new searchInfo();

        searchInfo.messageProperty().addListener((ObservableValue<? extends String> observableValue, String oldValue, String newValue) -> {
            if (!newValue.equals("")) {
                org.json.JSONObject o = new org.json.JSONObject(newValue);
                org.json.JSONArray arrayText = o.optJSONArray("text");
                if (arrayText != null) {
                    for(Object t : arrayText) {
                        wordFrequency.add(t.toString());
                    }

                    FrequencyAnalyzer a = new FrequencyAnalyzer();
                    Dimension dim = new Dimension((int)imageCloud.getFitWidth(), (int)imageCloud.getFitHeight());
                    WordCloud wc = new WordCloud(dim, CollisionMode.RECTANGLE);
                    wc.setPadding(2);
                    wc.setBackgroundColor(new Color(40, 75, 99));
                    wc.setBackground(new RectangleBackground(dim));
                    wc.setColorPalette(new ColorPalette(Color.black, Color.white));
                    wc.setFontScalar(new LinearFontScalar(10, 40));
                    wc.build(a.load(wordFrequency));
                    imageCloud.setImage(SwingFXUtils.toFXImage(wc.getBufferedImage(), null));
                }
                org.json.JSONArray arrayImg = o.optJSONArray("img");
                if (arrayImg != null) {
                    for(Object t : arrayImg) {


                        try {
                            imageNode.setImage(new Image( HttpClients.createDefault().execute(new HttpGet(t.toString())).getEntity().getContent()));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (ClientProtocolException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }


            }
        });

    }

    public void refreshTree(){
        rebuildTree(false, false);
    }


    public void rebuildTree(boolean rebuildConstraint) {
        rebuildTree(rebuildConstraint, false);
    }
    public void rebuildTree(boolean rebuildConstraint, boolean resetState) {
        // Sauvegarde de l'état des nœuds ouverts
        Map<String, Boolean> expandedStates = new HashMap<>();
        if(!resetState) {
            RecursiveTreeItem.saveExpandedState(rootItem, expandedStates);
        }
        rootItem = new RecursiveTreeItem<>(currentOntology.getRootNode(), VocabConcept::getGraphic, VocabConcept::getSubNodeInScheme, this);
        // Restauration de l'état des nœuds ouverts
        RecursiveTreeItem.restoreExpandedState(rootItem, expandedStates);
        setTree(rootItem);
        if(rebuildConstraint) {
            currentOntology.rebuildConstraints();
        }
    }

    public BooleanProperty isOnMainScheme() {
        return isOnMainScheme;
    }

    public void setIsOnMainScheme(Boolean isOn) {
        isOnMainScheme.set(isOn);
    }

    public StringProperty getCurrentSchemeName() {
        return schemeName;
    }

    public void setCurrentSchemeName(String name) {
        schemeName.setValue(name);
    }


    public StringProperty getFileName() {
        return fileName;
    }
    public IntegerProperty getMajorVersion() { return majorVersion;}
    public IntegerProperty getMinorVersion() { return minorVersion;}
    public void saveOnto() {
        mainApp.unbindVersion();
        Task<Boolean> task = new Task<Boolean>() {

            @Override
            protected Boolean call() throws Exception {
                if (!JenaTools.saveModel(currentOntology)) {
                    return false;
                } else {
                    // on synchronise le fichier local avec le fichier distant
                    return CloudConnector.update(currentOntology);
                }
            }
        };
        task.setOnSucceeded(event1 -> {
            mainApp.bindVersion();
            if (task.getValue()) {
                MainApp.logger.debug("sauvegarde reussi");
                modified(false);
            } else {
                MainApp.logger.error("echec de la sauvegarde");
                Alert errorSave = new Alert(Alert.AlertType.ERROR);
                errorSave.initModality(Modality.WINDOW_MODAL);
                errorSave.initOwner(MainApp.primaryStage);
                errorSave.setTitle("Error");
                errorSave.setHeaderText("An error occured during save");
                errorSave.show();
            }
        });
        new Thread(task).start();

    }

    public void startOpenFile(String ontoName) {
        if(currentOntology != null) {
            modified(false);
        }
        Ontology onto = Ontologies.getOntology(ontoName);
        if (onto != null) {

            Task<Boolean> task = new Task<Boolean>() {

                @Override
                protected Boolean call() throws Exception {
                    return onto.load();
                }
            };

            task.setOnSucceeded(event1 -> {
                if (onto.isLoaded().getValue()) {
//                    Task<Boolean> worker = createWorkerVocabLoad(onto);
//                    worker.setOnSucceeded(event -> {
//                        setTree(rootItem);
                        fileName.bind(onto.getName());
                        majorVersion.bind(onto.getMajorVersion());
                        minorVersion.bind(onto.getMinorVersion());

                        currentOntology = onto;
                        showDeprecated.bind(currentOntology.showDeprecatedProperty());

                        currentOntology.setCurrentSkosScheme(currentOntology.getMainScheme());
                        MainApp.getOntologyControler().setIsOnMainScheme(true);

                        MainApp.updateConceptScheme();
                        rebuildTree(false, true);

                        try {
                            MutablePair<String, Date> versionDate = CloudConnector.getDavVersion(currentOntology);
                            if(versionDate.getLeft() != null) {
                                currentOntology.setCurrentVersionLoaded(versionDate.getLeft());
                            }
                            syncCloud.unbind();
                            syncCloud.bind(currentOntology.isSynchroCloud());
                            currentOntology.isSynchro();
                        } catch (URISyntaxException | IOException e) {
                            e.printStackTrace();
                        }
                        cleanNodeDetails();
//                    });
//                    new Thread(worker).start();
                } else {
                    Alert ontoNotAvailable = new Alert(Alert.AlertType.ERROR);
                    ontoNotAvailable.setGraphic(new ImageView(UITools.getImage("resources/images/error.png")));
                    ontoNotAvailable.setContentText("You don't have permission to load the ontology " + onto.getName().getValue());
                    ontoNotAvailable.initOwner(MainApp.primaryStage);
                    ontoNotAvailable.showAndWait();
                }
            });


            if (!onto.isLocal() && !onto.isPublic() && !CloudConnector.isInitUser()) {
                if (UITools.startLogin()) {
                    new Thread(task).start();
                }
            } else {
                new Thread(task).start();
            }
        }
    }

    public Ontology getCurrentOntology() {
        return currentOntology;
    }
    public BooleanProperty getCanEditProperty() {
        return canEdit;
    }
    public BooleanProperty getSyncCloudProperty() {
        return syncCloud;
    }


    public Boolean canEdit() {
        return canEdit.getValue();
    }
    public BooleanProperty getModifiedProperty() {
        return modified;
    }

    public void modified(Boolean mod) {
        modified.setValue(mod);
    }
    public Boolean isModified() {
        return modified.getValue();
    }

    public TreeItem<VocabConcept> getVocabTree() {
        return rootItem;
    }

    public Task<Boolean> createWorkerVocabLoad(Ontology onto) {
        return new Task<Boolean>() {

            @Override
            protected Boolean call() throws Exception {
//                Platform.runLater(new Runnable() {
//                    @Override
//                    public void run() {
//                        createVocab(onto);
//                    }
//                });

                return true;
            }

        };

    }

    public Boolean tryLock() {
        try {
            canEdit.set(currentOntology.lockMode());
            return true;
        } catch (NotLoginException e) {
            if(UITools.startLogin()) {
                return tryLock();
            }
            return false;
        } catch (AlreayLockException e) {
            Alert alreadyLock = new Alert(Alert.AlertType.INFORMATION);
            alreadyLock.setHeaderText("The ontology " + currentOntology.getName().get() + " is already locked by " + e.getLockName() + " " + e.getDate());
            alreadyLock.initOwner(MainApp.primaryStage);
            alreadyLock.showAndWait();
            MainApp.logger.info("Ontology already lock");
        } catch (LocalLockException e) {
            Alert alreadyLock = new Alert(Alert.AlertType.INFORMATION);
            alreadyLock.setHeaderText("The ontology " + currentOntology.getName().get() + " is local. Edition is not allowed");
            alreadyLock.initOwner(MainApp.primaryStage);
            alreadyLock.showAndWait();
            MainApp.logger.info("can't lock local onto");
        } catch (IOException | URISyntaxException e) {
            MainApp.logger.error(e.getClass() + ":" + e.getMessage(), e);
            Alert exception = new Alert(Alert.AlertType.ERROR);
            exception.setTitle("Exception Dialog");
            exception.setContentText(e.getMessage());
            exception.showAndWait();
        } catch (CantWriteException e) {
            Alert alreadyLock = new Alert(Alert.AlertType.INFORMATION);
            alreadyLock.setHeaderText("You have insufficient rights to access ontology " + currentOntology.getName().get());
            alreadyLock.initOwner(MainApp.primaryStage);
            alreadyLock.showAndWait();
            MainApp.logger.info("insufficient right");
        }
        return false;
    }



    public Boolean enterEditionMode() {
//        try {
            if (currentOntology != null && !currentOntology.isLocked()) {
                return tryLock();
            }
//        } catch (AlreayLockException e) {
//            Alert alreadyLock = new Alert(Alert.AlertType.INFORMATION);
//            alreadyLock.setHeaderText("The ontology " + currentOntology.getName().get() + " is already locked by " + e.getLockName() + " " + e.getDate());
//            alreadyLock.initOwner(MainApp.primaryStage);
//            alreadyLock.showAndWait();
//            MainApp.logger.info("Ontology already lock");
//        } catch (LocalLockException e) {
//            Alert alreadyLock = new Alert(Alert.AlertType.INFORMATION);
//            alreadyLock.setHeaderText("The ontology " + currentOntology.getName().get() + " is local. Edition is not allowed");
//            alreadyLock.initOwner(MainApp.primaryStage);
//            alreadyLock.showAndWait();
//            MainApp.logger.info("can't lock local onto");
//        } catch (IOException | URISyntaxException e) {
//            MainApp.logger.error(e.getClass() + ":" + e.getMessage(), e);
//            Alert exception = new Alert(Alert.AlertType.ERROR);
//            exception.setTitle("Exception Dialog");
//            exception.setContentText(e.getMessage());
//            exception.showAndWait();
//        } catch (CantWriteException e) {
//            Alert alreadyLock = new Alert(Alert.AlertType.INFORMATION);
//            alreadyLock.setHeaderText("You have insufficient rights to access ontology " + currentOntology.getName().get());
//            alreadyLock.initOwner(MainApp.primaryStage);
//            alreadyLock.showAndWait();
//            MainApp.logger.info("insufficient right");
//        }
        return false;
    }
    /**
     * @param tree
     */
    public void setTree(TreeItem<VocabConcept> tree) {

        // Add observable list data to the table
        vocabTree.setRoot(tree);
        vocabTree.setCellFactory(factory -> new VocabTreeCell(mainApp));
    }

    public void setStatusBox(StatusBox statusBox) {
        this.statusBox.setSpacing(statusBox.getSpacing());
        this.statusBox.getChildren().clear();
        this.statusBox.getChildren().addAll(statusBox.getListNodes());
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
//        labelColumn.editableProperty().bind(mainApp.getEditProperty());
//        descColumn.editableProperty().bind(mainApp.getEditProperty());
    }

    public TreeItem<VocabConcept> getRootItem() {
        return rootItem;
    }

    /**
     * @param node
     */
    public void openNode(VocabConcept node) {
        vocabTree.getSelectionModel().select(searchNode(vocabTree.getRoot(), node));
        vocabTree.scrollTo(vocabTree.getSelectionModel().getSelectedIndex());
    }

    private TreeItem<VocabConcept> searchNode(TreeItem<VocabConcept> item, VocabConcept node) {

        if (item.getValue().equals(node)) {
            return item;
        } else {
            for (TreeItem<VocabConcept> n : item.getChildren()) {
                TreeItem<VocabConcept> maybe = searchNode(n, node);
                if (maybe != null) {
                    return maybe;
                }
            }
        }
        return null;
    }

    private void cleanNodeDetails() {
        if(currentNode != null) { // old node
            currentNode.getListSkosScheme().removeListener(listChangeListener);
            currentNode.scopeNoteProperty().removeListener(changeListener);
            currentNode.editorialNoteProperty().removeListener(changeListener);
            currentNode.noteProperty().removeListener(changeListener);
            currentNode.scopeNoteProperty().unbind();
            currentNode.editorialNoteProperty().unbind();
            currentNode.noteProperty().unbind();
        }
        currentNode = null;
        tableView.getItems().clear();
        if (searchInfo != null) {
            try {
                searchInfo.cancel();
                searchInfo.reset();
            } catch (IllegalStateException ise) {
                ise.printStackTrace();
            }
        }
        wordFrequency = new ArrayList<>();
        link.setText("");
        tableExact.getItems().clear();
        tableClose.getItems().clear();
        tableViewConceptScheme.setItems(null);
        buttonCopy.setVisible(false);
        labelChemin.setVisible(false);
        textScopeNote.textProperty().removeListener(changeListener);
        textEditorialNote.textProperty().removeListener(changeListener);
        textNote.textProperty().removeListener(changeListener);
        textScopeNote.textProperty().unbind();
        textEditorialNote.textProperty().unbind();
        textNote.textProperty().unbind();
        textScopeNote.setText("");
        textEditorialNote.setText("");
        textNote.setText("");
    }
    /**
     * @param treeNode
     */
    private void showNodeDetails(TreeItem<VocabConcept> treeNode) {
        if(addDim != null) {
            addDim.disableProperty().unbind();
            addDim.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
        }
        if(currentNode != null) { // old node
            currentNode.getListSkosScheme().removeListener(listChangeListener);
            currentNode.scopeNoteProperty().removeListener(changeListener);
            currentNode.editorialNoteProperty().removeListener(changeListener);
            currentNode.noteProperty().removeListener(changeListener);
            currentNode.scopeNoteProperty().unbind();
            currentNode.editorialNoteProperty().unbind();
            currentNode.noteProperty().unbind();
        }
        currentNode = treeNode.getValue();

        textScopeNote.textProperty().unbind();
        textEditorialNote.textProperty().unbind();
        textNote.textProperty().unbind();

        textScopeNote.editableProperty().bind(getCanEditProperty());
        textEditorialNote.editableProperty().bind(getCanEditProperty());
        textNote.editableProperty().bind(getCanEditProperty());

        conceptSchemeColumn.setCellFactory(skosSchemeStringTableColumn -> new XCellScheme(currentNode));
        tableView.getItems().clear();
        imageCloud.setImage(null);
        imageNode.setImage(null);
        boxDimension.getChildren().retainAll(boxDimension.getChildren().get(0));
        if(treeNode.getValue().isPO2Node().equalsIgnoreCase("attribute")) {
            for(ObservableStringValue val : treeNode.getValue().getListDimension()) {
                Label l = new Label(val.get());
                Button delDim = new Button(null, new ImageView(UITools.getImage("resources/images/del_16.png")));
                delDim.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));

                l.setGraphic(delDim);
                delDim.setOnMouseClicked(mouseEvent -> {
                    treeNode.getValue().removeDimension(val.get());
                    mainApp.getOntologyControler().modified(true);
                    boxDimension.getChildren().remove(l);
                });
                boxDimension.getChildren().add(l);
            }
            boxDimension.setVisible(true);
        } else {
            boxDimension.setVisible(false);
        }


        if (searchInfo != null) {
            try {
                searchInfo.cancel();
                searchInfo.reset();
            } catch (IllegalStateException ise) {
                ise.printStackTrace();
            }
        }
        wordFrequency = new ArrayList<>();

        if (currentNode != null) {

            link.setText(currentNode.getURI());
            link.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    MainApp.openBrowser(link.getText());
                }
            });
            currentNode.getListSkosScheme().addListener(listChangeListener);

            buttonCopy.setVisible(true);
            buttonCopy.setText(currentNode.toString());

            buttonCopy.setOnAction(event -> {
                ClipboardContent content = new ClipboardContent();
                content.putString(currentNode.getLabel("en").get());
                Clipboard.getSystemClipboard().setContent(content);
            });
            labelChemin.setVisible(true);
            StringBuilder sb = new StringBuilder();
            TreeItem<VocabConcept> tempNode = treeNode.getParent();
            while (tempNode != null) {
                sb.insert(0, " > "+tempNode.getValue().getLabel("en").get() );
                tempNode = tempNode.getParent();
                if(tempNode != null) {
                    tempNode = tempNode.getParent() != null ? tempNode : null;
                }
            }
            labelChemin.setText(sb.toString());


            ObservableList<VocabNodeWLang> print = FXCollections.observableArrayList();

            for(String l : VocabConcept.listLang) {
                VocabNodeWLang nodeW = new VocabNodeWLang(l, currentNode.getLabel(l).getValue(), UITools.getFlag(l), currentNode);
                print.add(nodeW);
            }

            tableView.setItems(print);

            textScopeNote.setText(currentNode.getScopeNote());
            textEditorialNote.setText(currentNode.getEditorialNote());
            textNote.setText(currentNode.getNote());

            currentNode.scopeNoteProperty().bind(textScopeNote.textProperty());
            currentNode.editorialNoteProperty().bind(textEditorialNote.textProperty());
            currentNode.noteProperty().bind(textNote.textProperty());
            currentNode.scopeNoteProperty().addListener(changeListener);
            currentNode.editorialNoteProperty().addListener(changeListener);
            currentNode.noteProperty().addListener(changeListener);

            tableExact.setItems(currentNode.getListExactMatch());
            tableClose.setItems(currentNode.getListCloseMatch());

            tableViewConceptScheme.setItems(currentNode.getListSkosScheme());

            ContextMenu addLink = new ContextMenu();
            javafx.scene.control.MenuItem itemAdd = new javafx.scene.control.MenuItem("add");
            itemAdd.setOnAction(event -> mainApp.showCreateNodeOverview(false, currentNode, currentNode.isPO2Node()));
            itemAdd.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
            addLink.getItems().add(itemAdd);

            labelColumn.editableProperty().unbind();
            descColumn.editableProperty().unbind();
            synonymsColumn.editableProperty().unbind();
            if(currentOntology.getRootNode().getSubNode().contains(currentNode)) {
                labelColumn.setEditable(false);
                descColumn.setEditable(false);
                synonymsColumn.setEditable(false);
                tableExact.setContextMenu(null);
            } else {
                labelColumn.editableProperty().bind(mainApp.getEditProperty());
                descColumn.editableProperty().bind(mainApp.getEditProperty());
                synonymsColumn.editableProperty().bind(mainApp.getEditProperty());

                tableExact.setContextMenu(addLink);
            }

            ArrayList<String> listToSearch = new ArrayList<>();
            for (ObservableStringValue s : currentNode.getListExactMatch()) {
                listToSearch.add(s.get());
            }

            if (searchInfo != null) {
                searchInfo.setUris(listToSearch);
                searchInfo.start();
            }
        }
    }

//    public VocabNode getNodeByURI(String url) {
//        return root.getNodeByURI(url);
//    }

    private JSONArray getHierarchyJSON(VocabConcept node) {
        JSONArray hierarchy = new JSONArray();
        for(VocabConcept child : node.getSubNode()) {
            JSONArray arr = new JSONArray();
            arr.put(node.getName());
            arr.put(child.getName());
            hierarchy.put(arr);
            hierarchy.put(getHierarchyJSON(child));
        }
        return hierarchy;
    }

    private JSONArray getVocabJSON(VocabConcept node) {
        JSONArray global = new JSONArray();
        JSONArray vocabArray = new JSONArray();
        vocabArray.put(node.getName());
        for(Entry<SimpleStringProperty, SimpleStringProperty> entry : node.getLabel().entrySet()) {
            String lang = entry.getKey().getValue();
            String label = node.getLabel(lang).getValue();
            String def = node.getDefinition(lang).getValue();
            if(label != null && !label.isEmpty()) {
                vocabArray.put(label+"@label-"+lang);
            }
            if(def != null && !def.isEmpty()) {
                vocabArray.put(def+"@def-"+lang);
            }

        }
        global.put(vocabArray);
        for(VocabConcept child : node.getSubNode()) {
            global.put(getVocabJSON(child));
        }
        return global;
    }
    public JSONObject getHierarchyVocabJSON() {
        JSONObject retour = new JSONObject();
        retour.put("hierarchy", getHierarchyJSON(rootItem.getValue()));
        retour.put("vocabulary", getVocabJSON(rootItem.getValue()));

        return retour;
    }

    public void exportOnto() throws IOException {
        File fTemp = File.createTempFile("tempZipFile","zip");
        ZipOutputStream zipFile = new ZipOutputStream(new FileOutputStream(fTemp));

        for (VocabConcept s : vocabTree.getRoot().getValue().getSubNode()) {
            LinkedHashMap<String, LinkedHashMap<String, String>> exp = s.exportNode();
            LinkedList<Pair<String, String>> hierarchy = s.exportHierarchy();
            ZipEntry entry = new ZipEntry(s.getLabel("en").get() + ".csv");
            zipFile.putNextEntry(entry);
            CharArrayWriter caw = new CharArrayWriter();
            CharArrayWriter cawH = new CharArrayWriter();
            CSVPrinter printer = new CSVPrinter(caw, CSVFormat.DEFAULT.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL));
            exp.values().stream().findFirst().ifPresent(slh -> {
                slh.keySet().forEach(v -> {
                    try {
                        printer.print(v);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            });
            printer.println();
            exp.forEach((id, m) -> {
                m.forEach((s1, s2) -> {
                    try {
                        printer.print(s2);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                try {
                    printer.println();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            caw.close();
            IOUtils.write(caw.toString(), zipFile, StandardCharsets.UTF_8);

            CSVPrinter printerH = new CSVPrinter(cawH, CSVFormat.DEFAULT.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL));
            printerH.printRecord("father","son");
            hierarchy.forEach(p -> {
                try {
                    printerH.printRecord(p.getKey(), p.getValue());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            cawH.close();
            ZipEntry entryH = new ZipEntry(s.getLabel("en").get() + "_hierarchy.csv");
            zipFile.putNextEntry(entryH);
            IOUtils.write(cawH.toString(), zipFile, StandardCharsets.UTF_8);
        }

        // export skos concept scheme
        ZipEntry entry = new ZipEntry("concept_scheme.csv");
        zipFile.putNextEntry(entry);
        CharArrayWriter caw = new CharArrayWriter();
        CSVPrinter printer = new CSVPrinter(caw, CSVFormat.DEFAULT.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL));
        printer.printRecord("uri", "label@en","description@en");
        for(SkosScheme ss : this.getCurrentOntology().getlistConceptScheme()) {
            printer.printRecord(ss.getUri(), ss.getName(), ss.getComment());
        }
        caw.close();
        IOUtils.write(caw.toString(), zipFile, StandardCharsets.UTF_8);

        zipFile.close();

        Platform.runLater(() -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Export Ontology " + currentOntology.getName().get());
            fileChooser.setInitialFileName(currentOntology.getName().get()+".zip");
            File fileExport = fileChooser.showSaveDialog(MainApp.primaryStage);
            if (fileExport != null) {
                try {
                    FileInputStream fin = new FileInputStream(fTemp);
                    FileOutputStream fout = new FileOutputStream(fileExport);
                    IOUtils.copy(fin, fout);
                    fout.close();
                } catch (IOException ex) {
                    MainApp.logger.error(ex.getMessage());
                }
            }
        });
    }

    public void importOnto() {

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Import Ontology " + currentOntology.getName().get());
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("zip files (*.zip)", "*.zip"));
            File fileImport = fileChooser.showOpenDialog(MainApp.primaryStage);
            if (fileImport != null) {
                try (ZipFile archive = new ZipFile(fileImport)) {
                    Path tempPath = Files.createTempDirectory("extractZip");
                    for (Enumeration<ZipArchiveEntry> ite = archive.getEntriesInPhysicalOrder(); ite.hasMoreElements(); ) {
                        ZipArchiveEntry entry = ite.nextElement();
                        File curfile = new File(tempPath.toFile(), entry.getName());
                        if (!entry.isDirectory()) {
                            File parent = curfile.getParentFile();
                            if (!parent.exists()) {
                                parent.mkdirs();
                            }
                            try {
                                OutputStream out2 = new FileOutputStream(curfile);
                                org.apache.commons.compress.utils.IOUtils.copy(archive.getInputStream(entry), out2);
                                out2.close();
                            } catch (FileNotFoundException fnf) {
                                MainApp.logger.error("Failed unzip file " + curfile.getAbsolutePath());
                            }
                        } else {
                            curfile.mkdirs();
                        }
                    }
                    MainApp.logger.debug("zip import extracted to : " + tempPath.toString());
                    Tools.addProgress(ProgressPO2.IMPORT, "import in progress");

                    new Thread( () -> {

                        HashMap<String, ArrayList<String>> rapport = new HashMap<>();
                        try {
                            rapport = parseImportFiles(tempPath.toFile());
                        } catch (IOException e) {
                            rapport.put("error", new ArrayList<>());
                            rapport.get("error").add(e.getLocalizedMessage());
                            MainApp.logger.error(e.getLocalizedMessage());
                        }
                        Tools.delProgress(ProgressPO2.IMPORT);

                        if(rapport.get("error").isEmpty()) {
                            // import ok
                            if(rapport.get("warning").isEmpty()) {
                                Platform.runLater(() -> {
                                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                    alert.initOwner(MainApp.primaryStage);
                                    alert.setResizable(true);
                                    alert.setTitle("Import done");
                                    alert.setHeaderText("Import done with success");
                                    alert.showAndWait();
                                });
                            } else {
                                HashMap<String, ArrayList<String>> finalRapport = rapport;
                                Platform.runLater(() -> {
                                    Alert alert = new Alert(Alert.AlertType.WARNING);
                                    alert.getDialogPane().setMinWidth(200.0);
                                    alert.initOwner(MainApp.primaryStage);
                                    alert.setTitle("Import done");
                                    alert.setResizable(true);
                                    alert.setHeaderText("Import done but with warning");
                                    alert.setContentText(finalRapport.get("warning").stream().map(s -> "* " + s).collect(Collectors.joining("\n")));
                                    alert.showAndWait();
                                });
                            }

                        } else {
                            HashMap<String, ArrayList<String>> finalRapport1 = rapport;
                            Platform.runLater(() -> {
                                Alert alert = new Alert(Alert.AlertType.ERROR);
                                alert.getDialogPane().setPrefSize(500.0, 300);
                                alert.initOwner(MainApp.primaryStage);
                                alert.setTitle("Import failed");
                                alert.setResizable(true);
                                alert.setHeaderText("Please check your import files");
                                alert.setContentText(finalRapport1.get("error").stream().map(s -> "* " + s).collect(Collectors.joining("\n")));
                                alert.showAndWait();
                            });
                        }
                    }).start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    }

    private HashMap<String, ArrayList<String>> parseImportFiles(File folder) throws IOException {
        HashMap<String, ArrayList<String>> rapport = new HashMap<>();
        rapport.put("error", new ArrayList<>());
        rapport.put("warning", new ArrayList<>());
        HashMap<String, VocabConcept> newSubRootNode = new HashMap<>();
        HashMap<String, ArrayList<VocabConcept>> oldFlatedNode = new HashMap<>();

        // on flated les anciens concepts
        for(VocabConcept conceptPO : currentOntology.getRootNode().getSubNode()) {
            if (!oldFlatedNode.containsKey(conceptPO.isPO2Node())) {
                oldFlatedNode.put(conceptPO.isPO2Node(), new ArrayList<>());
            }
            oldFlatedNode.get(conceptPO.isPO2Node()).addAll(conceptPO.flattened().collect(Collectors.toList()));
        }

        Collection<File> l = FileUtils.listFiles(folder, new String[] {"csv"}, true );
        l.removeIf(file -> file.getName().toLowerCase().contains("_hierarchy.csv"));

        // list of all key (label en) to check of multiple creation
        HashMap<String, String> key = new HashMap<>();
        for(File f : l) {
            MainApp.logger.debug("parse file " + f.getName());
            Tools.updateProgress(ProgressPO2.IMPORT, "Parsing file : " + f.getName());
            // on cherche le fichier de hierarchie correspondant
            File fh = new File(f.getAbsolutePath().replaceAll(".csv","_hierarchy.csv" ));
            if(!fh.exists()) {
                if("concept_scheme.csv".equalsIgnoreCase(f.getName())) {
                    BOMInputStream bis = new BOMInputStream(new FileInputStream(f));
                    CSVParser parser = CSVFormat.DEFAULT.withDelimiter(';').withHeader().parse(new InputStreamReader(bis, StandardCharsets.UTF_8));
                    Integer uriIndex = parser.getHeaderMap().getOrDefault("uri", 0);
                    Integer labelIndex = parser.getHeaderMap().getOrDefault("label@en", 1);
                    Integer descIndex = parser.getHeaderMap().getOrDefault("description@en", 2);
                    for (final CSVRecord record : parser) {
                        SkosScheme sc = currentOntology.getSkosSchemeByURI(record.get(uriIndex));
                        if(sc == null) {
                            sc = currentOntology.createSkosSchemeByURI(record.get(uriIndex));
                        }
                        sc.setName(record.get(labelIndex));
                        sc.setComment(record.get(descIndex));
                    }
                } else {
                    // error missing hierarchical file
                    MainApp.logger.error("missing hirarchical file for " + f.getName());
                    rapport.get("error").add("Missing hierarchical file for " + f.getName());
                }
            } else {
                BOMInputStream bis = new BOMInputStream(new FileInputStream(fh));
                CSVParser parserH = CSVFormat.DEFAULT.withDelimiter(';').withHeader().parse(new InputStreamReader(bis, StandardCharsets.UTF_8));
                Integer father = parserH.getHeaderMap().getOrDefault("father", 0);
                Integer son = parserH.getHeaderMap().getOrDefault("son", 1);
                Integer headerSize = parserH.getHeaderMap().size();
                if(headerSize != 2) {
                    rapport.get("error").add("In file " + fh.getName() + " : Bad header size");
                } else {
                    ArrayList<Pair<String, String>> mapHierarchy = new ArrayList();
                    HashSet<String> setFather = new HashSet();
                    HashSet<String> setSon = new HashSet();
                    for (final CSVRecord recordH : parserH) {
                        if(recordH.size() == headerSize) {
                            Pair<String, String> p = new Pair<>(Tools.normalize(recordH.get(father).trim().toLowerCase()), Tools.normalize(recordH.get(son).trim().toLowerCase()));
                            mapHierarchy.add(p);
                            setFather.add(p.getKey());
                            setSon.add(p.getValue());
                        } else {
                            rapport.get("error").add("In file " + fh.getName() + " line : "+(recordH.getRecordNumber()+1)+" has " +recordH.size()+" elements. Need "+ headerSize);
                        }
                    }
                    setFather.removeAll(setSon);
                    if(setFather.size() > 1) {
                        rapport.get("error").add("Multiple root found in hierarchical file " + fh.getName() + " : " + setFather.stream().collect(Collectors.joining(";")));
                    } if(setFather.isEmpty()) {
                        rapport.get("error").add("No root found in hierarchical file " + fh.getName());
                    }


                    String type = setFather.stream().findFirst().orElse("bad_root_node").toLowerCase();

                    BOMInputStream bis2 = new BOMInputStream(new FileInputStream(f));
                    CSVParser parser = CSVFormat.DEFAULT.withDelimiter(';').withHeader().parse(new InputStreamReader(bis2, StandardCharsets.UTF_8));
    //            LinkedHashMap<String, LinkedHashMap<String, String>> hash = new LinkedHashMap<>();
                    HashMap<String, VocabConcept> allNode = new HashMap<>();
                    Integer headerSizeDescription = parser.getHeaderMap().size();
                    double nbRecord = (double) parser.getRecords().size();
                    bis2 = new BOMInputStream(new FileInputStream(f));
                    parser = CSVFormat.DEFAULT.withDelimiter(';').withHeader().parse(new InputStreamReader(bis2, StandardCharsets.UTF_8));
                    try {
                        for (final CSVRecord record : parser) {
                            Tools.updateProgress(ProgressPO2.IMPORT, (double) (record.getRecordNumber()/nbRecord));
                            if (record.isConsistent()) {
                                String k = Tools.normalize(record.get("label@en").trim().toLowerCase());
                                if (key.containsKey(k)) {
                                    // error, multiple déclaration
                                    rapport.get("error").add("Multiple declaration for concept " + k + " (" + key.get(k) + " -- " +f.getName() + ")");
                                } else {
                                    key.put(k, f.getName());

                                }
                                VocabConcept vn = new VocabConcept(currentOntology);
                                vn.setPO2Node(type);
                                allNode.put(k, vn);

                                for (String head : parser.getHeaderMap().keySet()) {
                                    String value = record.get(head);
                                    if ("uri".equalsIgnoreCase(head)) {
                                        if(!value.isEmpty()) {
                                            vn.setURI(value);
                                        }
                                    }
                                    if (head.toLowerCase().startsWith("label@")) {
                                        String lang = head.toLowerCase().replaceAll("label@", "");
                                        vn.setLabel(lang, value, true);
                                    }
                                    if (head.toLowerCase().startsWith("altlabel@")) {
                                        String lang = head.toLowerCase().replaceAll("altlabel@", "");
                                        String[] listVal = value.split("::");
                                        Arrays.stream(listVal).forEach(s -> vn.setAltLabel(lang, s, true));
                                    }
                                    // old definition.
                                    if (head.toLowerCase().startsWith("description@")) {
                                        String lang = head.toLowerCase().replaceAll("description@", "");
                                        vn.setDefinition(lang, value, false);
                                    }
                                    if (head.toLowerCase().startsWith("definition@")) {
                                        String lang = head.toLowerCase().replaceAll("definition@", "");
                                        vn.setDefinition(lang, value, false);
                                    }
                                    if ("scopenote".equalsIgnoreCase(head)) {
                                        vn.setScopeNote(value);
                                    }
                                    if ("editorialnote".equalsIgnoreCase(head)) {
                                        vn.setEditorialNote(value);
                                    }
                                    if ("note".equalsIgnoreCase(head)) {
                                        vn.setNote(value);
                                    }
                                    if (head.toLowerCase().startsWith("exactmatch")) {
                                        vn.setExactMatch(value);
                                    }
                                    if (head.toLowerCase().startsWith("closematch")) {
                                        vn.setClosetMatch(value);
                                    }
                                    if ("attribute".equalsIgnoreCase(type)) {
                                        if (head.toLowerCase().startsWith("dimension")) {
                                            String[] dims = Arrays.stream(value.split("\\r?\\n?\\::")).map(String::trim).filter(s -> !s.isEmpty()).toArray(String[]::new);
                                            Arrays.stream(dims).forEach(s -> {
                                                if (!Tools.checkDimension(s)) {
                                                    rapport.get("warning").add(" Bad dimension " + s + " for attribute " + k);
                                                }
                                                vn.addDimension(s);
                                            });
                                        }
                                    }

                                    if ("conceptScheme".equalsIgnoreCase(head)) {
                                        String[] listScheme = value.split("::");
                                        Arrays.stream(listScheme).forEach(sString -> {
                                            SkosScheme sc = currentOntology.getSkosSchemeByURI(sString);
                                            if(sc == null) {
                                                sc = currentOntology.createSkosSchemeByURI(sString);
                                            }
                                            vn.addSkosScheme(sc);
                                        });
                                    }
                                }
                                // new VocabConcept done. Removing it from oldList if exist.
                                ArrayList<VocabConcept> oldVocab = oldFlatedNode.get(type);
                                if(oldVocab != null) {
                                    oldVocab.removeIf(vocabConcept -> vn.getURI().equalsIgnoreCase(vocabConcept.getURI()));
                                }

                            } else {
                                rapport.get("error").add("In file " + f.getName() + " line : " + (record.getRecordNumber() + 1) + " has " + record.size() + " elements. Need " + headerSizeDescription);
                            }
                        }
                    } catch (IllegalStateException e) {
                        rapport.get("error").add("An error occured in file " + f.getName() + " : " + e.getLocalizedMessage());
                    }

                    if(rapport.get("error").isEmpty()) {
                        mapHierarchy.stream().forEach(p -> {
                            VocabConcept fa = allNode.get(p.getKey());
                            VocabConcept so = allNode.get(p.getValue());
                            if(fa == null) {
                                rapport.get("error").add("Concept " + p.getKey()+" in hierarchy but not in description file");
                            }
                            if(!p.getValue().isEmpty() && so == null) {
                                rapport.get("error").add("Concept " + p.getValue()+" in hierarchy but not in description file");
                            }
                            if(fa != null && so != null) {
                                fa.addChildren(so);
                                so.addFather(fa);
                            }
                        });

                        if(rapport.get("error").isEmpty()) {
                            VocabConcept subRootPO = allNode.get(type);
                            // checking for cycle in hierarchy
                            LinkedList<String> cycle = checkCycle(subRootPO);
                            if (!cycle.isEmpty()) {
                                rapport.get("error").add("There is a cycle in your hierarchy file " + type + " : " + cycle.stream().collect(Collectors.joining(" -> ")));
                            }
                            newSubRootNode.put(type, subRootPO);
                            allNode.clear();
                        }
                    }
                }
            }
        }
        Tools.updateProgress(ProgressPO2.IMPORT, "Import in progress", 0.0);
        if(rapport.get("error").isEmpty()) {
            // on fait l'import réel
            Double progressCompteur = 0.0;
            for(VocabConcept conceptPO : currentOntology.getRootNode().getSubNode()) {
                Tools.updateProgress(ProgressPO2.IMPORT, (double) (progressCompteur++/(double)currentOntology.getRootNode().getSubNode().size()));
                VocabConcept n = newSubRootNode.get(conceptPO.isPO2Node());
                if(n == null) {
                    rapport.get("warning").add(" Unable to find new PO² concept " + conceptPO.isPO2Node());
                } else {
                    for(VocabConcept sn : conceptPO.getSubNode()) {
                        sn.getFathers().clear(); // on enleve le root des peres
                    }
                    conceptPO.getSubNode().clear(); // on enleve tout les anciens fils
                    for(VocabConcept sn : n.getSubNode()) { // on ajoute tous les nouveau fils
                        sn.getFathers().clear();
                        sn.addFather(conceptPO);
                        conceptPO.addChildren(sn);
                    }

                    // on rajoute les anciens concept non importe
                    oldFlatedNode.get(conceptPO.isPO2Node()).forEach(vocabConcept -> {
                        if(!vocabConcept.getDeprecated()) {
                            // show warning only if node is not already deprecated
                            rapport.get("warning").add(" Concept " + vocabConcept.getName() + " is now deprecated");
                        }
                        vocabConcept.setDeprecated(true);
                        vocabConcept.getFathers().clear();
                        vocabConcept.getSubNode().clear();
                        vocabConcept.addFather(conceptPO);
                        conceptPO.addChildren(vocabConcept);
                    });
                }
            }
            Platform.runLater(() -> {
                MainApp.getOntologyControler().modified(true);
            });
        }
        return rapport;
    }

    public void createSkosScheme(String sch) {
        SkosScheme newSch = currentOntology.createSkosScheme(sch);
        currentOntology.getRootNode().addSkosScheme(newSch);
        currentOntology.getRootNode().getSubNode().forEach(vocabNode -> {
            vocabNode.addSkosScheme(newSch);
        });
        MainApp.getOntologyControler().modified(true);
    }


    public LinkedList<String> checkCycle(VocabConcept v) {
        LinkedList<String> cycle = new LinkedList<>();
        v.setBeingVisited(true);
        cycle.add(v.getLabel("en").getValue());

        for (VocabConcept neighbor : v.getSubNode()) {
            if (neighbor.isBeingVisited()) {
                // backward edge exists
                cycle.add(neighbor.getLabel("en").getValue());
                return cycle;
            } else if (!neighbor.isVisited()) {
                LinkedList<String> temp = checkCycle(neighbor);
                if (!temp.isEmpty()) {
                    cycle.addAll(temp);
                    return cycle;
                }
            }
        }

        v.setBeingVisited(false);
        v.setVisited(true);
        cycle.remove(v.getLabel("en").getValue());
        return cycle;
    }
}
