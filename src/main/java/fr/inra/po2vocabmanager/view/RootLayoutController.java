/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.view;

import fr.inra.po2vocabmanager.MainApp;
import fr.inra.po2vocabmanager.utils.UITools;
import fr.inrae.po2engine.exception.AlreadyExistException;
import fr.inrae.po2engine.externalTools.CloudConnector;
import fr.inrae.po2engine.model.Datas;
import fr.inrae.po2engine.model.Ontologies;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.dataModel.ProjectFile;
import fr.inrae.po2engine.utils.PO2Properties;
import fr.inrae.po2engine.utils.Tools;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.util.Pair;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.function.UnaryOperator;

;

/**
 * @author stephane
 */
public class RootLayoutController {
    MainApp mainApp;

    @FXML
    MenuItem itemSeach;
    @FXML
    MenuItem itemSave;
    @FXML
    MenuItem itemEdit;
    @FXML
    MenuItem itemOpen;
    @FXML
    MenuItem itemPublish;
    @FXML
    MenuItem itemSHACLValidation;
    @FXML
    MenuItem itemSem;
    @FXML
    MenuItem itemReadMode;
    @FXML
    Menu itemImportExport;
    @FXML
    MenuItem itemImportOnto;
    @FXML
    MenuItem itemExportOnto;
    @FXML
    MenuItem switchMode;
    @FXML
    Menu view;
    @FXML
    Menu conceptScheme;
    @FXML
    CheckMenuItem showDeprecated;
    @FXML
    MenuItem addConceptScheme;
    @FXML
    MenuItem itemNewProject;
    @FXML
    MenuItem itemNewOntology;


    @FXML
    private void initialize() {
        itemSeach.setDisable(true);
        itemSave.setDisable(true);
        itemEdit.setDisable(true);
        itemReadMode.setDisable(true);
        itemOpen.setDisable(true);
        itemPublish.setDisable(true);
        itemPublish.setVisible(true);
        itemSHACLValidation.setDisable(true);
        itemSHACLValidation.setVisible(true);
        itemImportExport.setVisible(true);
        itemExportOnto.setDisable(true);
        itemImportOnto.setDisable(true);
        view.setDisable(true);
        addConceptScheme.setDisable(true);
        itemNewProject.setVisible(true);
        itemNewProject.setDisable(true);
        itemNewOntology.setVisible(true);
        itemNewOntology.setDisable(true);
        showDeprecated.setDisable(false);
        showDeprecated.setSelected(false);
    }

    /**
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        BooleanBinding bindingOnWrite = (mainApp.onOntologyViewProperty().or(mainApp.onDataViewProperty())).and(mainApp.getEditProperty().not());
        BooleanBinding bindingOnRead = (mainApp.onOntologyViewProperty().or(mainApp.onDataViewProperty())).and(mainApp.getEditProperty());

        itemEdit.disableProperty().bind(bindingOnWrite.not());
        itemReadMode.disableProperty().bind(bindingOnRead.not());
//        itemEdit.setDisable(false);
        itemSave.disableProperty().bind(mainApp.getModifiedProperty().not());
//        itemSave.disableProperty().bind(mainApp.getEditProperty().not());
        itemSeach.disableProperty().bind(Bindings.when(mainApp.fileNameProperty().isEqualTo("")).then(true).otherwise(false));
        itemOpen.disableProperty().bind(Bindings.not(mainApp.onOntologyViewProperty().or(mainApp.onDataViewProperty())));

        itemPublish.textProperty().bind(Bindings.when(mainApp.onOntologyViewProperty()).then("Semantize & Publish ontology").otherwise("Semantize & Publish data"));
        itemPublish.disableProperty().bind(mainApp.getEditProperty().not());

        itemSHACLValidation.disableProperty().bind(mainApp.onDataViewProperty().not());

//        itemImportExport.visibleProperty().bind(mainApp.onOntologyViewProperty());
        itemExportOnto.disableProperty().bind(mainApp.onOntologyViewProperty().not());
        itemImportOnto.disableProperty().bind(mainApp.onOntologyViewProperty().not().or(mainApp.getEditProperty().not()));

        view.disableProperty().bind(mainApp.onOntologyViewProperty().not());
        addConceptScheme.disableProperty().bind(mainApp.onOntologyViewProperty().not().or(mainApp.getEditProperty().not()));

//        itemNewProject.visibleProperty().bind(mainApp.onDataViewProperty());
        itemNewProject.disableProperty().bind(mainApp.onDataViewProperty().not());

//        itemNewOntology.visibleProperty().bind(mainApp.onOntologyViewProperty());
        itemNewOntology.disableProperty().bind(mainApp.onOntologyViewProperty().not());

        switchMode.textProperty().bind(Bindings.when(PO2Properties.productionModeProperty()).then("Switch to SandBox").otherwise("Switch to Production"));
        switchMode.setOnAction(event ->  {
            if(mainApp.canLeaveApp()) {
                PO2Properties.setProductionMode(!PO2Properties.isProductionMode());
                MainApp.logger.info("switching mode. In production : " +PO2Properties.isProductionMode());
                mainApp.run(null);
            }
        });

        showDeprecated.setOnAction(event -> {
            if(MainApp.getOntologyControler() != null && MainApp.getOntologyControler().getCurrentOntology() != null) {
                MainApp.getOntologyControler().getCurrentOntology().setShowDeprecated(showDeprecated.selectedProperty().get());
                MainApp.getOntologyControler().rebuildTree(true);
            }
        });
    }

    public void exportOnto() {
        MainApp.exportOnto();
    }

    public void importOnto() {
        MainApp.importOnto();
    }

    @FXML
    public void createNewOntology() {
        if(!mainApp.unlock()) {
            return;
        }
        if(!CloudConnector.isInitUser()) {
            if(!UITools.startLogin()) {
                return;
            }
        }

        if(MainApp.getOntologyControler() != null && MainApp.getOntologyControler().isModified()) {
            Alert confirm = new Alert(Alert.AlertType.CONFIRMATION);
            confirm.setTitle("Change");
            confirm.setHeaderText("Current ontology changed will be lost. Are you sure ?");
            ButtonType buttonCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
            ButtonType buttonYes = new ButtonType("Yes", ButtonBar.ButtonData.YES);
            confirm.getButtonTypes().setAll(buttonCancel, buttonYes);
            confirm.initOwner(MainApp.primaryStage);
//
            Optional<ButtonType> result = confirm.showAndWait();
            if (result.isPresent()) {
                if (result.get().equals(buttonCancel)) {
                    return;
                }
            }
        }
        Dialog<Pair<String, Boolean>> dialog = new Dialog<>();
        dialog.setTitle("New domain ontology");
        dialog.setHeaderText("New domain ontology creation");
        ButtonType createButtonType = new ButtonType("Create", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(createButtonType, ButtonType.CANCEL);
        dialog.initOwner(MainApp.primaryStage);
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField ontologyName = new TextField();
        ontologyName.setPromptText("ontology name");
        ontologyName.setTextFormatter(new TextFormatter<Object>(new UnaryOperator<TextFormatter.Change>() {
            @Override
            public TextFormatter.Change apply(TextFormatter.Change change) {
                if(change.getControlNewText().length() > 100) {
                    return null;
                }
                return change;
            }
        }));

        ChoiceBox<String> choiceTypePublic = new ChoiceBox<>();
        choiceTypePublic.getItems().add("Public");
        choiceTypePublic.getItems().add("Private");

        grid.add(new Label("Ontology name :"), 0, 0);
        grid.add(ontologyName, 1, 0);
        grid.add(new Label("Ontology type :"), 0, 1);
        grid.add(choiceTypePublic, 1, 1);

        Node createButton = dialog.getDialogPane().lookupButton(createButtonType);
        createButton.setDisable(true);

        ontologyName.textProperty().addListener((observable, oldValue, newValue) -> {
            createButton.setDisable(false);
            if(newValue.trim().isEmpty()) {
                createButton.setDisable(true);
            }
            boolean nameExist = Ontologies.getOntologies().keySet().stream().filter(d -> d.trim().equalsIgnoreCase(newValue.trim()) || Tools.normalize(d.trim()).equalsIgnoreCase(Tools.normalize(newValue.trim()))).findAny().isPresent();
            if(nameExist) {
                createButton.setDisable(true);
            }
            if(choiceTypePublic.getValue() == null || choiceTypePublic.getValue().trim().isEmpty()) {
                createButton.setDisable(true);
            }
        });

        choiceTypePublic.valueProperty().addListener((observableValue, s, t1) -> {
            createButton.setDisable(false);
            if(ontologyName.getText().trim().isEmpty()) {
                createButton.setDisable(true);
            }
            if(Ontologies.getOntologies().keySet().stream().filter(d -> d.trim().equalsIgnoreCase(ontologyName.getText().trim()) || Tools.normalize(d.trim()).equalsIgnoreCase(Tools.normalize(ontologyName.getText().trim()))).findAny().isPresent()) {
                createButton.setDisable(true);
            }
            if(choiceTypePublic.getValue() == null || choiceTypePublic.getValue().trim().isEmpty()) {
                createButton.setDisable(true);
            }
        });

        dialog.getDialogPane().setContent(grid);
        Platform.runLater(ontologyName::requestFocus);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == createButtonType) {
                return new Pair<String, Boolean>(ontologyName.getText(), "public".equalsIgnoreCase(choiceTypePublic.getValue()));
            }
            return null;
        });

        Optional<Pair<String, Boolean>> result = dialog.showAndWait();
        result.ifPresent(projectPublic -> {

            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    Ontology newOnto = null;
                    try {
                        newOnto = CloudConnector.createNewOntology(projectPublic.getKey(), projectPublic.getValue());
                    } catch (AlreadyExistException | IOException | URISyntaxException | InterruptedException e) {
                        MainApp.logger.error("error during ontology creation : " + projectPublic.getKey(), e);
                    }
                    if(newOnto != null) {
                        MainApp.getOntologyControler().startOpenFile(ontologyName.getText());
                    } else {
                        Alert errorCreate = new Alert(Alert.AlertType.ERROR);
                        errorCreate.initModality(Modality.WINDOW_MODAL);
                        errorCreate.initOwner(MainApp.primaryStage);
                        errorCreate.setTitle("Error");
                        errorCreate.setHeaderText("An error occurred during ontology creation");
                        errorCreate.show();
                    }
                }
            });
            t.start();
        });
    }
    @FXML
    public void createNewProject() {
        if(!mainApp.unlock()) {
            return;
        }
        if(!CloudConnector.isInitUser()) {
            if(!UITools.startLogin()) {
                return;
            }
        }
        if(MainApp.getDataControler() != null && MainApp.getDataControler().isModified()) {
            Alert confirm = new Alert(Alert.AlertType.CONFIRMATION);
            confirm.setTitle("Change");
            confirm.setHeaderText("Current data changed will be lost. Are you sure ?");
            ButtonType buttonCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
            ButtonType buttonYes = new ButtonType("Yes", ButtonBar.ButtonData.YES);
            confirm.getButtonTypes().setAll(buttonCancel, buttonYes);
            confirm.initOwner(MainApp.primaryStage);
//
            Optional<ButtonType> result = confirm.showAndWait();
            if (result.isPresent()) {
                if (result.get().equals(buttonCancel)) {
                    return;
                }
            }
        }
        Dialog<Pair<String, Boolean>> dialog = new Dialog<>();
        dialog.setTitle("New project");
        dialog.setHeaderText("New project creation");
        ButtonType createButtonType = new ButtonType("Create", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(createButtonType, ButtonType.CANCEL);
        dialog.initOwner(MainApp.primaryStage);
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField projectName = new TextField();
        projectName.setPromptText("project name");
        projectName.setTextFormatter(new TextFormatter<Object>(new UnaryOperator<TextFormatter.Change>() {
            @Override
            public TextFormatter.Change apply(TextFormatter.Change change) {
                if(change.getControlNewText().length() > 100) {
                    return null;
                }
                return change;
            }
        }));

        ChoiceBox<String> choiceTypePublic = new ChoiceBox<>();
        choiceTypePublic.getItems().add("Public");
        choiceTypePublic.getItems().add("Private");

        grid.add(new Label("Project name :"), 0, 0);
        grid.add(projectName, 1, 0);
        grid.add(new Label("Project type :"), 0, 1);
        grid.add(choiceTypePublic, 1, 1);

        Node createButton = dialog.getDialogPane().lookupButton(createButtonType);
        createButton.setDisable(true);

        projectName.textProperty().addListener((observable, oldValue, newValue) -> {
            createButton.setDisable(false);
            if(newValue.trim().isEmpty()) {
                createButton.setDisable(true);
            }
            boolean nameExist = Datas.getDatas().keySet().stream().filter(d -> d.trim().equalsIgnoreCase(newValue.trim()) || Tools.normalize(d.trim()).equalsIgnoreCase(Tools.normalize(newValue.trim()))).findAny().isPresent();
            if(nameExist) {
                createButton.setDisable(true);
            }
            if(choiceTypePublic.getValue() == null || choiceTypePublic.getValue().trim().isEmpty()) {
                createButton.setDisable(true);
            }
        });

        choiceTypePublic.valueProperty().addListener((observableValue, s, t1) -> {
            createButton.setDisable(false);
            if(projectName.getText().trim().isEmpty()) {
                createButton.setDisable(true);
            }
            if(Datas.getData(projectName.getText().trim()) != null || Datas.getData(Tools.normalize(projectName.getText().trim())) != null) {
                createButton.setDisable(true);
            }
            if(choiceTypePublic.getValue() == null || choiceTypePublic.getValue().trim().isEmpty()) {
                createButton.setDisable(true);
            }
        });

        dialog.getDialogPane().setContent(grid);
        Platform.runLater(projectName::requestFocus);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == createButtonType) {
                return new Pair<String, Boolean>(projectName.getText(), "public".equalsIgnoreCase(choiceTypePublic.getValue()));
            }
            return null;
        });

        Optional<Pair<String, Boolean>> result = dialog.showAndWait();
        result.ifPresent(projectPublic -> {

            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    ProjectFile newProject = null;
                    try {
                        newProject = CloudConnector.createNewProject(projectPublic.getKey(), projectPublic.getValue());
                    } catch (AlreadyExistException e) {
                        MainApp.logger.error("error during project creation : " + projectPublic.getKey(), e);
                    }
                    if(newProject != null) {
                        MainApp.getDataControler().startOpenFile(projectName.getText());
                    } else {
                        Alert errorCreate = new Alert(Alert.AlertType.ERROR);
                        errorCreate.initModality(Modality.WINDOW_MODAL);
                        errorCreate.initOwner(MainApp.primaryStage);
                        errorCreate.setTitle("Error");
                        errorCreate.setHeaderText("An error occurred during project creation");
                        errorCreate.show();
                    }
                }
            });
            t.start();
        });

    }

    public String getChangeLog() {
        String changelog = "";
        try {
            changelog = IOUtils.toString(MainApp.class.getClassLoader().getResourceAsStream("resources/changelog.txt"), StandardCharsets.UTF_8 );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return changelog;
    }

    public void showChangelog()  {
        String changelog = getChangeLog();
        Alert showChange = new Alert(Alert.AlertType.INFORMATION);
        showChange.getDialogPane().setPrefSize(550, 300);
//        showChange.initOwner(MainApp.primaryStage);
        showChange.initModality(Modality.APPLICATION_MODAL);
        TextArea ta = new TextArea("Current "+MainApp.getVersionDate() + "\n\n"+changelog);
        ta.setEditable(false);
        ta.setScrollLeft(0.0);
        ta.setWrapText(true);
        showChange.setTitle("Changelog");
        showChange.setHeaderText(null);
        showChange.getDialogPane().setContent(ta);
        showChange.setResizable(true);
        showChange.showAndWait();
    }

    public void enterWriteMode() {
        if (mainApp.getModifiedProperty().get()) {
            Alert confirm = new Alert(Alert.AlertType.CONFIRMATION);
            confirm.setTitle("Modification detected");
            confirm.setHeaderText("Please save before leaving edit mode");
            confirm.getButtonTypes().setAll(ButtonType.OK);
            confirm.initOwner(MainApp.primaryStage);
            confirm.initModality(Modality.APPLICATION_MODAL);
            confirm.showAndWait();

        } else {
            mainApp.unlock();
        }
    }

    public void updateConceptSchemeList() {
        if(MainApp.getOntologyControler() != null && MainApp.getOntologyControler().getCurrentOntology() != null) {
            conceptScheme.getItems().clear();
            ToggleGroup tg = new ToggleGroup();
            MainApp.getOntologyControler().getCurrentOntology().getlistConceptScheme().forEach(skosScheme -> {
                RadioMenuItem r = new RadioMenuItem(skosScheme.getName());
                r.setToggleGroup(tg);
                conceptScheme.getItems().add(r);
                if(skosScheme.equals(MainApp.getOntologyControler().getCurrentOntology().getCurrentScheme())) {
                    r.setSelected(true);
                }
            });
            tg.selectedToggleProperty().addListener(observable -> {
                RadioMenuItem r = (RadioMenuItem) tg.getSelectedToggle();
                Ontology onto = MainApp.getOntologyControler().getCurrentOntology();
                onto.setCurrentSkosScheme(r.getText());
                MainApp.getOntologyControler().setCurrentSchemeName(r.getText());
                MainApp.getOntologyControler().setIsOnMainScheme(onto.getCurrentScheme().equals(onto.getMainScheme()));
                MainApp.getOntologyControler().rebuildTree(true);
            });
        }
    }

    public void newConceptScheme() {
        UITools.createNewSkosScheme();
    }


    public void enterEditionMode() {
        if(mainApp.onOntologyViewProperty().get() && mainApp.getOntologyControler().getCurrentOntology() != null) {
            mainApp.enterEditionMode();
        } else {
            if(mainApp.onDataViewProperty().get() && mainApp.getDataControler().getCurrentData() != null) {
                mainApp.enterEditionMode();
            }
        }
    }

    public void showUnitCode() {
        UITools.showUnitUcumCode();
    }

    public void publish() {
        mainApp.semantize();
    }

    public void startSHACLValidation() {
        mainApp.startSHACLValidation();
    }

    /**
     * Function Call on search click or CTRL+F
     */
    public void showSearchDialog() {
        mainApp.showSearchView();
    }

    /**
     *
     */
    public void closeApp() {
        mainApp.closeApp();
    }

    /**
     * @throws InterruptedException
     */
    public void save() {
        mainApp.save();
    }

    /**
     *
     */
    public void openFile() {
        mainApp.openFile();
    }

    public void activeSearch() {
        itemSeach.setDisable(false);
    }

    public void activeSave() {
        itemSave.setDisable(false);
    }

//	public void activeEdit() {
//		itemEdit.setDisable(false);
//	}
}
