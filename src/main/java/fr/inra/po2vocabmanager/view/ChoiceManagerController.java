/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.view;

import fr.inra.po2vocabmanager.MainApp;
import fr.inrae.po2engine.model.Datas;
import fr.inrae.po2engine.model.Ontologies;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;


public class ChoiceManagerController {

    @FXML
    TextArea areaVocab;
    @FXML
    TextArea areaData;
    @FXML
    TextArea changeLog;
    @FXML
    Label labVersion;
    private MainApp mainApp;
    @FXML
    private void initialize() {
        // nothing to do here
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        labVersion.setText(MainApp.getVersionDate());
        areaVocab.setText(Ontologies.getOntologies().size() + "\n Ontologies");
        areaData.setText(Datas.getDatas().size() + "\n DataSets");
        changeLog.setEditable(false);
        changeLog.setScrollLeft(0.0);
        changeLog.setWrapText(true);
    }

    public void setChangeLog(String changeLog) {
        this.changeLog.setText(changeLog);
    }

    @FXML
    private void showOntologyManager() {
        this.mainApp.showOntologyMod();
    }

    @FXML
    private void showDataManager() {
        this.mainApp.showDataMod();
    }
}
