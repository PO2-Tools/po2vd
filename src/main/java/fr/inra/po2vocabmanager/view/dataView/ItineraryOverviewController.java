/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.view.dataView;

import fr.inra.po2vocabmanager.MainApp;
import fr.inra.po2vocabmanager.model.DataNode;
import fr.inra.po2vocabmanager.utils.JavaConnector;
import fr.inra.po2vocabmanager.utils.UITools;
import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.dataModel.CompositionFile;
import fr.inrae.po2engine.model.dataModel.ItineraryFile;
import fr.inrae.po2engine.model.dataModel.StepFile;
import fr.inrae.po2engine.utils.ProgressPO2;
import fr.inrae.po2engine.utils.Tools;
import guru.nidi.graphviz.attribute.Color;
import guru.nidi.graphviz.engine.*;
import guru.nidi.graphviz.model.Factory;
import guru.nidi.graphviz.model.MutableGraph;
import guru.nidi.graphviz.model.MutableNode;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import netscape.javascript.JSObject;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class ItineraryOverviewController {

   @FXML
    TitledPane itineraryPane;

   @FXML
    AnchorPane ap;
    @FXML
    VBox prodOfbox;

    MainApp mainApp;
    BooleanProperty showAllCompo =  new SimpleBooleanProperty(true);
    BooleanProperty showISCompo =  new SimpleBooleanProperty(false);
    BooleanProperty showNoneCompo = new SimpleBooleanProperty(false);
    private HashMap<String, StepFile> listIdStep;
    private HashMap<String, CompositionFile> listIdComposition;
    private JavaConnector jc;
    private ListView<TextField> listProduct;
    private WebView browser;
    private ItineraryFile itiFile;

    @FXML
    private void initialize() {

    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }


    public ItineraryOverviewController() {
        super();
    }

    public void showNodeDetails(DataNode node) {


        this.itiFile = (ItineraryFile) node.getFile();

        listProduct = new ListView<>();
        this.itiFile.checkValue();





        prodOfbox.getChildren().clear();

        Label lnum = new Label(itiFile.getItineraryNumber());
        Label lname = new Label("  --  name : ");
        TextField itiName = new TextField(itiFile.getItineraryName());
        itiName.disableProperty().bind(this.mainApp.getEditProperty().not());
        itiName.setStyle(" -fx-text-fill: black;  -fx-opacity: 1;");
        itiName.textProperty().addListener((observableValue, oldValue, newValue) ->  {
            itiFile.getProcessFile().getData().setModified(true);
            if(newValue != null && !newValue.isEmpty()) {
                itiFile.itineraryNameProperty().set(newValue);
            } else {
                itiFile.itineraryNameProperty().set("");
            }
        } );
        Label separator = new Label("");
        Label connectedComponent = new Label( "");
        connectedComponent.visibleProperty().bind(itiFile.connectedComposant.greaterThan(1));
        connectedComponent.textProperty().bind(new SimpleStringProperty(" -- ").concat(itiFile.connectedComposant.asString().concat(" connected components")));
        separator.setMaxWidth(Double.MAX_VALUE);
        HBox bb = new HBox();
        Background warn = new Background(new BackgroundFill(javafx.scene.paint.Color.DARKORANGE,null,null));
        Background norm = new Background(new BackgroundFill(null,null,null));
        bb.backgroundProperty().bind(Bindings.when(itiFile.connectedComposant.greaterThan(1)).then(warn).otherwise(norm));

        bb.setAlignment(Pos.CENTER_RIGHT);
        bb.setHgrow(separator, Priority.ALWAYS);
        bb.getChildren().addAll(lnum,lname, itiName, connectedComponent, separator);
        bb.setPadding(new Insets(0,3,0,0));
        bb.minWidthProperty().bind(itineraryPane.widthProperty().subtract(25));
        itineraryPane.setGraphic(bb);

        Label prodLabel = new Label("Product of interest : ");
        Button addProd = new Button();
        addProd.disableProperty().bind(this.mainApp.getEditProperty().not());
        prodLabel.setContentDisplay(ContentDisplay.RIGHT);
        addProd.setGraphic(new ImageView(UITools.getImage("resources/images/add_16.png")));
        prodLabel.setGraphic(addProd);


        VBox box = new VBox();

        box.getChildren().add(listProduct);
        for (String c : itiFile.getProductsOfInterest()) {
            if (!c.isEmpty()) {
                TextField comb = new TextField();
                comb.setText(c);
                comb.editableProperty().bind(MainApp.getDataControler().getCanEditProperty());
                UITools.addAutoComplete(comb, Ontology.listComponentProperty());
                listProduct.getItems().add(comb);
                comb.textProperty().addListener((observableValue, s, t1) -> updateListProduct(itiFile));
            }
        }
        listProduct.setCellFactory(cellData -> {
            return new ListCell<TextField>() {

                @Override
                protected void updateItem(TextField item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setText(null);
                        setStyle(null);
                        setGraphic(null);
                    } else {
                        Button delButton = new Button(null, new ImageView(UITools.getImage("resources/images/del_16.png")));
                        delButton.setOnMouseClicked(event1 -> {listProduct.getItems().remove(item);updateListProduct(itiFile);});
                        delButton.disableProperty().bind(MainApp.getDataControler().getCanEditProperty().not());

                        HBox hBox = new HBox();
                        hBox.getChildren().addAll(delButton, item);
                        hBox.maxWidthProperty().bind(listProduct.widthProperty());
                        setGraphic(hBox);
                    }
                }
            };
        });

        addProd.setOnMouseClicked(event1 -> {
            TextField comb = new TextField();
            comb.editableProperty().bind(MainApp.getDataControler().getCanEditProperty());
            UITools.addAutoComplete(comb, Ontology.listComponentProperty());
//            UITools.addPopOver(comb, Ontology.Component);
            listProduct.getItems().add(comb);
            updateListProduct(itiFile);
            comb.textProperty().addListener((observableValue, s, t1) -> updateListProduct(itiFile));
        });



        TextArea graphTips = new TextArea(
                "* Nodes :\n"+
                "   - Blue nodes: Parent step of subStep(s)\n"+
                "   - Black nodes: steps without observation.\n"+
                "   - Black/Green nodes: steps with observation.\n"+
                "   - Red nodes: compositions.\n"+
                "* Arrows :\n"+
                "   - Black arrows: Temporal relationships between steps\n"+
                "   - Blue arrows: SubSteps relationships\n"+
                "   - Red arrows: Input or output compositions\n"+
                "* Graph tips: \n" +
                "   - Use shift key + mouse to add a new link (Step / Step and Step / Composition).\n" +
                "   - Use del key on selected link or step to remove it.");
        graphTips.setWrapText(true);
        graphTips.setPrefHeight(300);

        Button exportImg = new Button("create snapshot");

        exportImg.setGraphic(new ImageView(UITools.getImage("resources/images/file-export-16.png")));

        Button resetLayout = new Button("reset nodes position");
        resetLayout.setGraphic(new ImageView(UITools.getImage("resources/images/resize-expand_16.png")));


        HBox bbb = new HBox(exportImg, resetLayout);
        VBox.setVgrow(listProduct, Priority.ALWAYS);
        prodOfbox.getChildren().addAll(prodLabel, listProduct, graphTips, bbb);


        resetLayout.setOnAction(actionEvent -> {
            Alert confirmationResetNode = new Alert(Alert.AlertType.CONFIRMATION);
            confirmationResetNode.initOwner(MainApp.primaryStage);
            confirmationResetNode.setTitle("Warning");
            confirmationResetNode.setHeaderText("Warning");
            confirmationResetNode.setContentText("Recalculating the graph layout will reset the current positions of the nodes. All manual positioning will be lost. Do you really want to continue ?");
            Optional<ButtonType> result = confirmationResetNode.showAndWait();
            if(result.isPresent() && result.get() == ButtonType.OK) {
                this.itiFile.clearNodePosition();
                drawGraph();
            }
        });

        exportImg.setOnAction(actionEvent -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Export graph");
            fileChooser.setInitialFileName("graph.png");
            File fileExport = fileChooser.showSaveDialog(MainApp.primaryStage);
            if (fileExport != null) {
                try {
                    FileOutputStream fout = new FileOutputStream(fileExport, false);
                    WritableImage img = new WritableImage(Double.valueOf(browser.getWidth()).intValue(), Double.valueOf(browser.getHeight()).intValue());
                    browser.snapshot(null, img);
                    BufferedImage bufferedImage = SwingFXUtils.fromFXImage(img, null);
                    ImageIO.write(bufferedImage,"png", fout);

//                    ImageIO.write(SwingFXUtils.fromFXImage(sp2.getImageView().getImage(), null),"png", fout);
                    fout.close();
                } catch (IOException ex) {
                    MainApp.logger.error(ex.getMessage());
                }
            }
        });



//        generateGraphSmart(sp2, file.getItinerary(), file.getItineraryName() + " (" + file.getItineraryNumber()+")");
//        generateGraph(sp2, file.getItinerary(), file.getItineraryName() + " (" + file.getItineraryNumber()+")");

        drawGraph();

    }

    private void drawGraph() {
        Tools.addProgress(ProgressPO2.GRAPH, "creating graph");
        browser = new WebView();
        AnchorPane.setTopAnchor(browser, 0.0);
        AnchorPane.setLeftAnchor(browser, 0.0);
        AnchorPane.setRightAnchor(browser, 0.0);
        AnchorPane.setBottomAnchor(browser, 0.0);
        ap.getChildren().clear();
        ap.getChildren().add(browser);

        WebEngine webEngine = browser.getEngine();
        webEngine.setJavaScriptEnabled(true);

        listIdStep = new HashMap<>();
        listIdComposition = new HashMap<>();

        // Important. garder une référence au javaconnector pour eviter le garbage collector ! bug openJDK
        jc = new JavaConnector(this.itiFile, listIdStep, listIdComposition);
        webEngine.setOnAlert(event -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(MainApp.primaryStage);
            alert.initModality(Modality.WINDOW_MODAL);
            alert.setContentText(event.getData());
            alert.showAndWait();
        });
        webEngine.setOnError(event -> {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(MainApp.primaryStage);
            alert.initModality(Modality.WINDOW_MODAL);
            alert.setContentText(event.getMessage());
            alert.showAndWait();
        });
        webEngine.setConfirmHandler(event -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(MainApp.primaryStage);
            alert.initModality(Modality.WINDOW_MODAL);
            alert.setContentText(event);
            alert.showAndWait();
            return true;
        });
        webEngine.getLoadWorker().stateProperty().addListener(
                new ChangeListener() {
                    @Override
                    public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                        if (newValue == Worker.State.SUCCEEDED) {
                            JSObject jsobj = (JSObject) webEngine.executeScript("window");
                            jsobj.setMember("javaConnector", jc);
//                            webEngine.executeScript("var firebug=document.createElement('script');firebug.setAttribute('src','https://lupatec.eu/getfirebug/firebug-lite-compressed.js');document.body.appendChild(firebug);(function(){if(window.firebug.version){firebug.init();}else{setTimeout(arguments.callee);}})();void(firebug);");

                            Platform.runLater(() -> {
                                new Thread(() -> {
                                    generateGraph2(browser, itiFile, itiFile.getItineraryName() + " (" + itiFile.getItineraryNumber()+")", listIdStep, listIdComposition);
                                    Tools.delProgress(ProgressPO2.GRAPH);
                                }).start();
                            });
                        }
                    }
                }
        );

        Platform.runLater(() -> {
            webEngine.load(MainApp.class.getResource("graph/graph.html").toExternalForm());
        });
    }

    private void generateGraph2(WebView webView, ItineraryFile itiFile, String title, HashMap<String, StepFile> listIdStep, HashMap<String, CompositionFile> listIdComposition) {

        ObservableList<Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>>> itinerary = itiFile.getItinerary();
            WebEngine webEngine = webView.getEngine();

//            Double width = webView.getWidth();
//            Double height = webView.getHeight();

                MutableGraph g = Factory.mutGraph(title);
                g.setDirected(true);
                g.graphAttrs().add("nodesep",1.8);
                g.graphAttrs().add("ranksep",1.5);

                HashMap<StepFile, MutableNode> listStepNode = new HashMap<>();
                HashMap<CompositionFile, MutableNode> listCompo = new HashMap<>();
                AtomicBoolean positionExist = new AtomicBoolean(false);

                itiFile.getListStep().filtered(s -> s.getFather() != null).forEach(s -> {
                    StepFile father = s.getFather();
                    if(!listStepNode.containsKey(father)) {
                        MutableNode ms = Factory.mutNode(father.getUuid(true));
                        ms.add("id", father.getUuid(true));
                        ms.add(guru.nidi.graphviz.attribute.Label.of(father.getId()));
                        ms.add("observation", father.getObservationFiles().size());
                        ms.add("hat", "yes");
                        ms.add("type", "step");
                        String pos = itiFile.getNodePosition(father.getUuid(true));
                        if(pos != null) {
                            ms.add("pos", pos);
                            positionExist.set(true);
                        }
                        listIdStep.put(father.getUuid(true), father);

                        g.add(ms);
                        listStepNode.put(father, ms);
                    }
                });

                for(StepFile s : itiFile.getListStep()) {
                    if(!listIdStep.containsValue(s)) {
                        MutableNode ms = Factory.mutNode(s.getUuid(true));
                        ms.add("id", s.getUuid(true));
                        ms.add(guru.nidi.graphviz.attribute.Label.of(s.getId()));
                        ms.add("observation", s.getObservationFiles().size());
                        ms.add("hat", "no");
                        ms.add("type", "step");
                        String pos = itiFile.getNodePosition(s.getUuid(true));
                        if(pos != null) {
                            ms.add("pos", pos);
                            positionExist.set(true);
                        }
                        listIdStep.put(s.getUuid(true),s);
                        listStepNode.put( s, ms);
                        g.add(ms);
                    }
                }

                // adding link substep
                listStepNode.keySet().stream().filter(s -> s.getFather() != null).forEach(s -> {
                    MutableNode p = null;
                    if(s.getFather() != null) {
                        p = listStepNode.get(s.getFather());
                    }
                    MutableNode f = null;
                    f = listStepNode.get(s);
                    if(p!=null && f!= null) {
                        p.addLink(f);
                    }
                });

                for (Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> pair : itinerary) {
                    MutableNode p = null;
                    if(pair.getKey().getValue().get() != null) {
                        p = listStepNode.get(pair.getKey().getValue().getValue());
                    }
                    MutableNode f = null;
                    if(pair.getValue().getValue().get() != null) {
                        f = listStepNode.get(pair.getValue().getValue().getValue());
                    }

                    if (p == null || f == null) {
                       // error
                    }

                    if(p!=null && f!= null) {
                        p.addLink(f);
                    }
                }

                if(showAllCompo.get()) {
                    for (StepFile sf : listStepNode.keySet()) {
                        if(!sf.getSubStep().isEmpty()) {
                        }
                        for (Map.Entry<CompositionFile, Boolean> en : sf.getCompositionFile().entrySet()) {
                            MutableNode pc = null;
                            pc = listCompo.get(en.getKey());
                            if (pc == null) {
                                pc = Factory.mutNode( en.getKey().getUuid(true));
                                pc.add(guru.nidi.graphviz.attribute.Label.of(en.getKey().getCompositionID().getValue().get()));
                                pc.add(Color.named("red"));
                                pc.add("id", en.getKey().getUuid(true));
                                pc.add("type", "composition");
                                String pos = itiFile.getNodePosition(en.getKey().getUuid(true));
                                if(pos != null) {
                                    pc.add("pos", pos);
                                    positionExist.set(true);
                                }
                                listIdComposition.put(en.getKey().getUuid(true), en.getKey());
                                g.add(pc);
                                listCompo.put(en.getKey(), pc);
                            }
                            if (en.getValue()) {
                                pc.addLink(listStepNode.get(sf));
                            } else {
                                listStepNode.get(sf).addLink(pc);
                            }
                        }
                    }
                }

        List<GraphvizEngine> listEngine = new ArrayList<>(
                Arrays.asList(new GraphvizCmdLineEngine(), new GraphvizV8Engine(), new GraphvizJdkEngine()));
        Graphviz.useEngine(listEngine);
        Graphviz viz = Graphviz.fromGraph(g);
        String json = "";
        if(positionExist.get()) {
            json = viz.engine(Engine.NEATO).render(Format.JSON).toString();
        } else {
            json = viz.engine(Engine.DOT).yInvert(true).render(Format.JSON).toString();
        }
//        try {
//            viz.engine(Engine.NEATO).render(Format.PNG).toFile(new File("resources/graph.png"));
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }

        JSONObject jsonGraph = new JSONObject(json);
        JSONArray listGraphNode = jsonGraph.optJSONArray("objects");


        if(listGraphNode != null) {
            Double maxHeaght = 0.0;
            for (Integer i = 0; i < listGraphNode.length(); i++) {
                JSONObject node = listGraphNode.getJSONObject(i);
                if(!node.optString("pos").isEmpty()) {
                    String[] pos = node.getString("pos").split(",");
                    maxHeaght = Math.max(maxHeaght,Double.valueOf(pos[1]) );
                    if(!positionExist.get()) { // pos calculatedby dot algo
                        itiFile.setNodePosition(node.getString("id"),+Double.valueOf(pos[0]) / 72.0, Double.valueOf(pos[1]) / 72.0 );
                    }
                }
            }
            for (Integer i = 0; i < listGraphNode.length(); i++) {
                JSONObject node = listGraphNode.getJSONObject(i);
                if(!node.optString("pos").isEmpty()) {
                    String[] pos = node.getString("pos").split(",");
                    String label = node.getString("label").replaceAll("\\\\N", "");
                    Double posX = Double.valueOf(pos[0]);
                    Double posY = Double.valueOf(pos[1]);
                    if (node.getString("type").equalsIgnoreCase("step")) {
                        Platform.runLater(() -> {
                            String type = node.getString("hat").equalsIgnoreCase("yes") ? "hat" : "step";
                            webEngine.executeScript("graph.addNodeNoUpdate('" + label.replaceAll("'", "\\\\'") + "','" + node.getString("id") + "','" + type + "', " + node.getInt("observation") + "," + posX + "," + posY + ");");
                        });
                    } else {
                        Platform.runLater(() -> {
                            webEngine.executeScript("graph.addNodeNoUpdate('" + label.replaceAll("'", "\\\\'") + "','" + node.getString("id") + "','composition', 0 ," + posX + "," + posY + ");");
                        });
                    }
                } else {
                    // cluster node
                    Double posX = 0.0;
                    Double posY = 0.0;

                    Platform.runLater(() -> {
                        webEngine.executeScript("graph.addNodeNoUpdate('','" + -1 + "','" + "cluster" + "', " + "0" + "," + posX + "," + posY + ");");
                    });
                }
            }
        }

        JSONArray listGraphEdge = jsonGraph.optJSONArray("edges");
        if(listGraphEdge != null) {
            for (Integer i = 0; i < listGraphEdge.length(); i++) {
                JSONObject edge = listGraphEdge.getJSONObject(i);
                Platform.runLater(() -> {
                    webEngine.executeScript("graph.addEdgeNoUpdate(" + edge.getInt("tail") + "," + edge.getInt("head") + ");");
                });
            }
        }
        Platform.runLater(() -> {
            webEngine.executeScript("graph.updateGraph()");
            webEngine.executeScript("graph.fitWindow()");
        });
    }

    private void updateListProduct(ItineraryFile file) {
        String val = listProduct.getItems().stream().map(textField -> textField.getText()).collect(Collectors.joining("::"));
//        file.getCProductsOfInterest().setValue(val);
//          file.getCProductsOfInterest().getValue().setValue(val);
        file.setProductsOfInterest(val);
//        System.out.println(file.getCProductsOfInterest().getValue().getValue());
    }

    public void unloadControler() {
        itineraryPane.setContent(null);
        itineraryPane.setGraphic(null);
    }
}
