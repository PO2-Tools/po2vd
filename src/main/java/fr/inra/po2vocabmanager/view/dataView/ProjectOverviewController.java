/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.view.dataView;

import fr.inra.po2vocabmanager.MainApp;
import fr.inra.po2vocabmanager.utils.UITools;
import fr.inra.po2vocabmanager.utils.XCell;
import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.model.dataModel.ProjectFile;
import fr.inrae.po2engine.model.partModel.ProjectAgentPart;
import fr.inrae.po2engine.utils.DataPartType;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

public class ProjectOverviewController {

    @FXML
    TextField projectTitle;

    @FXML
    TitledPane paneMaterial;

    @FXML
    TitledPane paneMethod;
    @FXML
    TableView<HashMap<KeyWords, ComplexField>> tableOrgAgent;
    @FXML
    TableColumn<HashMap<KeyWords, ComplexField>, ComplexField> colOrg;
    @FXML
    TableColumn<HashMap<KeyWords, ComplexField>, ComplexField>  colFamily;
    @FXML
    TableColumn<HashMap<KeyWords, ComplexField>, ComplexField>  colGiven;
    @FXML
    TableColumn<HashMap<KeyWords, ComplexField>, ComplexField>  colMail;
    @FXML
    TableColumn<HashMap<KeyWords, ComplexField>, ComplexField>  colAction;
    @FXML
    ComboBox<HashMap<KeyWords, ComplexField>> contactAgent;
    @FXML
    ComboBox<HashMap<KeyWords, ComplexField>> fundingAgent;

    @FXML
    TextArea description;
    @FXML
    ListView<String> listExternalLinks;
    @FXML
    Label linksLabel;
    private ObservableList<String> listLink;

    MainApp mainApp;

    @FXML
    private void initialize() {
        paneMaterial.setGraphic(new ImageView(UITools.getImage("resources/images/material_16.png")));
        paneMethod.setGraphic(new ImageView(UITools.getImage("resources/images/method_16.png")));
    }

    private void updateListLink() {
        String val = listLink.stream().collect(Collectors.joining("::"));
        this.mainApp.getDataControler().getCurrentData().getProjectFile().setExternalLink(val);
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        ProjectFile projectFile = mainApp.getDataControler().getCurrentData().getProjectFile();
        tableOrgAgent.setDisable(false);
        tableOrgAgent.editableProperty().bind(this.mainApp.getEditProperty());

        Button addLine = new Button("New line ");
        addLine.disableProperty().bind(this.mainApp.getEditProperty().not());
        addLine.setGraphic(new ImageView(UITools.getImage("resources/images/add_row-16.png")));
        addLine.setOnAction(actionEvent -> {
            projectFile.addAgent();
        });

        Label noContent = new Label("No content in table");
        noContent.setGraphic(addLine);
        noContent.setContentDisplay(ContentDisplay.BOTTOM);
        tableOrgAgent.setPlaceholder(noContent);

        colOrg.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().get(ProjectAgentPart.agentOrganisationK)));
        colFamily.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().get(ProjectAgentPart.agentFamilyNameK)));
        colGiven.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().get(ProjectAgentPart.agentGivenNameK)));
        colMail.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().get(ProjectAgentPart.agentMailK)));
        colAction.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().get(ProjectAgentPart.agentOrganisationK)));


        colOrg.setCellFactory(col -> {
            return new TableCell<HashMap<KeyWords, ComplexField>, ComplexField>() {

                @Override
                protected void updateItem(ComplexField item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setText(null);
                        setStyle(null);
                        setGraphic(null);
                    } else {
                        TextField text = new TextField();
                        text.editableProperty().bind(tableOrgAgent.editableProperty());
                        text.styleProperty().bind(item.styleProperty());
                        item.getValue().unbind();
                        text.setText(item.getValue().get());
                        item.getValue().bind(text.textProperty());

                        setGraphic(text);
                    }
                }
            };
        });
        colFamily.setCellFactory(col -> {
            return new TableCell<HashMap<KeyWords, ComplexField>, ComplexField>() {

                @Override
                protected void updateItem(ComplexField item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setText(null);
                        setStyle(null);
                        setGraphic(null);
                    } else {
                        TextField text = new TextField();
                        text.editableProperty().bind(tableOrgAgent.editableProperty());
                        text.styleProperty().bind(item.styleProperty());
                        item.getValue().unbind();
                        text.setText(item.getValue().get());
                        item.getValue().bind(text.textProperty());

                        setGraphic(text);
                    }
                }
            };
        });
        colGiven.setCellFactory(col -> {
            return new TableCell<HashMap<KeyWords, ComplexField>, ComplexField>() {

                @Override
                protected void updateItem(ComplexField item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setText(null);
                        setStyle(null);
                        setGraphic(null);
                    } else {
                        TextField text = new TextField();
                        text.editableProperty().bind(tableOrgAgent.editableProperty());
                        text.styleProperty().bind(item.styleProperty());
                        item.getValue().unbind();
                        text.setText(item.getValue().get());
                        item.getValue().bind(text.textProperty());

                        setGraphic(text);
                    }
                }
            };
        });
        colMail.setCellFactory(col -> {
            return new TableCell<HashMap<KeyWords, ComplexField>, ComplexField>() {

                @Override
                protected void updateItem(ComplexField item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setText(null);
                        setStyle(null);
                        setGraphic(null);
                    } else {
                        TextField text = new TextField();
                        text.editableProperty().bind(tableOrgAgent.editableProperty());
                        text.styleProperty().bind(item.styleProperty());
                        item.getValue().unbind();
                        text.setText(item.getValue().get());
                        item.getValue().bind(text.textProperty());

                        setGraphic(text);
                    }
                }
            };
        });

        colAction.setCellFactory(mapComplexFieldTableColumn -> {
            return new TableCell<HashMap<KeyWords, ComplexField>, ComplexField>() {

                @Override
                protected void updateItem(ComplexField complexField, boolean empty) {
                    super.updateItem(complexField, empty);
                    if (complexField != null && !empty) {
                        setText(null);
                        setStyle(null);
                        Button add = new Button("");
                        add.setGraphic(new ImageView(UITools.getImage("resources/images/add_16.png")));
                        add.setOnAction(event -> {
                            projectFile.addAgent();
                        });
                        add.disableProperty().bind(tableOrgAgent.editableProperty().not());

                        Button del = new Button("");
                        del.setGraphic(new ImageView(UITools.getImage("resources/images/del_16.png")));
                        del.setOnAction(event -> {
                            projectFile.removeAgent(tableOrgAgent.getItems().get(getIndex()));
                        });
                        del.disableProperty().bind(tableOrgAgent.editableProperty().not());

                        HBox hb = new HBox(add, del);
                        setGraphic(hb);
                    } else {
                        setText(null);
                        setStyle(null);
                        setGraphic(null);
                    }
                }
            };
        });

        tableOrgAgent.setItems(projectFile.getListAgent());

        ArrayList<String> ls = projectFile.getExternalLinks();
        String[] arrayLink = new String [ls.size()];
        ls.toArray(arrayLink);
        this.listLink = FXCollections.observableArrayList(Arrays.asList(arrayLink));
        this.listLink.addListener((ListChangeListener<? super String>) change -> {
            updateListLink();
        });


        Button addLink = new Button();
        addLink.disableProperty().bind(this.mainApp.getEditProperty().not());
        addLink.setGraphic(new ImageView(UITools.getImage("resources/images/add_16.png")));
        linksLabel.setGraphic(addLink);
        listExternalLinks.editableProperty().bind(this.mainApp.getEditProperty());
        listExternalLinks.setItems(listLink);
        listExternalLinks.setCellFactory(param -> new XCell());

        addLink.setOnAction(actionEvent -> {
           listLink.add("new link");
        });

        contactAgent.setEditable(false);
        contactAgent.disableProperty().bind(this.mainApp.getEditProperty().not());

        fundingAgent.setEditable(false);
        fundingAgent.disableProperty().bind(this.mainApp.getEditProperty().not());

        description.editableProperty().bind(this.mainApp.getEditProperty());

        // ne sera jamais éditable !!!!!!
        projectTitle.setEditable(false);



        projectTitle.setText(this.mainApp.getDataControler().getCurrentData().getProjectFile().getNameProperty().getValue());
        this.mainApp.getDataControler().getCurrentData().getProjectFile().getNameProperty().bind(projectTitle.textProperty());
        UITools.bindComboBoxAgent(projectFile.getContactProperty(),projectFile.getListAgent(),  contactAgent);
        UITools.bindComboBoxAgent(projectFile.getFundingProperty(),projectFile.getListAgent(),  fundingAgent);
        UITools.simpleBindValue(projectFile.getCDescription(), description);

        Data data = this.mainApp.getDataControler().getCurrentData();
        try {
            FXMLLoader loaderMaterial = UITools.getFXMLLoader("view/dataView/MaterialMethodOverview.fxml");
            SplitPane materialOverview = (SplitPane) loaderMaterial.load();
            MaterialMethodOverviewController materialController = loaderMaterial.getController();
            materialController.setMainApp(this.mainApp, DataPartType.MATERIAL_RAW);
            materialController.showItem(data.getMaterialMethodFile().getMaterialPart());
            paneMaterial.setContent(materialOverview);

            FXMLLoader loaderMethod = UITools.getFXMLLoader("view/dataView/MaterialMethodOverview.fxml");
            SplitPane methodOverview = (SplitPane) loaderMethod.load();
            MaterialMethodOverviewController methodController = loaderMethod.getController();
            methodController.setMainApp(this.mainApp, DataPartType.METHOD_RAW);
            methodController.showItem(data.getMaterialMethodFile().getMethodPart());
            paneMethod.setContent(methodOverview);
        } catch (IOException e) {
            e.printStackTrace();
        }
        projectFile.checkValue();
//        projectTitle.editableProperty().bind(mainApp.getEditProperty());
    }


}
