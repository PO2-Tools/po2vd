/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.view.dataView;

import fr.inra.po2vocabmanager.MainApp;
import fr.inra.po2vocabmanager.model.DataNode;
import fr.inra.po2vocabmanager.utils.UITools;
import fr.inra.po2vocabmanager.utils.XCell;
import fr.inrae.po2engine.model.Replicate;
import fr.inrae.po2engine.model.dataModel.GeneralFile;
import fr.inrae.po2engine.model.dataModel.StepFile;
import javafx.animation.TranslateTransition;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Shape;
import javafx.stage.Modality;
import javafx.util.Duration;
import org.controlsfx.control.textfield.AutoCompletionBinding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class ProcessOverviewController {

    @FXML
    TextField processTitle;
    @FXML
    TextField processType;
    @FXML
    TextField startDate;
    @FXML
    TextField endDate;
    @FXML
    TitledPane stepAvailable;
    @FXML
    TitledPane replicatesPane;
    @FXML
    TextArea description;
    @FXML
    Label linksLabel;
    @FXML
    ListView<String> listExternalLinks;
    private ObservableList<String> listLink;
    TreeTableView<StepFile> treeTableView;
    TreeTableColumn<StepFile, String> colStep;
    TreeTableColumn<StepFile, String> colIti;
    TreeTableColumn<StepFile, StepFile> colDel;

    TableView<Replicate> replicateTable;
    TableColumn<Replicate, Replicate> colRepName;
    TableColumn<Replicate, Integer> colRepId;
    TableColumn<Replicate, Replicate> colRepDesc;
    TableColumn<Replicate, Replicate> colRepDel;

    TreeItem<StepFile> rootTree;


    private MainApp mainApp;
    private GeneralFile generalFile;
    ArrayList<AutoCompletionBinding> listAutoCompletion ;

    @FXML
    private void initialize() {
        listAutoCompletion = new ArrayList<>();
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        processTitle.editableProperty().bind(mainApp.getEditProperty());
        startDate.editableProperty().bind(mainApp.getEditProperty());
        endDate.editableProperty().bind(mainApp.getEditProperty());
        processType.editableProperty().bind(mainApp.getEditProperty());
        description.editableProperty().bind(mainApp.getEditProperty());
    }

    private TranslateTransition createTranslateTransition(final Shape group) {
        final TranslateTransition transition = new TranslateTransition(Duration.seconds(0.25), group);
        transition.setOnFinished(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent t) {

                group.setLayoutX(group.getTranslateX() + group.getLayoutX());
                group.setLayoutY(group.getTranslateY() + group.getLayoutY());
                group.setTranslateX(0);
                group.setTranslateY(0);
            }
        });
        return transition;
    }

    private void generateTreeStep(StepFile s, TreeItem<StepFile> treeItem) {
        for(StepFile ss : s.getSubStep()) {
//            DataNode ssNode = MainApp.getDataControler().getDataNode(ss);
            TreeItem<StepFile> i = new TreeItem<>(ss);
            treeItem.getChildren().add(i);
            generateTreeStep(ss, i);
        }
    }

    private void buildStepTree(GeneralFile f) {
        rootTree.getChildren().clear();
        for(StepFile s : f.getListStep()) {
            if(s.getFather() == null) {
//                DataNode sNode = MainApp.getDataControler().getDataNode(s);
                TreeItem<StepFile> i = new TreeItem<>(s);
                generateTreeStep(s, i);
                rootTree.getChildren().add(i);
            }
        }
    }

    public void showNodeDetails(DataNode node) {
        for(AutoCompletionBinding auto : listAutoCompletion) {
            auto.dispose();
        }
        generalFile = (GeneralFile) node.getFile();
        generalFile.unbind();
        generalFile.checkValue();

        ChangeListener changeListener = (observable, oldValue, newValue) -> {
            generalFile.getData().setModified(true);
        };

        UITools.simpleBindValue(generalFile.getCTitle(), processTitle);
        UITools.simpleBindValue(generalFile.getCStartDate(), startDate);
        UITools.simpleBindValue(generalFile.getCEndDate(), endDate);
//        UITools.simpleBindValue(generalFile.getCSampleCode(), sampleCode);
//        UITools.simpleBindValue(generalFile.getCSampleName(), sampleName);
        UITools.simpleBindValue(generalFile.getCType(), processType);
        UITools.simpleBindValue(generalFile.getCDescription(), description);

        listAutoCompletion.add(UITools.addAutoComplete(processType, generalFile.getCType().getListConstraint()));
        generalFile.getListReplicate().forEach(r -> r.setValuesListener(changeListener));

        ArrayList<String> ls = generalFile.getExternalLinks();
        String[] arrayLink = new String [ls.size()];
        ls.toArray(arrayLink);
        this.listLink = FXCollections.observableArrayList(Arrays.asList(arrayLink));
        this.listLink.addListener((ListChangeListener<? super String>) change -> {
            updateListLink();
        });
        Button addLink = new Button();
        addLink.disableProperty().bind(this.mainApp.getEditProperty().not());
        addLink.setGraphic(new ImageView(UITools.getImage("resources/images/add_16.png")));
        linksLabel.setGraphic(addLink);
        listExternalLinks.editableProperty().bind(this.mainApp.getEditProperty());
        listExternalLinks.setItems(listLink);
        listExternalLinks.setCellFactory(param -> new XCell());

        addLink.setOnAction(actionEvent -> {
            listLink.add("new link");
        });
//            UITools.addPopOver(processType, Ontology.Process);

//        generalFile.setTitle(processTitle);
//        generalFile.setDate(date);
//        generalFile.setSampleName(sampleName);
//        generalFile.setSampleCode(sampleCode);

        treeTableView = new TreeTableView<>();
        treeTableView.setShowRoot(false);
        colStep = new TreeTableColumn<>("Step");
        colIti = new TreeTableColumn<>("Itinerary");
        colDel = new TreeTableColumn<>("");
        colDel.setMaxWidth(60);
        colDel.setMinWidth(60);
        colDel.setPrefWidth(60);

        treeTableView.getColumns().addAll(colStep, colIti, colDel);

        colStep.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getValue().getNameProperty().getValue()));
        colIti.setCellValueFactory(cellData -> {
            StepFile s = cellData.getValue().getValue();
            StringBuilder retour = new StringBuilder();
            if(MainApp.getDataControler().getDataNodes(s) != null) {
                MainApp.getDataControler().getDataNodes(s).forEach(dataNode -> {
                    retour.append(dataNode.getItineraryFile().getItineraryNumber());
                    retour.append(" (");
                    retour.append(dataNode.getItineraryFile().getItineraryName());
                    retour.append(")\n");
                });
            }
            return new SimpleStringProperty(retour.toString());
        });
        colDel.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getValue()));
        colDel.setCellFactory(cell -> {

            return new TreeTableCell<>(){
                @Override
                protected void updateItem(StepFile s, boolean b) {
                    super.updateItem(s, b);

                    if(s == null || b) {
                        setGraphic(null);
                    } else {
                        Button buttonDel = new Button();
                        buttonDel.setGraphic(new ImageView(UITools.getImage("resources/images/del_16.png")));
                        buttonDel.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                        buttonDel.setOnAction(event -> {
                            StepFile sf = s;
                            Alert a = new Alert(Alert.AlertType.CONFIRMATION);
                            a.setTitle("Delete Step");
                            a.setHeaderText("Delete step " + sf.getNameProperty().getValue() + " and its observations ?\nThis action will permanently remove this step (including \nsubsteps and observations) from all itineraries");
                            a.initModality(Modality.WINDOW_MODAL);
                            a.initOwner(MainApp.primaryStage);
                            Optional<ButtonType> res = a.showAndWait();
                            if (res.get() == ButtonType.OK) {
                                GeneralFile gf = sf.getGeneralFile();
                                sf.removeFile();
                                MainApp.getDataControler().deleteDataNode(sf);
                                buildStepTree(gf);

//                            for(ItineraryFile iteFile : stepFile.getGeneralFile().getItinerary()) {
//                                MainApp.getDataControler().getDataNode(iteFile).removeSubNode(MainApp.getDataControler().getDataNodes(stepFile));
//                            }
                                s.getData().setModified(true);
                            }
                        });
                        setGraphic(buttonDel);
                    }
                }
            };
        });



        rootTree = new TreeItem<>();
        buildStepTree(generalFile);
        treeTableView.setRoot(rootTree);
        treeTableView.getSortOrder().clear();
        treeTableView.getSortOrder().add(colStep);
        treeTableView.setColumnResizePolicy(TreeTableView.CONSTRAINED_RESIZE_POLICY);

        stepAvailable.setContent(treeTableView);

        replicateTable = new TableView<>();
        replicateTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        replicateTable.editableProperty().bind(mainApp.getEditProperty());
        colRepId = new TableColumn<>("id");
        colRepId.setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData.getValue().getId()).asObject());
        colRepId.setMaxWidth(60);
        colRepId.setMinWidth(60);
        colRepId.setPrefWidth(60);

        colRepName = new TableColumn<>("Name");
        colRepName.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue()));
        colRepName.setCellFactory(param -> {
                return new TableCell<Replicate, Replicate>() {
                    @Override
                    public void updateItem(Replicate item, boolean empty) {
                        super.updateItem(item, empty);
                        TextField text = new TextField();
                        if (item != null && !empty) {
                            text.editableProperty().bind(replicateTable.editableProperty());
                            item.nameProperty().unbind();
                            text.setText(item.getName());
                            item.nameProperty().bind(text.textProperty());
                            setGraphic(text);
                        } else {
                            setText(null);
                            setStyle(null);
                            setGraphic(null);
                        }
                    }
                };
        });

        colRepDesc = new TableColumn<>("Description");
        colRepDesc.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue()));
        colRepDesc.setCellFactory(param -> {
            return new TableCell<Replicate, Replicate>() {
                @Override
                public void updateItem(Replicate item, boolean empty) {
                    super.updateItem(item, empty);
                    TextField text = new TextField();
                    if (item != null && !empty) {
                        text.editableProperty().bind(replicateTable.editableProperty());
                        item.descriptionProperty().unbind();
                        text.setText(item.getDescription());
                        item.descriptionProperty().bind(text.textProperty());
                        setGraphic(text);
                    } else {
                        setText(null);
                        setStyle(null);
                        setGraphic(null);
                    }
                }
            };
        });

        colRepDel = new TableColumn<>();
        colRepDel.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue()));
        colRepDel.setMaxWidth(60);
        colRepDel.setMinWidth(60);
        colRepDel.setPrefWidth(60);
        AtomicReference<GeneralFile> currentGF = new AtomicReference<>(generalFile);

        colRepDel.setCellFactory(cell -> {

            return new TableCell<>(){
                @Override
                protected void updateItem(Replicate replicate, boolean b) {
                    super.updateItem(replicate, b);

                    if(replicate == null || b) {
                        setGraphic(null);
                    } else {
                        Button buttonDel = new Button();
                        buttonDel.setGraphic(new ImageView(UITools.getImage("resources/images/del_16.png")));
                        if(replicate.getId() == 0) {
                            buttonDel.setDisable(true);
                        } else {
                            buttonDel.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                        }
                        buttonDel.setOnAction(event -> {
                            Alert a = new Alert(Alert.AlertType.CONFIRMATION);
                            a.setTitle("Delete Replicate");
                            a.setHeaderText("Delete Replicate " + replicate.getName());
                            a.initModality(Modality.WINDOW_MODAL);
                            a.initOwner(MainApp.primaryStage);
                            Optional<ButtonType> res = a.showAndWait();
                            if (res.get() == ButtonType.OK) {
                                GeneralFile gf = currentGF.get();
                                gf.removeReplicate(replicate);
                                gf.getData().setModified(true);
                            }
                        });
                        setGraphic(buttonDel);
                    }
                }
            };
        });

        replicateTable.getColumns().addAll(colRepId, colRepName, colRepDesc, colRepDel);

        replicatesPane.setContent(replicateTable);
        replicateTable.setItems(generalFile.getListReplicate());

    }

    private void updateListLink() {
        String val = listLink.stream().collect(Collectors.joining("::"));
        generalFile.setExternalLink(val);
    }

    public void unloadControler() {
        treeTableView.getColumns().clear();
        colDel = null;
        colIti = null;
        colStep = null;
        treeTableView = null;
        rootTree = null;
        stepAvailable.setContent(null);


    }

//    private void generateGraphIti(String title) {
//
//        UITools.addProgress(true, "generation in progress, ", ProgressPO2.GRAPH);
//        UITools.setProgress(ProgressPO2.GRAPH, 0.0);
//        Task t = new Task() {
//            @Override
//            protected Object call() throws Exception {
//
//                MutableGraph g = Factory.mutGraph(title);
//                g.setDirected(false);
//
////                HashMap<String, MutableNode> listAllNode = new HashMap<>();
////                HashMap<String, StepFile> listAllStep = new HashMap<>();
//                HashMap<StepFile, ArrayList<MutableNode>> listStepIti = new HashMap<>();
//                HashMap<MutableNode, ArrayList<MutableNode>> listLink = new HashMap<>();
//
//                Integer nbIti = generalFile.getItinerary().size();
//                Double pas = 1.0 / nbIti;
//                Integer compt = 0;
//
//                for(GeneralItineraryPart gip : generalFile.getItinerary()) {
//                    Integer finalCompt = compt++;
//                    Platform.runLater(() -> UITools.setProgress(ProgressPO2.GRAPH, pas * finalCompt));
//                    MutableNode n = Factory.mutNode(gip.getItiNumber().getValue() + " (" + gip.getItiName().getValue()+")");
//                    g.add(n);
//                    for (Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> pair : gip.getItinerary()) {
//                        StepFile p = pair.getKey().getValue().get();
//                        StepFile f = pair.getValue().getValue().get();
//
//                        if(p != null) {
//                            if(!listStepIti.containsKey(p)) {
//                                listStepIti.put(p, new ArrayList<>());
//                            }
//                            if(!listStepIti.get(p).contains(n)) {
//                                listStepIti.get(p).add(n);
//                            }
//                        }
//                        if(f != null) {
//                            if(!listStepIti.containsKey(f)) {
//                                listStepIti.put(f, new ArrayList<>());
//                            }
//                            if(!listStepIti.get(f).contains(n)) {
//                                listStepIti.get(f).add(n);
//                            }
//                        }
//                    }
//                }
//
//                for(Map.Entry<StepFile, ArrayList<MutableNode>> entry : listStepIti.entrySet()) {
//                    for(int i = 0; i < entry.getValue().size()-1; i++) {
//                        MutableNode n1 = entry.getValue().get(i);
//                        for(int j = i+1; j < entry.getValue().size(); j++ ) {
//                            MutableNode n2 = entry.getValue().get(j);
//                            if(listLink.containsKey(n1)) {
//                                if(!listLink.get(n1).contains(n2)) {
//                                    listLink.get(n1).add(n2);
//                                }
//                            } else {
//                                if(listLink.containsKey(n2)) {
//                                    if(!listLink.get(n2).contains(n1)) {
//                                        listLink.get(n2).add(n1);
//                                    }
//                                } else {
//                                    listLink.put(n1, new ArrayList<>());
//                                    listLink.get(n1).add(n2);
//                                }
//                            }
//                        }
//                    }
//                }
//
//                for(Map.Entry<MutableNode, ArrayList<MutableNode>> entry : listLink.entrySet()) {
//                    for(MutableNode n : entry.getValue()) {
//                        g.add(entry.getKey().addLink(n));
//                    }
//                }
//
//
//
//                Graphviz viz = Graphviz.fromGraph(g);
//                BufferedImage bimg = viz.render(Format.SVG).toImage();
//                Platform.runLater(() -> UITools.delProgress(ProgressPO2.GRAPH));
//
//
//                Platform.runLater(() -> {
//                    FileChooser fileChooser = new FileChooser();
//                    fileChooser.setTitle(processTitle.getText());
//                    fileChooser.setInitialFileName(processTitle.getText() + ".png");
//                    File fileExport = fileChooser.showSaveDialog(MainApp.primaryStage);
//                    if (fileExport != null) {
//                        try {
//                            FileOutputStream fout = new FileOutputStream(fileExport, false);
//                            ImageIO.write(bimg, "png", fout);
//                            fout.close();
//                        } catch (IOException ex) {
//                            MainApp.logger.error(ex.getMessage());
//                        }
//                    }
//                });
//                return null;
//            }
//        };
//        new Thread(t).start();
//
//    }


//    private void generateGraph(String title) {
//        UITools.addProgress(true, "generation in progress, ", ProgressPO2.GRAPH);
//        UITools.setProgress(ProgressPO2.GRAPH, 0.0);
//        Task t = new Task() {
//            @Override
//            protected Object call() throws Exception {
//
//                MutableGraph g = Factory.mutGraph(title);
//                g.setDirected(true);
//
//                HashMap<String, MutableNode> listAllNode = new HashMap<>();
//                HashMap<String, StepFile> listAllStep = new HashMap<>();
//                HashMap<StepFile, MutableNode> listStepNode = new HashMap<>();
//
//                Integer nbIti = generalFile.getItinerary().size();
//                Double pas = 1.0 / nbIti;
//                Integer compt = 0;
//                for(GeneralItineraryPart gip : generalFile.getItinerary()) {
//                    compt++;
//                    Integer finalCompt = compt;
//                    Platform.runLater(() -> UITools.setProgress(ProgressPO2.GRAPH, pas * finalCompt));
//                    System.out.println("new val : " + pas * compt);
//
//                    for (Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> pair : gip.getItinerary()) {
//                        MutableNode p = null;
//                        if(pair.getKey().getValue().get() != null) {
//                            p = listAllNode.get(pair.getKey().getValue().getValue().getName() + " (" + pair.getKey().getValue().get().getComplexName().getID().get() + ")");
//                        }
//                        MutableNode f = null;
//                        if(pair.getValue().getValue().get() != null) {
//                            f = listAllNode.get(pair.getValue().getValue().getValue().getName() + " (" + pair.getValue().getValue().get().getComplexName().getID().get() + ")");
//                        }
//
//                        if (p == null) {
//                            if(!pair.getKey().getKey().getValue().get().isEmpty() && pair.getKey().getValue().get() != null) {
//                                p = Factory.mutNode(pair.getKey().getValue().get().getName() + " (" + pair.getKey().getValue().get().getComplexName().getID().get() + ")");
//                                listAllNode.put(pair.getKey().getValue().get().getName() + " (" + pair.getKey().getValue().get().getComplexName().getID().get() + ")", p);
//                                listAllStep.put(pair.getKey().getValue().get().getName() + " (" + pair.getKey().getValue().get().getComplexName().getID().get() + ")", pair.getKey().getValue().getValue());
//                                listStepNode.put( pair.getKey().getValue().getValue(), p);
//                            }
//                        }
//                        if(p!= null) {
//                            g.add(p);
//                        }
//                        if (f == null) {
//                            if(!pair.getValue().getKey().getValue().get().isEmpty() && pair.getValue().getValue().get() != null) {
//                                f = Factory.mutNode(pair.getValue().getValue().get().getName() + " (" + pair.getValue().getValue().get().getComplexName().getID().get() + ")");
//                                listAllNode.put(pair.getValue().getValue().get().getName() + " (" + pair.getValue().getValue().get().getComplexName().getID().get() + ")", f);
//                                listAllStep.put(pair.getValue().getValue().get().getName() + " (" + pair.getValue().getValue().get().getComplexName().getID().get() + ")", pair.getValue().getValue().getValue());
//                                listStepNode.put(pair.getValue().getValue().getValue(), f);
//
//                            }
//                        }
//                        if(f!= null) {
//                            g.add(f);
//                        }
//
//                        if(p!=null && f!= null) {
//                            MutableNode link = p.addLink(f);
//                            g.add(link);
//                        }
//                    }
//                }
//
//                Graphviz viz = Graphviz.fromGraph(g);
//                BufferedImage bimg = viz.render(Format.SVG).toImage();
//                Platform.runLater(() -> UITools.delProgress(ProgressPO2.GRAPH));
//
//
//                Platform.runLater(() -> {
//                    FileChooser fileChooser = new FileChooser();
//                    fileChooser.setTitle(processTitle.getText());
//                    fileChooser.setInitialFileName(processTitle.getText() + ".png");
//                    File fileExport = fileChooser.showSaveDialog(MainApp.primaryStage);
//                    if (fileExport != null) {
//                        try {
//                            FileOutputStream fout = new FileOutputStream(fileExport, false);
//                            ImageIO.write(bimg, "png", fout);
//                            fout.close();
//                        } catch (IOException ex) {
//                            MainApp.logger.error(ex.getMessage());
//                        }
//                    }
//                });
//                return null;
//            }
//        };
//        new Thread(t).start();
//
//    }


}
