/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.view.dataView;

import fr.inra.po2vocabmanager.MainApp;
import fr.inra.po2vocabmanager.model.DataNode;
import fr.inra.po2vocabmanager.utils.UITools;
import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Replicate;
import fr.inrae.po2engine.model.dataModel.CompositionFile;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.model.dataModel.ObservationFile;
import fr.inrae.po2engine.model.dataModel.StepFile;
import fr.inrae.po2engine.model.partModel.TablePart;
import fr.inrae.po2engine.utils.DataPartType;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import org.apache.commons.lang3.tuple.MutablePair;

import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;

public class ObservationOverviewController {

    @FXML
    TextField observationType;
    @FXML
    TextField observationName;
    @FXML
    TitledPane observationPanel;
    @FXML
    VBox boxContent;
    @FXML
    TextField observationDate;
    @FXML
    TextField observationTime;
    @FXML
    TextField observationTimeD;
    @FXML
    TextArea observationDesc;
    @FXML
    TextField observationScale;
    @FXML
    ListView listObjectObserved;
    @FXML
    ComboBox<HashMap<KeyWords, ComplexField>> orgAgent;
    ObservationFile file;
    ObservableList<MutablePair<String, MutablePair<String, SimpleBooleanProperty>>> listItem;

    MainApp mainApp;


    @FXML
    private void initialize() {

    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        observationType.setEditable(false);
        observationType.setDisable(true);

        observationName.editableProperty().bind(mainApp.getEditProperty());
        observationDate.editableProperty().bind(mainApp.getEditProperty());
        observationTime.editableProperty().bind(mainApp.getEditProperty());
        observationTimeD.editableProperty().bind(mainApp.getEditProperty());
        observationDesc.editableProperty().bind(mainApp.getEditProperty());
        observationScale.editableProperty().bind(mainApp.getEditProperty());
//        listObjectObserved.disableProperty().bind(mainApp.getEditProperty().not());

        orgAgent.setEditable(false);
        orgAgent.disableProperty().bind(this.mainApp.getEditProperty().not());
    }



    public void bindFoi(ComplexField cField, ListView<MutablePair<String, MutablePair<String, SimpleBooleanProperty>>> listFOI, StepFile stepFile) {
        for(String s : cField.getValue().get().split("::")) {
            s = s.trim();
            if(s.equalsIgnoreCase("itinerary")) {
                for(MutablePair<String, MutablePair<String, SimpleBooleanProperty>> tt : listFOI.getItems()) {
                    if(tt.getRight().getLeft().equalsIgnoreCase(s)) {
                        tt.getRight().getRight().setValue(true);
                    }
                }
            } else {
                if (s.equalsIgnoreCase("step")) {
                    for(MutablePair<String, MutablePair<String, SimpleBooleanProperty>> tt : listFOI.getItems()) {
                        if(tt.getRight().getLeft().equalsIgnoreCase(s)) {
                            tt.getRight().getRight().setValue(true);
                        }
                    }
                } else {
                    // il s'agit d'une composition
                    for(CompositionFile comp : stepFile.getCompositionFile().keySet()) {
                        if(comp.getFileName().equalsIgnoreCase(s)) {
                            for(MutablePair<String, MutablePair<String, SimpleBooleanProperty>> tt : listFOI.getItems()) {
                                if(tt.getRight().getLeft().equalsIgnoreCase(s)) {
                                    tt.getRight().getRight().setValue(true);
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    public void showNodeDetails(DataNode node, Replicate replicate) {

        ToggleGroup groupFOI = new ToggleGroup();

        file = (ObservationFile) node.getFile();
        file.checkValue();
        if(replicate != null) {
            observationPanel.setText("Observation (replicate : " + replicate.getName() + ")");
        } else {
            observationPanel.setText("Observation");
        }
        UITools.simpleBindValue(file.getCName(), observationType);

        UITools.simpleBindValue(file.getCID(), observationName);
        UITools.simpleBindValue(file.getCDate(),replicate, observationDate);
        UITools.simpleBindValue(file.getCDuree(),replicate, observationTimeD);
        UITools.simpleBindValue(file.getCHeure(),replicate,observationTime);
        UITools.simpleBindValue(file.getCDescrition(), observationDesc);
        UITools.simpleBindValue(file.getCScale(), observationScale);
        UITools.bindComboBoxAgent(file.getAgentsMap(),file.getData().getProjectFile().getListAgent(), replicate, orgAgent);
        UITools.addAutoComplete(observationScale, file.getCScale().getListConstraint());


        listItem = FXCollections.observableArrayList();
        if(file.getStepFile() != null) { // sinon c'est une observation d'itinéraire.
            listObjectObserved.setDisable(false);
            listObjectObserved.setVisible(true);
            groupFOI.selectedToggleProperty().addListener(observable -> {
                RadioButton r = (RadioButton) groupFOI.getSelectedToggle();
                file.setFoi(r.getUserData().toString());
            });
            listItem.add(new MutablePair<>("Step - " + file.getStepFile().getId(), new MutablePair<>("step", new SimpleBooleanProperty(false))));

            for (CompositionFile comp : file.getStepFile().getCompositionFile().keySet()) {
                String inout = Boolean.TRUE.equals(file.getStepFile().getCompositionFile().get(comp)) ? "Input" : "Output";
                listItem.add(new MutablePair<>(inout+" - " + comp.getHeaderPart().getCompositionID() , new MutablePair<>(comp.getFileName(), new SimpleBooleanProperty(false))));
            }

            listObjectObserved.setItems(listItem);
            bindFoi(file.getCFoi(), listObjectObserved, file.getStepFile());

            for (MutablePair<String, MutablePair<String, SimpleBooleanProperty>> item : listItem) {
                item.getRight().getRight().addListener((observableValue, wasSel, isSel) -> {
                    file.setFOI(listItem);
                    MainApp.getDataControler().getCurrentData().setModified(true);
                });
            }
        } else {
            listObjectObserved.setDisable(false); // observation d'itinéraire. on active les objects observed avec 1 seul et on ne laisse pas le choix.
            listObjectObserved.setVisible(true);
            listItem.add(new MutablePair<>("Itinerary " + file.getItineraryFile().getItineraryName(), new MutablePair<>("Itinerary " + file.getItineraryFile().getItineraryName(), new SimpleBooleanProperty(true))));
            listObjectObserved.setItems(listItem);
        }
        listObjectObserved.setCellFactory(mutablePairListView -> {
            ListCell<MutablePair<String, MutablePair<String, SimpleBooleanProperty>>> cell = new ListCell<MutablePair<String, MutablePair<String, SimpleBooleanProperty>>>() {
                @Override
                public void updateItem(MutablePair<String, MutablePair<String, SimpleBooleanProperty>> stringMutablePairMutablePair, boolean b) {
                    super.updateItem(stringMutablePairMutablePair, b);
                    if (stringMutablePairMutablePair != null && !b) {
                        RadioButton r = new RadioButton(stringMutablePairMutablePair.getLeft());
                        r.setUserData(stringMutablePairMutablePair.getRight().getLeft());
                        r.setSelected(stringMutablePairMutablePair.getRight().getRight().get());
                        r.setToggleGroup(groupFOI);
                        setGraphic(r);
                        setText(null);
                        setStyle(" -fx-text-fill: black;  -fx-opacity: 1;");

                    } else {
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
            cell.disableProperty().bind(mainApp.getEditProperty().not());
            return cell;
        });

        boxContent.getChildren().retainAll(observationPanel);

        TitledPane matMetPane = new TitledPane();
        VBox.setVgrow(matMetPane, Priority.ALWAYS);
        matMetPane.setText("Materials & Methods");
        matMetPane.setMinWidth(Double.NEGATIVE_INFINITY);
        FXMLLoader loaderMatMet = UITools.getFXMLLoader("view/dataView/MaterialMethodLinkOverview.fxml");
        SplitPane matmetOverview = null;
        try {
            matmetOverview = loaderMatMet.load();
            matmetOverview.setMinHeight(200);
            matmetOverview.setPrefHeight(300);
        } catch (IOException e) {
            e.printStackTrace();
        }
        MaterialMethodLinkOverviewController matmetController = loaderMatMet.getController();
        matmetController.setMainApp(this.mainApp, null);
//        matmetController.setListConsign(f.getListConsign());
        matmetController.showItem(file);
        matMetPane.setContent(matmetOverview);
        boxContent.getChildren().add(matMetPane);

        int compteur = 0;
        for(TablePart tablePart : file.getContentList()) {
            compteur++;
            TitledPane tablePane = new TitledPane();

            TableView table = new TableView();
            table.setMinHeight(200);
            table.editableProperty().bind(this.mainApp.getEditProperty());
            UITools.bindTable(tablePart.getTable(), replicate, table);
            tablePane.setContent(table);
            boxContent.getChildren().add(tablePane);

            Label l = new Label("Table "+compteur + "  ");
            l.setAlignment(Pos.CENTER_LEFT);

            ComboBox<String> comboType = new ComboBox<>();
            comboType.setEditable(false);
            comboType.disableProperty().bind(mainApp.getEditProperty().not());
            comboType.setStyle(" -fx-text-fill: black;  -fx-opacity: 1;");
            comboType.getItems().addAll(DataPartType.RAW_DATA.getPrefLabel("en"), DataPartType.CALC_DATA.getPrefLabel("en"));
            comboType.setValue(tablePart.getPartType().getPrefLabel("en"));
            comboType.valueProperty().addListener((observable, oldValue, newValue) -> {
                MainApp.getDataControler().getCurrentData().setModified(true);
                if(newValue != null && newValue.equalsIgnoreCase(DataPartType.RAW_DATA.getPrefLabel("en"))) {
                    tablePart.setType(DataPartType.RAW_DATA);
                }
                if(newValue != null && newValue.equalsIgnoreCase(DataPartType.CALC_DATA.getPrefLabel("en"))) {
                    tablePart.setType(DataPartType.CALC_DATA);
                }
            });


            Button delTable = new Button(null, new ImageView(UITools.getImage("resources/images/del_16.png")));
            delTable.setTooltip(new Tooltip("delete Table"));
            delTable.setOnMouseClicked(event -> {
                // demande de confirmation
                Alert a = new Alert(Alert.AlertType.CONFIRMATION);
                a.setTitle("Delete Observation Data");
                a.setHeaderText("Delete Observation Data ?");
                a.initModality(Modality.WINDOW_MODAL);
                a.initOwner(MainApp.primaryStage);
                Optional<ButtonType> res = a.showAndWait();
                if(res.get() == ButtonType.OK) {
                    file.removeTablePart(tablePart);
                    showNodeDetails(node, replicate);
                }

            });
            delTable.disableProperty().bind(this.mainApp.getEditProperty().not());

            Label emptyLabel = new Label(" ");
            emptyLabel.setMaxWidth(Double.MAX_VALUE);
            HBox bb = new HBox();
            bb.setAlignment(Pos.CENTER_RIGHT);
            bb.getChildren().addAll(l,comboType,emptyLabel, delTable);
            HBox.setHgrow(emptyLabel, Priority.ALWAYS);
            bb.setPadding(new Insets(0,3,0,0));
            bb.minWidthProperty().bind(tablePane.widthProperty().subtract(25));
            tablePane.setGraphic(bb);
        }

    }

    public void unloadControler() {
        this.file = null;
    }
}
