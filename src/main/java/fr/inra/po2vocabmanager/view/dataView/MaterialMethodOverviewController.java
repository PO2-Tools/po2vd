/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.view.dataView;

import fr.inra.po2vocabmanager.MainApp;
import fr.inra.po2vocabmanager.utils.FilterableTreeItem;
import fr.inra.po2vocabmanager.utils.TreeItemPredicate;
import fr.inra.po2vocabmanager.utils.UITools;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.model.partModel.ConsignPart;
import fr.inrae.po2engine.model.partModel.MaterialMethodPart;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.FieldState;
import fr.inrae.po2engine.utils.Platform;
import fr.inrae.po2engine.utils.PlatformMaterial;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.util.Callback;

import java.util.Map;
import java.util.Optional;

public class MaterialMethodOverviewController {



    @FXML
    Button add;
    @FXML
    Button del;
    @FXML
    ListView<MaterialMethodPart> listView;
    @FXML
    SplitPane pane;
    @FXML
    AnchorPane rightPane;


    MainApp mainApp;
    KeyWords type = null;
    Map<MaterialMethodPart, ConsignPart> listConsign;

    @FXML
    private void initialize() {

    }

    public void setListConsign(Map<MaterialMethodPart, ConsignPart> listConsign) {
        this.listConsign = listConsign;
    }

    public void setMainApp(MainApp mainApp, KeyWords type) {
        this.mainApp = mainApp;
        this.type = type;
        listView.editableProperty().bind(mainApp.getEditProperty());
        add.disableProperty().bind(mainApp.getEditProperty().not());
        add.setGraphic(new ImageView(UITools.getImage("resources/images/add_16.png")));

        del.disableProperty().bind(Bindings.or(mainApp.getEditProperty().not(), listView.getSelectionModel().selectedItemProperty().isNotNull().not()));
        del.setGraphic(new ImageView(UITools.getImage("resources/images/del_16.png")));

    }

    public void showItem(ObservableList<MaterialMethodPart> listMaterial) {
        rightPane.getChildren().clear();
        listMaterial.forEach(MaterialMethodPart::checkValue);
        listView.setItems(listMaterial);
        if(listConsign == null) {
            // on supprime complétement le matiel (on est sur la liste générale.
            del.setOnMouseClicked(event -> {
                listMaterial.remove(listView.getSelectionModel().getSelectedItem());
                MainApp.getDataControler().getCurrentData().setModified(true);
            });

            add.setOnMouseClicked(event -> {

                if(this.type != null && this.type.equals(DataPartType.MATERIAL_RAW)) {
                    Dialog<RadioButton> newMatMet = new Dialog();
                    newMatMet.setResizable(false);
                    VBox b = new VBox();
                    b.setSpacing(10);
                    String title = "new material";
                    RadioButton rbScratch = new RadioButton("Create from scratch");
                    TextField newID = new TextField();
                    newID.setPromptText("Name");
                    newID.disableProperty().bind(rbScratch.selectedProperty().not());

                    RadioButton rbImport = new RadioButton("Import from platform");
                    TextField importFilter = new TextField();
                    importFilter.setPromptText("Filter");

                    final ToggleGroup group = new ToggleGroup();
                    rbScratch.setToggleGroup(group);
                    rbImport.setToggleGroup(group);
                    rbScratch.setSelected(true);
                    PlatformMaterial plRoot = new PlatformMaterial();
                    plRoot.setOnlyPlatform(true);
                    FilterableTreeItem<PlatformMaterial> rootMaterial = new FilterableTreeItem<>(plRoot);
                    for(String pf : Platform.getListPlatform().keySet()) {
                        PlatformMaterial onlyPl = new PlatformMaterial();
                        onlyPl.setOnlyPlatform(true);
                        onlyPl.setName(pf);
                        FilterableTreeItem<PlatformMaterial> plat = new FilterableTreeItem<>(onlyPl);
                        rootMaterial.getInternalChildren().add(plat);
                        for(PlatformMaterial mat : Platform.getListPlatform().get(pf)) {
                            FilterableTreeItem<PlatformMaterial> mater = new FilterableTreeItem<>(mat);
                            plat.getInternalChildren().add(mater);
                        }
                    }

                    rootMaterial.predicateProperty().bind(Bindings.createObjectBinding(() -> {
                        if (importFilter.getText() == null || importFilter.getText().isEmpty())
                            return null;
                        return TreeItemPredicate.create(value -> value.toString().toLowerCase().contains(importFilter.getText().toLowerCase()));
                    }, importFilter.textProperty()));

                    TreeView<PlatformMaterial> treeView = new TreeView<>(rootMaterial);
                    treeView.setShowRoot(false);
                    treeView.disableProperty().bind(rbImport.selectedProperty().not());
                    importFilter.disableProperty().bind(rbImport.selectedProperty().not());

                    HBox importBox = new HBox(5.0);
                    importBox.getChildren().addAll(rbImport, importFilter);

                    b.getChildren().addAll(rbScratch, newID, importBox, treeView);
                    newMatMet.getDialogPane().setPrefSize(400, 300);

                    newMatMet.setTitle(title);
                    newMatMet.initModality(Modality.APPLICATION_MODAL);
                    newMatMet.initOwner(listView.getScene().getWindow());
                    newMatMet.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);

                    newMatMet.getDialogPane().lookupButton(ButtonType.OK).disableProperty().bind(Bindings.or(Bindings.and(rbScratch.selectedProperty(), newID.textProperty().isEmpty()), Bindings.and(rbImport.selectedProperty(), treeView.getSelectionModel().selectedItemProperty().isNull())));

                    newMatMet.getDialogPane().setContent(b);

                    newMatMet.setResultConverter(dialogButton -> {
                        if(dialogButton == ButtonType.OK) {
                            return (RadioButton) group.getSelectedToggle();
                        }
                        return null;
                    });

                    Optional<RadioButton> result = newMatMet.showAndWait();
                    result.ifPresent(matRadio -> {
                        if(matRadio.equals(rbScratch)) {
                            MaterialMethodPart materialContentPart = new MaterialMethodPart(MainApp.getDataControler().getCurrentData().getProjectFile(), this.type);
                            materialContentPart.setId(newID.getText());
                            MainApp.getDataControler().getCurrentData().setModified(true);
                        } else {
                            // import
                            PlatformMaterial importPF = treeView.getSelectionModel().getSelectedItem().getValue();
                            if (importPF != null) {
                                MainApp.getDataControler().getCurrentData().getProjectFile().importMaterialFromPlatform(importPF);
                                MainApp.getDataControler().getCurrentData().setModified(true);
                            }
                        }
                        // restart checkValue for all
                        listMaterial.forEach(MaterialMethodPart::checkValue);
                    });
                }
                if(this.type != null && this.type.equals(DataPartType.METHOD_RAW)) {
                    Dialog<String> newMatMet = new Dialog();
                    newMatMet.setResizable(false);
                    VBox b = new VBox();
                    b.setSpacing(10);
                    Label lab = new Label("new method name");
                    TextField newID = new TextField();
                    b.getChildren().addAll(lab, newID);

                    newMatMet.setResultConverter(dialogButton -> {
                        if(dialogButton == ButtonType.OK) {
                            return newID.getText();
                        }
                        return null;
                    });
                    newMatMet.getDialogPane().setPrefSize(300, 200);

                    newMatMet.initModality(Modality.APPLICATION_MODAL);
                    newMatMet.initOwner(listView.getScene().getWindow());
                    newMatMet.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
                    newMatMet.getDialogPane().lookupButton(ButtonType.OK).disableProperty().bind(newID.textProperty().isEmpty());

                    newMatMet.getDialogPane().setContent(b);

                    Optional<String> result = newMatMet.showAndWait();
                    result.ifPresent(newMatMetID -> {
                        MaterialMethodPart materialContentPart = new MaterialMethodPart(MainApp.getDataControler().getCurrentData().getProjectFile(), this.type);
                        materialContentPart.setId(newMatMetID);
                        MainApp.getDataControler().getCurrentData().setModified(true);
                    });
                }

            });
        } else {
            del.setOnMouseClicked(event -> {
                Alert a = new Alert(Alert.AlertType.CONFIRMATION);
                a.setTitle("Delete " + this.type.getPrefLabel("en") + " ?");
                a.setHeaderText("Delete " + this.type.getPrefLabel("en") + " ?");
                a.initModality(Modality.WINDOW_MODAL);
                a.initOwner(MainApp.primaryStage);
                Optional<ButtonType> res = a.showAndWait();
                if(res.get() == ButtonType.OK) {
                    listMaterial.remove(listView.getSelectionModel().getSelectedItem());
                    MainApp.getDataControler().getCurrentData().setModified(true);
                }

            });

            add.setOnMouseClicked(event -> {});
        }

        listView.setCellFactory(new Callback<ListView<MaterialMethodPart>, ListCell<MaterialMethodPart>>() {

            @Override
            public ListCell<MaterialMethodPart> call(ListView<MaterialMethodPart> p) {

                ListCell<MaterialMethodPart> cell = new ListCell<MaterialMethodPart>() {

                    @Override
                    protected void updateItem(MaterialMethodPart t, boolean bln) {
                        super.updateItem(t, bln);
                        if(t == null || bln) {
                            textProperty().unbind();
                            setText(null);
                            styleProperty().unbind();
                            setStyle("");
                            setGraphic(null);
                        } else {
                            textProperty().bind(Bindings.concat(t.getOntoType().getValue()," (", t.getID().getValue(),")"));
                            styleProperty().bind(Bindings.when(t.stateProperty().isEqualTo(FieldState.WARNING)).then("-fx-border-color: orange;").otherwise(Bindings.when(t.stateProperty().isEqualTo(FieldState.ERROR)).then("-fx-border-color: red;").otherwise("")));
                        }
                    }

                };

                return cell;
            }
        });

        listView.getSelectionModel().selectedItemProperty()
                .addListener(new ChangeListener<MaterialMethodPart>() {
                    public void changed(ObservableValue<? extends MaterialMethodPart> observable,
                                        MaterialMethodPart oldValue, MaterialMethodPart newValue) {
//                        GridPane grid = new GridPane();
//                        grid.setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));
                        if(newValue != null) {
//                            if (listConsign != null) {
//                                grid.setVgap(10);
//                                grid.setHgap(5);
//                                Label prop = new Label("Properties");
//                                prop.setRotate(-90);
//                                prop.setFont(Font.font("Arial", FontWeight.BOLD, 15));
//                                prop.setPadding(new Insets(0, 0, 0, 0));
//                                Group propHolder = new Group(prop);
//
//                                Label cons = new Label("Guidelines");
//                                cons.setRotate(-90);
//                                cons.setFont(Font.font("Arial", FontWeight.BOLD, 15));
//                                cons.setPadding(new Insets(0, 0, 0, 0));
//
//                                Group consHolder = new Group(cons);
//                                grid.add(propHolder, 0, 0);
//                                grid.add(consHolder, 0, 1);
//                                ColumnConstraints c = new ColumnConstraints();
//                                c.setPercentWidth(5);
//                                c.setHalignment(HPos.RIGHT);
//
//                                TableView tableView = new TableView();
//                                tableView.setEditable(false); // ne sera jamais éditable. On affiche un matériel (ou méthode lié)
//                                newValue.bindData(tableView);
//                                GridPane.setFillWidth(tableView, true);
//                                grid.add(tableView, 1, 0);
//
//                                if (listConsign.get(newValue) != null) { // on a des consignes
//                                    TableView tableConsign = new TableView();
//                                    tableConsign.editableProperty().bind(mainApp.getEditProperty());
//                                    listConsign.get(newValue).bindData(tableConsign);
//                                    grid.add(tableConsign, 1, 1);
//                                    GridPane.setFillWidth(tableConsign, true);
//
//                                }
//
//
//                                ColumnConstraints c1 = new ColumnConstraints();
//                                c1.setPercentWidth(95);
//
//                                grid.getColumnConstraints().addAll(c, c1);
//
//                            } else {
//                                Button addLine = new Button("", new ImageView(UITools.getImage("resources/images/add_16.png")));
//                                addLine.disableProperty().bind(mainApp.getEditProperty().not());
//                                addLine.setOnMouseClicked(event -> newValue.addLine());
                                TableView tableView = new TableView();
                                tableView.editableProperty().bind(mainApp.getEditProperty());
                                UITools.bindSimpleTable(newValue.getDataTable(), null,  tableView);
//                                newValue.bindData(tableView);
//                                grid.add(tableView, 0, 0);
//                                grid.add(addLine, 0, 1);
//                                addLine.setAlignment(Pos.BOTTOM_RIGHT);
//                                ColumnConstraints c = new ColumnConstraints();
//                                c.setFillWidth(true);
//                                c.setPercentWidth(100);
//                                grid.getColumnConstraints().addAll(c);

                            AnchorPane.setBottomAnchor(tableView, 0.0);
                            AnchorPane.setTopAnchor(tableView, 0.0);
                            AnchorPane.setLeftAnchor(tableView, 0.0);
                            AnchorPane.setRightAnchor(tableView, 0.0);
                            rightPane.getChildren().add(tableView);
                        }

//                        pane.setCenter(grid);
//                        TableView tableView = new TableView();
//                        newValue.bindData(tableView);
//                        pane.setCenter(tableView);
//
//                        if(listConsign != null) {
//                            if(listConsign.get(newValue) != null) { // on a des consignes
//                                TableView tableConsign = new TableView();
//                                listConsign.get(newValue).bindData(tableConsign);
//                                pane.setRight(tableConsign);
//                            }
//                        }
                    }
                });


    }
}
