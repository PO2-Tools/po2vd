/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.view.dataView;


import fr.inra.po2vocabmanager.MainApp;
import fr.inra.po2vocabmanager.model.DataNode;
import fr.inra.po2vocabmanager.utils.*;
import fr.inrae.po2engine.exception.AlreayLockException;
import fr.inrae.po2engine.exception.CantWriteException;
import fr.inrae.po2engine.exception.LocalLockException;
import fr.inrae.po2engine.exception.NotLoginException;
import fr.inrae.po2engine.externalTools.CloudConnector;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Datas;
import fr.inrae.po2engine.model.dataModel.*;
import fr.inrae.po2engine.utils.DataTools;
import fr.inrae.po2engine.utils.Report;
import fr.inrae.po2engine.utils.Tools;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.util.Duration;
import org.apache.commons.lang3.tuple.MutablePair;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public class DataOverviewController {
    @FXML
    AnchorPane infoPane;
    @FXML
    HBox statusBox;
    @FXML
    private TreeView<DataNode> dataTree;
    private TreeItem<DataNode> rootItem;
    private HashMap<GenericFile,ObservableList<DataNode>> listDataNode = new HashMap<>();

    private StringProperty fileName = new SimpleStringProperty();
    private Data currentData = null;

    private MainApp mainApp;
    private BooleanProperty canEdit = new SimpleBooleanProperty(false);
    private BooleanProperty modified = new SimpleBooleanProperty(false);
    private BooleanProperty syncCloud = new SimpleBooleanProperty(false);
    private IntegerProperty majorVersion = new SimpleIntegerProperty(0);
    private IntegerProperty minorVersion = new SimpleIntegerProperty(0);

    // overview
    AnchorPane projectOverview;
    ProjectOverviewController projectControler;
    AnchorPane processOverview;
    ProcessOverviewController processControler;
    ScrollPane stepOverview;
    StepOverviewController stepControler;
    ScrollPane observationOverview;
    ObservationOverviewController observationControler;
    AnchorPane itineraryOverview;
    ItineraryOverviewController itineraryControler;



    @FXML
    private void initialize() {
        dataTree.setShowRoot(true);
        dataTree.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {if (newValue != null) {showNodeDetails(newValue.getValue(), oldValue != null ? oldValue.getValue():null);}});
//        initProjectOverview();
//        initProcessOverview();
//        initStepOverview();
//        initObservationOverview();
//        initItineraryOverview();
    }

    public void initProcessOverview() {
        try {
            FXMLLoader loader = UITools.getFXMLLoader("view/dataView/ProcessOverview.fxml");
            processOverview = loader.load();
            AnchorPane.setTopAnchor(processOverview, 10.0);
            AnchorPane.setLeftAnchor(processOverview, 10.0);
            AnchorPane.setRightAnchor(processOverview, 10.0);
            AnchorPane.setBottomAnchor(processOverview, 10.0);
            processControler = loader.getController();
            MainApp.logger.debug("Initialisation du process Overview OK");
        } catch (IOException e) {
            MainApp.logger.error(e.getClass() + ":" + e.getMessage(), e);
        }
    }

    public void initStepOverview() {
        try {
            FXMLLoader loader = UITools.getFXMLLoader("view/dataView/StepOverview.fxml");
            stepOverview = loader.load();
            AnchorPane.setTopAnchor(stepOverview, 10.0);
            AnchorPane.setLeftAnchor(stepOverview, 10.0);
            AnchorPane.setRightAnchor(stepOverview, 10.0);
            AnchorPane.setBottomAnchor(stepOverview, 10.0);
            stepControler = loader.getController();
            MainApp.logger.debug("Initialisation du step Overview OK");
        } catch (IOException e) {
            MainApp.logger.error(e.getClass() + ":" + e.getMessage(), e);
        }
    }

    public void initObservationOverview() {
        try {
            FXMLLoader loader = UITools.getFXMLLoader("view/dataView/ObservationOverview.fxml");
            observationOverview = loader.load();
            AnchorPane.setTopAnchor(observationOverview, 10.0);
            AnchorPane.setLeftAnchor(observationOverview, 10.0);
            AnchorPane.setRightAnchor(observationOverview, 10.0);
            AnchorPane.setBottomAnchor(observationOverview, 10.0);
            observationControler = loader.getController();
            MainApp.logger.debug("Initialisation du observation Overview OK");
        } catch (IOException e) {
            MainApp.logger.error(e.getClass() + ":" + e.getMessage(), e);
        }
    }

    public void initItineraryOverview() {
        try {
            FXMLLoader loader = UITools.getFXMLLoader("view/dataView/ItineraryOverview.fxml");
            itineraryOverview = loader.load();
            AnchorPane.setTopAnchor(itineraryOverview, 10.0);
            AnchorPane.setLeftAnchor(itineraryOverview, 10.0);
            AnchorPane.setRightAnchor(itineraryOverview, 10.0);
            AnchorPane.setBottomAnchor(itineraryOverview, 10.0);
            itineraryControler = loader.getController();
            MainApp.logger.debug("Initialisation du itinerary Overview OK");
        } catch (IOException e) {
            MainApp.logger.error(e.getClass() + ":" + e.getMessage(), e);
        }
    }

    public void initProjectOverview() {
        try {
            FXMLLoader loader = UITools.getFXMLLoader("view/dataView/ProjectOverview.fxml");
            projectOverview = loader.load();
            AnchorPane.setTopAnchor(projectOverview, 10.0);
            AnchorPane.setLeftAnchor(projectOverview, 10.0);
            AnchorPane.setRightAnchor(projectOverview, 10.0);
            AnchorPane.setBottomAnchor(projectOverview, 10.0);
            projectControler = loader.getController();
            MainApp.logger.debug("Initialisation du project Overview OK");
        } catch (IOException e) {
            MainApp.logger.error(e.getClass() + ":" + e.getMessage(), e);
        }
    }

    public IntegerProperty getMajorVersion() { return majorVersion;}
    public IntegerProperty getMinorVersion() { return minorVersion;}


    public ObservableList<DataNode> getDataNodes(GenericFile file) {
        if(listDataNode.containsKey(file)) {
            return listDataNode.get(file);
        }
        return FXCollections.observableArrayList();
    }

    public DataNode getDataNode(GenericFile file) {
        if(listDataNode.containsKey(file)) {
            return listDataNode.get(file).stream().findAny().orElse(null);
        } else {
            return null;
        }
    }

    public DataNode getDataNode(GenericFile file, ItineraryFile iti) {
        if(listDataNode.containsKey(file)) {
            return listDataNode.get(file).stream().filter(dataNode -> dataNode.getItineraryFile().equals(iti)).findFirst().orElse(null);
        } else {
            return null;
        }
    }

    public void showNodeDetails(GenericFile newFile, GenericFile oldFile) {
        if(listDataNode.containsKey(newFile)) {
            DataNode newNode = listDataNode.get(newFile).stream().findAny().orElse(null);
            DataNode oldNode = null;
            if(listDataNode.containsKey(oldFile)) {
                oldNode = listDataNode.get(oldFile).stream().findAny().orElse(null);
            }
            showNodeDetails(newNode, oldNode);
        }
    }
    public void showNodeDetails(DataNode newNode, DataNode oldNode) {
        infoPane.getChildren().clear();
        if(oldNode != null) {
            if(oldNode.getFile() != null) {
                oldNode.getFile().unbind();
                oldNode.getFile().setSelectedInTree(false);
            }
//            if(oldNode.getItineraryFile() != null) {
//                oldNode.getItineraryFile().getItiPart().setSelectedInTree(false);
//            }
        }
        refreshTree();
        switch (newNode.getType()) {
            case PROJECT:
                newNode.getFile().setSelectedInTree(true);
                projectControler.setMainApp(this.mainApp);
                infoPane.getChildren().add(projectOverview);
                break;
            case PROCESS:
                newNode.getFile().setSelectedInTree(true);
                processControler.setMainApp(this.mainApp);
                GeneralFile currentProcess = (GeneralFile)newNode.getFile();
                if(!currentProcess.isLoaded()) {
                    // Checking available RAM
                    Boolean startLoad = true;

                    if(Tools.getUsedMemory() > 80.0) { // in percentage
                        MainApp.getRuntime().gc();
                    }
                    if(Tools.getUsedMemory() > 80.0) {
                        MainApp.logger.debug("not enouth memory available !!!");
                        Alert mess = new Alert(Alert.AlertType.CONFIRMATION);
                        mess.initOwner(MainApp.primaryStage);
                        mess.initModality(Modality.WINDOW_MODAL);
                        mess.setTitle("Not enough memory");
                        mess.setHeaderText("Not enough memory");
                        mess.setContentText("It seems that you are using " + Tools.getUsedMemory()+ " % of your memory. Would you like continue anyway ?");
                        Optional<ButtonType> result = mess.showAndWait();
                        if (result.get() == ButtonType.OK){
                            startLoad = true;
                        } else {
                            startLoad = false;
                        }
                    }

                    if(startLoad) {
                        Task<Boolean> worker = new Task<Boolean>() {
                            @Override
                            protected Boolean call() {
                                return currentData.analyseProcess(currentProcess);
                            }
                        };
                        worker.setOnSucceeded(event -> {
//                            rootItem = new RecursiveTreeItem<>(root, DataNode::getGraphic, DataNode::getSubNode, null);
                            buildProcessTree(currentProcess);
                            DataNode gfNode = getDataNode(currentProcess);
                            if(gfNode != null) {
                                gfNode.updateImage();
                            }
                            processControler.showNodeDetails(newNode);
                            infoPane.getChildren().add(processOverview);

                        });
                        new Thread(worker).start();
                    }
                } else {
                    processControler.showNodeDetails(newNode);
                    infoPane.getChildren().add(processOverview);
                }
                break;
            case STEP:
                newNode.getFile().setSelectedInTree(true);
                stepControler.setMainApp(this.mainApp);
                stepControler.showNodeDetails(newNode, newNode.getItineraryFile().getProcessFile().getCurrentReplicate());
                infoPane.getChildren().add(stepOverview);
                break;
            case OBSERVATION:
                newNode.getFile().setSelectedInTree(true);
                observationControler.setMainApp(this.mainApp);
                observationControler.showNodeDetails(newNode, newNode.getItineraryFile().getProcessFile().getCurrentReplicate());
                infoPane.getChildren().add(observationOverview);
                break;
            case ITINERARY:
                newNode.getFile().setSelectedInTree(true);
                infoPane.getChildren().add(itineraryOverview);
                itineraryControler.setMainApp(this.mainApp);
                itineraryControler.showNodeDetails(newNode);
                break;
            default:
                break;
        }


    }

    public void buildProjectTree(ProjectFile projectFile) {
        rootItem.getValue().getSubNodeNoSort().clear();
        for (GeneralFile gf : projectFile.getData().getListGeneralFile()) {
            DataNode gfNode = new DataNode(DataNodeType.PROCESS);
            MainApp.getDataControler().addNode(gf, gfNode, rootItem.getValue());
        }
    }

    public void buildProcessTree(GeneralFile currentProcess) {
        if(currentProcess != null) {
            DataNode processNode = getDataNode(currentProcess);
            if(processNode != null) {
                // on vide les subNode

                for(ItineraryFile itiF : currentProcess.getItinerary()) {
                    buildItineraryTree(itiF);
                }
            }
        }

    }

    public ItineraryOverviewController getItineraryControler() {
        return this.itineraryControler;
    }

    public void buildItineraryTree(ItineraryFile itiF) {
        DataNode processNode = getDataNode(itiF.getProcessFile());
        DataNode itineraryNode = getDataNode(itiF);
        if(itineraryNode != null) {
            List<DataNode> tempList = FXCollections.observableArrayList(itineraryNode.getSubNodeNoSort());
            for(DataNode allNode : tempList) {
                MainApp.getDataControler().deleteDataNode(allNode);
            }
        } else {
            itineraryNode = new DataNode(DataNodeType.ITINERARY);
            addNode(itiF, itineraryNode, processNode);
        }

        // node observation.
        for(ObservationFile obf : itiF.getListObservation()) {
            DataNode dataNodeObs = new DataNode(DataNodeType.OBSERVATION);
            addNode(obf, dataNodeObs ,itineraryNode);
        }
        // node step (and step obs)
        for(StepFile stepF : itiF.getListStep()) {
            buildStepTree(stepF, itiF);
        }
    }

    private DataNode buildStepTree(StepFile s, ItineraryFile i) {
        if(s != null) {
            DataNode dataNodeStep = null;
//            Boolean reuseNode = false;
            // checlink step node alrready existe (for this iti)
            List<DataNode> oldNodes = getDataNodes(s);
            if(oldNodes != null) {
                dataNodeStep = oldNodes.stream().filter(dataNode -> dataNode.getItineraryFile().equals(i)).findAny().orElse(null);
            }
            if(dataNodeStep != null) {
                return dataNodeStep;
            }

            dataNodeStep = new DataNode(DataNodeType.STEP);
            dataNodeStep.setItineraryFile(i);
            // creating obs step node
            for(ObservationFile obs : s.getObservationFiles()) {
                DataNode nodeObs = new DataNode(DataNodeType.OBSERVATION);
                nodeObs.setItineraryFile(i);
                addNode(obs, nodeObs ,dataNodeStep);
            }

            // searching parent (si s est une sous etape alors parent etape sinon itineraire direct
            DataNode parent;
            if(s.getFather() != null) {
                parent = buildStepTree(s.getFather(), i);
            } else {
                parent = MainApp.getDataControler().getDataNode(i);
            }
            addNode(s, dataNodeStep, parent);

            return dataNodeStep;
        }
        return null;
    }



    public void selectNode(GenericFile f) {
        if(f != null) {
            DataNode node = listDataNode.get(f).stream().findAny().orElse(null);
            selectNode(node);
        }
    }
    public void selectNode(DataNode node) {
        if(node != null) {
            TreeItem<DataNode> treeItem = searchNode(rootItem, node);
//            dataTree.getSelectionModel().clearSelection();
            dataTree.getSelectionModel().select(treeItem);
            if(treeItem != null) {
                treeItem.setExpanded(true);
            }
        }
    }

    private TreeItem<DataNode> searchNode(TreeItem<DataNode> item, DataNode node) {

        if (item.getValue().equals(node)) {
            return item;
        } else {
            for (TreeItem<DataNode> n : item.getChildren()) {
                TreeItem<DataNode> maybe = searchNode(n, node);
                if (maybe != null) {
                    return maybe;
                }
            }
        }
        return null;
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    public void setStatusBox(StatusBox statusBox) {
        this.statusBox.setSpacing(statusBox.getSpacing());
        this.statusBox.getChildren().clear();
        this.statusBox.getChildren().addAll(statusBox.getListNodes());
    }

    public void startOpenFile(String dataName) {
        Data data = Datas.getData(dataName);
        if (data != null) {
            Task<Boolean> task = new Task<Boolean>() {

                @Override
                protected Boolean call() throws Exception {
                    return data.load();
                }
            };

            task.setOnSucceeded(event1 -> {
                if (data.isLoaded().getValue()) {

                    Task<Boolean> worker = createWorkerDataLoad(data);
                    worker.setOnSucceeded(event -> {
                        if(worker.getValue()) {
                            initProjectOverview();
                            initProcessOverview();
                            initStepOverview();
                            initObservationOverview();
                            initItineraryOverview();

                            currentData = data;
                            currentData.setModified(false);
                            majorVersion.bind(currentData.getMajorVersion());
                            minorVersion.bind(currentData.getMinorVersion());
                            currentData.modifiedProperty().addListener((observableValue, aBoolean, t1) -> {
                                Platform.runLater(() -> {
                                    modified.set(t1);
                                });
                            });

                            setTree(rootItem);
                            fileName.bind(currentData.getName());
                            try {
                                MutablePair<String, Date> versionDate = CloudConnector.getDavVersion(currentData);
                                if(versionDate.getLeft() != null) {
                                    currentData.setCurrentVersionLoaded(versionDate.getLeft());
                                }
                                syncCloud.unbind();
                                syncCloud.bind(currentData.isSynchroCloud());
                                currentData.isSynchro();
                            } catch (URISyntaxException | IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            // error de chargement !!!
                            MainApp.logger.error("Erreur lors du chargement de " + dataName);
                        }

                    });
                    new Thread(worker).start();
                } else {
                    Alert dataNotAvailable = new Alert(Alert.AlertType.ERROR);
                    dataNotAvailable.setGraphic(new ImageView(UITools.getImage("resources/images/error.png")));
                    dataNotAvailable.setContentText("You don't have permission to load " + data.getName().get());
                    dataNotAvailable.initOwner(MainApp.primaryStage);
                    dataNotAvailable.showAndWait();
                }
            });


            if (!data.isLocal() && !data.isPublic() && !CloudConnector.isInitUser()) {
                if (UITools.startLogin()) {
                    new Thread(task).start();
                }
            } else {
                new Thread(task).start();
            }
        }
    }
    public void refreshTree(){
        dataTree.refresh();
    }

    public void addNode(GenericFile file, DataNode node, DataNode father) {
        if(!listDataNode.containsKey(file)) {
            listDataNode.put(file, FXCollections.observableArrayList());
        }
        listDataNode.get(file).add(node);
        node.setFile(file);
        if(node.getType().equals(DataNodeType.ITINERARY)) {
            node.setItineraryFile((ItineraryFile) file);
        }
        if(father != null && father.getItineraryFile() != null) {
            node.setItineraryFile(father.getItineraryFile());
        }
        node.nameProperty().bindBidirectional(file.getNameProperty());
        if(father != null) {
            father.addSubNode(node);
            node.getFathers().add(father);
        }
    }

    public void setTree(TreeItem<DataNode> tree) {
        dataTree.setRoot(tree);
        dataTree.setCellFactory(factory -> new DataTreeCell(this.mainApp));
        infoPane.getChildren().clear();
    }
     public Task<Boolean> createWorkerDataLoad(Data data) {
         return new Task<Boolean>() {

             @Override
             protected Boolean call() throws Exception {

                 Report report = DataTools.analyseFiles(data);
                 if(report.success()) {
                     ProjectFile project = data.getProjectFile();
                     DataNode root = new DataNode("root", project, DataNodeType.PROJECT);
                     rootItem = new RecursiveTreeItem<>(root, DataNode::getGraphic, DataNode::getSubNode, null);
                     MainApp.getDataControler().clearDataNode();
                     MainApp.getDataControler().addNode(project, root, null);
                     MainApp.getDataControler().buildProjectTree(project);
                     return true;
                 } else {
                     Alert error = new Alert(Alert.AlertType.ERROR);
                     error.setTitle("Failed to load data");
                     error.setContentText(report.prettyPrintError());
                     error.initModality(Modality.WINDOW_MODAL);
                     error.initOwner(MainApp.primaryStage);
                     error.show();
                     return false;
                 }
             }

         };
     }

    public StringProperty getFileName() {
        return fileName;
    }

    public BooleanProperty getCanEditProperty() {
        return canEdit;
    }
    public BooleanProperty getSyncCloudProperty() {
        return syncCloud;
    }


    public Data getCurrentData() {
        return currentData;
    }

    public Boolean canEdit() {
        return canEdit.getValue();
    }

    public BooleanProperty getModifiedProperty() {
        return modified;
    }
//    public void modified(Boolean mod) {
//         modified.setValue(mod);
//    }
    public Boolean isModified() {
        return modified.getValue();
    }

    public Boolean tryLock() {
        try {
            canEdit.set(currentData.lockMode());
            return true;
        } catch (NotLoginException e) {
            if(UITools.startLogin()) {
                return tryLock();
            }
            return false;
        }catch (AlreayLockException e) {
            Platform.runLater(()-> {
                Alert alreadyLock = new Alert(Alert.AlertType.INFORMATION);
                alreadyLock.setHeaderText(currentData.getName().get() + " is already locked by " + e.getLockName() + " " + e.getDate());
                alreadyLock.initOwner(MainApp.primaryStage);
                alreadyLock.showAndWait();
            });

            MainApp.logger.info("Data already lock");
        } catch (LocalLockException e) {
            Platform.runLater(()-> {
                Alert alreadyLock = new Alert(Alert.AlertType.INFORMATION);
                alreadyLock.setHeaderText(currentData.getName().get() + " is local. Edition is not allowed");
                alreadyLock.initOwner(MainApp.primaryStage);
                alreadyLock.showAndWait();
            });
            MainApp.logger.info("can't lock local onto");
        } catch (IOException e) {
            Platform.runLater(()-> {
                MainApp.logger.error(e.getClass() + ":" + e.getMessage(), e);
                Alert exception = new Alert(Alert.AlertType.ERROR);
                exception.setTitle("Exception Dialog");
                exception.setContentText(e.getMessage());
                exception.showAndWait();
            });
        } catch (URISyntaxException e) {
            Platform.runLater(()-> {
                Alert exception = new Alert(Alert.AlertType.ERROR);
                exception.setTitle("Exception Dialog");
                exception.setContentText(e.getMessage());
                exception.showAndWait();
            });
            MainApp.logger.error(e.getClass() + ":" + e.getMessage(), e);
        } catch (CantWriteException e) {
            Platform.runLater(()-> {
                Alert cantwrite = new Alert(Alert.AlertType.INFORMATION);
                cantwrite.setHeaderText("You have insufficient rights to access data " + currentData.getName().get());
                cantwrite.initOwner(MainApp.primaryStage);
                cantwrite.showAndWait();
            });
            MainApp.logger.info("insufficient right");
        }
        return false;
    }

    public void enterEditionMode() {
        if (!CloudConnector.isInitUser()) {
            UITools.startLogin();
        }
        if(CloudConnector.isInitUser()) {
            Task<Boolean> task = new Task<Boolean>() {
                @Override
                protected Boolean call() {
//                    try {
                        if (currentData != null && !currentData.isLocked()) {
                            return tryLock();
                        }
//                        return true;
//                    } catch (AlreayLockException e) {
//                        Platform.runLater(()-> {
//                            Alert alreadyLock = new Alert(Alert.AlertType.INFORMATION);
//                            alreadyLock.setHeaderText(currentData.getName().get() + " is already locked by " + e.getLockName() + " " + e.getDate());
//                            alreadyLock.initOwner(MainApp.primaryStage);
//                            alreadyLock.showAndWait();
//                        });
//
//                        MainApp.logger.info("Data already lock");
//                    } catch (LocalLockException e) {
//                        Platform.runLater(()-> {
//                            Alert alreadyLock = new Alert(Alert.AlertType.INFORMATION);
//                            alreadyLock.setHeaderText(currentData.getName().get() + " is local. Edition is not allowed");
//                            alreadyLock.initOwner(MainApp.primaryStage);
//                            alreadyLock.showAndWait();
//                        });
//                        MainApp.logger.info("can't lock local onto");
//                    } catch (IOException e) {
//                        Platform.runLater(()-> {
//                            MainApp.logger.error(e.getClass() + ":" + e.getMessage(), e);
//                            Alert exception = new Alert(Alert.AlertType.ERROR);
//                            exception.setTitle("Exception Dialog");
//                            exception.setContentText(e.getMessage());
//                            exception.showAndWait();
//                        });
//                    } catch (URISyntaxException e) {
//                        Platform.runLater(()-> {
//                            Alert exception = new Alert(Alert.AlertType.ERROR);
//                            exception.setTitle("Exception Dialog");
//                            exception.setContentText(e.getMessage());
//                            exception.showAndWait();
//                        });
//                        MainApp.logger.error(e.getClass() + ":" + e.getMessage(), e);
//                    } catch (CantWriteException e) {
//                        Platform.runLater(()-> {
//                            Alert cantwrite = new Alert(Alert.AlertType.INFORMATION);
//                            cantwrite.setHeaderText("You have insufficient rights to access data " + currentData.getName().get());
//                            cantwrite.initOwner(MainApp.primaryStage);
//                            cantwrite.showAndWait();
//                            });
//                        MainApp.logger.info("insufficient right");
//                    }
                    return false;
                }
            };
            task.setOnSucceeded(workerStateEvent -> {
            });
            new Thread(task).start();
        }
    }

    public Boolean saveData() {
        try {
            if (!currentData.getProjectFile().saveData(false)) {
                MainApp.logger.error("echec de la sauvegarde data");
                Alert errorSave = new Alert(Alert.AlertType.ERROR);
                errorSave.initModality(Modality.WINDOW_MODAL);
                errorSave.initOwner(MainApp.primaryStage);
                errorSave.setTitle("Error");
                errorSave.setHeaderText("An error occured during save");
                errorSave.show();
                return false;
            } else {
                // on zip et on synchronise le fichier zip des données
                if (CloudConnector.update(currentData)) {
                    MainApp.logger.debug("sauvegarde data reussi");
                    Platform.runLater(() -> currentData.setModified(false));
                    return true;
                } else {
                    MainApp.logger.error("echec de la sauvegarde data");
                    Alert errorSave = new Alert(Alert.AlertType.ERROR);
                    errorSave.initModality(Modality.WINDOW_MODAL);
                    errorSave.initOwner(MainApp.primaryStage);
                    errorSave.setTitle("Error");
                    errorSave.setHeaderText("An error occured during save");
                    errorSave.show();
                    return false;
                }
            }
        } catch (Exception e) {
            MainApp.logger.error(e.getClass() + ":" + e.getMessage() + " - task save data failed", e);
            return false;
        }
    }

    public  TreeView<DataNode> getTree() {
        return dataTree;
    }

    public void unloadControler() {
        processControler.unloadControler();
        observationControler.unloadControler();
        stepControler.unloadControler();
        itineraryControler.unloadControler();
    }

    public void clearDataNode() {
        listDataNode.clear();
    }

    public void deleteDataNode(DataNode node) {
        List<DataNode> tempList = FXCollections.observableArrayList(node.getSubNodeNoSort());
        tempList.forEach(dataNode -> {
            deleteDataNode(dataNode);
//            dataNode.clearSubNode();
        });
        for (DataNode f : node.getFathers()) {
            f.getSubNodeNoSort().remove(node);
        }
        listDataNode.values().forEach(dataNodes -> {
            dataNodes.removeIf(dataNode1 -> dataNode1.equals(node));
        });
    }

    public void deleteDataNode(GenericFile gf) {
        List<DataNode> l = FXCollections.observableArrayList(getDataNodes(gf));
        if(l != null) {
            for (DataNode dn : l) {
                deleteDataNode(dn);
//                List<DataNode> dnSon = dn.getSubNode();
//                dnSon.forEach(dataNode -> {
//                    deleteDataNode(dataNode.getFile());
//                    dataNode.clearSubNode();
//                });
                //            List<DataNode> dnFathers = dn.getFathers();
                //
                //           for(DataNode dnf : dnFathers) {
                //               deleteDataNode(dnf.getFile());
                //               dnf.removeSubNode(dn);
                //           }
            }
        }
//        listDataNode.remove(gf);
    }

    public void highlightStep(StepFile step, ItineraryFile itineraryFile) {
        DataNode node = getDataNode(step, itineraryFile);
        if(node != null) {
           highlightNode(node);
        }
    }

    public void highlightNode(DataNode node) {
        if(node != null) {
            TreeItem<DataNode> treeItem = searchNode(rootItem, node);
            if(treeItem != null) {
                openTreeItem(treeItem);
                Platform.runLater(() -> {
                    Timeline timeline = new Timeline(
                            new KeyFrame(Duration.seconds(0.2), evt -> {
                                if(treeItem.getGraphic().getParent() != null) {
                                    treeItem.getGraphic().getParent().setVisible(false);
                                }
                            }),
                            new KeyFrame(Duration.seconds( 0.4), evt -> {
                                if(treeItem.getGraphic().getParent() != null) {
                                    treeItem.getGraphic().getParent().setVisible(true);
                                }
                            }));
                    timeline.setCycleCount(10);
                    timeline.play();
                });

            }
        }
    }

    public void openTreeItem(TreeItem item) {
        if(item != null) {
            openTreeItem(item.getParent());
            item.setExpanded(true);
        }
    }
}
