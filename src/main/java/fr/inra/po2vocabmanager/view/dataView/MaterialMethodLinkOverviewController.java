/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.view.dataView;

import fr.inra.po2vocabmanager.MainApp;
import fr.inra.po2vocabmanager.utils.FilterableTreeItem;
import fr.inra.po2vocabmanager.utils.UITools;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.model.dataModel.ObservationFile;
import fr.inrae.po2engine.model.dataModel.StepFile;
import fr.inrae.po2engine.model.partModel.MaterialMethodLinkPart;
import fr.inrae.po2engine.model.partModel.MaterialMethodPart;
import fr.inrae.po2engine.utils.DataPartType;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.util.Callback;

import java.util.Optional;

public class MaterialMethodLinkOverviewController {



    @FXML
    Button add;
    @FXML
    Button del;
    @FXML
    ListView<MaterialMethodLinkPart> listView;
    @FXML
    SplitPane pane;
    @FXML
    AnchorPane rightPane;


    private MainApp mainApp;
    private KeyWords type = null;

    @FXML
    private void initialize() {

    }

    public void setMainApp(MainApp mainApp, KeyWords type) {
        this.mainApp = mainApp;
        this.type = type;
        listView.editableProperty().bind(mainApp.getEditProperty());
        add.disableProperty().bind(mainApp.getEditProperty().not());
        add.setGraphic(new ImageView(UITools.getImage("resources/images/add_16.png")));

        del.disableProperty().bind(Bindings.or(mainApp.getEditProperty().not(), listView.getSelectionModel().selectedItemProperty().isNotNull().not()));
        del.setGraphic(new ImageView(UITools.getImage("resources/images/del_16.png")));

    }

    public void showItem(ObservationFile obsFile) {
        showItem(null, obsFile);
    }

    public void showItem(StepFile stepFile) {
        showItem(stepFile, null);
    }

//    public void showItem(ObservableList<MaterialMethodLinkPart> listMaterialMethodLink) {
    private void showItem(StepFile stepFile, ObservationFile obsFile) {
        rightPane.getChildren().clear();
        if(stepFile == null && obsFile == null) {
            return;
        }

        listView.setItems(stepFile != null ? stepFile.getListMaterialMethodRO() : obsFile.getListMaterialMethodRO());
//        if(listConsign == null) {
//            // on supprime complétement le matiel (on est sur la liste générale.
            if(stepFile != null) {
                del.setOnMouseClicked(event -> { stepFile.removeMaterialMethod(listView.getSelectionModel().getSelectedItem());MainApp.getDataControler().getCurrentData().setModified(true);});
            } else {
                del.setOnMouseClicked(event -> { obsFile.removeMaterialMethod(listView.getSelectionModel().getSelectedItem());MainApp.getDataControler().getCurrentData().setModified(true);});
            }
//
            add.setOnMouseClicked(event -> {

                MaterialMethodPart mmpRoot = new MaterialMethodPart(mainApp.getDataControler().getCurrentData().getProjectFile(), DataPartType.MATERIAL_RAW, false);
                MaterialMethodPart mmpRootMat = new MaterialMethodPart(mainApp.getDataControler().getCurrentData().getProjectFile(), DataPartType.MATERIAL_RAW, false);
                mmpRootMat.setId("Materials");
                MaterialMethodPart mmpRootMet = new MaterialMethodPart(mainApp.getDataControler().getCurrentData().getProjectFile(), DataPartType.METHOD_RAW, false);
                mmpRootMet.setId("Methods");

                FilterableTreeItem<MaterialMethodPart> rootMaterialMethod = new FilterableTreeItem<>(mmpRoot);
                FilterableTreeItem<MaterialMethodPart> rootMaterial = new FilterableTreeItem<>(mmpRootMat);
                FilterableTreeItem<MaterialMethodPart> rootMethod = new FilterableTreeItem<>(mmpRootMet);
                rootMaterialMethod.getInternalChildren().add(rootMaterial);
                rootMaterialMethod.getInternalChildren().add(rootMethod);


                mainApp.getDataControler().getCurrentData().getMaterialMethodFile().getMaterialPart().forEach(m -> {
                    FilterableTreeItem<MaterialMethodPart> tempMat = new FilterableTreeItem<>(m);
                    rootMaterial.getInternalChildren().add(tempMat);
                });

                mainApp.getDataControler().getCurrentData().getMaterialMethodFile().getMethodPart().forEach(m -> {
                    FilterableTreeItem<MaterialMethodPart> tempMet = new FilterableTreeItem<>(m);
                    rootMethod.getInternalChildren().add(tempMet);
                });

                TreeView<MaterialMethodPart> treeView = new TreeView<>(rootMaterialMethod);
                treeView.setShowRoot(false);
//                importFilter.disableProperty().bind(rbImport.selectedProperty().not());


//                for(String pf : Platform.getListPlatform().keySet()) {
//                    PlatformMaterial onlyPl = new PlatformMaterial();
//                    onlyPl.setOnlyPlatform(true);
//                    onlyPl.setName(pf);
//                    FilterableTreeItem<PlatformMaterial> plat = new FilterableTreeItem<>(onlyPl);
//                    rootMaterial.getInternalChildren().add(plat);
//                    for(PlatformMaterial mat : Platform.getListPlatform().get(pf)) {
//                        FilterableTreeItem<PlatformMaterial> mater = new FilterableTreeItem<>(mat);
//                        plat.getInternalChildren().add(mater);
//                    }
//                }

//                rootMaterial.predicateProperty().bind(Bindings.createObjectBinding(() -> {
//                    if (importFilter.getText() == null || importFilter.getText().isEmpty())
//                        return null;
//                    return TreeItemPredicate.create(value -> value.toString().toLowerCase().contains(importFilter.getText().toLowerCase()));
//                }, importFilter.textProperty()));

//                TreeView<PlatformMaterial> treeView = new TreeView<>(rootMaterial);
//                treeView.setShowRoot(false);
//                treeView.disableProperty().bind(rbImport.selectedProperty().not());
//                importFilter.disableProperty().bind(rbImport.selectedProperty().not());
//




                Dialog<MaterialMethodPart> newMatMetLink = new Dialog();
                newMatMetLink.setResizable(true);
                String title = "material or method used";
                newMatMetLink.setTitle(title);
                newMatMetLink.initModality(Modality.APPLICATION_MODAL);
                newMatMetLink.initOwner(listView.getScene().getWindow());
                newMatMetLink.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
                newMatMetLink.getDialogPane().setPrefSize(400, 300);
//                ComboBox<MaterialMethodPart> combo = new ComboBox<>();
//                combo.setEditable(false);
//                ObservableList<MaterialMethodPart> listMatMet = FXCollections.observableArrayList();
//                listMatMet.addAll(mainApp.getDataControler().getCurrentData().getMaterialMethodFile().getMaterialPart());
//                listMatMet.addAll(mainApp.getDataControler().getCurrentData().getMaterialMethodFile().getMethodPart());
//                combo.setItems(listMatMet);
//                combo.setCellFactory(new Callback<ListView<MaterialMethodPart>, ListCell<MaterialMethodPart>>() {
//                    @Override
//                    public ListCell<MaterialMethodPart> call(ListView<MaterialMethodPart> l) {
//                        return new ListCell<MaterialMethodPart>() {
//
//
//                            @Override
//                            protected void updateItem(MaterialMethodPart item, boolean empty) {
//                                super.updateItem(item, empty);
//                                if (item == null || empty) {
//                                    setGraphic(null);
//                                } else {
//                                    setText(item.getOntoType().getValue().get() + " ("+item.getID().getValue().get()+")");
//                                }
//                            }
//                        } ;
//                    }
//                });
                newMatMetLink.getDialogPane().lookupButton(ButtonType.OK).disableProperty().bind(
                        Bindings.or(treeView.getSelectionModel().selectedItemProperty().isNull(),
                                    Bindings.or(treeView.getSelectionModel().selectedItemProperty().isEqualTo(rootMaterial),
                                                treeView.getSelectionModel().selectedItemProperty().isEqualTo(rootMethod))));

                newMatMetLink.getDialogPane().setContent(treeView);
                newMatMetLink.setResultConverter(dialogButton -> {
                    if(dialogButton == ButtonType.OK) {
                        return treeView.getSelectionModel().getSelectedItem().getValue();
                    }
                    return null;
                });
                Optional<MaterialMethodPart> result = newMatMetLink.showAndWait();
                result.ifPresent(newMatMetID -> {
                    MaterialMethodLinkPart newLink = new MaterialMethodLinkPart(newMatMetID);
                    Boolean addIsOk = false;
                    if(stepFile != null) {
                        addIsOk = stepFile.addMaterialMethod(newLink);
                    } else {
                        addIsOk = obsFile.addMaterialMethod(newLink);
                    }

                    if(!addIsOk) {
                        // il y a deja 1 materiel.
                        Dialog alreadyMat = new Alert(Alert.AlertType.ERROR);
                        alreadyMat.initOwner(MainApp.primaryStage);
                        alreadyMat.initModality(Modality.WINDOW_MODAL);
                        alreadyMat.setTitle("Material already exist");
                        alreadyMat.setContentText("A materiel is already present. Only 1 is allowed");
                        alreadyMat.show();
                    } else {
                        MainApp.getDataControler().getCurrentData().setModified(true);
                    }


//                    if(newMatMetID.isMaterial() && listMaterialMethodLink.stream().filter(m -> m.getMaterialMethodPartPart().isMaterial()).findAny().isPresent()) {
//                        Dialog alreadyMat = new Alert(Alert.AlertType.ERROR);
//                        alreadyMat.initOwner(MainApp.primaryStage);
//                        alreadyMat.initModality(Modality.WINDOW_MODAL);
//                        alreadyMat.setTitle("Material already exist");
//                        alreadyMat.setContentText("A materiel is already present. Only 1 is allowed");
//                        alreadyMat.show();
//                    } else {
//                        // on ajoute
//                        MaterialMethodLinkPart newLink = new MaterialMethodLinkPart(newMatMetID);
//                        listMaterialMethodLink.add(newLink);
//                        MainApp.getDataControler().getCurrentData().setModified(true);
//                    }


                });

            });

        listView.setCellFactory(new Callback<ListView<MaterialMethodLinkPart>, ListCell<MaterialMethodLinkPart>>() {

            @Override
            public ListCell<MaterialMethodLinkPart> call(ListView<MaterialMethodLinkPart> p) {

                ListCell<MaterialMethodLinkPart> cell = new ListCell<MaterialMethodLinkPart>() {

                    @Override
                    protected void updateItem(MaterialMethodLinkPart t, boolean bln) {
                        super.updateItem(t, bln);
                        if(t == null || bln) {
                            textProperty().unbind();
                            setText(null);
                            styleProperty().unbind();
                            setStyle("");
                            setGraphic(null);
                        } else {
                            if(t.getMaterialMethodPartPart() == null) {
                                // le materiel ou la méthode n'existe pas !!!!!
                                setText("not exist");
                                setStyle("-fx-border-color: red;");
                            } else {
                                textProperty().bind(Bindings.concat(t.getMaterialMethodPartPart().getOntoType()," (", t.getMaterialMethodPartPart().getID(),")"));
                                if(t.getMaterialMethodPartPart().isMaterial()) {
                                    setGraphic(new ImageView(UITools.getImage("resources/images/material_16.png")));
                                } else {
                                    setGraphic(new ImageView(UITools.getImage("resources/images/method_16.png")));
                                }

                            }
                        }
                    }

                };

                return cell;
            }
        });

        listView.getSelectionModel().selectedItemProperty()
                .addListener(new ChangeListener<MaterialMethodLinkPart>() {
                    public void changed(ObservableValue<? extends MaterialMethodLinkPart> observable,
                                        MaterialMethodLinkPart oldValue, MaterialMethodLinkPart newValue) {
                        GridPane grid = new GridPane();
//                        grid.setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));
                        if(newValue != null) {

                                grid.setVgap(10);
                                grid.setHgap(5);

                                GridPane paneAddLabel = new GridPane();


                                Label cons = new Label("Parameters");
//                                cons.setRotate(-90);
                                cons.setFont(Font.font("Arial", FontWeight.BOLD, 15));
                                cons.setPadding(new Insets(0, 0, 0, 0));

//                                Button addLine = new Button("", new ImageView(UITools.getImage("resources/images/add_16.png")));
//                                addLine.disableProperty().bind(mainApp.getEditProperty().not());
//                                addLine.setOnMouseClicked(event -> newValue.getValue().addLine());

                            HBox box = new HBox();
                            box.getChildren().addAll(cons/*, addLine*/);
                            box.setAlignment(Pos.CENTER);
                            HBox.setHgrow(cons, Priority.ALWAYS);



//                                Group consHolder = new Group();
//                                consHolder.getChildren().addAll(cons);
                                grid.add(box, 0, 0);
                                ColumnConstraints c = new ColumnConstraints();
                                c.setPercentWidth(100);
//                                c.setHalignment(HPos.RIGHT);

                                TableView tableConsign = new TableView();
                                tableConsign.editableProperty().bind(mainApp.getEditProperty());

                                UITools.bindSimpleTable(newValue.getConsignPart().getDataTable(), null, tableConsign);
                                grid.add(tableConsign, 0, 1);
                                GridPane.setFillWidth(tableConsign, true);
                                grid.getColumnConstraints().addAll(c);

                        }
                        AnchorPane.setBottomAnchor(grid, 0.0);
                        AnchorPane.setTopAnchor(grid, 0.0);
                        AnchorPane.setLeftAnchor(grid, 0.0);
                        AnchorPane.setRightAnchor(grid, 0.0);
                        rightPane.getChildren().add(grid);
//                        TableView tableView = new TableView();
//                        newValue.bindData(tableView);
//                        pane.setCenter(tableView);
//
//                        if(listConsign != null) {
//                            if(listConsign.get(newValue) != null) { // on a des consignes
//                                TableView tableConsign = new TableView();
//                                listConsign.get(newValue).bindData(tableConsign);
//                                pane.setRight(tableConsign);
//                            }
//                        }
                    }
                });


    }
}
