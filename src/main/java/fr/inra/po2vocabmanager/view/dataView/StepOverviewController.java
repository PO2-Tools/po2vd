/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.view.dataView;

import fr.inra.po2vocabmanager.MainApp;
import fr.inra.po2vocabmanager.model.DataNode;
import fr.inra.po2vocabmanager.utils.UITools;
import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Replicate;
import fr.inrae.po2engine.model.dataModel.CompositionFile;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.model.dataModel.StepFile;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import org.controlsfx.control.textfield.AutoCompletionBinding;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class StepOverviewController {
    @FXML
    private Label stepIdLabel;
    @FXML
    TextField stepTitle;
    @FXML
    TextField stepID;
    @FXML
    TextArea description;
    @FXML
    TextField date;
    @FXML
    TextField time;
    @FXML
    TextField duree;
    @FXML
    ComboBox<HashMap<KeyWords, ComplexField>> orgAgent;
    @FXML
    TextField scale;

    @FXML
    TitledPane stepPanel;
    @FXML
    VBox boxContent;


    StepFile f;
    MainApp mainApp;
    ArrayList<AutoCompletionBinding> listAutoCompletion ;

    @FXML
    private void initialize() {
        listAutoCompletion = new ArrayList<>();
        stepIdLabel.setGraphic(new ImageView(UITools.getImage("resources/images/help_16.png")));
        stepIdLabel.setTooltip(new Tooltip("Use  to differentiate several step of same type"));
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        stepTitle.editableProperty().bind(this.mainApp.getEditProperty());
        stepTitle.disableProperty().bind(this.mainApp.getEditProperty().not());

        stepID.editableProperty().bind(this.mainApp.getEditProperty());
        stepID.disableProperty().bind(this.mainApp.getEditProperty().not());

        description.editableProperty().bind(this.mainApp.getEditProperty());
        description.disableProperty().bind(this.mainApp.getEditProperty().not());

        date.editableProperty().bind(this.mainApp.getEditProperty());
        date.disableProperty().bind(this.mainApp.getEditProperty().not());

        time.editableProperty().bind(this.mainApp.getEditProperty());
        time.disableProperty().bind(this.mainApp.getEditProperty().not());

        duree.editableProperty().bind(this.mainApp.getEditProperty());
        duree.disableProperty().bind(this.mainApp.getEditProperty().not());

        orgAgent.setEditable(false);
        orgAgent.disableProperty().bind(this.mainApp.getEditProperty().not());

        scale.editableProperty().bind(this.mainApp.getEditProperty());
        scale.disableProperty().bind(this.mainApp.getEditProperty().not());
    }

    public void showNodeDetails(DataNode node, Replicate replicate) {
        for(AutoCompletionBinding auto : listAutoCompletion) {
            auto.dispose();
        }
        f = (StepFile) node.getFile();
        f.checkValue();

        if(replicate != null) {
            stepPanel.setText("Step (replicate : " + replicate.getName() + ")");
        } else {
            stepPanel.setText("Step");
        }

        UITools.simpleBindValue(f.getCType(), stepTitle);
        UITools.simpleBindValue(f.getCId(), stepID);
        UITools.simpleBindValue(f.getCDescrition(), description);
        UITools.simpleBindValue(f.getCDate(), replicate, date);
        UITools.simpleBindValue(f.getCTime(), replicate, time);
        UITools.simpleBindValue(f.getCDuration(), replicate, duree);
        UITools.bindComboBoxAgent(f.getAgentsMap(),f.getData().getProjectFile().getListAgent(), replicate, orgAgent);
        UITools.simpleBindValue(f.getCScale(), scale);
        listAutoCompletion.add(UITools.addAutoComplete(scale, f.getCScale().getListConstraint()));

        listAutoCompletion.add(UITools.addAutoComplete(stepTitle, f.getCType().getListConstraint()));
//        UITools.addPopOver(stepTitle, Ontology.Step);

        f.getNameProperty().addListener(observable -> MainApp.getDataControler().refreshTree());

        f.getCId().getValue().addListener(observable->MainApp.getDataControler().refreshTree());

        boxContent.getChildren().retainAll(stepPanel);


        TitledPane matMetPane = new TitledPane();
        VBox.setVgrow(matMetPane, Priority.ALWAYS);
        matMetPane.setText("Materials & Methods");
        matMetPane.setMinWidth(Double.NEGATIVE_INFINITY);
        FXMLLoader loaderMatMet = UITools.getFXMLLoader("view/dataView/MaterialMethodLinkOverview.fxml");
        SplitPane matmetOverview = null;
        try {
            matmetOverview = (SplitPane) loaderMatMet.load();
            matmetOverview.setMinHeight(200);
            matmetOverview.setPrefHeight(300);
        } catch (IOException e) {
            e.printStackTrace();
        }
        MaterialMethodLinkOverviewController matmetController = loaderMatMet.getController();
        matmetController.setMainApp(this.mainApp, null);
//        matmetController.setListConsign(f.getListConsign());
        matmetController.showItem(f);
        matMetPane.setContent(matmetOverview);
        boxContent.getChildren().add(matMetPane);


        for(Map.Entry<CompositionFile, Boolean> entry : f.getCompositionFile().entrySet()) {
            CompositionFile compoFile = entry.getKey();
            Boolean isInput = entry.getValue();

            TitledPane mixturePane = new TitledPane();
            mixturePane.setExpanded(false);
            mixturePane.setCollapsible(false);

            ComboBox<String> comboType = new ComboBox<>();
            comboType.getItems().addAll("Input", "Output");
            comboType.setValue((isInput?"Input ":"Output "));
            comboType.valueProperty().addListener((observableValue, oldValue, newValue) -> {
                if(newValue != null) {
                    entry.setValue(newValue.equalsIgnoreCase("input"));
                    MainApp.getDataControler().getCurrentData().setModified(true);
                }
            });
            comboType.disableProperty().bind(this.mainApp.getEditProperty().not());
            comboType.setStyle(" -fx-text-fill: black;  -fx-opacity: 1;");



            Label l = new Label(" Composition --- ");
            l.setContentDisplay(ContentDisplay.RIGHT);
            Label lc = new Label("   Composition type : ");
            Label ln = new Label("   Composition name : ");
            Label lt = new Label("  Type : ");

            TextField compoName = new TextField(compoFile.getHeaderPart().getCompositionID().getValue().get());
            compoName.disableProperty().bind(this.mainApp.getEditProperty().not());
            compoName.setStyle(" -fx-text-fill: black;  -fx-opacity: 1;");
            compoName.textProperty().addListener((observableValue, oldValue, newValue) ->  {
                MainApp.getDataControler().getCurrentData().setModified(true);
                if(newValue != null && !newValue.isEmpty()) {
                    compoFile.getHeaderPart().setCompositionID(newValue);
                } else {
                    compoName.setText(compoFile.getHeaderPart().getCompositionID().getValue().get());
                }
            } );

            TextField compoType = new TextField();
            UITools.simpleBindValue(compoFile.getCType(), compoType);
            listAutoCompletion.add(UITools.addAutoComplete(compoType, compoFile.getHeaderPart().getCompoType().getListConstraint()));
//            UITools.addPopOver(compoType, Ontology.Component);
            compoType.disableProperty().bind(this.mainApp.getEditProperty().not());


            Label separator = new Label("");
            separator.setMaxWidth(Double.MAX_VALUE);
            Button delTable = new Button(null, new ImageView(UITools.getImage("resources/images/del_16.png")));
            delTable.setTooltip(new Tooltip("delete Composition"));
            delTable.setOnMouseClicked(event -> {
                // demande de confirmation
                Alert a = new Alert(Alert.AlertType.CONFIRMATION);
                a.setTitle("Delete Composition");
                a.setHeaderText("Delete Composition ?");
                a.initModality(Modality.WINDOW_MODAL);
                a.initOwner(MainApp.primaryStage);
                Optional<ButtonType> res = a.showAndWait();
                if(res.get() == ButtonType.OK) {
                    f.removeCompositionFile(compoFile);
                    showNodeDetails(node, replicate);
                }

            });
            delTable.disableProperty().bind(this.mainApp.getEditProperty().not());

            HBox bb = new HBox();
            bb.setAlignment(Pos.CENTER_RIGHT);
            bb.getChildren().addAll(l,lt,comboType,lc,compoType,ln,compoName,separator, delTable);
            bb.setHgrow(separator, Priority.ALWAYS);
            bb.setHgrow(compoType, Priority.ALWAYS);
            bb.setPadding(new Insets(0,3,0,0));
            bb.minWidthProperty().bind(mixturePane.widthProperty().subtract(25));
            mixturePane.setGraphic(bb);

            VBox.setVgrow(mixturePane, Priority.ALWAYS);
            if(isInput) {
                boxContent.getChildren().add(2, mixturePane);
            } else {
                boxContent.getChildren().add(mixturePane);
            }
        }
    }

    public void unloadControler() {
        if(f != null) {
            f.unbind();
            f = null;
        }
        stepID.styleProperty().unbind();
        stepTitle.styleProperty().unbind();
        scale.styleProperty().unbind();
        for(AutoCompletionBinding auto : listAutoCompletion) {
            auto.dispose();
        }
        listAutoCompletion.clear();
    }
}
