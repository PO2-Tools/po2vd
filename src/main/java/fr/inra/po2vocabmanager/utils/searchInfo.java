/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.utils;

import fr.inra.po2vocabmanager.MainApp;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import org.apache.jena.atlas.json.JsonArray;
import org.apache.jena.atlas.json.JsonObject;
import org.apache.jena.atlas.web.TypedInputStream;
import org.apache.jena.http.HttpOp;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import java.util.ArrayList;
import java.util.List;

public class searchInfo extends Service<Void> {

    private List<String> uris;

    public void setUris(List<String> uris) {
        this.uris = uris;
    }

    @Override
    protected Task<Void> createTask() {
        return new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                String queryResult = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>"
                        + " PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
                        + " PREFIX rdfs:   <http://www.w3.org/2000/01/rdf-schema#>"
                        + " PREFIX dbo: <http://dbpedia.org/ontology/>"
                        + " select ?stringLab ?stringComment ?img"
                        + " Where {"
                        + " 	OPTIONAL { {?concept rdfs:label ?lab} UNION {?concept skos:prefLabel ?lab}UNION {?concept skos:altLabel ?lab} UNION {?concept dbo:thumbnail ?img}. BIND(STR(?lab) as ?stringLab)}"
                        + " }";

                for (String uri : uris) {
                    try {
                        final TypedInputStream is = HttpOp.httpGet(uri, "application/rdf+xml");
                        Model m = ModelFactory.createDefaultModel();
                        m.read(is, null);
                        Query q2 = QueryFactory.create(queryResult);
                        QueryExecution qexec2 = QueryExecutionFactory.create(q2, m);
                        ResultSet results2 = qexec2.execSelect();
                        JsonObject o = new JsonObject();
                        JsonArray a = new JsonArray();
                        JsonArray im = new JsonArray();
                        o.put("text", a);
                        o.put("img", im);
                        while (results2.hasNext()) {
                            QuerySolution binding = results2.nextSolution();
                            if (binding.get("stringLab") != null) {
                                String lab = binding.get("stringLab").toString().replaceAll("^\"|\"$", "");
                                if (!"".equals(lab)) {
                                    a.add(lab);

                                }
                            }
                            if (binding.get("stringComment") != null) {
                                String comment = binding.get("stringComment").toString();
                                if (!"".equals(comment)) {
                                    a.add(comment);
                                }
                            }
                            if (binding.get("img") != null) {
                                String img = binding.get("img").toString();
                                if (!"".equals(img)) {
                                    im.add(img);
                                }
                            }

                        }
                        updateMessage(o.toString());
                        ArrayList<String> listToSearch2 = new ArrayList<>();
                        String queryString = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#> " + "select ?close " + "Where { {?concept skos:closeMatch ?close} UNION {?concept skos:exactMatch ?close} } ";

                        Query q = QueryFactory.create(queryString);
                        QueryExecution qexec = QueryExecutionFactory.create(q, m);
                        ResultSet results = qexec.execSelect();

                        while (results.hasNext()) {
                            QuerySolution binding = results.nextSolution();
                            listToSearch2.add(binding.get("close").toString());
                        }
                        for (String uri2 : listToSearch2) {
                            try {
                                final TypedInputStream is2 = HttpOp.httpGet(uri2, "application/rdf+xml");
                                Model m2 = ModelFactory.createDefaultModel();
                                m2.read(is2, null);
                                Query q22 = QueryFactory.create(queryResult);
                                QueryExecution qexec22 = QueryExecutionFactory.create(q22, m2);
                                ResultSet results22 = qexec22.execSelect();
                                o = new JsonObject();
                                a = new JsonArray();
                                im = new JsonArray();
                                o.put("text", a);
                                o.put("img", im);
                                while (results22.hasNext()) {

                                    QuerySolution binding = results22.nextSolution();
                                    if (binding.get("stringLab") != null) {
                                        String lab = binding.get("stringLab").toString().replaceAll("^\"|\"$", "");
                                        if (!"".equals(lab)) {
                                            a.add(lab);
                                        }
                                    }
                                    if (binding.get("stringComment") != null) {
                                        String comment = binding.get("stringComment").toString();
                                        if (!"".equals(comment)) {
                                            a.add(comment);
                                        }
                                    }
                                    if (binding.get("img") != null) {
                                        String img = binding.get("img").toString();
                                        if (!"".equals(img)) {
                                            im.add(img);
                                        }
                                    }


                                }
                                updateMessage(o.toString());
                            } catch (Exception e) {
                                MainApp.logger.warn("Impossible de charger2 : " + uri2 + " - " + e);
                            }
                        }

                    } catch (Exception e) {
                        MainApp.logger.warn("Impossible de charger1 : " + uri + " - " + e);
                    }
                }
                return null;
            }
        };
    }

}
