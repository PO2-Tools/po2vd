/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.utils;

import fr.inra.po2vocabmanager.MainApp;
import fr.inra.po2vocabmanager.model.DataNode;
import fr.inrae.po2engine.externalTools.RDF4JTools;
import fr.inrae.po2engine.model.Replicate;
import fr.inrae.po2engine.model.dataModel.*;
import fr.inrae.po2engine.utils.ImportReport;
import fr.inrae.po2engine.utils.Report;
import fr.inrae.po2engine.utils.Tools;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTreeCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Pair;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.controlsfx.control.CheckTreeView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class DataTreeCell extends TextFieldTreeCell<DataNode> {

    private MainApp mainApp;


    public DataTreeCell(MainApp mainApp) {
        super();
        this.mainApp = mainApp;
    }

    public void copyItinerary(GeneralFile generalFile) {
        Dialog<String> diagNewIti = new Dialog();
        diagNewIti.initModality(Modality.WINDOW_MODAL);
        diagNewIti.initOwner(MainApp.primaryStage);
        diagNewIti.setTitle("Copy Itinerary");

        ComboBox<ItineraryFile> comboIti = new ComboBox<>();

        Callback<ListView<ItineraryFile>, ListCell<ItineraryFile>> cellFactoryInitPart = new Callback<ListView<ItineraryFile>, ListCell<ItineraryFile>>() {
            @Override
            public ListCell<ItineraryFile> call(ListView<ItineraryFile> l) {
                return new ListCell<ItineraryFile>() {
                    @Override
                    protected void updateItem(ItineraryFile item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            setText(item.getNameProperty().get());
                        }
                    }
                } ;
            }
        };


        comboIti.setCellFactory(cellFactoryInitPart);
        comboIti.setButtonCell(cellFactoryInitPart.call(null));

        ObservableList<ItineraryFile> listIti = FXCollections.observableArrayList(generalFile.getItinerary());
        comboIti.setItems(listIti);
        VBox vd = new VBox(5);
        vd.getChildren().addAll(comboIti);
        diagNewIti.getDialogPane().setContent(vd);

        diagNewIti.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        Button bok = (Button) diagNewIti.getDialogPane().lookupButton(ButtonType.OK);
        bok.disableProperty().bind(comboIti.valueProperty().isNull());
        bok.setOnAction(actionEvent -> {
            ItineraryFile originalIti = comboIti.getValue();
            ItineraryFile itineraryPartT = new ItineraryFile(generalFile);
            DataNode dataNodeIti = new DataNode(DataNodeType.ITINERARY);
            MainApp.getDataControler().addNode(itineraryPartT, dataNodeIti, MainApp.getDataControler().getDataNode(generalFile));
//            dataNodeIti.nameProperty().bind(Bindings.concat(itineraryPartT.getItiNumber(), " (",itineraryPartT.getItiName(),")"));
//            DataNode dataNodeIti = new DataNode("Itinerary " + (generalFile.getItinerary().size()+1), DataNodeType.ITINERARY);

//            itineraryPartT.setDataNode(dataNodeIti);
//            dataNodeIti.setItineraryPart(itineraryPartT);
//            generalFile.getDataNode().addSubNode(dataNodeIti);

            Map<CompositionFile, CompositionFile> mappingCompo = new HashMap<>();
            Map<StepFile, StepFile> mappingStep = new HashMap<>();

            ArrayList<StepFile> rootStepToClone = new ArrayList<>();
            for(StepFile curStep : originalIti.getListStep()) {
                // on remonte jusqu'au pere
                while (curStep.getFather() != null) {
                    curStep = curStep.getFather();
                }
                if (!rootStepToClone.contains(curStep)) {
                    rootStepToClone.add(curStep);
                }
            }
            for(StepFile curStep : rootStepToClone) {
                StepFile newCopyStep = new StepFile(curStep.getOntoType(), generalFile);
                newCopyStep.setId(curStep.getId());
//                itineraryPartT.getItiPart().addStep(newCopyStep);
//                generalFile.getContentPart().createStepDataNode(newCopyStep);
                newCopyStep.cloneFrom(curStep, itineraryPartT, mappingCompo, originalIti, mappingStep);
            }
            // clonage des observation
            for(ObservationFile obs : originalIti.getListObservation()) {
                // creating new observation
                ObservationFile newFile = new ObservationFile(itineraryPartT);
                newFile.cloneFrom(obs, mappingCompo);
            }
            // clonage de l'itinéraire
            itineraryPartT.cloneFrom(originalIti, mappingStep);

            itineraryPartT.getProcessFile().getData().setModified(true);
            MainApp.getDataControler().buildItineraryTree(itineraryPartT);
            MainApp.getDataControler().selectNode(itineraryPartT);
        });

        diagNewIti.showAndWait();
    }

    private static StepFile addStepData(ItineraryFile itiFile, StepFile parentStep, GeneralFile current, boolean rbCopy, StepFile stepToClone) {
        StepFile s = new StepFile("new step", current);
        s.setId("");
        if(rbCopy && stepToClone != null) {
            s.cloneFrom(stepToClone, itiFile, new HashMap<>(), itiFile, null);
        }

        if(parentStep != null) {
            parentStep.addSubStep(s);
        }

        itiFile.addStep(s);
        DataNode stepNode = new DataNode(DataNodeType.STEP);
        stepNode.setItineraryFile(itiFile);
        if(parentStep != null) {
            MainApp.getDataControler().addNode(s,stepNode, MainApp.getDataControler().getDataNode(parentStep, itiFile));
        } else {
            MainApp.getDataControler().addNode(s,stepNode, MainApp.getDataControler().getDataNode(itiFile) );
        }

        if(rbCopy) {
            MainApp.getDataControler().buildItineraryTree(itiFile);
        }
        MainApp.getDataControler().selectNode(s);
        current.getData().setModified(true);
        return s;
    }

    private static void newStepImport(ItineraryFile iti, StepFile parent, int replicateID) {
        File fileImport = getFile(MainApp.primaryStage);
        if (fileImport == null) {
            return;
        }
        MainApp.logger.info("start importing file " + fileImport.getName());
        ImportReport importReport = iti.checkImportStepFromXLSX(fileImport);
        if(importReport.success()) {
            StepFile step = iti.importStepFromXLSX(fileImport, replicateID);
            if(parent != null && step != null) {
                parent.addSubStep(step);
            }
            handleInterfaceUpdate(step.getGeneralFile(), importReport.success(), importReport.toString());
        } else {
            showErrorImportAlert(importReport.toString());
        }
    }

    private static File getFile(Stage primaryStage) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Import file");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Xlsx files (*.xlsx)", "*.xlsx"));
        return fileChooser.showOpenDialog(primaryStage);
    }

    public void addExistingStep(ItineraryFile itineraryFile) {
        Dialog<ButtonType> addStep = new Dialog<>();
        addStep.getDialogPane().setPrefSize(300, 500);
        addStep.initModality(Modality.WINDOW_MODAL);
        addStep.initOwner(MainApp.primaryStage);

        AnchorPane pane = new AnchorPane();
        VBox vb = new VBox(1.0);
        vb.getChildren().add(new Label("Select steps to add"));
        CheckBoxTreeItem<Object> fakeRoot = new CheckBoxTreeItem<>(null);
        CheckTreeView<Object> treeStep = new CheckTreeView<>(fakeRoot);
        treeStep.setShowRoot(false);
        itineraryFile.getProcessFile().getItinerary().stream().filter(iti -> iti != itineraryFile).forEach(itiFile -> {
            CheckBoxTreeItem<Object> ckIti = new CheckBoxTreeItem<>(itiFile, new Label(itiFile.getItineraryNumber() + " - " + itiFile.getItineraryName()));
            fakeRoot.getChildren().add(ckIti);
            itiFile.getListStep().filtered(step -> step.isLeaf() && !itineraryFile.getListStep().contains(step)).forEach(step -> {
                CheckBoxTreeItem<Object> ckStep = new CheckBoxTreeItem<>(step);
                ckIti.getChildren().add(ckStep);
            });
        });
        // add all step without itinerary
        itineraryFile.getProcessFile().getListStep().stream().filter(s -> MainApp.getDataControler().getDataNode(s) == null).forEach(s -> {
            CheckBoxTreeItem<Object> ckStep = new CheckBoxTreeItem<>(s);
            fakeRoot.getChildren().add(ckStep);
        });

        vb.getChildren().add(treeStep);

        CheckBox addLink = new CheckBox("include links between steps");
        addLink.setSelected(true);

        vb.getChildren().add(addLink);

        pane.getChildren().add(vb);

        AnchorPane.setTopAnchor(vb, 2.0);
        AnchorPane.setLeftAnchor(vb, 2.0);
        AnchorPane.setRightAnchor(vb, 2.0);
        AnchorPane.setBottomAnchor(vb, 2.0);

        addStep.getDialogPane().setContent(pane);
        addStep.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        Optional<ButtonType> result = addStep.showAndWait();
        if (result.isPresent()) {
            if (result.get().equals(ButtonType.OK)) {
                treeStep.getCheckModel().getCheckedItems().forEach(ti -> {
                    if(ti.getValue() instanceof StepFile) {
                        StepFile step = (StepFile) ti.getValue();
                        if(step != null && !itineraryFile.getListStep().contains(step)) {
                            itineraryFile.addLinkItinerary(step, null);
                        }
                    }
                });
                if(addLink.isSelected()) { // we need to copy link also
                    treeStep.getCheckModel().getCheckedItems().forEach(ti -> {
                        if(ti.getValue() instanceof StepFile) {
                            StepFile step = (StepFile) ti.getValue();
                            ItineraryFile iti = (ItineraryFile) ti.getParent().getValue();
                            List<StepFile> listChild = iti.getItinerary().stream().filter(link -> link.getKey().getValue().get() != null && link.getKey().getValue().get().equals(step)).map(link -> link.getValue().getValue().get()).collect(Collectors.toList());
                            List<StepFile> listParent = iti.getItinerary().stream().filter(link -> link.getValue().getValue().get() != null && link.getValue().getValue().get().equals(step)).map(link -> link.getKey().getValue().get()).collect(Collectors.toList());

                            listChild.stream().filter(s -> itineraryFile.contains(s)).forEach(s -> itineraryFile.addLinkItinerary(step, s));
                            listParent.stream().filter(p -> itineraryFile.contains(p)).forEach(p -> itineraryFile.addLinkItinerary(p, step));
                        }
                    });
                }
                MainApp.getDataControler().buildItineraryTree(itineraryFile);
                MainApp.getDataControler().selectNode(itineraryFile);
            }
        }
    }


    private static void newItineraryObsImport(ItineraryFile iti, int replicateID) {
        File fileImport = getFile(MainApp.primaryStage);
        if (fileImport == null) {
            return;
        }
        MainApp.logger.info("start importing/create file " + fileImport.getName());
        Report r = iti.checkCreateObservationFromXLSX(fileImport, replicateID);
        if(r.success()) {
            ObservationFile obs = iti.createObservationFromXLSX(fileImport, replicateID);
            handleInterfaceUpdate(obs.getItineraryFile().getProcessFile(), r.success(), "File import completed");
            MainApp.getDataControler().selectNode(obs);
            MainApp.logger.info("import done with sucess");
        } else {
            showErrorImportAlert(r.prettyPrintError());
        }
    }

    public void addStepFromScratch(ItineraryFile itineraryFile, StepFile parentStep) {
        if(parentStep != null && parentStep.getFather() != null) {
            Alert notPossible = new Alert(Alert.AlertType.INFORMATION);
            notPossible.setTitle("Add substep");
            notPossible.setContentText("Adding substep here is not allowed");
            notPossible.initModality(Modality.APPLICATION_MODAL);
            notPossible.initOwner(MainApp.primaryStage);
            notPossible.showAndWait();
            return;
        }
        GeneralFile current = itineraryFile.getProcessFile();
        addStepData(itineraryFile, parentStep, current, false, null);
    }

    public void copyStep(ItineraryFile itineraryFile, StepFile parentStep) {
        if(parentStep != null && parentStep.getFather() != null) {
            Alert notPossible = new Alert(Alert.AlertType.INFORMATION);
            notPossible.setTitle("Add substep");
            notPossible.setContentText("Adding substep here is not allowed");
            notPossible.initModality(Modality.APPLICATION_MODAL);
            notPossible.initOwner(MainApp.primaryStage);
            notPossible.showAndWait();
            return;
        }
        GeneralFile current = itineraryFile.getProcessFile();

        ChoiceDialog<StepFile> diagCloneStep = new ChoiceDialog<>();
        diagCloneStep.initModality(Modality.WINDOW_MODAL);
        diagCloneStep.initOwner(MainApp.primaryStage);
        diagCloneStep.setTitle("Create a new step by cloning an existing one");

        ObservableList<StepFile> listCombo = FXCollections.observableArrayList(current.getListStep());
        diagCloneStep.getItems().addAll(listCombo);
        Optional<StepFile> result = diagCloneStep.showAndWait();
        result.ifPresent(step -> addStepData(itineraryFile, parentStep, current, true, step));
    }

    private static void stepImport(StepFile file, boolean isNewStep, int replicateID) {
            File fileImport = getFile(MainApp.primaryStage);
            if (fileImport != null) {
            MainApp.logger.info("start importing file " + fileImport.getName());
            ImportReport importReport = file.realPreConst(fileImport, isNewStep);
            if(importReport.success()) {

                // validation de l'import par l'utilisateur
                Dialog<ButtonType> validateImport = new Dialog();
                validateImport.setTitle("import validation");
                validateImport.initOwner(MainApp.primaryStage);
                validateImport.initModality(Modality.WINDOW_MODAL);
                ButtonType importButtonType = new ButtonType("Import", ButtonBar.ButtonData.OK_DONE);
                validateImport.getDialogPane().getButtonTypes().addAll(importButtonType, ButtonType.CANCEL);

                GridPane paneImport = new GridPane();
                paneImport.setHgap(20);
                paneImport.setVgap(20);

                Integer comptLine = 0;
                Text stepText = new Text(file.getNameProperty().getValue());
                Text sepa = new Text(" : ");
                paneImport.add(new ImageView(UITools.getImage("resources/images/treeview/step.png")), 0, comptLine);
                paneImport.add(stepText, 1, comptLine);
                paneImport.add(sepa, 2, comptLine);
                paneImport.add(new ImageView(UITools.getImage("resources/images/update_30.png")), 3, comptLine);

                comptLine++;


                for (ObservationFile obsF : file.getObservationFiles()) {
                    Text obsText = new Text(obsF.getCID().getValue().get());
                    Text sepa2 = new Text(" : ");
                    paneImport.add(new ImageView(UITools.getImage("resources/images/treeview/observation.png")), 0, comptLine);
                    paneImport.add(obsText, 1, comptLine);
                    paneImport.add(sepa2, 2, comptLine);
                    if (importReport.getObsMapping().containsValue(obsF)) {
                        paneImport.add(new ImageView(UITools.getImage("resources/images/update_30.png")), 3, comptLine);
                    } else {
                        paneImport.add(new ImageView(UITools.getImage("resources/images/del_30.png")), 3, comptLine);
                    }
                    comptLine++;
                }

                for(XSSFSheet oo : importReport.getObsMapping().keySet()) {
                    if(importReport.getObsMapping().get(oo) == null) { // new obs
                        Text obsText = new Text(oo.getSheetName());
                        Text sepa2 = new Text(" : ");
                        paneImport.add(new ImageView(UITools.getImage("resources/images/treeview/observation.png")), 0, comptLine);
                        paneImport.add(obsText, 1, comptLine);
                        paneImport.add(sepa2, 2, comptLine);
                        paneImport.add(new ImageView(UITools.getImage("resources/images/add_30.png")), 3, comptLine);
                        comptLine++;
                    }
                }

                for (CompositionFile compoF : file.getCompositionFile().keySet()) {
                    Text compoText = new Text(compoF.getCompositionID().getValue().get());
                    Text sepa2 = new Text(" : ");
                    paneImport.add(new ImageView(UITools.getImage("resources/images/treeview/composition.png")), 0, comptLine);
                    paneImport.add(compoText, 1, comptLine);
                    paneImport.add(sepa2, 2, comptLine);
                    if(importReport.getCompoMapping().values().stream().map(org.apache.commons.lang3.tuple.Pair::getLeft).noneMatch(c -> c.equals(compoF))) {
                        paneImport.add(new ImageView(UITools.getImage("resources/images/del_30.png")), 3, comptLine);
                    } else {
                        paneImport.add(new ImageView(UITools.getImage("resources/images/update_30.png")), 3, comptLine);
                    }
                    comptLine++;
                }

                for(XSSFSheet oo : importReport.getCompoMapping().keySet()) {
                    if(importReport.getCompoMapping().get(oo).getLeft() == null) { // new Compo
                        Text compoText = new Text(oo.getSheetName());
                        Text sepa2 = new Text(" : ");
                        paneImport.add(new ImageView(UITools.getImage("resources/images/treeview/composition.png")), 0, comptLine);
                        paneImport.add(compoText, 1, comptLine);
                        paneImport.add(sepa2, 2, comptLine);
                        paneImport.add(new ImageView(UITools.getImage("resources/images/add_30.png")), 3, comptLine);
                        comptLine++;
                    }
                }

                validateImport.getDialogPane().setContent(paneImport);
                Optional<ButtonType> result = Optional.empty();
                if(!isNewStep) {
                    result = validateImport.showAndWait();
                }
                if (isNewStep || result.isPresent() && result.get() == importButtonType) {
                    // l'utilisateur valide la la mise à jour
                    Report report = file.constructDataFromImport(fileImport, isNewStep, replicateID);
                    handleInterfaceUpdate(file.getGeneralFile(), report.success(), report.toString());
                    MainApp.getDataControler().selectNode(file);
                }
            } else {
                showErrorImportAlert(importReport.toString());
            }
        } else {
        }
    }

    private static void importObservation(ObservationFile obsFile, int replicateID) {
        File fileImport = getFile(MainApp.primaryStage);
        if (fileImport != null) {
            MainApp.logger.info("start importing/update file " + fileImport.getName());
            // temp obs file
            ObservationFile tempF = null;
            if(obsFile.getStepFile() != null) {
                Report r = obsFile.getStepFile().checkUpdateObservationFromXLSX(fileImport, obsFile, replicateID);
                if(r.success()) {
                    obsFile.getStepFile().updateObservationFromXLSX(fileImport, obsFile, replicateID);
                    MainApp.getDataControler().refreshTree();
                    MainApp.getDataControler().showNodeDetails(obsFile, obsFile);
                    MainApp.logger.info("import done with sucess");
                } else {
                    showErrorImportAlert(r.prettyPrintError());
                }
            }
            if(obsFile.getItineraryFile() != null) {
                Report r = obsFile.getItineraryFile().checkUpdateObservationFromXLSX(fileImport, obsFile, replicateID);
                if(r.success()) {
                    obsFile.getItineraryFile().updateObservationFromXLSX(fileImport, obsFile, replicateID);
                    MainApp.getDataControler().refreshTree();
                    MainApp.getDataControler().showNodeDetails(obsFile, obsFile);
                    MainApp.logger.info("import done with sucess");
                } else {
                    showErrorImportAlert(r.prettyPrintError());
                }
            }
        }
    }

    private static void exportObservation(ObservationFile obsFile, int replicateID) {
        List<String> choices = new ArrayList<>();
        choices.add("with ontology");
        choices.add("without ontology");
        ChoiceDialog<String> dialog = new ChoiceDialog<>("without ontology", choices);
        dialog.setTitle("Export Observation");
        dialog.setHeaderText(null);
        dialog.setContentText("Export Observation data ");
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(MainApp.primaryStage);
        Boolean exportOnto = false;
        Optional<String> result = dialog.showAndWait();

        if (result.isPresent() ) {
            exportOnto = result.get().equals("with ontology");
            Boolean finalExportOnto = exportOnto;
            if (exportOnto && MainApp.getOntologyControler().getCurrentOntology() == null) {
                Alert noOntology = new Alert(Alert.AlertType.ERROR);
                noOntology.setTitle("No ontology loaded");
                noOntology.setHeaderText("Please load an ontology");
                noOntology.setContentText("In order to create an export file, please load the corresponding ontology");
                noOntology.initOwner(MainApp.primaryStage);
                noOntology.initModality(Modality.APPLICATION_MODAL);
                noOntology.showAndWait();
            } else {
                XSSFWorkbook book = new XSSFWorkbook();


                Task<Boolean> taskExport = new Task<Boolean>() {
                    @Override
                    protected Boolean call() throws Exception {
                        XSSFSheet sheet = Tools.createSheet(book,"obs|"+Tools.removeNonAlphaNumChar(obsFile.getNameProperty().getValue()));

                        Integer compteur = 0;
                        Integer maxColumn = 0;
                        for (LinkedList<String> line : obsFile.getDataForExcel(replicateID)) {
                            XSSFRow r = sheet.createRow(compteur++);
                            Integer compteurCell = 0;
                            for (String val : line) {
                                r.createCell(compteurCell++).setCellValue(val);
                                maxColumn = Math.max(maxColumn, compteurCell);
                            }
                        }
                        Tools.autoResize(sheet, maxColumn);


                        if(finalExportOnto) {

                            JSONObject hierarchyVocab = MainApp.getOntologyControler().getHierarchyVocabJSON();
                            // Récupération de l'ontology au format JSON
                            XSSFSheet sheetOntoVocab = Tools.createSheet(book,"onto-vocab|" + Tools.removeNonAlphaNumChar(MainApp.getOntologyControler().getCurrentOntology().getName().getValue()));
                            JSONArray vocab = (JSONArray) hierarchyVocab.get("vocabulary");

                            compteur = 0;
                            maxColumn = 0;
                            for (
                                    int i = 0; i < vocab.length(); i++) {
                                JSONArray array = (JSONArray) vocab.get(i);
                                XSSFRow r = sheetOntoVocab.createRow(compteur++);
                                for (int j = 0; j < array.length(); j++) {
                                    r.createCell(j).setCellValue((String) array.get(j));
                                    maxColumn = Math.max(maxColumn, j);
                                }
                            }
                            Tools.autoResize(sheetOntoVocab, maxColumn);


                            XSSFSheet sheetOntohierarchy = Tools.createSheet(book,"onto-hierarchy|" + Tools.removeNonAlphaNumChar(MainApp.getOntologyControler().getCurrentOntology().getName().getValue()));
                            JSONArray hierarchy = (JSONArray) hierarchyVocab.get("hierarchy");

                            compteur = 0;
                            maxColumn = 0;

                            for (
                                    int x = 0; x < hierarchy.length(); x++) {
                                XSSFRow r = sheetOntohierarchy.createRow(compteur++);
                                JSONArray arrayDesc = (JSONArray) hierarchy.get(x);
                                for (int j = 0; j < arrayDesc.length(); j++) {
                                    r.createCell(j).setCellValue((String) arrayDesc.get(j));
                                    maxColumn = Math.max(maxColumn, j);
                                }
                            }
                            Tools.autoResize(sheetOntohierarchy, maxColumn);
                        }
                        return true;
                    }
                };
                taskExport.setOnSucceeded(workerStateEvent -> {
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Export file");
                    String fileName = obsFile.getFileName();
                    if(replicateID != -1) {
                        fileName+="-"+replicateID;
                    }
                    fileChooser.setInitialFileName(fileName+".xlsx");
                    File fileExport = fileChooser.showSaveDialog(MainApp.primaryStage);
                    if (fileExport != null) {
                        try {
                            FileOutputStream fout = new FileOutputStream(fileExport, false);
                            book.write(fout);
                            fout.close();
                        } catch (IOException ex) {
                            MainApp.logger.error(ex.getMessage());
                        }
                    }
                });
                new Thread(taskExport).start();
            }
        }
    }

    private static void exportStepFile(StepFile stepFile, int replicateID) {

        List<String> choices = new ArrayList<>();
        choices.add("with ontology");
        choices.add("without ontology");
        ChoiceDialog<String> dialog = new ChoiceDialog<>("without ontology", choices);
        dialog.setTitle("Export Step");
        dialog.setHeaderText(null);
        dialog.setContentText("Export step data ");
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(MainApp.primaryStage);
        Boolean exportOnto = false;
        Optional<String> result = dialog.showAndWait();

        if (result.isPresent() ) {
            exportOnto = result.get().equals("with ontology");
            if(exportOnto && MainApp.getOntologyControler().getCurrentOntology() == null) {
                Alert noOntology = new Alert(Alert.AlertType.ERROR);
                noOntology.setTitle("No ontology loaded");
                noOntology.setHeaderText("Please load an ontology");
                noOntology.setContentText("In order to create an export file, please load the corresponding ontology");
                noOntology.initOwner(MainApp.primaryStage);
                noOntology.initModality(Modality.APPLICATION_MODAL);
                noOntology.showAndWait();
            } else {
                XSSFWorkbook book = new XSSFWorkbook();
                Boolean finalExportOnto = exportOnto;
                Task<Boolean> taskExport = new Task<Boolean>() {
                    @Override
                    protected Boolean call() throws Exception {
                        XSSFSheet sheet = Tools.createSheet(book,"step|"+ DigestUtils.md5Hex(stepFile.getNameProperty().getValue()));

                        Integer compteur = 0;
                        Integer maxColumn = 0;

                        for (LinkedList<String> line : stepFile.getDataForExcel(true, replicateID)) {
                            XSSFRow r = sheet.createRow(compteur++);
                            Integer compteurCell = 0;
                            for (String val : line) {
                                r.createCell(compteurCell++).setCellValue(val);
                                maxColumn = Math.max(maxColumn, compteurCell);
                            }
                        }


                        // composition
                        for (Map.Entry<CompositionFile, Boolean> entry : stepFile.getCompositionFile().entrySet()) {
                            XSSFSheet sheetCompo = Tools.createSheet(book,"compo-"+(entry.getValue() ? "input" : "output")+"|"+Tools.removeNonAlphaNumChar(entry.getKey().getCompositionID().getValue().get()));
                            Integer compteurCompo = 0;
                            Integer maxColumnCompo = 0;
                            for (LinkedList<String> line : entry.getKey().getDataForExcel()) {
                                XSSFRow r = sheetCompo.createRow(compteurCompo++);
                                Integer compteurCell = 0;
                                for (String val : line) {
                                    r.createCell(compteurCell++).setCellValue(val);
                                    maxColumnCompo = Math.max(maxColumnCompo, compteurCell);
                                }
                            }
                            Tools.autoResize(sheetCompo, maxColumnCompo);
                        }



                        Tools.autoResize(sheet, maxColumn);

                        for(ObservationFile obs : stepFile.getObservationFiles()) {
                            XSSFSheet sheetObs = Tools.createSheet(book,"obs|"+Tools.removeNonAlphaNumChar(obs.getCID().getValue().get()));

                            Integer compteurObs = 0;
                            Integer maxColumnObs = 0;
                            for (LinkedList<String> line : obs.getDataForExcel(replicateID)) {
                                XSSFRow r = sheetObs.createRow(compteurObs++);
                                Integer compteurCell = 0;
                                for (String val : line) {
                                    r.createCell(compteurCell++).setCellValue(val);
                                    maxColumnObs = Math.max(maxColumnObs, compteurCell);
                                }
                            }
                            Tools.autoResize(sheetObs, maxColumnObs);
                        }

                        if(finalExportOnto) {

                            JSONObject hierarchyVocab = MainApp.getOntologyControler().getHierarchyVocabJSON();
                            // Récupération de l'ontology au format JSON
                            XSSFSheet sheetOntoVocab = Tools.createSheet(book,"onto-vocab|" + Tools.removeNonAlphaNumChar(MainApp.getOntologyControler().getCurrentOntology().getName().getValue()));
                            JSONArray vocab = (JSONArray) hierarchyVocab.get("vocabulary");

                            compteur = 0;
                            maxColumn = 0;
                            for (int i = 0; i < vocab.length(); i++) {
                                JSONArray array = (JSONArray) vocab.get(i);
                                XSSFRow r = sheetOntoVocab.createRow(compteur++);
                                for (int j = 0; j < array.length(); j++) {
                                    r.createCell(j).setCellValue((String) array.get(j));
                                    maxColumn = Math.max(maxColumn, j);
                                }
                            }
                            Tools.autoResize(sheetOntoVocab, maxColumn);


                            XSSFSheet sheetOntohierarchy = Tools.createSheet(book,"onto-hierarchy|" + Tools.removeNonAlphaNumChar(MainApp.getOntologyControler().getCurrentOntology().getName().getValue()));
                            JSONArray hierarchy = (JSONArray) hierarchyVocab.get("hierarchy");

                            compteur = 0;
                            maxColumn = 0;

                            for (int x = 0; x < hierarchy.length(); x++) {
                                XSSFRow r = sheetOntohierarchy.createRow(compteur++);
                                JSONArray arrayDesc = (JSONArray) hierarchy.get(x);
                                for (int j = 0; j < arrayDesc.length(); j++) {
                                    r.createCell(j).setCellValue((String) arrayDesc.get(j));
                                    maxColumn = Math.max(maxColumn, j);
                                }
                            }
                            Tools.autoResize(sheetOntohierarchy, maxColumn);
                        }
                        return true;
                    }
                };
                taskExport.setOnSucceeded(workerStateEvent -> {
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Export file");
                    String fileName = stepFile.getNameProperty().getValue();
                    if(replicateID != -1) {
                        fileName+="-"+replicateID;
                    }
                    fileChooser.setInitialFileName(fileName+".xlsx");
                    File fileExport = fileChooser.showSaveDialog(MainApp.primaryStage);
                    if (fileExport != null) {
                        try {
                            FileOutputStream fout = new FileOutputStream(fileExport, false);
                            book.write(fout);
                            fout.close();
                        } catch (IOException ex) {
                            MainApp.logger.error(ex.getMessage());
                        }
                    }
                });
                new Thread(taskExport).start();
            }
        }

    }

    private void newStepObsImport(StepFile step, int replicateID) {
        File fileImport = getFile(MainApp.primaryStage);
        if (fileImport == null) {
            return;
        }
        MainApp.logger.info("start importing/create file " + fileImport.getName());
        Report r = step.checkCreateObservationFromXLSX(fileImport, replicateID);
        if(r.success()) {
            ObservationFile obs = step.createObservationFromXLSX(fileImport, replicateID);
            handleInterfaceUpdate(obs.getStepFile().getGeneralFile(), r.success(), "File import completed");
            MainApp.getDataControler().selectNode(obs);
            MainApp.logger.info("import done with sucess");
        } else {
            showErrorImportAlert(r.prettyPrintError());
        }
    }

    private static void handleInterfaceUpdate(GeneralFile file, boolean reportBoolean, String reportString) {
        if(reportBoolean) {
            MainApp.getDataControler().buildProcessTree(file);
            showOkImportAlert();
        } else {
            showErrorImportAlert(reportString);
        }
    }

    private static void showErrorImportAlert(String report) {
        Alert errorImport = new Alert(Alert.AlertType.ERROR);
        errorImport.setTitle("Import error");
        errorImport.setContentText(report);
        errorImport.initModality(Modality.APPLICATION_MODAL);
        errorImport.initOwner(MainApp.primaryStage);
        errorImport.showAndWait();
    }

    private static void showOkImportAlert() {
        Alert okImport = new Alert(Alert.AlertType.INFORMATION);
        okImport.setGraphic(new ImageView(UITools.getImage("resources/images/valid.png")));
        okImport.setHeaderText(null);
        okImport.setTitle("Successful import");
        okImport.setContentText("Successful import");
        okImport.initModality(Modality.APPLICATION_MODAL);
        okImport.initOwner(MainApp.primaryStage);
        okImport.showAndWait();
    }

    @Override
    public void updateItem(DataNode item, boolean empty) {
        super.updateItem(item, empty);
        if (empty || item == null) {
            setText(null);
            setGraphic(null);
            setBackground(null);
            setContextMenu(null);
        } else {
            setText(item.toString());
            if(item.getFile() != null) {
                if(item.getFile().isSelectedInTree()) {
                    setBackground(new Background(new BackgroundFill(Color.DEEPSKYBLUE,CornerRadii.EMPTY,Insets.EMPTY)));
                } else {
                    setBackground(null);
                }
            }
            if (item.getType().equals(DataNodeType.PROCESS)) {
                GeneralFile processFile = (GeneralFile)item.getFile();
//                setContentDisplay(ContentDisplay.RIGHT);
                if(processFile.getListReplicate().size() > 1) {
                    Label l = new Label();
                    l.textProperty().bind(item.nameProperty());
                    ComboBox<Replicate> comboReplicate = new ComboBox<>();
                    comboReplicate.setItems(processFile.getListReplicate());
                    if(processFile.getCurrentReplicate() != null) {
                        comboReplicate.setValue(processFile.getCurrentReplicate());
                    }

                    comboReplicate.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                        if(newValue != null && newValue != oldValue) {
                            processFile.setCurrentReplicate(newValue);
                            if(MainApp.getDataControler().getTree().getSelectionModel().getSelectedItem() != null) {
                                DataNode currentSelectedNode = MainApp.getDataControler().getTree().getSelectionModel().getSelectedItem().getValue();
                                if(currentSelectedNode.getType().equals(DataNodeType.OBSERVATION) || currentSelectedNode.getType().equals(DataNodeType.STEP) || currentSelectedNode.getType().equals(DataNodeType.PROCESS) ) {
                                    MainApp.getDataControler().showNodeDetails(currentSelectedNode, currentSelectedNode);
                                }
                            }
                        }
                    });
                    setText("");
                    setGraphic(new HBox(item.getGraphic(), l, comboReplicate));
                }
            }

            setEditable(false);
            ContextMenu menu = new ContextMenu();
            switch (item.getType()) {
                case UNDEFINED:
                    break;
                case PROJECT:
                    MenuItem addProcess = new MenuItem("New Process");
                    addProcess.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/process_loaded.png")));
                    addProcess.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    addProcess.setOnAction(event -> {
                        ProjectFile projectFile = (ProjectFile) item.getFile();
                        GeneralFile newProcess = new GeneralFile(projectFile);
                        DataNode gfNode = new DataNode(DataNodeType.PROCESS);
                        DataNode fatherNode = MainApp.getDataControler().getDataNode(projectFile);
                        MainApp.getDataControler().addNode(newProcess, gfNode, fatherNode);

                        newProcess.getData().analyseProcess(newProcess);
                        gfNode.updateImage();

                        projectFile.getData().setModified(true);
                        MainApp.getDataControler().selectNode(newProcess);
                    });

                    MenuItem exportProject = new MenuItem("Export Project");
                    exportProject.setGraphic(new ImageView(UITools.getImage("resources/images/file-export-16.png")));
                    exportProject.setOnAction(event -> {
                        new Thread(() -> {
                            ProjectFile projectFile = (ProjectFile) item.getFile();
                            File exportFile = projectFile.exportProject();
                            Platform.runLater(() -> {
                                FileChooser fileChooser = new FileChooser();
                                fileChooser.setTitle("Export Project");
                                fileChooser.setInitialFileName(projectFile.getNameProperty().get()+".xlsx");
                                File destinationFile = fileChooser.showSaveDialog(MainApp.primaryStage);
                                if (destinationFile != null) {
                                    try {
                                        FileUtils.copyFile(exportFile, destinationFile);
                                    } catch (IOException ex) {
                                        MainApp.logger.error(ex.getMessage());
                                    }
                                }
                            });
                        }).start();
                    });

                    MenuItem importProject = new MenuItem("Import Project");
                    importProject.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    importProject.setGraphic(new ImageView(UITools.getImage("resources/images/file-import-16.png")));
                    importProject.setOnAction(event -> {
                        ProjectFile projectFile = (ProjectFile) item.getFile();
                        Platform.runLater(() -> {
                            FileChooser fileChooser = new FileChooser();
                            fileChooser.setTitle("Import Project");
                            fileChooser.setInitialFileName(projectFile.getNameProperty().get()+".xlsx");
                            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("xlsx (*.xlsx)", "*.xlsx"));
                            File locationFile = fileChooser.showOpenDialog(MainApp.primaryStage);
                            if (locationFile != null) {
                                new Thread(() -> {
                                    Report report = null;
                                    try {
                                        report = projectFile.importProject(locationFile);
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                    Report finalReport = report;
                                    Platform.runLater(() -> {
                                        String helperPath = "";
                                        for(Iterator<String> si = finalReport.getInfos().iterator(); si.hasNext();)
                                        {
                                            String infos = si.next();
                                            try {
                                                JSONObject jo = new JSONObject(infos);
                                                if (jo.keySet().contains("helper_file")) {
                                                    helperPath = jo.getString("helper_file");
                                                    si.remove();
                                                }
                                            } catch (JSONException e) {
                                                // not a json string
                                            }
                                        }
                                            MainApp.getDataControler().buildProjectTree(projectFile);
                                            projectFile.getData().getListGeneralFile().forEach(gf -> {
                                                MainApp.getDataControler().buildProcessTree(gf);
                                            });
                                            MainApp.getDataControler().showNodeDetails(projectFile, projectFile);
                                            if(finalReport != null && finalReport.success()) {
                                                showOkImportAlert();
                                            } else {
                                                    Alert errorImport = new Alert(Alert.AlertType.INFORMATION);
                                                    errorImport.setTitle("IMPORT RESULT");
                                                    errorImport.setHeaderText("Number of errors by type, complete list of errors, complete list of warning.\n\"Download log file\" for an exhaustive list of warnings and errors.\n\"Download Help file\" for your file with highlighted problematic cells in yellow or orange.");
                                                    errorImport.setResizable(true);

                                                    //TextArea ta = new TextArea(finalReport.prettyPrintInfos());
                                                    TreeItem<String> tiInfos = new TreeItem<String> ("Errors and warnings by categories");
                                                    tiInfos.setExpanded(true);
                                                    tiInfos.getChildren().add(new TreeItem<String> (finalReport.prettyPrintInfos()));

                                                    TreeItem<String> tiErrors = new TreeItem<String> ("List of errors");
                                                    tiErrors.setExpanded(false);
                                                    tiErrors.getChildren().add(new TreeItem<String> (finalReport.prettyPrintError()));

                                                    TreeItem<String> tiWarnings = new TreeItem<String> ("List of warning");
                                                    tiWarnings.setExpanded(false);
                                                    tiWarnings.getChildren().add(new TreeItem<String> (finalReport.prettyPrintWarning()));

                                                    tiInfos.addEventHandler(javafx.scene.control.TreeItem.branchExpandedEvent(), e -> {
                                                            tiErrors.setExpanded(false);
                                                            tiWarnings.setExpanded(false);
                                                    });
                                                    tiErrors.addEventHandler(javafx.scene.control.TreeItem.branchExpandedEvent(), e -> {
                                                        tiInfos.setExpanded(false);
                                                        tiWarnings.setExpanded(false);
                                                    });
                                                    tiWarnings.addEventHandler(javafx.scene.control.TreeItem.branchExpandedEvent(), e -> {
                                                        tiInfos.setExpanded(false);
                                                        tiErrors.setExpanded(false);
                                                    });

                                                    TreeItem<String> rootItem = new TreeItem<String> ();
                                                    rootItem.getChildren().add(tiInfos);
                                                    rootItem.getChildren().add(tiErrors);
                                                    rootItem.getChildren().add(tiWarnings);
                                                    TreeView<String> tv = new TreeView<String> (rootItem);
                                                    tv.showRootProperty().set(false);
                                                    errorImport.getDialogPane().setContent(tv);

                                                    ButtonType saveLog = new ButtonType("Download log file", javafx.scene.control.ButtonBar.ButtonData.LEFT);
                                                    errorImport.getButtonTypes().add(saveLog);
                                                    errorImport.getDialogPane().lookupButton(saveLog).addEventFilter(
                                                        javafx.event.ActionEvent.ACTION,
                                                        event1 -> {
                                                            event1.consume();
                                                            FileChooser fc = new FileChooser();
                                                            fc.setTitle("save log file");
                                                            fc.setInitialFileName("import_log_file.txt");//locationFile
                                                            File fileExport = fc.showSaveDialog(MainApp.primaryStage);
                                                            if (fileExport != null) {
                                                                try {
                                                                    FileOutputStream fout = new FileOutputStream(fileExport, false);
                                                                    org.apache.commons.io.IOUtils.write("ERRORS:\n\n", fout, java.nio.charset.StandardCharsets.UTF_8);
                                                                    org.apache.commons.io.IOUtils.write(finalReport.prettyPrintError(), fout, java.nio.charset.StandardCharsets.UTF_8);
                                                                    org.apache.commons.io.IOUtils.write("\n-----------------------------------------------------\nWARNINGS:\n\n", fout, java.nio.charset.StandardCharsets.UTF_8);
                                                                    org.apache.commons.io.IOUtils.write(finalReport.prettyPrintWarning(), fout, java.nio.charset.StandardCharsets.UTF_8);
                                                                    fout.close();
                                                                } catch (IOException ex) {
                                                                    MainApp.logger.error(ex.getMessage());
                                                                }
                                                            }
                                                        }
                                                    );

                                                    ButtonType saveHelpFile = new ButtonType("Download Help file", javafx.scene.control.ButtonBar.ButtonData.LEFT);
                                                    errorImport.getButtonTypes().add(saveHelpFile);
                                                    if(helperPath.isEmpty()) {
                                                        errorImport.getDialogPane().lookupButton(saveHelpFile).setDisable(true);
                                                    }
                                                String finalHelperPath = helperPath;
                                                errorImport.getDialogPane().lookupButton(saveHelpFile).addEventFilter(
                                                        javafx.event.ActionEvent.ACTION,
                                                        event1 -> {
                                                            event1.consume();
                                                            FileChooser fc = new FileChooser();
                                                            fc.setTitle("Save Help file");
                                                            fc.setInitialFileName(FilenameUtils.removeExtension(locationFile.getName())+"_helper_file.xlsx");
                                                            File fileExport = fc.showSaveDialog(MainApp.primaryStage);
                                                            if (fileExport != null) {
                                                                try {
                                                                    FileUtils.copyFile(new File(finalHelperPath), fileExport);
                                                                } catch (IOException ex) {
                                                                    MainApp.logger.error(ex.getMessage());
                                                                }
                                                            }
                                                        }
                                                    );

                                                    errorImport.initModality(Modality.APPLICATION_MODAL);
                                                    errorImport.initOwner(MainApp.primaryStage);
                                                    errorImport.showAndWait();
                                            }
                                        });


                                }).start();
                            }
                        });
                    });

                    MenuItem uploadSHACLConstraints = new MenuItem("Upload SHACL");
                    uploadSHACLConstraints.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    uploadSHACLConstraints.setGraphic(new ImageView(UITools.getImage("resources/images/file-export-16.png")));
                    uploadSHACLConstraints.setOnAction(event -> {
                        ProjectFile projectFile = (ProjectFile) item.getFile();
                        Platform.runLater(() -> {
                            FileChooser fileChooser = new FileChooser();
                            fileChooser.setTitle("Choose SHACL Constraints File");
                            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Turtle file (*.ttl)", "*.ttl"));
                            File shaclConstraintsFile = fileChooser.showOpenDialog(MainApp.primaryStage);
                            if (shaclConstraintsFile != null) {
                                new Thread(() -> {
                                    Platform.runLater(() -> {
                                        try {
                                            String ID = Tools.normalize(projectFile.getNameProperty().get());
                                            InputStream stream = new FileInputStream(shaclConstraintsFile);

                                            long start = System.nanoTime();
                                            RDF4JTools.updateDataShaclGraph(ID, stream, "Data");
                                            long finish = System.nanoTime();
                                            long timeElapsed = (long)((finish - start)*0.000001);

                                            // Autre option pour mesurer le temps
                                            //Instant start = Instant.now();
                                            //...
                                            //Instant finish = Instant.now();
                                            //long timeElapsed = Duration.between(start, finish).toMillis();

                                            Alert okImport = new Alert(Alert.AlertType.INFORMATION);
                                            okImport.setGraphic(new ImageView(UITools.getImage("resources/images/valid.png")));
                                            okImport.setHeaderText(null);
                                            okImport.setTitle("Uploading duration");
                                            okImport.setContentText(((Long)timeElapsed).toString() + " ms");
                                            okImport.initModality(Modality.APPLICATION_MODAL);
                                            okImport.initOwner(MainApp.primaryStage);
                                            okImport.showAndWait();

                                        } catch (IOException ex) {
                                            MainApp.logger.error(ex.getMessage());
                                        }
                                    });
                                }).start();
                            };
                        });
                    });

                    MenuItem downloadSHACLConstraints = new MenuItem("Download SHACL");
                    downloadSHACLConstraints.setGraphic(new ImageView(UITools.getImage("resources/images/file-export-16.png")));
                    downloadSHACLConstraints.setOnAction(event -> {
                        ProjectFile projectFile = (ProjectFile) item.getFile();
                        Platform.runLater(() -> {
                            String repositoryID = Tools.normalize(projectFile.getNameProperty().get());
                            FileChooser fileChooser = new FileChooser();
                            fileChooser.setTitle("Choose name for SHACL Constraints File");
                            fileChooser.setInitialFileName(repositoryID + "_SHACL_Constraints.ttl");
                            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Turtle file (*.ttl)", "*.ttl"));
                            File shaclConstraintsFile = fileChooser.showSaveDialog(MainApp.primaryStage);
                            if (shaclConstraintsFile != null) {
                                new Thread(() -> {
                                    Platform.runLater(() -> {
                                        try {
                                            OutputStream stream = new FileOutputStream(shaclConstraintsFile);

                                            RDF4JTools.downloadShaclGraph(repositoryID, stream,"Data");

                                            Alert okImport = new Alert(Alert.AlertType.INFORMATION);
                                            okImport.setGraphic(new ImageView(UITools.getImage("resources/images/valid.png")));
                                            okImport.setHeaderText(null);
                                            okImport.setTitle("Downloading SHACL file");
                                            okImport.setContentText("File downloaded...");
                                            okImport.initModality(Modality.APPLICATION_MODAL);
                                            okImport.initOwner(MainApp.primaryStage);
                                            okImport.showAndWait();

                                        } catch (IOException ex) {
                                            MainApp.logger.error(ex.getMessage());
                                        }
                                    });
                                }).start();
                            };
                        });
                    });

                    menu.getItems().add(addProcess);
                    menu.getItems().add(exportProject);
                    menu.getItems().add(importProject);
                    menu.getItems().add(uploadSHACLConstraints);
                    menu.getItems().add(downloadSHACLConstraints);
                    break;
                case PROCESS:
                    MenuItem newRep = new MenuItem("New biological replicate");
                    newRep.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/replicate_16.png")));
                    newRep.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    newRep.setOnAction(event -> {
                        GeneralFile current = (GeneralFile) item.getFile();
                        Replicate newReplicate = current.addReplicate("new replicate", "");
                        if(newReplicate != null) {
                            current.setCurrentReplicate(newReplicate);
                            MainApp.getDataControler().showNodeDetails(getItem(), getItem());
                        }
                    });
                    menu.getItems().add(newRep);

                    Menu newIti = new Menu("New Itinerary");
                    newIti.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/itinerary.png")));
                    newIti.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));

                    MenuItem addItinerary = new MenuItem("New Itinerary from scratch");
                    addItinerary.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/itinerary.png")));
                    addItinerary.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    addItinerary.setOnAction(event -> {
                        GeneralFile current = (GeneralFile) item.getFile();
                        ItineraryFile itineraryFile = new ItineraryFile(current);
                        DataNode dataNodeIti = new DataNode(DataNodeType.ITINERARY);
//                        dataNodeIti.nameProperty().bind(Bindings.concat(itineraryPartT.getItiNumber(), " (",itineraryPartT.getItiName(),")"));
//                        itineraryPartT.setDataNode(dataNodeIti);
//                        dataNodeIti.setItineraryPart(itineraryPartT);
//                        current.getDataNode().addSubNode(dataNodeIti);
                        MainApp.getDataControler().addNode(itineraryFile, dataNodeIti, MainApp.getDataControler().getDataNode(current));
                        MainApp.getDataControler().selectNode(itineraryFile);

                    });
                    newIti.getItems().add(addItinerary);

                    MenuItem addCopyItinerary = new MenuItem("Copy an existing Itinerary");
                    addCopyItinerary.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/itinerary.png")));
                    addCopyItinerary.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    addCopyItinerary.setOnAction(event -> {
                        GeneralFile current = (GeneralFile) item.getFile();
                        copyItinerary(current);
                    });
                    newIti.getItems().add(addCopyItinerary);
                    menu.getItems().add(newIti);

                    MenuItem unloadProcess = new MenuItem("Unload Process");
                    unloadProcess.setGraphic(new ImageView(UITools.getImage("resources/images/revert_16.png")));
                    GeneralFile gf = (GeneralFile) item.getFile();
                    unloadProcess.disableProperty().bind(Bindings.not(gf.getLoaded()));
                    unloadProcess.setOnAction(event -> {
                        if(mainApp.getModifiedProperty().get()) {
                            // please save project before
                            Alert a = new Alert(Alert.AlertType.INFORMATION);
                            a.setTitle("Unable to unload");
                            a.setHeaderText("Please save your project before unload process");
                            a.initModality(Modality.WINDOW_MODAL);
                            a.initOwner(MainApp.primaryStage);
                            a.showAndWait();
                        } else {
                            MainApp.getDataControler().selectNode(gf.getData().getProjectFile());
                            gf.unload();
                            DataNode gfNode = MainApp.getDataControler().getDataNode(gf);
                            if (gfNode != null) {
                                gfNode.clearSubNode();
                                gfNode.updateImage();
                            }
                            MainApp.getDataControler().unloadControler();
                        }

                    });
                    menu.getItems().add(unloadProcess);

                    MenuItem semantizeProcess = new MenuItem("Semantize Process");
                    semantizeProcess.setGraphic(new ImageView(UITools.getImage("resources/images/share_16.png")));
                    semantizeProcess.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    semantizeProcess.setOnAction(event -> {
                        GeneralFile current = (GeneralFile) item.getFile();
                        this.mainApp.semantize(current);
                    });
                    menu.getItems().add(semantizeProcess);

                    MenuItem deleteProcess = new MenuItem("Delete Process");
                    deleteProcess.setGraphic(new ImageView(UITools.getImage("resources/images/del_16.png")));
                    deleteProcess.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    deleteProcess.setOnAction(event -> {
                        Alert a = new Alert(Alert.AlertType.CONFIRMATION);
                        a.setTitle("Delete Process");
                        a.setHeaderText("Delete Process " + item.toString() + " ?");
                        a.initModality(Modality.WINDOW_MODAL);
                        a.initOwner(MainApp.primaryStage);
                        Optional<ButtonType> res = a.showAndWait();
                        if (res.get() == ButtonType.OK) {
                            item.getFile().removeFile();
                            // on supprime réelement le process sur le disque.
                            try {
                                FileUtils.deleteDirectory(new File(gf.getFolderPath()));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            MainApp.getDataControler().deleteDataNode(gf);
                            gf.getData().setModified(true);
                        }
                    });

                    menu.getItems().add(new SeparatorMenuItem());
                    menu.getItems().add(deleteProcess);
                    break;
                case OBSERVATION:
                    ObservationFile obsFile = (ObservationFile) item.getFile();
                    MenuItem addDataObs = new MenuItem("New Data Table");
                    addDataObs.setGraphic(new ImageView(UITools.getImage("resources/images/add_16.png")));
                    addDataObs.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    addDataObs.setOnAction(event -> {
                        // ask for SimpleData or ComplexeData
                        Dialog<ButtonType> dialog = new Dialog<>();
                        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
                        dialog.initModality(Modality.WINDOW_MODAL);
                        dialog.initOwner(MainApp.primaryStage);
                        dialog.setTitle("new data table");
                        dialog.setHeaderText("Please select data type");
                        RadioButton rbSimple = new RadioButton();
                        rbSimple.setText("Simple table");
                        rbSimple.setUserData("simple");
                        RadioButton rbComplexe = new RadioButton();
                        rbComplexe.setText("Complexe table");
                        rbComplexe.setUserData("complexe");
                        ToggleGroup g = new ToggleGroup();
                        rbSimple.setToggleGroup(g);
                        rbComplexe.setToggleGroup(g);
                        dialog.getDialogPane().lookupButton(ButtonType.OK).disableProperty().bind(g.selectedToggleProperty().isNull());
                        VBox b = new VBox(3.0);
                        b.getChildren().addAll(rbSimple, rbComplexe);
                        dialog.getDialogPane().setContent(b);
                        Optional<ButtonType> res = dialog.showAndWait();
                        if (res.get() == ButtonType.OK) {
                            ObservationFile f = (ObservationFile) item.getFile();
                            if (g.getSelectedToggle().getUserData().toString().equalsIgnoreCase("simple")) {
                                f.createObsData(true);
                            } else {
                                f.createObsData(false);
                            }
                            if(f.getStepFile() != null) {
                                f.getStepFile().getGeneralFile().getData().setModified(true);
                            } else {
                                if(f.getItineraryFile() != null) {
                                    f.getItineraryFile().getProcessFile().getData().setModified(true);
                                }
                            }
                            MainApp.getDataControler().showNodeDetails(item, item);
                        }

                    });
                    menu.getItems().add(addDataObs);

                    GeneralFile processFile = null;
                    if(obsFile.getStepFile() != null) {
                        processFile = obsFile.getStepFile().getGeneralFile();
                    } else {
                        processFile = obsFile.getItineraryFile().getProcessFile();
                    }

                    if(processFile.getListReplicate().size() > 1) {
                        Menu exportObs = new Menu("Export Observation");
                        exportObs.setGraphic(new ImageView(UITools.getImage("resources/images/file-export-16.png")));

                        MenuItem exportCurRep = new MenuItem("for current replicate");
                        GeneralFile finalProcessFile = processFile;
                        exportCurRep.setOnAction(event -> {
                            exportObservation(obsFile, finalProcessFile.getCurrentReplicate().getId());
                        });
                        MenuItem exportAllRep = new MenuItem("for all replicates");
                        exportAllRep.setOnAction(event -> {
                            exportObservation(obsFile, -1);
                        });

                        exportObs.getItems().add(exportCurRep);
                        exportObs.getItems().add(exportAllRep);
                        menu.getItems().add(exportObs);
                    } else {
                        MenuItem exportObs = new MenuItem("Export Observation");
                        exportObs.setGraphic(new ImageView(UITools.getImage("resources/images/file-export-16.png")));
                        exportObs.setOnAction(event -> {
                            exportObservation(obsFile, 0);
                        });
                        menu.getItems().add(exportObs);
                    }




                    if(processFile.getListReplicate().size() > 1) {
                        Menu importObs = new Menu("Import Observation");
                        importObs.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                        importObs.setGraphic(new ImageView(UITools.getImage("resources/images/file-import-16.png")));

                        MenuItem importCurRep = new MenuItem("for current replicate");
                        importCurRep.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                        GeneralFile finalProcessFile = processFile;
                        importCurRep.setOnAction(event -> {
                            importObservation(obsFile, finalProcessFile.getCurrentReplicate().getId());
                        });
                        MenuItem importAllRep = new MenuItem("for all replicates");
                        importAllRep.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                        importAllRep.setOnAction(event -> {
                            importObservation(obsFile, -1);
                        });

                        importObs.getItems().add(importCurRep);
                        importObs.getItems().add(importAllRep);
                        menu.getItems().add(importObs);
                    } else {
                        MenuItem importObs = new MenuItem("Import Observation");
                        importObs.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                        importObs.setGraphic(new ImageView(UITools.getImage("resources/images/file-import-16.png")));
                        importObs.setOnAction(event -> {
                            importObservation(obsFile, 0);
                        });
                        menu.getItems().add(importObs);
                    }

                    MenuItem moveObs = new MenuItem("Move Observation");
                    moveObs.setGraphic(new ImageView(UITools.getImage("resources/images/move_16.png")));
                    moveObs.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    menu.getItems().add(moveObs);
                    GeneralFile finalProcessFile2 = processFile;
                    moveObs.setOnAction(event -> {
                        Dialog<ButtonType> moveObsDiag = new Dialog<>();
                        moveObsDiag.getDialogPane().setPrefSize(500, 400);
                        moveObsDiag.initModality(Modality.WINDOW_MODAL);
                        moveObsDiag.initOwner(MainApp.primaryStage);

                        AnchorPane pane = new AnchorPane();
                        VBox vb = new VBox(1.0);
                        vb.getChildren().add(new Label("Select the step where you want to move this observation"));
                        TreeItem<GenericFile> fakeRoot = new TreeItem<>(null);
                        TreeView<GenericFile> treeStep = new TreeView<>(fakeRoot);
                        treeStep.setCellFactory(factory -> new TextFieldTreeCell<>(){});

                        treeStep.setShowRoot(false);
                        finalProcessFile2.getItinerary().forEach(itiFile -> {
                            TreeItem<GenericFile> ckIti = new TreeItem<>(itiFile);
                            fakeRoot.getChildren().add(ckIti);
                            itiFile.getListStep().forEach(step -> {
                                TreeItem<GenericFile> ckStep = new TreeItem<>(step);
                                ckIti.getChildren().add(ckStep);
                            });
                        });

                        vb.getChildren().add(treeStep);
                        pane.getChildren().add(vb);

                        AnchorPane.setTopAnchor(vb, 2.0);
                        AnchorPane.setLeftAnchor(vb, 2.0);
                        AnchorPane.setRightAnchor(vb, 2.0);
                        AnchorPane.setBottomAnchor(vb, 2.0);

                        moveObsDiag.getDialogPane().setContent(pane);
                        moveObsDiag.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

                        Button bok = (Button) moveObsDiag.getDialogPane().lookupButton(ButtonType.OK);
                        bok.disableProperty().bind(treeStep.getSelectionModel().selectedItemProperty().isNull());

                        Optional<ButtonType> result = moveObsDiag.showAndWait();

                        if (result.isPresent()) {
                            if (result.get().equals(ButtonType.OK)) {
                                GenericFile moveTo = treeStep.getSelectionModel().getSelectedItem().getValue();
                                if(moveTo != null) {
                                    // will only remove obs from iti obs or step obs
                                    obsFile.removeFile();
                                    // clear old FOI
                                    obsFile.setFoi("");
                                    if(moveTo instanceof ItineraryFile) {
                                        ItineraryFile itiFile = (ItineraryFile) moveTo;
                                        obsFile.setItineraryFile(itiFile);
                                        itiFile.addObservation(obsFile);
                                        MainApp.getDataControler().buildProcessTree(itiFile.getProcessFile());
                                    }
                                    if(moveTo instanceof StepFile) {
                                        StepFile stepFile = (StepFile) moveTo;
                                        obsFile.setStepFile(stepFile);
                                        stepFile.addObservationFile(obsFile);
                                        MainApp.getDataControler().buildProcessTree(stepFile.getGeneralFile());
                                    }
                                    MainApp.getDataControler().selectNode(obsFile);
                                }
                            }
                        }
                    });


                    MenuItem deleteObs = new MenuItem("Delete Observation");
                    deleteObs.setGraphic(new ImageView(UITools.getImage("resources/images/del_16.png")));
                    deleteObs.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    deleteObs.setOnAction(event -> {
                        Alert a = new Alert(Alert.AlertType.CONFIRMATION);
                        a.setTitle("Delete Observation");
                        a.setHeaderText("Delete observation " + item.toString() + " ?");
                        a.initModality(Modality.WINDOW_MODAL);
                        a.initOwner(MainApp.primaryStage);
                        Optional<ButtonType> res = a.showAndWait();
                        if (res.get() == ButtonType.OK) {
                            item.getFile().removeFile();
                            MainApp.getDataControler().deleteDataNode(item.getFile());
                        }
                    });

                    menu.getItems().add(new SeparatorMenuItem());
                    menu.getItems().add(deleteObs);
                    break;

                case STEP:
                    StepFile s = (StepFile) item.getFile();
                    processFile = s.getGeneralFile();
                    Menu newSubStep = new Menu("New SubStep");
                    newSubStep.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/step.png")));
                    newSubStep.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));

                    MenuItem newSubStepFC = new MenuItem("New SubStep from scratch");
                    newSubStepFC.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/step.png")));
                    newSubStepFC.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    newSubStepFC.setOnAction(event -> {
                        addStepFromScratch(item.getItineraryFile(), s);
                    });

                    MenuItem cloneSubStep = new MenuItem("Copy an existing Step");
                    cloneSubStep.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/step.png")));
                    cloneSubStep.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    cloneSubStep.setOnAction(event -> {
                        copyStep(item.getItineraryFile(), s);
                    });

                    MenuItem addProcessSubStepFromCSV = new MenuItem("New SubStep From CSV");
                    addProcessSubStepFromCSV.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/step_csv.png")));
                    addProcessSubStepFromCSV.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    addProcessSubStepFromCSV.setOnAction(event -> {
                        newStepImport(item.getItineraryFile(), s, -1);
                    });

                    newSubStep.getItems().add(newSubStepFC);
                    newSubStep.getItems().add(cloneSubStep);
                    newSubStep.getItems().add(addProcessSubStepFromCSV);

                    if (s.getFather() == null) menu.getItems().add(newSubStep);

                    Menu newObs = new Menu("New Observation");
                    newObs.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    newObs.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/observation.png")));

                    MenuItem newObsFC = new MenuItem("New Observation from scratch");
                    newObsFC.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/observation.png")));
                    newObsFC.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    newObsFC.setOnAction(event -> {
                        ObservationFile newFile = new ObservationFile(s);
                        for(DataNode nodeS : MainApp.getDataControler().getDataNodes(s)) {
                            DataNode obsNode = new DataNode(DataNodeType.OBSERVATION);
                            MainApp.getDataControler().addNode(newFile, obsNode, nodeS);
                        }

                        s.getData().setModified(true);
                        MainApp.getDataControler().selectNode(newFile);
                    });

                    MenuItem addNewStepObservationFromCsv = new MenuItem("New Observation from CSV");
                    addNewStepObservationFromCsv.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/observation_csv.png")));
                    addNewStepObservationFromCsv.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    addNewStepObservationFromCsv.setOnAction(event -> {
                        newStepObsImport(s, -1);
                    });

                    newObs.getItems().add(newObsFC);
                    newObs.getItems().add(addNewStepObservationFromCsv);

                    menu.getItems().add(newObs);

                    MenuItem newMix = new MenuItem("New Composition");
                    newMix.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/composition.png")));
                    newMix.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    newMix.setOnAction(event -> {
                        ObservableList<Pair<CompositionFile, StepFile>> listCompo = FXCollections.observableArrayList();
                        for(StepFile as : s.getGeneralFile().getListStep()) {
                            if(as != s) {
                                Boolean add = false;
                                ObservableList<DataNode> listStepNode = MainApp.getDataControler().getDataNodes(as);

                                for(DataNode ns : listStepNode) {
                                    if(ns.getItineraryFile().equals(item.getItineraryFile())) {
                                        add = true;
                                    }
                                }
                                if(add) {
                                    for (Iterator<CompositionFile> iteCompo = as.getCompositionFile().keySet().iterator(); iteCompo.hasNext(); ) {
                                        CompositionFile ct = iteCompo.next();
                                        listCompo.add(new Pair<>(ct, as));
                                    }
                                }
                            }
                        }
                        Callback<ListView<Pair<CompositionFile, StepFile>>, ListCell<Pair<CompositionFile, StepFile>>> callback = new Callback<ListView<Pair<CompositionFile, StepFile>>, ListCell<Pair<CompositionFile, StepFile>>>() {
                            @Override public ListCell<Pair<CompositionFile, StepFile>> call(ListView<Pair<CompositionFile, StepFile>> param) {
                                final ListCell<Pair<CompositionFile, StepFile>> cell = new ListCell<Pair<CompositionFile, StepFile>>() {

                                    @Override public void updateItem(Pair<CompositionFile, StepFile> item,
                                                                     boolean empty) {
                                        super.updateItem(item, empty);
                                        if (item != null) {
                                            setText(item.getValue().getNameProperty().get()+ " - " + item.getKey().getCompositionID().getValue().get());
                                        }
                                        else {
                                            setText(null);
                                        }
                                    }
                                };
                                return cell;
                            }};
                        ComboBox<Pair<CompositionFile, StepFile>> comboCompo = new ComboBox<>();
                        comboCompo.setCellFactory(callback);
                        comboCompo.setButtonCell(callback.call(null));
                        comboCompo.setItems(listCompo);

                        // choix du type (inpout vs output)
                        Dialog<String> diagNewCompo = new Dialog();
                        diagNewCompo.initModality(Modality.WINDOW_MODAL);
                        diagNewCompo.initOwner(MainApp.primaryStage);

                        diagNewCompo.setTitle("add new composition");
                        GridPane p1 = new GridPane();
                        ColumnConstraints c1 = new ColumnConstraints();
                        c1.setPercentWidth(50);
                        p1.getColumnConstraints().add(c1);
                        diagNewCompo.getDialogPane().setContent(p1);
                        VBox bg = new VBox(10);
                        ComboBox<String> comboType = new ComboBox<>();
                        comboType.getItems().addAll("Input", "Output");
                        comboType.setValue("Input");
                        bg.getChildren().addAll(new Label("composition type :"), comboType);
                        p1.add(bg, 0, 0);


                        VBox vd = new VBox(5);
                        RadioButton rbPrevius = new RadioButton("From existing step :");
                        RadioButton rbNew = new RadioButton("new composition :");
                        final ToggleGroup group = new ToggleGroup();
                        rbPrevius.setToggleGroup(group);
                        rbNew.setToggleGroup(group);
                        rbNew.setSelected(true);
                        TextField textNew = new TextField();
                        textNew.setPromptText("new composition name");
                        vd.getChildren().addAll(rbNew,textNew,rbPrevius ,comboCompo);

                        p1.add(vd, 1,0);
                        diagNewCompo.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
                        Button bok = (Button) diagNewCompo.getDialogPane().lookupButton(ButtonType.OK);
                        bok.disableProperty().bind(Bindings.or(Bindings.and(rbNew.selectedProperty(), textNew.textProperty().isNotEmpty()), Bindings.and(rbPrevius.selectedProperty(), comboCompo.valueProperty().isNotNull())).not());
                        bok.setOnAction(actionEvent -> {

                            if(rbNew.isSelected()) {
                                CompositionFile compo = new CompositionFile(s, comboType.getValue().equalsIgnoreCase("input") ? true : false);
                                compo.setCompositionID(textNew.getText());
                            } else {
                                //rbPrevius is selected
                                s.addCompositionFile(comboCompo.getSelectionModel().getSelectedItem().getKey(), comboType.getValue().equalsIgnoreCase("input") ? true : false);
                            }
                            mainApp.getDataControler().showNodeDetails(s, s);
                        });
                        diagNewCompo.showAndWait();
                        // choix nouvelle ou anciene (cherchez dans l'étape parent les anciennes
                        // choix identifian si nouvelle !!!



//
                    });
                    if (s.getSubStep().isEmpty()) menu.getItems().add(newMix);

                    MenuItem moveStep = new MenuItem("Move Step");
                    moveStep.setDisable(false);
                    moveStep.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    moveStep.setGraphic(new ImageView(UITools.getImage("resources/images/move_16.png")));
                    GeneralFile finalProcessFile1 = processFile;
                    moveStep.setOnAction(event -> {
                        StepFile stepFile = (StepFile) item.getFile();
//                        GeneralFile processFile = stepFile.getGeneralFile();

                        Dialog<ButtonType> moveStepDiag = new Dialog<>();
                        moveStepDiag.getDialogPane().setPrefSize(500, 400);
                        moveStepDiag.initModality(Modality.WINDOW_MODAL);
                        moveStepDiag.initOwner(MainApp.primaryStage);

                        AnchorPane pane = new AnchorPane();
                        VBox vb = new VBox(1.0);
                        vb.getChildren().add(new Label("Select the step / itinerary where you want to move this step"));
                        TreeItem<GenericFile> fakeRoot = new TreeItem<>(null);
                        TreeView<GenericFile> treeStep = new TreeView<>(fakeRoot);
                        treeStep.setCellFactory(factory -> new TextFieldTreeCell<>(){});

                        treeStep.setShowRoot(false);
                        finalProcessFile1.getItinerary().forEach(itiFile -> {
                            TreeItem<GenericFile> ckIti = new TreeItem<>(itiFile);
                            fakeRoot.getChildren().add(ckIti);
                            itiFile.getListStep().forEach(step -> {
                                TreeItem<GenericFile> ckStep = new TreeItem<>(step);
                                ckIti.getChildren().add(ckStep);
                            });
                        });

                        vb.getChildren().add(treeStep);
                        pane.getChildren().add(vb);

                        AnchorPane.setTopAnchor(vb, 2.0);
                        AnchorPane.setLeftAnchor(vb, 2.0);
                        AnchorPane.setRightAnchor(vb, 2.0);
                        AnchorPane.setBottomAnchor(vb, 2.0);

                        moveStepDiag.getDialogPane().setContent(pane);
                        moveStepDiag.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

                        Button bok = (Button) moveStepDiag.getDialogPane().lookupButton(ButtonType.OK);
                        bok.disableProperty().bind(treeStep.getSelectionModel().selectedItemProperty().isNull());

                        Optional<ButtonType> result = moveStepDiag.showAndWait();

                        if (result.isPresent()) {
                            if (result.get().equals(ButtonType.OK)) {
                                GenericFile moveTo = treeStep.getSelectionModel().getSelectedItem().getValue();
                                if(moveTo != null) {
                                    stepFile.removeFather();
                                    // remove step from old itinerary
                                    item.getItineraryFile().removeStepFromItinerary(stepFile);

                                    if(moveTo instanceof ItineraryFile) {
                                        ItineraryFile itiFile = (ItineraryFile) moveTo;
                                        itiFile.addStep(stepFile);
                                        MainApp.getDataControler().buildProcessTree(itiFile.getProcessFile());
                                    }
                                    if(moveTo instanceof StepFile) {
                                        StepFile stepFileFather = (StepFile) moveTo;
                                        stepFileFather.addSubStep(stepFile);
                                        for(ItineraryFile iti : finalProcessFile1.getItinerary()) {
                                            if(iti.contains(stepFileFather)) {
                                                iti.addStep(stepFile);
                                            }
                                        }
                                        MainApp.getDataControler().buildProcessTree(finalProcessFile1);
                                    }
                                    MainApp.getDataControler().selectNode(stepFile);
                                }
                            }
                        }

                    });
                    menu.getItems().add(moveStep);


                    if(processFile.getListReplicate().size() > 1) {
                        Menu exportStep = new Menu("Export Step");
                        exportStep.setGraphic(new ImageView(UITools.getImage("resources/images/file-export-16.png")));

                        MenuItem exportCurRep = new MenuItem("for current replicate");
                        GeneralFile finalProcessFile = processFile;
                        exportCurRep.setOnAction(event -> {
                            exportStepFile(s, finalProcessFile.getCurrentReplicate().getId());
                        });
                        MenuItem exportAllRep = new MenuItem("for all replicates");
                        exportAllRep.setOnAction(event -> {
                            exportStepFile(s, -1);
                        });

                        exportStep.getItems().add(exportCurRep);
                        exportStep.getItems().add(exportAllRep);
                        menu.getItems().add(exportStep);
                    } else {
                        MenuItem exportStep = new MenuItem("Export Step");
                        exportStep.setGraphic(new ImageView(UITools.getImage("resources/images/file-export-16.png")));
                        exportStep.setOnAction(event -> {
                            exportStepFile(s, 0);
                        });
                        menu.getItems().add(exportStep);
                    }


                    if(processFile.getListReplicate().size() > 1) {
                        Menu importStep = new Menu("Import Step");
                        importStep.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                        importStep.setGraphic(new ImageView(UITools.getImage("resources/images/file-import-16.png")));

                        MenuItem importCurRep = new MenuItem("for current replicate");
                        importCurRep.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                        GeneralFile finalProcessFile = processFile;
                        importCurRep.setOnAction(event -> {
                            stepImport(s, false, finalProcessFile.getCurrentReplicate().getId());
                        });
                        MenuItem importAllRep = new MenuItem("for all replicates");
                        importAllRep.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                        importAllRep.setOnAction(event -> {
                            stepImport(s, false, -1);
                        });

                        importStep.getItems().add(importCurRep);
                        importStep.getItems().add(importAllRep);
                        menu.getItems().add(importStep);
                    } else {
                        MenuItem importStep = new MenuItem("Import Step");
                        importStep.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));

                        importStep.setGraphic(new ImageView(UITools.getImage("resources/images/file-export-16.png")));
                        importStep.setOnAction(event -> {
                            stepImport(s, false, 0);
                        });
                        menu.getItems().add(importStep);
                    }


                    MenuItem deleteStepButton = new MenuItem("Delete Step");
                    deleteStepButton.setGraphic(new ImageView(UITools.getImage("resources/images/del_16.png")));
                    deleteStepButton.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    deleteStepButton.setOnAction(event -> {
                        StepFile stepFile = (StepFile) item.getFile();
                        Alert a = new Alert(Alert.AlertType.CONFIRMATION);
                        a.setTitle("Delete Step");
                        a.setHeaderText("Delete step " + item.toString() + " and its observations ?\nThis action will permanently remove this step (including \nsubsteps and observations) from all itineraries");
                        a.initModality(Modality.WINDOW_MODAL);
                        a.initOwner(MainApp.primaryStage);
                        Optional<ButtonType> res = a.showAndWait();
                        if (res.get() == ButtonType.OK) {
                            item.getFile().removeFile();
                            MainApp.getDataControler().deleteDataNode(stepFile);
//                            for(ItineraryFile iteFile : stepFile.getGeneralFile().getItinerary()) {
//                                MainApp.getDataControler().getDataNode(iteFile).removeSubNode(MainApp.getDataControler().getDataNodes(stepFile));
//                            }
                            s.getData().setModified(true);
                        }
                    });

                    MenuItem removeStepFromParent = new MenuItem("Remove Step from his parent");
                    removeStepFromParent.setGraphic(new ImageView(UITools.getImage("resources/images/del_16.png")));
                    removeStepFromParent.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    removeStepFromParent.setOnAction(event -> {
                        StepFile stepFile = (StepFile) item.getFile();
                        stepFile.removeFather();
                        ItineraryFile itiFile = item.getItineraryFile();
                        MainApp.getDataControler().buildItineraryTree(itiFile);
                        MainApp.getDataControler().selectNode(itiFile);
                    });
                    if (s.getFather() != null) menu.getItems().add(removeStepFromParent);

                    MenuItem removeStepIti = new MenuItem("Remove Step from itinerary");
                    removeStepIti.setGraphic(new ImageView(UITools.getImage("resources/images/del_16.png")));
                    removeStepIti.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    removeStepIti.setOnAction(event -> {
                        StepFile stepFile = (StepFile) item.getFile();
                        ItineraryFile itiFile = item.getItineraryFile();
                        itiFile.removeStepFromItinerary(stepFile);
                        MainApp.getDataControler().buildItineraryTree(itiFile);
                        MainApp.getDataControler().selectNode(itiFile);
                    });

                    menu.getItems().add(new SeparatorMenuItem());
                    menu.getItems().add(removeStepIti);
                    menu.getItems().add(deleteStepButton);
                    break;
                case ITINERARY:
                    MenuItem delItinerary = new MenuItem("Delete Itinerary");
                    delItinerary.setGraphic(new ImageView(UITools.getImage("resources/images/del_16.png")));
                    delItinerary.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    delItinerary.setOnAction(event -> {
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Delete itinerary");
                        alert.setContentText("Delete itinerary ?\n Observations on Itinerary are destroy,\n all steps and steps observations are keep. ");
                        alert.initModality(Modality.WINDOW_MODAL);
                        alert.initOwner(MainApp.primaryStage);
                        Optional<ButtonType> result = alert.showAndWait();
                        ItineraryFile iti = (ItineraryFile) item.getFile();
                        if (result.get() == ButtonType.OK){
//                            for(ObservationFile obf : iti.getItiPart().getListObservation()) {
//                                MainApp.getDataControler().deleteDataNode(obf);
//                            }
//                            for(StepFile stf : iti.getItiPart().getListStep()) {
//                                System.out.println("deal step : " + stf.getNameAndID());
//                                MainApp.getDataControler().deleteDataNode(stf);
//                            }
                            iti.clear();
                            iti.getProcessFile().removeItinerary(iti);
                            MainApp.getDataControler().deleteDataNode(iti);
                            MainApp.getDataControler().buildProcessTree(iti.getProcessFile());
//                            item.getItineraryFile().getItiPart().getProcessFile().getDataNode().removeSubNode(item);
                        } else {
                            event.consume();
                        }

                    });

                    Menu newStep = new Menu("New Step");
                    newStep.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/step.png")));
                    newStep.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));

                    MenuItem addProcessStep = new MenuItem("New Step from scratch");
                    addProcessStep.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/step.png")));
                    addProcessStep.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    addProcessStep.setOnAction(event -> {
                        ItineraryFile iti = (ItineraryFile) item.getFile();
                        addStepFromScratch(iti, null);
                    });

                    MenuItem cloneStep = new MenuItem("Copy an existing Step");
                    cloneStep.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/step.png")));
                    cloneStep.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    cloneStep.setOnAction(event -> {
                        ItineraryFile iti = (ItineraryFile) item.getFile();
                        copyStep(iti, null);
                    });

                    MenuItem addProcessStepFromCSV = new MenuItem("New Step From CSV");
                    addProcessStepFromCSV.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/step_csv.png")));
                    addProcessStepFromCSV.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    addProcessStepFromCSV.setOnAction(event -> {
                        ItineraryFile iti = (ItineraryFile) item.getFile();
                        newStepImport(iti, null, -1);
                    });

                    newStep.getItems().add(addProcessStep);
                    newStep.getItems().add(cloneStep);
                    newStep.getItems().add(addProcessStepFromCSV);

                    MenuItem addProcessExistingStep = new MenuItem("Reuse an existing Step");
                    addProcessExistingStep.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/step.png")));
                    addProcessExistingStep.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    addProcessExistingStep.setOnAction(event -> {
                        ItineraryFile iti = (ItineraryFile) item.getFile();
                        addExistingStep(iti);
                    });

                    Menu newObsIti = new Menu("New Observation");
                    newObsIti.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/observation.png")));
                    newObsIti.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));

                    MenuItem newObsItiFC = new MenuItem("New Observation from scratch");
                    newObsItiFC.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/observation.png")));
                    newObsItiFC.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    newObsItiFC.setOnAction(event -> {
                        ItineraryFile iti = (ItineraryFile) item.getFile();
                        ObservationFile newFile = new ObservationFile(iti);
                        DataNode obsNode = new DataNode(DataNodeType.OBSERVATION);
                        MainApp.getDataControler().addNode(newFile, obsNode, item);
                        iti.getProcessFile().getData().setModified(true);
                        MainApp.getDataControler().selectNode(newFile);
                    });

                    MenuItem addNewItineraryObservationFromCsv = new MenuItem("New Observation from CSV");
                    addNewItineraryObservationFromCsv.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/observation_csv.png")));
                    addNewItineraryObservationFromCsv.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    addNewItineraryObservationFromCsv.setOnAction(event -> {
                        ItineraryFile iti = (ItineraryFile) item.getFile();
                        newItineraryObsImport(iti, -1);
                    });

                    newObsIti.getItems().add(newObsItiFC);
                    newObsIti.getItems().add(addNewItineraryObservationFromCsv);


                    menu.getItems().add(newStep);
                    menu.getItems().add(addProcessExistingStep);
                    menu.getItems().add(newObsIti);
                    menu.getItems().add(new SeparatorMenuItem());
                    menu.getItems().add(delItinerary);
                    break;

                default:
                    break;
            }
            this.setContextMenu(menu);
        }
    }
}
