/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.utils;

import fr.inra.po2vocabmanager.MainApp;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;

public class XCell extends ListCell<String> {
    HBox hbox = new HBox();
    Pane pane = new Pane();
    Button button = new Button("");
    Hyperlink hl = new Hyperlink("");
    TextField tf = new TextField();

    public XCell() {
        super();
        tf.setTooltip(new Tooltip("Press enter to validate"));
        tf.setOnAction(e -> {
            commitEdit(getItem());
        });
        tf.addEventFilter(KeyEvent.KEY_RELEASED, e -> {
            if (e.getCode() == KeyCode.ESCAPE) {
                cancelEdit();
            }
        });
        button.setGraphic(new ImageView(UITools.getImage("resources/images/del_16.png")));
        hbox.getChildren().addAll(hl, pane, button);
        HBox.setHgrow(pane, Priority.ALWAYS);
        button.setOnAction(event -> getListView().getItems().remove(getItem()));
        button.disableProperty().bind(MainApp.getDataControler().getCanEditProperty().not());
        hl.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                MainApp.openBrowser(hl.getText());
            }
        });
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        setText(null);
        if(empty) {
            setGraphic(null);
        } else if(isEditing()) {
            tf.setText(item);
            setGraphic(tf);
        } else {
            hl.setText(item);
            setGraphic(hbox);
        }
    }

    @Override
    public void startEdit() {
        super.startEdit();
        tf.setText(getItem());
        setText(null);
        setGraphic(tf);
        tf.selectAll();
        tf.requestFocus();
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        hl.setText(getItem());
        setGraphic(hbox);
    }

    @Override
    public void commitEdit(String item) {
        super.commitEdit(tf.getText());
        hl.setText(tf.getText());
        setText(null);
        setGraphic(hbox);
    }
}
