/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.utils;

import fr.inra.po2vocabmanager.MainApp;
import fr.inra.po2vocabmanager.model.DataNode;
import fr.inrae.po2engine.model.dataModel.CompositionFile;
import fr.inrae.po2engine.model.dataModel.ItineraryFile;
import fr.inrae.po2engine.model.dataModel.StepFile;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class JavaConnector {

    ItineraryFile itiFile;
    HashMap<String, StepFile> listStep;
    HashMap<String, CompositionFile> listComposition;

    public JavaConnector(ItineraryFile file, HashMap<String, StepFile> listStep,  HashMap<String, CompositionFile> listComposition) {
        this.itiFile = file;
        this.listStep = listStep;
        this.listComposition = listComposition;
    }

    public Integer checkEdition() {
        return MainApp.getDataControler().canEdit() ? 1 : 0;
    }

    public void log(String message) {
        System.out.println("log : " + message);
    }

    public void nodeDragged(String allNodes) throws NumberFormatException{
        JSONArray nodesArray = new JSONArray(allNodes);
        nodesArray.forEach(node -> {
            JSONObject obj = (JSONObject) node;
            this.itiFile.setNodePosition(obj.optString("id", ""), obj.optDouble("x", 0.0) / 72.0, obj.optDouble("y", 0.0) / 72.0);
        });
    }

    public void addEdge(String source, String target) {
        if(!MainApp.getDataControler().canEdit()) {
            return;
        }

        JSONObject oSource = new JSONObject(source);
        JSONObject oTarget = new JSONObject(target);
        if(listStep.containsKey(oSource.getString("id"))) { // stepFile ->
            if(listStep.containsKey(oTarget.getString("id"))) { // -> stepFile  => enchainement step
                itiFile.addLinkItinerary(listStep.get(oSource.getString("id")), listStep.get(oTarget.getString("id")));
            } else {
                if(listComposition.containsKey(oTarget.getString("id"))) {  // -> compoFile  => compo output
                    listStep.get(oSource.getString("id")).addCompositionFile(listComposition.get(oTarget.getString("id")), false);
                } else {
                    // error
                }
            }
        } else {
            if(listComposition.containsKey(oSource.getString("id"))) { // compoFile ->
                if(listStep.containsKey(oTarget.getString("id"))) { // -> stepFile  => compo input
                    listStep.get(oTarget.getString("id")).addCompositionFile(listComposition.get(oSource.getString("id")), true);
                } else {
                    // error. Cas impossible
                }
            }
        }
    }

    public void delEdge(String source, String target) {
        if(!MainApp.getDataControler().canEdit()) {
            return;
        }
        JSONObject oSource = new JSONObject(source);
        JSONObject oTarget = new JSONObject(target);
        if(listStep.containsKey(oSource.getString("id"))) { // stepFile ->
            if(listStep.containsKey(oTarget.getString("id"))) { // -> stepFile  => enchainement step
                itiFile.removeLinkItinerary(listStep.get(oSource.getString("id")),listStep.get(oTarget.getString("id")) );
            } else {
                if(listComposition.containsKey(oTarget.getString("id"))) {  // -> compoFile
                    listStep.get(oSource.getString("id")).removeCompositionFile(listComposition.get(oTarget.getString("id")));
                } else {
                    // error
                }
            }
        } else {
            if(listComposition.containsKey(oSource.getString("id"))) { // compoFile ->
                if(listStep.containsKey(oTarget.getString("id"))) { // -> stepFile  => compo input
                    listStep.get(oTarget.getString("id")).removeCompositionFile(listComposition.get(oSource.getString("id")));
                } else {
                    // error. Cas impossible
                }
            }
        }
    }

    public void delNode(String node) {
        if( !MainApp.getDataControler().canEdit() ) {
            return;
        }
        JSONObject oStep = new JSONObject(node);
        StepFile s = listStep.get(oStep.getString("id"));
        if(s != null) {
            itiFile.removeStepFromItinerary(s);
            MainApp.getDataControler().buildItineraryTree(itiFile);
            DataNode itineraryNode = MainApp.getDataControler().getDataNode(itiFile);
            if(itineraryNode != null) {
                MainApp.getDataControler().getItineraryControler().showNodeDetails(itineraryNode);
            }
        }
    }

    public void highlightStep(String node) {
        JSONObject oStep = new JSONObject(node);
        StepFile s = listStep.get(oStep.getString("id"));
        if(s != null) {
            MainApp.getDataControler().highlightStep(s, itiFile);
        }
    }
}