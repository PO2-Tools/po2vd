/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.utils; import fr.inra.po2vocabmanager.model.DataNode;
import fr.inra.po2vocabmanager.view.ontoView.OntologyOverviewController;
import fr.inrae.po2engine.model.VocabConcept;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.util.Callback;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RecursiveTreeItem<T> extends TreeItem<T> {

    private Callback<T, ObservableList<? extends T>> childrenFactory;

    private Callback<T, Node> graphicsFactory;

    private OntologyOverviewController controller;

    // Méthode récursive pour obtenir le chemin d'un TreeItem
    public static String getPath(TreeItem<?> item) {
        if (item.getParent() == null) {
            return "/"; // Retourne la racine
        }
        int index = item.getParent().getChildren().indexOf(item);
        return getPath(item.getParent()) + index + "/";
    }

    // Méthode pour sauvegarder l'état d'ouverture des nœuds en fonction de leur chemin
    public static void saveExpandedState(TreeItem<?> item, Map<String, Boolean> expandedStates) {
        if(item == null) return;
        expandedStates.put(RecursiveTreeItem.getPath(item), item.isExpanded());
        for (TreeItem<?> child : item.getChildren()) {
            saveExpandedState(child, expandedStates);
        }
    }

    // Méthode pour restaurer l'état d'ouverture des nœuds en fonction de leur chemin
    public static void restoreExpandedState(TreeItem<?> item, Map<String, Boolean> expandedStates) {
        Boolean isExpanded = expandedStates.get(RecursiveTreeItem.getPath(item));
        if (isExpanded != null) {
            item.setExpanded(isExpanded);
        }
        for (TreeItem<?> child : item.getChildren()) {
            restoreExpandedState(child, expandedStates);
        }
    }

    private Comparator<TreeItem<? extends T>> comparator = (o1, o2) -> {
        if(o1.getValue() instanceof DataNode && o2.getValue() instanceof DataNode) {
            DataNode do1 = ((DataNode) o1.getValue());
            DataNode do2 = ((DataNode) o2.getValue());
            return do1.compareTo(do2);
        } else {
            if(o1.getValue() instanceof VocabConcept && o2.getValue() instanceof VocabConcept) {
                VocabConcept vo1 = ((VocabConcept) o1.getValue());
                VocabConcept vo2 = ((VocabConcept) o2.getValue());
                return vo1.compareTo(vo2);
            }
        }
        return 0;
    };


    public RecursiveTreeItem(Callback<T, ObservableList<? extends T>> childrenFactory){
        this(null, childrenFactory, null);
    }

    public RecursiveTreeItem(final T value, Callback<T, ObservableList<? extends T>> childrenFactory, OntologyOverviewController controller){
        this(value, (item) -> null, childrenFactory, controller);

    }

    public RecursiveTreeItem(final T value, Callback<T, Node> graphicsFactory, Callback<T, ObservableList<? extends T>> childrenFactory, OntologyOverviewController controller){
        super(value, graphicsFactory.call(value));
        this.controller = controller;
        this.graphicsFactory = graphicsFactory;
        this.childrenFactory = childrenFactory;

        if(value != null) {
            addChildrenListener(value);
        }

        valueProperty().addListener((obs, oldValue, newValue)->{
            if(newValue != null){
                addChildrenListener(newValue);
            }
        });

        this.setExpanded(false);
    }

    private void addChildrenListener(T value){
        final ObservableList<? extends T> children = childrenFactory.call(value);

        children.forEach(child ->  RecursiveTreeItem.this.getChildren().add(new RecursiveTreeItem<>(child, this.graphicsFactory, childrenFactory, this.controller)));

        children.addListener((ListChangeListener<T>) change -> {
            while(change.next()){
                if(change.wasAdded()){
                    change.getAddedSubList().forEach(t-> RecursiveTreeItem.this.getChildren().add(new RecursiveTreeItem<>(t, this.graphicsFactory, childrenFactory, this.controller)));
                    FXCollections.sort(RecursiveTreeItem.this.getChildren(), comparator);
                }

                if(change.wasRemoved()){
                    change.getRemoved().forEach(t->{
                        final List<TreeItem<T>> itemsToRemove = RecursiveTreeItem.this.getChildren().stream().filter(treeItem -> treeItem.getValue().equals(t)).collect(Collectors.toList());

                        RecursiveTreeItem.this.getChildren().removeAll(itemsToRemove);
                    });
                }

            }
            if(controller != null && controller.getCurrentOntology() != null) {
                controller.getCurrentOntology().rebuildConstraints();
            }
        });
    }
}
