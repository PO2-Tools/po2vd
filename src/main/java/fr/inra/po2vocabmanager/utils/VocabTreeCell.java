/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.utils;

import fr.inra.po2vocabmanager.MainApp;
import fr.inrae.po2engine.externalTools.RDF4JTools;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.SkosScheme;
import fr.inrae.po2engine.model.VocabConcept;
import fr.inrae.po2engine.utils.Tools;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTreeCell;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class VocabTreeCell extends TextFieldTreeCell<VocabConcept> {

    private MainApp mainApp;


    public VocabTreeCell(MainApp mainApp) {
        super();
        this.mainApp = mainApp;

        this.setOnDragDetected(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (!isEmpty() && mainApp.getEditProperty().get() && MainApp.getOntologyControler().isOnMainScheme().get() && !getItem().getDeprecated()) {
                    MainApp.logger.debug("Starting DnD " + getItem().getName());
                    Dragboard db = startDragAndDrop(TransferMode.COPY_OR_MOVE);
                    ClipboardContent cc = new ClipboardContent();
                    cc.putString(getItem().isPO2Node());
                    cc.putUrl(getItem().getURI());
                    db.setContent(cc);
                    db.setDragView(new Label(getItem().getName()).snapshot(null, null));

                }
                event.consume();
            }
        });

        this.setOnDragOver(event -> {
            if (!isEmpty()
                    && event.getGestureSource() != this
                    && event.getDragboard().hasUrl()
                    && event.getDragboard().hasString()
                    && event.getDragboard().getString().equals(getItem().isPO2Node())) {
                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            }
            event.consume();

        });

        this.setOnDragEntered(event -> {
            if(getItem() != null) {
                this.setText("--> " + getItem() + " <--");
            }
        });

        this.setOnDragExited(event -> {
            if(getItem() != null) {
                this.setText(getItem().toString());
            }
        });

        this.setOnDragDropped(event -> {
            if (!isEmpty() && (event.getTransferMode() == TransferMode.COPY || event.getTransferMode() == TransferMode.MOVE )) {
                VocabConcept moveNode = mainApp.getOntologyControler().getCurrentOntology().getNodeByURI(event.getDragboard().getUrl());
                if (moveNode != null) {
                    MainApp.logger.debug("Stopping DnD " + moveNode.getName());
                    VocabConcept loop = moveNode.getNodeByURI(getItem().getURI());
                    if (loop != null) {
                        Alert loopAlert = new Alert(Alert.AlertType.ERROR);
                        loopAlert.setTitle("Loop creation");
                        loopAlert.setHeaderText("Movement cancelled. You are creating a loop.");
                        loopAlert.initOwner(MainApp.primaryStage);
                        loopAlert.initModality(Modality.WINDOW_MODAL);
                        event.setDropCompleted(true);
                        loopAlert.show();
                    } else {
                        if (event.getTransferMode() == TransferMode.MOVE ) {
                            HashMap<VocabConcept, CheckBox> listParent = new HashMap<>();
                            if (moveNode.getFathers().size() == 1) {
                                CheckBox b = new CheckBox();
                                b.setSelected(true);
                                listParent.put(moveNode.getFathers().get(0), b);
                            } else {
                                if (moveNode.getFathers().size() > 1) {
                                    Dialog selectParent = new Dialog();
                                    selectParent.setTitle(moveNode.getName() + " have multiple parents");
                                    DialogPane dp = selectParent.getDialogPane();
                                    VBox box = new VBox();
                                    Label l = new Label("This concept have " + moveNode.getFathers().size() + " parents.\nPlease select from which parents you want to move this concept.\n\n");
                                    box.getChildren().add(l);
                                    for (VocabConcept p : moveNode.getFathers()) {
                                        CheckBox b = new CheckBox(p.getName());
                                        listParent.put(p, b);
                                        box.getChildren().add(b);
                                    }
                                    dp.setContent(box);
                                    dp.getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
                                    selectParent.initOwner(MainApp.primaryStage);
                                    Optional<ButtonType> click = selectParent.showAndWait();

                                    if (click.isPresent() && click.get().getButtonData().equals(ButtonBar.ButtonData.CANCEL_CLOSE)) {
                                        listParent.clear();
                                    }

                                }
                            }
                            for (Map.Entry<VocabConcept, CheckBox> en : listParent.entrySet()) {

                                if (en.getValue().isSelected() && !en.getKey().equals(getItem())) {
                                    en.getKey().getSubNode().removeAll(moveNode);
                                    moveNode.getFathers().remove(en.getKey());

                                    if (!getItem().getSubNode().contains(moveNode)) {
                                        getItem().addChildren(moveNode);
                                    }
                                    if (!moveNode.getFathers().contains(getItem())) {
                                        moveNode.addFather(getItem());
                                    }
                                    MainApp.getOntologyControler().modified(true);
                                }

                            }
                            event.setDropCompleted(true);
                            MainApp.getOntologyControler().refreshTree();
                        }

                        if (event.getTransferMode() == TransferMode.COPY ) {
                            MainApp.logger.debug("Stopping DnD on copy on " + moveNode.getName());
                            getItem().addChildren(moveNode);
                            moveNode.addFather(getItem());
                            MainApp.getOntologyControler().modified(true);
                            event.setDropCompleted(true);
                            MainApp.getOntologyControler().refreshTree();
                        }



                    }
                }
            }

            event.consume();
        });
    }

    @Override
    public void updateItem(VocabConcept item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            setText(null);
            setGraphic(null);

            ContextMenu menu = new ContextMenu();
            
            MenuItem uploadSHACLConstraints = new MenuItem("Upload SHACL");
            uploadSHACLConstraints.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
            uploadSHACLConstraints.setGraphic(new ImageView(UITools.getImage("resources/images/file-export-16.png")));
            uploadSHACLConstraints.setOnAction(event -> {
                Ontology onto = MainApp.getOntologyControler().getCurrentOntology();
                Platform.runLater(() -> {
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Choose SHACL Constraints File");
                    fileChooser.setInitialFileName("constraints.ttl");
                    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Turtle file (*.ttl)", "*.ttl"));
                    File shaclConstraintsFile = fileChooser.showOpenDialog(MainApp.primaryStage);
                    if (shaclConstraintsFile != null) {
                        new Thread(() -> {
                            Platform.runLater(() -> {
                                try {
                                    String ID = Tools.normalize(onto.getName().get());
                                    InputStream stream = new FileInputStream(shaclConstraintsFile);

                                    RDF4JTools.updateOntologyShaclGraph(ID, stream, "Onto");

                                    Alert okImport = new Alert(Alert.AlertType.INFORMATION);
                                    okImport.setGraphic(new ImageView(UITools.getImage("resources/images/valid.png")));
                                    okImport.setHeaderText(null);
                                    okImport.setTitle("Uploading SHACL file");
                                    okImport.setContentText("File transmitted...");
                                    okImport.initModality(Modality.APPLICATION_MODAL);
                                    okImport.initOwner(MainApp.primaryStage);
                                    okImport.showAndWait();

                                } catch (IOException ex) {
                                    MainApp.logger.error(ex.getMessage());
                                }
                            });
                        }).start();
                    };
                });
            });
            
            MenuItem downloadSHACLConstraints = new MenuItem("Download SHACL");
            downloadSHACLConstraints.setGraphic(new ImageView(UITools.getImage("resources/images/file-export-16.png")));
            downloadSHACLConstraints.setOnAction(event -> {
                Ontology onto = MainApp.getOntologyControler().getCurrentOntology();
                Platform.runLater(() -> {
                    String ID = Tools.normalize(onto.getName().get());
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Choose name for SHACL destination constraints file");
                    fileChooser.setInitialFileName(ID + "_SHACL_Constraints.ttl");
                    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Turtle file (*.ttl)", "*.ttl"));
                    File shaclConstraintsFile = fileChooser.showSaveDialog(MainApp.primaryStage);
                    if (shaclConstraintsFile != null) {
                        new Thread(() -> {
                            Platform.runLater(() -> {
                                try {
                                    OutputStream stream = new FileOutputStream(shaclConstraintsFile);

                                    RDF4JTools.downloadShaclGraph(ID, stream, "Onto");

                                    Alert okImport = new Alert(Alert.AlertType.INFORMATION);
                                    okImport.setGraphic(new ImageView(UITools.getImage("resources/images/valid.png")));
                                    okImport.setHeaderText(null);
                                    okImport.setTitle("Downloading SHACL file");
                                    okImport.setContentText("File downloaded...");
                                    okImport.initModality(Modality.APPLICATION_MODAL);
                                    okImport.initOwner(MainApp.primaryStage);
                                    okImport.showAndWait();

                                } catch (IOException ex) {
                                    MainApp.logger.error(ex.getMessage());
                                }
                            });
                        }).start();
                    };
                });
            });

            menu.getItems().add(uploadSHACLConstraints);
            menu.getItems().add(downloadSHACLConstraints);

            this.setContextMenu(menu);
        } else {
            setText(item.toString());
            setEditable(false);
            ContextMenu menu = new ContextMenu();
            if(MainApp.getOntologyControler().isOnMainScheme().get()) {
                if(!item.getDeprecated()) {
                    MenuItem addNode = new MenuItem("add a new " + item.isPO2Node());
                    addNode.setOnAction(event -> showNewNodeWindow(item, item.isPO2Node()));
                    addNode.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    menu.getItems().add(addNode);
                }

                if (item.isDeletable() && !item.getDeprecated()) {
                    Menu addScheme = new Menu("add "+item.getName()+" to concept scheme");
                    addScheme.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    MainApp.getOntologyControler().getCurrentOntology().getlistConceptScheme().forEach(skosScheme -> {
                        if(!item.getListSkosScheme().contains(skosScheme)) {
                            MenuItem newScheme = new MenuItem(skosScheme.getName());
                            newScheme.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                            newScheme.setOnAction(event -> {
                                TreeItem<VocabConcept> tempNode = this.getTreeItem();
                                while (tempNode != null) {
                                    tempNode.getValue().addSkosScheme(skosScheme);
                                    tempNode = tempNode.getParent();
                                    if(tempNode != null) {
                                        tempNode = tempNode.getParent() != null ? tempNode : null;
                                    }
                                }
                                MainApp.getOntologyControler().refreshTree();
                                MainApp.getOntologyControler().modified(true);
                            });
                            addScheme.getItems().add(newScheme);
                        }
                    });
                    menu.getItems().add(addScheme);

                    Menu addHierarchyScheme = new Menu("add "+item.getName()+" and sub nodes to concept scheme");
                    addHierarchyScheme.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    MainApp.getOntologyControler().getCurrentOntology().getlistConceptScheme().forEach(skosScheme -> {
                        if(!item.getListSkosScheme().contains(skosScheme)) {
                            MenuItem newScheme = new MenuItem(skosScheme.getName());
                            newScheme.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                            newScheme.setOnAction(event -> {
                                TreeItem<VocabConcept> startNode = this.getTreeItem();
                                TreeItem<VocabConcept> tempNode = startNode;
                                while (tempNode != null) {
                                    tempNode.getValue().addSkosScheme(skosScheme);
                                    tempNode = tempNode.getParent();
                                    if(tempNode != null) {
                                        tempNode = tempNode.getParent() != null ? tempNode : null;
                                    }
                                }
                                // on fait les fils
                                addSubNodeToScheme(startNode, skosScheme);
                                MainApp.getOntologyControler().refreshTree();
                                MainApp.getOntologyControler().modified(true);
                            });
                            addHierarchyScheme.getItems().add(newScheme);
                        }
                    });
                    menu.getItems().add(addHierarchyScheme);


                    MenuItem grabNode = new MenuItem("grab informations");
                    grabNode.setOnAction(event ->
                            new Thread(() -> {
                                item.grabInfo();
                                Platform.runLater(() -> mainApp.getOntologyControler().modified(true));
                            }).start()
                    );
                    grabNode.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    menu.getItems().add(grabNode);

                    MenuItem deprecatedNode = new MenuItem("Depreciate " + item.getName());
                    deprecatedNode.setOnAction(event -> depreciateNode(item));
                    deprecatedNode.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    menu.getItems().add(deprecatedNode);

                    if(item.getFathers().size() > 1) {
                        MenuItem removeNodeFromOneParent = new MenuItem("Remove " + item.getName() + " from one parent");
                        removeNodeFromOneParent.setOnAction(event -> {
                            Dialog selectParent = new Dialog();
                            selectParent.setTitle(item.getName() + " have multiple parents");
                            DialogPane dp = selectParent.getDialogPane();
                            VBox box = new VBox();
                            Label l = new Label("This concept have " + item.getFathers().size() + " parents.\nPlease select from which parents you want to remove this concept.\n\n");
                            box.getChildren().add(l);
                            VocabConcept parentToRemoveFrom = null;
                            ToggleGroup group = new ToggleGroup();
                            for (VocabConcept p : item.getFathers()) {
                                RadioButton b = new RadioButton(p.getName());
                                b.setUserData(p);
                                b.setToggleGroup(group);
                                box.getChildren().add(b);
                            }
                            dp.setContent(box);
                            dp.getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
                            selectParent.initOwner(MainApp.primaryStage);
                            Optional<ButtonType> click = selectParent.showAndWait();

                            if (click.isPresent() && click.get().getButtonData().equals(ButtonBar.ButtonData.OK_DONE)) {
                                if(group.getSelectedToggle() != null) {
                                    VocabConcept p = (VocabConcept) group.getSelectedToggle().getUserData();
                                    p.getSubNode().removeAll(item);
                                    item.getFathers().remove(p);
                                    MainApp.getOntologyControler().modified(true);
                                    MainApp.getOntologyControler().refreshTree();
                                }
                            }
                        });
                        removeNodeFromOneParent.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                        menu.getItems().add(removeNodeFromOneParent);
                    }

                } else {
                    if (item.isDeletable() && item.getDeprecated()) {
                        MenuItem deleteNode = new MenuItem("Delete " + item.getName());
                        deleteNode.setOnAction(event -> deleteNodeWindow(item));
                        deleteNode.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                        menu.getItems().add(deleteNode);
                    }
                }
            } else {
                if (item.isDeletable()) {
                    MenuItem delscheme = new MenuItem("remove from concept scheme " + MainApp.getOntologyControler().getCurrentSchemeName().get());
                    delscheme.setOnAction(event -> {
                        item.removeSkosConceptSchemeAllSon(MainApp.getOntologyControler().getCurrentOntology().getCurrentScheme());
                        MainApp.getOntologyControler().rebuildTree(true);
                        MainApp.getOntologyControler().modified(true);
                    });
                    delscheme.disableProperty().bind(Bindings.not(mainApp.getEditProperty()));
                    menu.getItems().add(delscheme);
                }
            }
            this.setContextMenu(menu);
        }
    }

    private void showNewNodeWindow(VocabConcept node, String type) {
        mainApp.showCreateNodeOverview(true, node, type);
    }

    private void deleteNodeWindow(VocabConcept node) {
        mainApp.showDeleteNodeOverview(node);
    }

    private void depreciateNode(VocabConcept node) {
        mainApp.showDepreciateNodeOverview(node);
    }

    private void addSubNodeToScheme(TreeItem<VocabConcept> node, SkosScheme scheme) {
        node.getValue().addSkosScheme(scheme);
        node.getChildren().forEach(vocabConceptTreeItem -> addSubNodeToScheme(vocabConceptTreeItem, scheme));
    }


}
