/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager;

//import com.sun.javafx.css.StyleManager;

import fr.inra.po2vocabmanager.model.DataNode;
import fr.inra.po2vocabmanager.utils.LoggingOutputStream;
import fr.inra.po2vocabmanager.utils.StatusBox;
import fr.inra.po2vocabmanager.utils.UITools;
import fr.inra.po2vocabmanager.view.ChoiceManagerController;
import fr.inra.po2vocabmanager.view.RootLayoutController;
import fr.inra.po2vocabmanager.view.SplashController;
import fr.inra.po2vocabmanager.view.dataView.DataOverviewController;
import fr.inra.po2vocabmanager.view.ontoView.*;
import fr.inrae.po2engine.externalTools.CloudConnector;
import fr.inrae.po2engine.externalTools.RDF4JTools;
import fr.inrae.po2engine.model.*;
import fr.inrae.po2engine.model.dataModel.GeneralFile;
import fr.inrae.po2engine.model.dataModel.ItineraryFile;
import fr.inrae.po2engine.utils.PO2Properties;
import fr.inrae.po2engine.utils.ProgressPO2;
import fr.inrae.po2engine.utils.Report;
import fr.inrae.po2engine.utils.Tools;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.collections.transformation.FilteredList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.*;
import javafx.util.Pair;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipOutputStream;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.update4j.LaunchContext;
import org.update4j.inject.InjectTarget;
import org.update4j.service.Launcher;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


/**
 * @author stephane
 */

class PO2Manager {
    public static void main(String [] args) {
        MainApp.main(args);
    }
}

public class MainApp extends Application implements Launcher  {

    private HBox statusBox;
    private static String versionDate;
    @InjectTarget
    public static Stage primaryStage;
    @InjectTarget
    public static Stage splashScreenStage;

    public static Logger logger;
    private String appName = "PO² Manager ";//(v @VERSION@) ";
    private StringProperty title = new SimpleStringProperty();
    private StringProperty fileName = new SimpleStringProperty("");
    private StringProperty version = new SimpleStringProperty("");
    private BorderPane rootLayout;
    private static RootLayoutController rootLayoutController;
    private static OntologyOverviewController ontologyControler;
    private AnchorPane ontologyOverview = null;
    private AnchorPane dataOverview = null;
    private static DataOverviewController dataControler;
    private static MainApp mainApp;
    private static Runtime rt = Runtime.getRuntime();
    private static ScheduledExecutorService executor;

    private BooleanProperty onOntologyView = new SimpleBooleanProperty(false);
    private BooleanProperty onDataView = new SimpleBooleanProperty(false);
    private BooleanProperty modified = new SimpleBooleanProperty(false);
    private BooleanProperty canEdit = new SimpleBooleanProperty(false);


    /**
     *
     *
     */
    public static void main(String[] args) {
        // only use in dev mode. In production mode, this is the run method launch by update4J
        launch(args); // -> launch th start javafx method
    }

    public static void openBrowser(String uri) {
        mainApp.getHostServices().showDocument(uri);
    }

    public static void initTimerTask() {
        // timer check cloud data & onto synchro every 30s
        executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(() -> {
            if(dataControler != null && dataControler.getCurrentData() != null) {
                dataControler.getCurrentData().isSynchro();
            }
            if(ontologyControler != null && ontologyControler.getCurrentOntology() != null) {
                ontologyControler.getCurrentOntology().isSynchro();
            }
        }, 0, 30, TimeUnit.SECONDS);

    }

    public static void setVersionDate(String versionDate1) {
        versionDate = versionDate1;
    }

    public static String getVersionDate() {
        return versionDate;
    }

    public static Runtime getRuntime() {
        return rt;
    }

    public static void exportOnto() {
        if(ontologyControler.getCurrentOntology() != null) {
            try {
                ontologyControler.exportOnto();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void importOnto() {
        if(ontologyControler.getCurrentOntology() != null) {
            ontologyControler.importOnto();
        }
    }

    public static void updateConceptScheme() {
        if (rootLayoutController != null) {
            rootLayoutController.updateConceptSchemeList();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        MainApp.primaryStage = primaryStage;
        splashScreenStage = new Stage(StageStyle.UNDECORATED);
        run(null); // call manually the run method (update4J launcher)
    }


    @Override
    public void run(LaunchContext context) {
        System.setProperty("log4j.configurationFile", "resources/log4j2.xml");
        logger = LogManager.getLogger(MainApp.class);
        // redirect all sdterr to logger error
        System.setErr(new PrintStream(new LoggingOutputStream(
                logger, Level.ERROR), true));

        logger.debug("starting PO² Manager");
        Locale.setDefault(Locale.ENGLISH);
        initTimerTask();
        Platform.runLater(() -> {
            MainApp.primaryStage.getIcons().add(new Image(MainApp.class.getClassLoader().getResourceAsStream("resources/images/PO2Manager.png")));

            UITools.initProgressBar();

            Application.setUserAgentStylesheet(null);

            StringProperty inprod = new SimpleStringProperty();
            inprod.bind(Bindings.when(PO2Properties.productionModeProperty().not()).then("!!! sandbox !!! ").otherwise(""));
            title.bind(Bindings.when(fileName.isEqualTo("")).then(inprod.concat(appName)).otherwise(inprod.concat(appName).concat(" - ").concat(fileName).concat(" - V ").concat(version)));
            primaryStage.titleProperty().bind(Bindings.when(modified).then(title.concat(" *")).otherwise(title));
            try {
                FXMLLoader loader = UITools.getFXMLLoader("view/Splash.fxml");
                StackPane SplashLayout = (StackPane) loader.load();
                SplashController c = loader.getController();

                Scene sSplash = new Scene(SplashLayout);

                splashScreenStage.getIcons().add(UITools.getImage("resources/images/PO2Manager.png"));

                splashScreenStage.setScene(sSplash);
                splashScreenStage.show();
                c.startSplash(this);




            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public long version() {
        return Launcher.super.version();
    }


    public Boolean unlock() {
        if (modified.get()) {
            Alert confirm = new Alert(AlertType.CONFIRMATION);
            confirm.setTitle("Change");
            if(onOntologyView.get()) {
                confirm.setHeaderText("Current ontology changed will be lost. Are you sure ?");
            }
            if(onDataView.get()) {
                confirm.setHeaderText("Current data changed will be lost. Are you sure ?");
            }
            ButtonType buttonCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
//            ButtonType buttonNo = new ButtonType("No", ButtonData.NO);
            ButtonType buttonYes = new ButtonType("Yes", ButtonData.YES);
            confirm.getButtonTypes().setAll(buttonCancel, buttonYes);
            confirm.initOwner(primaryStage);

            Optional<ButtonType> result = confirm.showAndWait();
            if (result.isPresent()) {
                if (result.get().equals(buttonCancel)) {
                    return false;
                } else if (result.get().equals(buttonYes)) {
                    if(onOntologyView.get()) {
                        if (ontologyControler.getCurrentOntology() != null) {
                            ontologyControler.getCurrentOntology().unlockMode();
                            ontologyControler.getCanEditProperty().set(false);
                        }
                    }
                    if(onDataView.get()) {
                        if(dataControler.getCurrentData() != null) {
                            dataControler.getCurrentData().unlockMode();
                            dataControler.getCanEditProperty().set(false);
                        }
                    }
                }
            }
        } else {
            if(onOntologyView.get()) {
                if (ontologyControler.getCurrentOntology() != null) {
                    ontologyControler.getCurrentOntology().unlockMode();
                    ontologyControler.getCanEditProperty().set(false);
                }
            }
            if(onDataView.get()) {
                if(dataControler.getCurrentData() != null) {
                    dataControler.getCurrentData().unlockMode();
                    dataControler.getCanEditProperty().set(false);
                }
            }

        }
        return true;
    }

//    public void openOntoFile() { // TO_REMOVE
//
//        if (ontologyControler.isModified()) {
//            Alert confirm = new Alert(AlertType.CONFIRMATION);
//            confirm.setTitle("Change");
//            confirm.setHeaderText("Would you want to save current ontology ?");
//            ButtonType buttonCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
//            ButtonType buttonNo = new ButtonType("No", ButtonData.NO);
//            ButtonType buttonYes = new ButtonType("Yes", ButtonData.YES);
//            confirm.getButtonTypes().setAll(buttonNo, buttonCancel, buttonYes);
//            confirm.initOwner(primaryStage);
//
//            Optional<ButtonType> result = confirm.showAndWait();
//            if (result.isPresent()) {
//                if (result.get().equals(buttonCancel)) {
//                    return;
//                } else if (result.get().equals(buttonYes)) {
//                    ontologyControler.saveOnto();
//                }
//            }
//        }
//        if (ontologyControler.getCurrentOntology() != null) {
//            ontologyControler.getCurrentOntology().unlockMode();
//            ontologyControler.getCanEditProperty().set(false);
//        }
//
//        CloudConnector.checkOnline();
//        Dialog<String> openOntology = new Dialog<>();
//        openOntology.setTitle("Open Ontology");
//        DialogPane dialogPane = openOntology.getDialogPane();
//        dialogPane.setPrefSize(600, 400);
//        dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
//
//        Accordion accord = new Accordion();
//        TextArea area = new TextArea();
//        area.setEditable(false);
//        for (Entry<String, Ontology> en : Ontologies.getOntologies().entrySet()) {
//            Ontology o = en.getValue();
//            TitledPane pane = new TitledPane(o.getName().get(), null);
//            pane.setTooltip(new Tooltip(o.getName().get()));
//            pane.setFont(Font.font("Verdena", FontWeight.BOLD, 13));
//
//            ImageView pubPriv = null;
//            if (!o.isLocal()) {
//                if (o.isPublic()) {
//                    pubPriv = new ImageView(UITools.getImage("resources/images/onto_public.png"));
//                } else {
//                    pubPriv = new ImageView(UITools.getImage("resources/images/onto_private.png"));
//                }
//            } else {
//                pubPriv = new ImageView(UITools.getImage("resources/images/onto_local.png"));
//            }
//            pubPriv.maxHeight(0.2);
//            pubPriv.prefHeight(0.2);
//            pubPriv.setPreserveRatio(true);
//            pane.setGraphic(pubPriv);
//            pane.setOnMouseClicked(event-> {
//                area.setText(o.getDescription());
//            });
//            accord.getPanes().add(pane);
//        }
//        ScrollPane scroll = new ScrollPane(accord);
//
//        SplitPane split = new SplitPane();
//        split.setDividerPositions(0.35);
//        scroll.minWidthProperty().bind(split.widthProperty().multiply(0.35));
//        scroll.maxWidthProperty().bind(split.widthProperty().multiply(0.35));
//        accord.minWidthProperty().bind(scroll.minWidthProperty());
//        accord.maxWidthProperty().bind(scroll.maxWidthProperty());
//
//        split.setOrientation(Orientation.HORIZONTAL);
//        split.getItems().add(scroll);
//        split.getItems().add(area);
//
//        dialogPane.setContent(split);
//
//
//        openOntology.setResultConverter(clickButton -> {
//            if (clickButton == ButtonType.OK) {
//                return accord.getExpandedPane().getText();
//            }
//            return null;
//        });
//
//
//        openOntology.initOwner(primaryStage);
//        Optional<String> ontologyToOpen = openOntology.showAndWait();
//        ontologyToOpen.ifPresent(ontoName ->
//                ontologyControler.startOpenFile(ontologyToOpen.get())
//        );
//
//
//    }

    public void closeApp() {
        primaryStage.fireEvent(new WindowEvent(primaryStage, WindowEvent.WINDOW_CLOSE_REQUEST));
    }

    public void openFile() {
        StringProperty toOpen = new SimpleStringProperty("");
        if(!unlock()) {
            return;
        }

        CloudConnector.checkOnline();
        Dialog<String> openFile = new Dialog<>();
        if(onOntologyView.get()) {
            openFile.setTitle("Open Ontology");
        }
        if(onDataView.get()) {
            openFile.setTitle("Open Data");
        }
        DialogPane dialogPane = openFile.getDialogPane();
        dialogPane.setPrefSize(700, 460);
        ButtonType adhoc = new ButtonType("adHoc", ButtonData.LEFT);

        dialogPane.getButtonTypes().addAll(adhoc, ButtonType.OK, ButtonType.CANCEL);
        Node adhocButton = dialogPane.lookupButton(adhoc);
        adhocButton.setVisible(false);
        dialogPane.addEventHandler(KeyEvent.KEY_PRESSED, ev -> {
            adhocButton.setVisible(ev.isControlDown());
            ev.consume();
        });
        Accordion accord = new Accordion();
        TextArea area = new TextArea();
        area.setEditable(false);
        Map<String, OntoData> map = null;
        if(onOntologyView.get()) {
            map = Ontologies.getOntologies();
        }
        if(onDataView.get()) {
            map = Datas.getDatas();
        }
        dialogPane.lookupButton(ButtonType.OK).disableProperty().bind(toOpen.isEmpty());

        ObservableMap<String, Accordion> listAllAccordion = FXCollections.observableHashMap();
        ObservableMap<Accordion, ObservableList<TitledPane>> listProject = FXCollections.observableHashMap();
        ObservableMap<Accordion, FilteredList<TitledPane>> listProjectFilter = FXCollections.observableHashMap();
        listAllAccordion.put("none", accord);
        listProject.put(accord, FXCollections.observableArrayList());

        for (Entry<String, OntoData> en : map.entrySet()) {
            OntoData o = en.getValue();
            TitledPane pane = new TitledPane(o.getName().get(), null);
            pane.setTooltip(new Tooltip(o.getName().get()));
            pane.setFont(Font.font("Verdena", FontWeight.BOLD, 13));


            ImageView pubPriv = null;
            if (!o.isLocal()) {
                if (o.isPublic()) {
                    pubPriv = new ImageView(UITools.getImage("resources/images/onto_public.png"));
                } else {
                    pubPriv = new ImageView(UITools.getImage("resources/images/onto_private.png"));
                }
            } else {
                pubPriv = new ImageView(UITools.getImage("resources/images/onto_local.png"));
            }
            pubPriv.maxHeight(0.2);
            pubPriv.prefHeight(0.2);
            pubPriv.setPreserveRatio(true);
            pane.setGraphic(pubPriv);

            pane.setOnMouseClicked(event-> {
                new Thread(() -> {
                    area.setText("Loading informations ...");
                    o.initResource();
                    toOpen.setValue(pane.getText());
                    if(accord.getExpandedPane() == null) {
                        accord.setExpandedPane(pane);
                    }
                    String description = "Description : \n";
                    String log = "History : \n";
                    String semInfo = "Semantics informations : \n";
                    if (o.getDescription() != null) {
                        description += o.getDescription();
                    }

                    semInfo += "    - Description : "+o.getSemDescription()+"\n"+"    - Endpoint : "+o.getSemURIRepository()+"\n"+"    - Statements : "+o.getSemStatementSize();

                    if (o.getOntoDataLogString() != null && o.getOntoDataLogString().length() > 0) {
                        log += o.getOntoDataLogString();
                    }

                    if (onOntologyView.get()) {
                        area.setText("      --- Ontology " + o.getName().getValue() + " ---\n\n" + description +"\n\n"+semInfo+"\n\n" +log);
                    }
                    if (onDataView.get()) {
                        area.setText("      --- Data " + o.getName().getValue() + " ---\n\n" + description +"\n\n"+semInfo+"\n\n" +log);
                    }
                }).start();

            });

            if(o.getGroup() != null) {
                if(!listAllAccordion.containsKey(o.getGroup())) {
                    Accordion accordP = new Accordion();
                    accordP.setPadding(new Insets(0, 0, 0, 15.0));

                    TitledPane tp = new TitledPane(o.getGroup(), null);
                    tp.setId("group");
                    ImageView iv = new ImageView(UITools.getImage("resources/images/onto_group.png"));
                    iv.maxHeight(0.2);
                    iv.prefHeight(0.2);
                    tp.setGraphic(iv);
                    tp.setContent(accordP);

                    listAllAccordion.put(o.getGroup(), accordP);
                    listProject.put(accordP, FXCollections.observableArrayList());
                    listProject.get(listAllAccordion.get("none")).add(tp);
                }
                listProject.get(listAllAccordion.get(o.getGroup())).add(pane);
            } else {
                listProject.get(listAllAccordion.get("none")).add(pane);
            }



        }

        BorderPane mainPane = new BorderPane();
        HBox hb = new HBox(5);
        hb.setPadding(new Insets(0,0,10,0));
        hb.getChildren().add(new Label("Filter : "));
        TextField fieldFilter = new TextField();
        hb.getChildren().add(fieldFilter);
        mainPane.setTop(hb);

        Comparator<TitledPane> tpComparator = Comparator.comparing(titledPane -> titledPane.getText().toLowerCase());

        for(Entry<Accordion, ObservableList<TitledPane>> entry : listProject.entrySet()) {
            entry.getValue().sort(tpComparator);
            FilteredList<TitledPane> filterList = entry.getValue().filtered(titledPane -> true);
            listProjectFilter.put(entry.getKey(), filterList);
            Bindings.bindContent(entry.getKey().getPanes(), filterList);
        }
        fieldFilter.textProperty().addListener((obs, o,n) -> {
            listProjectFilter.values().stream().forEach(titledPanes -> titledPanes.setPredicate(n.isEmpty() ? s -> true : s -> {
                if("group".equalsIgnoreCase(s.getId())) return true;
                return s.getText().toLowerCase().contains(n.toLowerCase());
            }));
        });

        ScrollPane scroll = new ScrollPane(accord);
        SplitPane split = new SplitPane();
        split.setDividerPositions(0.30);
        accord.prefWidthProperty().bind(scroll.widthProperty());

        split.setOrientation(Orientation.HORIZONTAL);

        split.getItems().add(scroll);
        split.getItems().add(area);

        mainPane.setCenter(split);
        dialogPane.setContent(mainPane);


        openFile.setResultConverter(clickButton -> {
            if (clickButton == ButtonType.OK) {
                return toOpen.getValue();
            }
            if(clickButton == adhoc) {
                return "adHocButton";
            }
            return null;
        });


        openFile.initOwner(primaryStage);
        Optional<String> fileToOpen = openFile.showAndWait();
        fileToOpen.ifPresent(ontoName -> {
                    if(!ontoName.equals("adHocButton")) {
                        if (onOntologyView.get()) {
                            ontologyControler.startOpenFile(fileToOpen.get());
                        }
                        if (onDataView.get()) {
                            // on décharge les données
                            TreeItem<DataNode> currentNode = dataControler.getTree().getSelectionModel().getSelectedItem();
                            if (currentNode != null) {
                                DataNode node = currentNode.getValue();
                                if (node != null && node.getFile() != null) {
                                    node.getFile().unbind();
                                }
                            }
//                            dataControler.getTree().getSelectionModel().clearSelection();
                            if (dataControler.getCurrentData() != null) {
                                dataControler.getCurrentData().init();
                            }
                            dataControler.startOpenFile(fileToOpen.get());
                        }
                    } else {
                        try {
                            FileChooser fileChooser = new FileChooser();
                            fileChooser.setTitle("Import ad hoc " + (onDataView.get() ? "data" : "ontology"));
                            if(onOntologyView.get()) {
                                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("semantic files (*.rdf/ttl/owl)", "*.rdf", "*.ttl", "*.owl"));
                            } else {
                                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("zip files (*.zip)", "*.zip"));
                            }
                            File fileImport = fileChooser.showOpenDialog(mainApp.primaryStage);
                            if (fileImport != null) {
                                if (onOntologyView.get()) {
                                    Ontology o = Ontologies.addOntology(FilenameUtils.removeExtension(fileImport.getName()));
                                    o.setLocal(true);
                                    File fo = new File(Tools.ontoPath + o.getName().get());
                                    if(fo.exists()) {
                                        fo.delete();
                                    }
                                    fo.mkdirs();
                                    FileOutputStream fos = new FileOutputStream(fo.getAbsolutePath() + File.separator + fileImport.getName());
                                    IOUtils.copy(new FileInputStream(fileImport), fos);
                                    fos.close();

                                    ontologyControler.startOpenFile(o.getName().get());
                                }

                                if (onDataView.get()) {
                                    TreeItem<DataNode> currentNode = dataControler.getTree().getSelectionModel().getSelectedItem();
                                    if (currentNode != null) {
                                        DataNode node = currentNode.getValue();
                                        if (node != null && node.getFile() != null) {
                                            node.getFile().unbind();
                                        }
                                    }
//                                    dataControler.getTree().getSelectionModel().clearSelection();
                                    if (dataControler.getCurrentData() != null) {
                                        dataControler.getCurrentData().init();
                                    }

                                    ZipFile dataZip = new ZipFile(fileImport);

                                    Data d = Datas.addData(FilenameUtils.removeExtension(fileImport.getName()));
                                    d.setLocal(true);
                                    Data.unzip(dataZip, d.getName().get());
                                    dataControler.startOpenFile(d.getName().get());

                                }


                            }
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
        );
    }

    public boolean canLeaveApp() {
        String message = "All ";
        Boolean isModified = false;
        if (ontologyControler.isModified()) {
            message += "ontology ";
            isModified = true;
        }
        if(dataControler.isModified()) {
            if(isModified) {
                message += "and ";
            }
            message += "project ";
            isModified = true;
        }
        message += "unsaved will be lost. Are you sure ?";
        if(isModified) {
            Alert confirm = new Alert(AlertType.CONFIRMATION);
            confirm.setTitle("Change");
            confirm.setHeaderText(message);
            ButtonType buttonCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
            ButtonType buttonYes = new ButtonType("Yes", ButtonData.YES);
            confirm.getButtonTypes().setAll(buttonCancel, buttonYes);
            confirm.initOwner(primaryStage);
//
            Optional<ButtonType> result = confirm.showAndWait();
            if (result.isPresent()) {
                if (result.get().equals(buttonCancel)) {
                    return false;
                }
            }
        }
        if (ontologyControler.getCurrentOntology() != null) {
            ontologyControler.getCurrentOntology().unlockMode();
        }
        if (dataControler.getCurrentData() != null) {
            dataControler.getCurrentData().unlockMode();
        }
        return true;
    }

    /**
     * Init the rootLayout
     */
    public void initRootLayout() {
        try {

            MainApp.mainApp = this;
            // Load root layout from fxml file.
            FXMLLoader loader = UITools.getFXMLLoader("view/RootLayout.fxml");
            rootLayout = (BorderPane) loader.load();
            rootLayoutController = loader.getController();
            rootLayoutController.setMainApp(this);
            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
//            scene.getStylesheets().add("file:resources/stylesheet/stylesheet.css");
            scene.getStylesheets().add(MainApp.class.getClassLoader().getResource("resources/stylesheet/stylesheet.css").toString());
//            scene.getStylesheets().add("file:resources/stylesheet/treeskins.css");
            scene.getStylesheets().add(MainApp.class.getClassLoader().getResource("resources/stylesheet/treeskins.css").toString());
            primaryStage.setScene(scene);
            primaryStage.show();
            primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {

                @Override
                public void handle(WindowEvent event) {
                    if(canLeaveApp()) {
                        if(executor != null) {
                            executor.shutdown();
                            Tools.shutDownExecutor();
                            MainApp.logger.debug("user leave app !");
                            System.exit(0);
                        }
                    } else {
                        event.consume();
                    }


                }
            });
            logger.debug("Initialisation du RootLayout OK");
            initOntologyOverview();
            initDataOverview();

            canEdit.bind(Bindings.when(onOntologyView).then(ontologyControler.getCanEditProperty()).otherwise(dataControler.getCanEditProperty()));
            modified.bind(Bindings.when(onOntologyView).then(ontologyControler.getModifiedProperty()).otherwise(dataControler.getModifiedProperty()));
            bindVersion();
            fileName.bind(Bindings.when(onOntologyView).then(ontologyControler.getFileName()).otherwise(dataControler.getFileName()));
            initStatusBox();

        } catch (IOException e) {
            logger.error("erreur de l'initialisation du RootLayout", e);
        }
    }

    public void unbindVersion() {
        version.unbind();
    }

    public void bindVersion() {
        version.bind(Bindings.when(onOntologyView).then(StringBinding.stringExpression(ontologyControler.getMajorVersion()).concat(".").concat(ontologyControler.getMinorVersion())).otherwise(StringBinding.stringExpression(dataControler.getMajorVersion()).concat(".").concat(dataControler.getMinorVersion())));
    }

    public BooleanProperty onOntologyViewProperty() {
        return onOntologyView;
    }

    public BooleanProperty onDataViewProperty() {
        return onDataView;
    }
    public void showChoiceOverview() {
        try {
            // Load person overview.
            FXMLLoader loader = UITools.getFXMLLoader("view/ChoiceManager.fxml");
            BorderPane choiceOverview = (BorderPane) loader.load();

            // Set person overview into the center of root layout.
            rootLayout.setCenter(choiceOverview);


            // Give the controller access to the main app.
            ChoiceManagerController choiceControler = loader.getController();
            choiceControler.setMainApp(this);
            choiceControler.setChangeLog(rootLayoutController.getChangeLog());
//            TextArea ta = new TextArea(rootLayoutController.getChangeLog());
//            ta.setEditable(false);
//            ta.setScrollLeft(0.0);
//            ta.setWrapText(true);
//            rootLayout.setBottom(ta);
            logger.debug("Initialisation du choice overview OK");
        } catch (IOException e) {
            logger.error(e.getClass() + ":" + e.getMessage(), e);
        }
    }

    public void initStatusBox() {
        dataControler.setStatusBox(new StatusBox(this));
        ontologyControler.setStatusBox(new StatusBox(this));
    }


    public void showOntologyMod() {
        rootLayout.setCenter(ontologyOverview);
        onOntologyView.setValue(true);
        onDataView.setValue(false);
    }

    public void showDataMod() {
        rootLayout.setCenter(dataOverview);
        onOntologyView.setValue(false);
        onDataView.setValue(true);
    }

    public void initDataOverview() {
        try {
            FXMLLoader loader = UITools.getFXMLLoader("view/dataView/DataOverview.fxml");
            dataOverview = (AnchorPane) loader.load();
            dataControler = loader.getController();
            dataControler.setMainApp(this);
            logger.debug("Initialisation du data Overview OK");
        } catch (IOException e) {
            logger.error(e.getClass() + ":" + e.getMessage(), e);
        }
    }
    /**
     * Init the Ontology Overview
     */
    public void initOntologyOverview() {
        try {
            // Load person overview.
            FXMLLoader loader = UITools.getFXMLLoader("view/ontoView/OntologyOverview.fxml");
            ontologyOverview = (AnchorPane) loader.load();

            // Give the controller access to the main app.
            ontologyControler = loader.getController();
            // ontologyControler.setTree(rootItem);
            ontologyControler.setMainApp(this);
            logger.debug("Initialisation de l'ontology Overview OK");
        } catch (IOException e) {
            logger.error(e.getClass() + ":" + e.getMessage(), e);
        }
    }

    public void showDepreciateNodeOverview(VocabConcept node) {
        if(node.getSubNode().isEmpty()) {
            Alert a = new Alert(AlertType.CONFIRMATION);
            a.setTitle("Depreciate " + node.getName() + " ?");
            a.setHeaderText("Depreciate " + node.getName() + " ?");
            a.initModality(Modality.WINDOW_MODAL);
            a.initOwner(MainApp.primaryStage);
            Optional<ButtonType> res = a.showAndWait();
            if(res.get() == ButtonType.OK) {
                logger.debug("node "+ node.getName() + " mark as deprecated");
                VocabConcept top = node.getTopConcept();
                node.setDeprecated(true);
                node.getFathers().forEach(vocabConcept -> vocabConcept.getSubNode().remove(node));
                node.getFathers().clear();

                node.addFather(top);
                top.addChildren(node);
                MainApp.getOntologyControler().modified(true);
            }
        } else {
            logger.debug("Depreciation canceled. Sub node existe for " + node.getName());
            Alert a = new Alert(AlertType.ERROR);
            a.setContentText("Concept " +node.getName() + " has subnodes. Depreciation canceled");
            a.initModality(Modality.WINDOW_MODAL);
            a.initOwner(MainApp.primaryStage);
            a.showAndWait();
        }
    }
    public void showDeleteNodeOverview(VocabConcept node) {
        try {
            FXMLLoader loader = UITools.getFXMLLoader("view/ontoView/DeleteNodeOverview.fxml");
            BorderPane deleteNodeOverview = (BorderPane) loader.load();

            Stage modelStage = new Stage();
            modelStage.setTitle("Delete node");
            modelStage.initModality(Modality.WINDOW_MODAL);
            modelStage.initOwner(primaryStage);
            Scene scene = new Scene(deleteNodeOverview);

            modelStage.setScene(scene);

            // Give the controller access to the main app.

            DeleteNodeOverviewController deleteNodeControler = loader.getController();
            deleteNodeControler.setMainApp(this);
            deleteNodeControler.setNode(node);
            deleteNodeControler.setTitle("Remove node " + node.toString());
            modelStage.showAndWait();
            logger.debug("Initialisation du deleteNode Overview OK");
        } catch (IOException e) {
            logger.error(e.getClass() + ":" + e.getMessage(), e);
        }
    }

    /**
     *
     */
    public void showCreateNodeOverview(Boolean create, VocabConcept node, String type) {
        new SearchConcept(this,create,  type, node);
    }

    /**
     * Launch a new Search Modal Window
     */
    public void showSearchView() {
        try {
            // Load person overview.
            FXMLLoader loader = UITools.getFXMLLoader("view/ontoView/SearchOverview.fxml");
            BorderPane searchOverview = (BorderPane) loader.load();

            Stage modelStage = new Stage();
            modelStage.setTitle("Search");
            modelStage.initModality(Modality.NONE);
            modelStage.initOwner(primaryStage);
            Scene scene = new Scene(searchOverview);

            modelStage.setScene(scene);

            // Give the controller access to the main app.
            SearchOverviewController controller = loader.getController();
            controller.setMainApp(this);
            modelStage.showAndWait();

            logger.debug("Initialisation du Search Overview OK");
        } catch (IOException e) {
            logger.error("erreur de l'initialisation du Search Overview", e);
        }
    }

    /**
     * open the node in tree
     *
     * @param node : node to open
     */
    public void openNode(VocabConcept node) {
        ontologyControler.openNode(node);
    }

    public static OntologyOverviewController getOntologyControler() {
        return ontologyControler;
    }

    public static DataOverviewController getDataControler() {
        return dataControler;
    }

    public StringProperty fileNameProperty() {
        return fileName;
    }

    public BooleanProperty getEditProperty() {
        return this.canEdit;
    }

    public BooleanProperty getModifiedProperty() {
        return this.modified;
    }

    public void enterEditionMode() {
        if(onOntologyView.getValue()) {
            ontologyControler.enterEditionMode();
        } else {
            dataControler.enterEditionMode();
        }
    }

    public void save() {
        if(onOntologyView.getValue()) {
            ontologyControler.getCurrentOntology().addAuthor(CloudConnector.getDisplayLogin());
            ontologyControler.saveOnto();

        } else {
//            Tools.startProgressBar(true, "save in progress");
            unbindVersion();
            Task<Boolean> task = new Task<Boolean>() {
                @Override
                protected Boolean call() throws Exception {
                    return dataControler.saveData();
                }
            };
            task.setOnSucceeded(workerStateEvent -> {
                bindVersion();
                if(!task.getValue()) {
                    Alert errorSave = new Alert(AlertType.ERROR);
                    errorSave.initModality(Modality.WINDOW_MODAL);
                    errorSave.initOwner(MainApp.primaryStage);
                    errorSave.setTitle("Error");
                    errorSave.setHeaderText("An error occured during save");
                    errorSave.show();
                }
            });
            new Thread(task).start();
        }
    }

    public void semantize() {
        semantize(null);
    }
    public void semantize(GeneralFile process) {
        Boolean data = mainApp.onDataViewProperty().get();
        // Vérification
        ArrayList<String> errorCantPublish = new ArrayList<>();
        if (mainApp.getOntologyControler().getCurrentOntology() == null) {
            errorCantPublish.add("Please select an ontology");
        } else {
            if (mainApp.getOntologyControler().isModified()) {
                errorCantPublish.add("Please save ontology");
            }
        }

        if(data) {
            if (mainApp.getDataControler().getCurrentData() == null) {
                errorCantPublish.add("Please select a project");
            } else {
                if (mainApp.getDataControler().isModified()) {
                    errorCantPublish.add("Please save data");
                }
            }

        }
        if (errorCantPublish.size() > 0) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Unable to publish");
            alert.setHeaderText("Please correct the errors before publishing");
            alert.setContentText(errorCantPublish.stream().map(x -> " - " + x).collect(Collectors.joining("\n")));
            alert.initOwner(MainApp.primaryStage);
            alert.initModality(Modality.WINDOW_MODAL);
            alert.showAndWait();
            return;
        }

        ArrayList<ItineraryFile> listIti = new ArrayList<>();
        Pair<GeneralFile, ArrayList<ItineraryFile>> processToDeal = null;
        Boolean fullPublish = true;
        if(process != null) {
            fullPublish = false;
            Dialog<ArrayList<ItineraryFile>> dialog = new Dialog<>();
            dialog.initModality(Modality.APPLICATION_MODAL);
            dialog.initOwner(MainApp.primaryStage);
            dialog.setTitle("Itineraries selection");
            dialog.setHeaderText("Please select itineraries to semantize");
            dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);
            dialog.getDialogPane().setPrefSize(550.0,400.0);

            HashMap<ItineraryFile, CheckBox> ll = new HashMap<>();
            ll.putAll(process.getItinerary().stream().map(generalItineraryPart -> new Pair<ItineraryFile, CheckBox>(generalItineraryPart, new CheckBox(generalItineraryPart.getItineraryNumber() + " -- " + generalItineraryPart.getItineraryName()))).collect(Collectors.toMap(Pair::getKey, Pair::getValue )));
            ScrollPane scrollPane = new ScrollPane();
            scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);

            VBox vb = new VBox(3);
            vb.getChildren().addAll(ll.values().stream().sorted(Comparator.comparingInt(value -> Integer.parseInt(value.getText().replaceAll("\\D+","")))).collect(Collectors.toList()));
            scrollPane.setContent(vb);

            dialog.getDialogPane().setContent(scrollPane);

            dialog.setResultConverter(dialogButton -> {
                ArrayList<ItineraryFile> list = new ArrayList<>();
                if (dialogButton == ButtonType.OK) {
                    list.addAll(ll.entrySet().stream().filter(e -> e.getValue().isSelected()).map(Entry::getKey).collect(Collectors.toList()));
                }
                return list;
            });

            Optional<ArrayList<ItineraryFile>> result = dialog.showAndWait();
            result.ifPresent(listIti::addAll);
            processToDeal = new Pair<>(process, listIti);
        }

        Dialog diagSem = new Dialog();
        diagSem.getDialogPane().setPrefSize(550.0,400.0);
        String mess = data ? "data":"ontology";
        diagSem.setTitle("Publish "+mess);
        diagSem.setHeaderText("You are going to semantize your "+mess+". This process can take a while. Please do not close this window until the process is done.");
//        Button buttonStart = new Button();
//        buttonStart.setGraphic(new ImageView(UITools.getImage("resources/images/go-53.png")));
//        diagSem.setGraphic(buttonStart);
//        VBox boxListStep = new VBox(3);
//        ProgressIndicator[] indicatorProgress = new ProgressIndicator[4];
//        for (int i = 0; i < 4; i++) {
//            indicatorProgress[i] = new ProgressIndicator(0.0);
//        }
        Pair<GeneralFile, ArrayList<ItineraryFile>> finalProcessToDeal = processToDeal;
        Boolean finalFullPublish = fullPublish;

        diagSem.initOwner(MainApp.primaryStage);
        diagSem.initModality(Modality.WINDOW_MODAL);
        ButtonType buttonTypeSem = new ButtonType("Semantize", ButtonData.YES);
        diagSem.getDialogPane().getButtonTypes().addAll(buttonTypeSem,ButtonType.CLOSE );

        Optional<ButtonType> diagRes = diagSem.showAndWait();

        if (diagRes.isPresent() && diagRes.get() == buttonTypeSem){
            unbindVersion();
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String ID = "";
                        JSONObject listMessages ;
                        if(data) {
                            listMessages = getDataControler().getCurrentData().semantize(finalProcessToDeal,getOntologyControler().getCurrentOntology());
                            ID = Tools.normalize(getDataControler().getCurrentData().getProjectFile().getNameProperty().getValue());
                        } else {
                            listMessages = getOntologyControler().getCurrentOntology().semantize();
                            ID = Tools.normalize(getOntologyControler().getCurrentOntology().getName().get());

                        }
                        String finalID = ID;
                        Platform.runLater(() -> {
                            bindVersion();
                            if (listMessages.getJSONArray("error").length() > 0) {
                                // semantisation KO.
                                Alert KOSem = new Alert(AlertType.ERROR);
                                KOSem.setResizable(true);
                                KOSem.setTitle("Error");
                                KOSem.setHeaderText("An error occured");
                                KOSem.setContentText(listMessages.getJSONArray("error").toString());
                                KOSem.initOwner(MainApp.primaryStage);
                                KOSem.initModality(Modality.WINDOW_MODAL);
                                KOSem.show();
                            } else {
                                Alert OKSem = new Alert(AlertType.INFORMATION);
                                OKSem.setResizable(true);
                                OKSem.setTitle("Success");
                                OKSem.setHeaderText("Successful semantisation");
                                ButtonType saveLog = new ButtonType("download log file", ButtonData.LEFT);
                                ButtonType saveTTL = new ButtonType("download semantic file", ButtonData.LEFT);
                                OKSem.getButtonTypes().add(saveLog);
                                OKSem.getButtonTypes().add(saveTTL);

                                StringBuilder message = new StringBuilder();
                                Button bPublish = new Button("Publish now");
                                bPublish.setGraphic(new ImageView(UITools.getImage("resources/images/share_16.png")));

                                message.append("Your ");
                                message.append(data ? "data are" : "ontology is");
                                message.append(" not yet published  :  ");
                                if (!finalFullPublish) {
                                    message.append("\nOnly full project can be published on triplestore.  ");
                                    bPublish.setDisable(true);
                                }
                                VBox vb = new VBox();
                                HBox hb = new HBox();
                                vb.getChildren().add(hb);
                                Label labelMessage = new Label(message.toString());
                                hb.getChildren().add(labelMessage);
                                hb.getChildren().add(bPublish);

                                bPublish.setOnAction(actionEvent -> {
                                    Task<Boolean> t = new Task<Boolean>() {
                                        @Override
                                        protected Boolean call() throws Exception {
                                            if(data) {
                                                return getDataControler().getCurrentData().publish(listMessages);
                                            } else {
                                                return getOntologyControler().getCurrentOntology().publish(listMessages);
                                            }
                                        }
                                    };
                                    t.setOnSucceeded(workerStateEvent -> {
                                        if (t.getValue()) {
                                            String repoName = RDF4JTools.getRepository(finalID);
                                            Hyperlink linkRepo = new Hyperlink();
                                            linkRepo.setText(repoName);
                                            linkRepo.setOnAction(new EventHandler<ActionEvent>() {
                                                @Override
                                                public void handle(ActionEvent e) {
                                                    MainApp.openBrowser(repoName);
                                                }
                                            });
                                            StringBuilder message2 = new StringBuilder("Your ");
                                            message2.append(data ? "data are" : "ontology is");
                                            message2.append(" accessible through the endpoint : \n");
                                            vb.getChildren().remove(hb);
                                            VBox nvb = new VBox();
                                            nvb.getChildren().addAll(new Label(message2.toString()), linkRepo);
                                            vb.getChildren().add(0, nvb);
                                        } else {
                                            StringBuilder message2 = new StringBuilder("Fail to publish");
                                            vb.getChildren().remove(hb);
                                            VBox nvb = new VBox();
                                            nvb.getChildren().addAll(new Label(message2.toString()));
                                            vb.getChildren().add(0, nvb);
                                        }
                                    });

                                    new Thread(t).start();

                                });

                                TreeItem<String> rootLog = new TreeItem<>("root");
                                buildTreeLogPublish(rootLog, listMessages.getJSONArray("listMessages"));
                                String logMessage = buildTextLogPublish(0, listMessages.getJSONArray("listMessages"));

                                OKSem.getDialogPane().lookupButton(saveLog).addEventFilter(
                                        ActionEvent.ACTION,
                                        event -> {
                                            event.consume();
                                            FileChooser fileChooser = new FileChooser();
                                            fileChooser.setTitle("save log file");
                                            fileChooser.setInitialFileName("log_" + finalID + ".txt");
                                            File fileExport = fileChooser.showSaveDialog(MainApp.primaryStage);
                                            if (fileExport != null) {
                                                try {
                                                    FileOutputStream fout = new FileOutputStream(fileExport, false);
                                                    org.apache.commons.io.IOUtils.write(logMessage, fout, StandardCharsets.UTF_8);
                                                    fout.close();
                                                } catch (IOException ex) {
                                                    MainApp.logger.error(ex.getMessage());
                                                }
                                            }
                                        }
                                );

                                OKSem.getDialogPane().lookupButton(saveTTL).addEventFilter(
                                        ActionEvent.ACTION,
                                        event -> {
                                            event.consume();
                                            String extension = data ? "zip" : "ttl";
                                            FileChooser fileChooser = new FileChooser();
                                            fileChooser.setTitle("save semantic file");
                                            fileChooser.setInitialFileName(finalID + "." + extension);
                                            File fileExport = fileChooser.showSaveDialog(MainApp.primaryStage);
                                            if (fileExport != null) {
                                                try {
                                                    if (data) {
                                                        ZipOutputStream zipFile = new ZipOutputStream(new FileOutputStream(fileExport));
                                                        ZipEntry entry1 = new ZipEntry(finalID + ".ttl");
                                                        zipFile.putNextEntry(entry1);
                                                        IOUtils.copy(new FileInputStream(listMessages.getString("file_data")), zipFile);
                                                        ZipEntry entry2 = new ZipEntry("onto_" + finalID + ".ttl");
                                                        zipFile.putNextEntry(entry2);
                                                        IOUtils.copy(new FileInputStream(listMessages.getString("file_onto")), zipFile);
                                                        zipFile.close();
                                                    } else {
                                                        FileOutputStream fout = new FileOutputStream(fileExport, false);
                                                        FileInputStream fin = new FileInputStream(listMessages.getString("file_onto"));
                                                        org.apache.commons.io.IOUtils.copy(fin, fout);
                                                        fin.close();
                                                        fout.close();
                                                    }

                                                } catch (IOException ex) {
                                                    MainApp.logger.error(ex.getMessage());
                                                }
                                            }
                                        }
                                );
                                TreeView<String> treeLog = new TreeView<>(rootLog);
                                treeLog.setShowRoot(false);
                                treeLog.setMaxWidth(Double.MAX_VALUE);
                                treeLog.setMaxHeight(Double.MAX_VALUE);
                                vb.getChildren().add(treeLog);

                                OKSem.getDialogPane().setContent(vb);
                                OKSem.initOwner(MainApp.primaryStage);
                                OKSem.initModality(Modality.WINDOW_MODAL);
                                OKSem.show();
                            }
                        });
                    } catch (IOException | RDFParseException e) {
                        Tools.delProgress(ProgressPO2.PUBLISH);
                        e.printStackTrace();
                        Platform.runLater(() -> {
                            diagSem.close();
                            Alert errorSem = new Alert(AlertType.ERROR);
                            errorSem.setTitle("error");
                            errorSem.setHeaderText("An error occurred");
                            errorSem.setContentText("An error occurred. If the problem persist, contact the administrator");
                            errorSem.initOwner(MainApp.primaryStage);
                            errorSem.initModality(Modality.WINDOW_MODAL);
                            errorSem.show();
                        });

                    }
                }
            });
            t.start();
        }
    }

    public void startSHACLValidation() {

        // Au prealable :
        // - le jeu de donnees doit avoir ete publie
        // - le fichier de contrainte a du etre transmis (download)

        // Verification qu'un jeu de donnees est bien ouvert,
        // et que ce jeu a bien ete publie.
        ArrayList<String> errorCantStartSValidation = new ArrayList<>();
        if(getDataControler().getCurrentData() == null) {
            errorCantStartSValidation.add("No dataset loaded");
        } else {
            String dataName = Tools.normalize(getDataControler().getCurrentData().getProjectFile().getNameProperty().getValue());
            // Verification que le jeu de donnees a bien ete publie ?
            String repoName = RDF4JTools.getRepository(dataName);
            if (repoName.equals("error")) {
                errorCantStartSValidation.add("First, publish the dataset to validate");
            }
        }

        // Message d'alerte en cas de probleme
        if (errorCantStartSValidation.size() > 0) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Unable to start SHACL validation");
            alert.setHeaderText("Please correct the errors before starting SHACL validation");
            alert.setContentText(errorCantStartSValidation.stream().map(x -> " - " + x).collect(Collectors.joining("\n")));
            alert.initOwner(MainApp.primaryStage);
            alert.initModality(Modality.WINDOW_MODAL);
            alert.showAndWait();
            return;
        }

        // Option 1
        /*
        unbindVersion();
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject listMessages ;
                    listMessages = getDataControler().getCurrentData().startShaclValidation();

                    if (listMessages.getJSONArray("error").length() > 0) {
                        // Validation KO.
                        Alert KOVal = new Alert(AlertType.ERROR);
                        KOVal.setResizable(true);
                        KOVal.setTitle("Validation Errors");
                        KOVal.setHeaderText("An error occured");
                        KOVal.setContentText(listMessages.getJSONArray("error").toString());
                        KOVal.initOwner(MainApp.primaryStage);
                        KOVal.initModality(Modality.WINDOW_MODAL);
                        KOVal.show();
                    } else {
                        Alert OKVal = new Alert(AlertType.INFORMATION);
                        OKVal.setResizable(true);
                        OKVal.setTitle("Success");
                        OKVal.setHeaderText("Validation successfull");
                        OKVal.initOwner(MainApp.primaryStage);
                        OKVal.initModality(Modality.WINDOW_MODAL);
                        OKVal.show();
                    }

                } catch (IOException | RDFParseException e) {
                    e.printStackTrace();
                    Platform.runLater(() -> {
                        Alert errorSem = new Alert(AlertType.ERROR);
                        errorSem.setTitle("ERROR");
                        errorSem.setHeaderText("An error occurred");
                        errorSem.setContentText("An error occurred. If the problem persist, contact the administrator");
                        errorSem.initOwner(MainApp.primaryStage);
                        errorSem.initModality(Modality.WINDOW_MODAL);
                        errorSem.show();
                    });
                }
            }
        });
        t.start();*/
        
        // Option 2
        unbindVersion();
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Tools.addProgress(ProgressPO2.ANALYSE, "Validating ?");
                    Tools.updateProgress(ProgressPO2.ANALYSE, "Validating...");

                    long start = System.nanoTime();
                    String dataName = Tools.normalize(getDataControler().getCurrentData().getProjectFile().getNameProperty().getValue());

                    String ontologyName = "";
                    if(ontologyControler != null && ontologyControler.getCurrentOntology() != null)
                        ontologyName = Tools.normalize(getOntologyControler().getCurrentOntology().getName().get());

                    Report r = RDF4JTools.startShaclValidation(dataName);
                    long finish = System.nanoTime();
                    long timeElapsed = (long)((finish - start)*0.000001);

                    Platform.runLater(() -> {
                        Alert SHACLValidationEndMessage = r.success() ? new Alert(AlertType.CONFIRMATION) : new Alert(AlertType.WARNING);
                        SHACLValidationEndMessage.setResizable(true);
                        SHACLValidationEndMessage.setTitle("SHACL validation ended");
                        SHACLValidationEndMessage.setHeaderText("REPORT");
                        StringBuilder contentText = new StringBuilder("Elapsed time : " + ((Long)timeElapsed).toString() + " ms\r\n\r\n") ;
                        if (r.getInfos().size() > 0) {
                            contentText.append("Information constraints : \r\n");
                            contentText.append(r.prettyPrintInfos());
                            contentText.append("\r\n");
                        }
                        if (r.getWarning().size() > 0) {
                            contentText.append("Warning constraints : \r\n");
                            contentText.append(r.prettyPrintWarning());
                            contentText.append("\r\n");
                        }
                        if (r.getError().size() > 0) {
                            contentText.append("Violation constraints : \r\n");
                            contentText.append(r.prettyPrintError());
                            contentText.append("\r\n");
                        }
                        SHACLValidationEndMessage.setContentText(contentText.toString());

                        ButtonType saveLog = new ButtonType("Download log file", ButtonData.LEFT);
                        SHACLValidationEndMessage.getButtonTypes().add(saveLog);
                        SHACLValidationEndMessage.getDialogPane().lookupButton(saveLog).addEventFilter(
                                ActionEvent.ACTION,
                                event -> {
                                    event.consume();
                                    FileChooser fileChooser = new FileChooser();
                                    fileChooser.setTitle("save log file");
                                    fileChooser.setInitialFileName("results_SHACL_validation.txt");
                                    File fileExport = fileChooser.showSaveDialog(MainApp.primaryStage);
                                    if (fileExport != null) {
                                        try {
                                            FileOutputStream fout = new FileOutputStream(fileExport, false);
                                            org.apache.commons.io.IOUtils.write(contentText.toString(), fout, StandardCharsets.UTF_8);
                                            fout.close();
                                        } catch (IOException ex) {
                                            MainApp.logger.error(ex.getMessage());
                                        }
                                    }
                                }
                        );


                        SHACLValidationEndMessage.initOwner(MainApp.primaryStage);
                        SHACLValidationEndMessage.initModality(Modality.WINDOW_MODAL);
                        SHACLValidationEndMessage.showAndWait();
                    });
                                
                    Tools.delProgress(ProgressPO2.ANALYSE);
                } catch (Exception e) {
                    Tools.delProgress(ProgressPO2.ANALYSE);
                    e.printStackTrace();
                    Platform.runLater(() -> {
                        Alert errorSem = new Alert(AlertType.ERROR);
                        errorSem.setTitle("ERROR");
                        errorSem.setHeaderText("An error occurred");
                        errorSem.setContentText("An error occurred. If the problem persist, contact the administrator");
                        errorSem.initOwner(MainApp.primaryStage);
                        errorSem.initModality(Modality.WINDOW_MODAL);
                        errorSem.show();
                    });
                }
            }
        });
        t.start();
    }

    public String buildTextLogPublish(Integer niv, JSONArray jsonArray) {
        StringBuilder builder = new StringBuilder();

        for(Iterator<Object> ii = jsonArray.iterator(); ii.hasNext();) {
            JSONObject json = (JSONObject) ii.next();
            for(int hi = 0; hi < niv; hi++) {
                builder.append(" ");
            }
            builder.append(json.getString("name"));

            for (Iterator<Object> i = json.getJSONArray("warning").iterator(); i.hasNext(); ) {
                String mess = (String) i.next();
                builder.append("\n ");
                for(int hi = 0; hi < niv; hi++) {
                    builder.append(" ");
                }
                builder.append("(warn) ");
                builder.append(mess);
            }
            for (Iterator<Object> i = json.getJSONArray("error").iterator(); i.hasNext(); ) {
                String mess = (String) i.next();
                builder.append("\n ");
                for(int hi = 0; hi < niv; hi++) {
                    builder.append(" ");
                }
                builder.append("(error) ");
                builder.append(mess);
            }

            builder.append("\n");
            builder.append(buildTextLogPublish(niv+1, json.getJSONArray("subObject")));

        }

        return builder.toString();
    }

    public void buildTreeLogPublish(TreeItem<String> localRoot, JSONArray jsonArray) {
        for(Iterator<Object> ii = jsonArray.iterator(); ii.hasNext();) {
            JSONObject json = (JSONObject) ii.next();
            TreeItem<String> item = new TreeItem<>(json.getString("name"));
            switch (json.getString("type")) {
                case "process" :item.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/process_loaded.png")));
                    break;
                case "step" :item.setGraphic(new ImageView(UITools.getImage(("resources/images/treeview/step.png"))));
                    break;
                case "observation" :item.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/observation.png")));
                    break;
                case "composition" :item.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/composition.png")));
                    break;
                case "Material/Method" :item.setGraphic(new ImageView(UITools.getImage("resources/images/treeview/method_16.png")));
                    break;
                default: break;
            }

            localRoot.getChildren().add(item);

            for (Iterator<Object> i = json.getJSONArray("warning").iterator(); i.hasNext(); ) {
                String mess = (String) i.next();
                TreeItem<String> itemW = new TreeItem<>(mess);
                itemW.setGraphic(new ImageView(UITools.getImage("resources/images/warning_16.png")));
                item.getChildren().add(itemW);
            }
            for (Iterator<Object> i = json.getJSONArray("error").iterator(); i.hasNext(); ) {
                String mess = (String) i.next();
                TreeItem<String> itemE = new TreeItem<>(mess);
                itemE.setGraphic(new ImageView(UITools.getImage("resources/images/del_16.png")));
                item.getChildren().add(itemE);
            }
            buildTreeLogPublish(item, json.getJSONArray("subObject"));
        }
    }

    public void importQUDT(VocabConcept item) {
        FXMLLoader loader = UITools.getFXMLLoader("view/ontoView/QudtImport.fxml");
        BorderPane qudtImportOverview = null;
        try {
            qudtImportOverview = (BorderPane) loader.load();
            Dialog<String> dialogImportQUDT = new Dialog<>();
            dialogImportQUDT.setTitle("Import from QUDT");
            dialogImportQUDT.initOwner(primaryStage);
            dialogImportQUDT.initModality(Modality.WINDOW_MODAL);
            dialogImportQUDT.getDialogPane().setContent(qudtImportOverview);
            dialogImportQUDT.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

            QudtImportController qudtImportControler = loader.getController();

            dialogImportQUDT.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
